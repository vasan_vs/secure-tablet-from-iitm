/*
 *  max17044_battery.c
 *  fuel-gauge systems for lithium-ion (Li+) batteries
 *
 *  Copyright (C) 2009 Samsung Electronics
 *  Minkyu Kang <mk7.kang@samsung.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/platform_device.h>
#include <linux/mutex.h>
#include <linux/err.h>
#include <linux/i2c.h>
#include <linux/delay.h>
#include <linux/power_supply.h>
#include <linux/max17044_battery.h>
#include <linux/slab.h>

#define MAX17044_VCELL_MSB	0x02
#define MAX17044_VCELL_LSB	0x03
#define MAX17044_SOC_MSB	0x04
#define MAX17044_SOC_LSB	0x05
#define MAX17044_MODE_MSB	0x06
#define MAX17044_MODE_LSB	0x07
#define MAX17044_VER_MSB	0x08
#define MAX17044_VER_LSB	0x09
#define MAX17044_RCOMP_MSB	0x0C
#define MAX17044_RCOMP_LSB	0x0D
#define MAX17044_CMD_MSB	0xFE
#define MAX17044_CMD_LSB	0xFF

#define MAX17044_DELAY		1000
#define MAX17044_BATTERY_FULL	95

struct max17044_chip {
	struct i2c_client		*client;
	struct delayed_work		work;
	struct power_supply		battery;
	struct max17044_platform_data	*pdata;

	/* State Of Connect */
	int online;
	/* battery voltage */
	int vcell;
	/* battery capacity */
	int soc;
	/* State Of Charge */
	int status;
};

static int max17044_get_property(struct power_supply *psy,
			    enum power_supply_property psp,
			    union power_supply_propval *val)
{
	struct max17044_chip *chip = container_of(psy,
				struct max17044_chip, battery);
printk (KERN_ERR "max17044_get_property");

	switch (psp) {
	case POWER_SUPPLY_PROP_STATUS:
		val->intval = chip->status;
		break;
	case POWER_SUPPLY_PROP_ONLINE:
		val->intval = chip->online;
		break;
	case POWER_SUPPLY_PROP_VOLTAGE_NOW:
		val->intval = chip->vcell;
		break;
	case POWER_SUPPLY_PROP_CAPACITY:
		val->intval = chip->soc;
		break;
	default:
		return -EINVAL;
	}
	return 0;
}

static int max17044_write_reg(struct i2c_client *client, int reg, u8 value)
{
	int ret;
printk (KERN_ERR "max17044_write_reg");
	ret = i2c_smbus_write_byte_data(client, reg, value);

	if (ret < 0)
		dev_err(&client->dev, "%s: err %d\n", __func__, ret);

	return ret;
}

static int max17044_read_reg(struct i2c_client *client, int reg)
{
	int ret;
printk (KERN_ERR "max17044_read_reg");
	ret = i2c_smbus_read_byte_data(client, reg);

	if (ret < 0)
		dev_err(&client->dev, "%s: err %d\n", __func__, ret);

	return ret;
}

static void max17044_reset(struct i2c_client *client)
{
	printk (KERN_ERR "max17044_reset");
	max17044_write_reg(client, MAX17044_CMD_MSB, 0x54);
	max17044_write_reg(client, MAX17044_CMD_LSB, 0x00);
}

static void max17044_get_vcell(struct i2c_client *client)
{
	struct max17044_chip *chip = i2c_get_clientdata(client);
	u8 msb;
	u8 lsb;
	printk (KERN_ERR "max17044_get_vcell %x %x",max17044_read_reg(client, MAX17044_VCELL_MSB),max17044_read_reg(client, MAX17044_VCELL_LSB));
	msb = max17044_read_reg(client, MAX17044_VCELL_MSB);
	lsb = max17044_read_reg(client, MAX17044_VCELL_LSB);

	chip->vcell = (msb << 4) + (lsb >> 4);
}

static void max17044_get_soc(struct i2c_client *client)
{
	struct max17044_chip *chip = i2c_get_clientdata(client);
	u8 msb;
	u8 lsb;
	printk (KERN_ERR "max17044_get_soc %x %x",max17044_read_reg(client, MAX17044_SOC_MSB),max17044_read_reg(client, MAX17044_SOC_LSB));
	msb = max17044_read_reg(client, MAX17044_SOC_MSB);
	lsb = max17044_read_reg(client, MAX17044_SOC_LSB);

	chip->soc = msb;
}

static void max17044_get_version(struct i2c_client *client)
{
	u8 msb;
	u8 lsb;
printk (KERN_ERR "max17044_get_version");
	msb = max17044_read_reg(client, MAX17044_VER_MSB);
	lsb = max17044_read_reg(client, MAX17044_VER_LSB);

	dev_info(&client->dev, "MAX17044 Fuel-Gauge Ver %d%d\n", msb, lsb);
}

static void max17044_get_online(struct i2c_client *client)
{
	struct max17044_chip *chip = i2c_get_clientdata(client);
printk (KERN_ERR "max17044_get_online");
	if (chip->pdata && chip->pdata->battery_online)
		chip->online = chip->pdata->battery_online();
	else
		chip->online = 1;
}

static void max17044_get_status(struct i2c_client *client)
{
	struct max17044_chip *chip = i2c_get_clientdata(client);
printk (KERN_ERR "max17044_get_status");
chip->status = POWER_SUPPLY_STATUS_CHARGING;
	if (!chip->pdata || !chip->pdata->charger_online || !chip->pdata->charger_enable) {
//		chip->status = POWER_SUPPLY_STATUS_UNKNOWN;
		return;
	}

	if (chip->pdata->charger_online()) {
		if (chip->pdata->charger_enable())
			chip->status = POWER_SUPPLY_STATUS_CHARGING;
		else
			chip->status = POWER_SUPPLY_STATUS_NOT_CHARGING;
	} else {
		chip->status = POWER_SUPPLY_STATUS_DISCHARGING;
	}

	if (chip->soc > MAX17044_BATTERY_FULL)
		chip->status = POWER_SUPPLY_STATUS_FULL;
}

static void max17044_work(struct work_struct *work)
{
	struct max17044_chip *chip;
	int old_usb_online, old_online, old_vcell, old_soc;
	printk (KERN_ERR "max17044_work");
	
	chip = container_of(work, struct max17044_chip, work.work);

	old_vcell = chip->vcell;
	old_soc = old_soc;
	max17044_get_online(chip->client);

	max17044_get_vcell(chip->client);
	max17044_get_soc(chip->client);
//	max17044_get_online(chip->client);
	max17044_get_status(chip->client);

	if((old_vcell != chip->vcell) || (old_soc != chip->soc))
		power_supply_changed(&chip->battery);
	
	schedule_delayed_work(&chip->work, MAX17044_DELAY);

}

static enum power_supply_property max17044_battery_props[] = {
	POWER_SUPPLY_PROP_STATUS,
	POWER_SUPPLY_PROP_ONLINE,
	POWER_SUPPLY_PROP_VOLTAGE_NOW,
	POWER_SUPPLY_PROP_CAPACITY,
};

static int __devinit max17044_probe(struct i2c_client *client,
			const struct i2c_device_id *id)
{
	struct i2c_adapter *adapter = to_i2c_adapter(client->dev.parent);
	struct max17044_chip *chip;
	int ret;
printk (KERN_ERR "max17044_probe");
	if (!i2c_check_functionality(adapter, I2C_FUNC_SMBUS_BYTE))
	{
		printk (KERN_ERR "i2c check functionality failed");
		return -EIO;
	
	}
	chip = kzalloc(sizeof(*chip), GFP_KERNEL);
	if (!chip)
	{
		printk (KERN_ERR "chip not present");
		return -ENOMEM;
	}
	chip->client = client;
	chip->pdata = client->dev.platform_data;

	i2c_set_clientdata(client, chip);

	chip->battery.name		= "battery";
	chip->battery.type		= POWER_SUPPLY_TYPE_BATTERY;
	chip->battery.get_property	= max17044_get_property;
	chip->battery.properties	= max17044_battery_props;
	chip->battery.num_properties	= ARRAY_SIZE(max17044_battery_props);

	ret = power_supply_register(&client->dev, &chip->battery);
	if (ret) {
		dev_err(&client->dev, "failed: power supply register\n");
		kfree(chip);
		return ret;
	}

	max17044_reset(client);
	max17044_get_version(client);

	INIT_DELAYED_WORK_DEFERRABLE(&chip->work, max17044_work);
	schedule_delayed_work(&chip->work, MAX17044_DELAY);

	return 0;
}

static int __devexit max17044_remove(struct i2c_client *client)
{
	struct max17044_chip *chip = i2c_get_clientdata(client);
printk (KERN_ERR "max17044_remove");
	power_supply_unregister(&chip->battery);
	cancel_delayed_work(&chip->work);
	kfree(chip);
	return 0;
}

#ifdef CONFIG_PM

static int max17044_suspend(struct i2c_client *client,
		pm_message_t state)
{
	struct max17044_chip *chip = i2c_get_clientdata(client);
printk (KERN_ERR "max17044_suspend");
	cancel_delayed_work(&chip->work);
	return 0;
}

static int max17044_resume(struct i2c_client *client)
{
	struct max17044_chip *chip = i2c_get_clientdata(client);
printk (KERN_ERR "max17044_resume");
	schedule_delayed_work(&chip->work, MAX17044_DELAY);
	return 0;
}

#else

#define max17044_suspend NULL
#define max17044_resume NULL

#endif /* CONFIG_PM */

static const struct i2c_device_id max17044_id[] = {
	{ "max17044", 0x36 },
	{ }
};
MODULE_DEVICE_TABLE(i2c, max17044_id);

static struct i2c_driver max17044_i2c_driver = {
	.driver	= {
		.name	= "max17044",
	},
	.probe		= max17044_probe,
	.remove		= __devexit_p(max17044_remove),
	.suspend	= max17044_suspend,
	.resume		= max17044_resume,
	.id_table	= max17044_id,
};

static int __init max17044_init(void)
{
printk (KERN_ERR "max17044_init");
	return i2c_add_driver(&max17044_i2c_driver);
}
module_init(max17044_init);

static void __exit max17044_exit(void)
{
printk (KERN_ERR "max17044_exit");
	i2c_del_driver(&max17044_i2c_driver);
}
module_exit(max17044_exit);

MODULE_AUTHOR("Minkyu Kang <mk7.kang@samsung.com>");
MODULE_DESCRIPTION("MAX17044 Fuel Gauge");
MODULE_LICENSE("GPL");
