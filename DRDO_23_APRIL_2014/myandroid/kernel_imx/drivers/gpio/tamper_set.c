#include <linux/module.h>    // included for all kernel modules
#include <linux/kernel.h>    // included for KERN_INFO
#include <linux/init.h>      // included for __init and __exit macros
#include <asm/io.h>
#include <mach/hardware.h>
#include <linux/delay.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Amit");
MODULE_DESCRIPTION("module to set tamper");

static long int id=0;
//static int size=0;
module_param(id, long, 0);
//module_param(size, int, 0);

static int __init tamper_set_init(void)
{
/*	int SNVS_LPZMKR0,snvs_check;						//backup
    printk(KERN_INFO "*************************************TAMPER SET\n");
	SNVS_LPZMKR0 = 0x55555555;
	__raw_writel(SNVS_LPZMKR0, IO_ADDRESS(0x020cc06c));
	mdelay(100);
	snvs_check = __raw_readl(IO_ADDRESS(0x020cc06c));
	printk (KERN_ERR "SNVS_LPZMKR0 %x", snvs_check);
*/

        int SNVS_LPZMKR0,snvs_check;                                            //backup
//	printk(KERN_INFO "*************************************TAMPER SET\n");
       // SNVS_LPZMKR0 = 0x55555555;
        
	__raw_writel(id, IO_ADDRESS(0x020cc06c));
        mdelay(100);
        snvs_check = __raw_readl(IO_ADDRESS(0x020cc06c));
        printk (KERN_ERR "%x", snvs_check);



/*
//	int i=0;
	long int SNVS_LPZMKR0,SNVS_LPZMKR,snvs_check;
//    printk(KERN_INFO "*************************************TAMPER SET %x \n",id);
        SNVS_LPZMKR0 = id;
//        do{
//	SNVS_LPZMKR =SNVS_LPZMKR0 & 0xffffffff;
	printk(KERN_INFO "*************************************TAMPER SET %x \n",SNVS_LPZMKR0);
	__raw_writel(SNVS_LPZMKR, IO_ADDRESS(0x020cc06c));	
//	SNVS_LPZMKR0=SNVS_LPZMKR0/100000000;
	snvs_check = __raw_readl(IO_ADDRESS(0x020cc06c));
     	printk (KERN_ERR "SNVS_LPZMKR %x \n", snvs_check);

//	size=size/8;
//	i++;
 //	}while(size<=1);
	
	snvs_check = __raw_readl(IO_ADDRESS(0x020cc06c));
      printk (KERN_ERR "SNVS_LPZMKR0 %x", snvs_check);
*/
    return 0;    
}

static void __exit tamper_set_exit(void)
{
    printk(KERN_INFO "Exiting Tamper Set\n");
}

module_init(tamper_set_init);
module_exit(tamper_set_exit);

