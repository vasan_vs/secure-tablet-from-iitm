#include <linux/module.h>    // included for all kernel modules
#include <linux/kernel.h>    // included for KERN_INFO
#include <linux/init.h>      // included for __init and __exit macros
#include <asm/io.h>
#include <mach/hardware.h>
#include <linux/delay.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Amit");
MODULE_DESCRIPTION("module to detct tamper");

static int __init tamper_detect_init(void)
{
        int SNVS_LPZMKR0;
        SNVS_LPZMKR0 = __raw_readl(IO_ADDRESS(0x020cc06c));
	 printk (KERN_ERR "%x", SNVS_LPZMKR0);
    return 0;
}

static void __exit tamper_detect_exit(void)
{
    printk(KERN_INFO "Exiting Tamper Detect\n");
}

module_init(tamper_detect_init);
module_exit(tamper_detect_exit);

