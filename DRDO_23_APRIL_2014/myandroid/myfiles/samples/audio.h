
struct platform_device {
        const char      * name;
        int             id; 
        struct device   dev;
        u32             num_resources;
        struct resource * resource;

        const struct platform_device_id *id_entry;

        /* MFD cell pointer */
        struct mfd_cell *mfd_cell;

        /* arch specific additions */
        struct pdev_archdata    archdata;
};

/*
 * This struct is to define the number of SSIs on a platform,
 * DAM source port config, DAM external port config,
 * regulator names, and other stuff audio needs.
 */
struct mxc_audio_platform_data {
        int ssi_num;
        int src_port;
        int ext_port;

        int intr_id_hp;
        int ext_ram;
        struct clk *ssi_clk[2];

        int hp_gpio;
        int hp_active_low;      /* headphone irq is active low */

        int mic_gpio;
        int mic_active_low;     /* micphone irq is active low */

        int sysclk;
        const char *codec_name;

        int (*init) (void);     /* board specific init */
        int (*amp_enable) (int enable);
        int (*clock_enable) (int enable);
        int (*finit) (void);    /* board specific finit */
        void *priv;             /* used by board specific functions */
};

struct imx_ssi_platform_data {
        unsigned int flags;
#define IMX_SSI_DMA            (1 << 0)
#define IMX_SSI_USE_AC97       (1 << 1)
#define IMX_SSI_NET            (1 << 2)
#define IMX_SSI_SYN            (1 << 3)
#define IMX_SSI_USE_I2S_SLAVE  (1 << 4)
        void (*ac97_reset) (struct snd_ac97 *ac97);
        void (*ac97_warm_reset)(struct snd_ac97 *ac97);
};
