#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

int main(int argc, char **argv)
{
	int fd;
	char read_buf[1024];
	char write_buf[1024];
	int ret_val;

	fd=open(argv[1],O_RDWR|O_NONBLOCK);
	if(fd<0)
	{
		write(1,"ERROR: in opening file",22);
		exit(0);	
	}

	write(1,"gsm_atdt: writing to file\n",26);

	ret_val=write(fd,"atdt " , 5);

	sprintf(write_buf,"gsm_atdt: write call rev val is %d\n",ret_val);

	write(1,write_buf,strlen(write_buf));

	ret_val=read(fd, read_buf, sizeof(read_buf));

	sprintf(write_buf,"gsm_atdt: read call ret_val: %d and reaf_buf: %s\n",ret_val,read_buf);

	write(1,write_buf,strlen(write_buf));

	if(ret_val>0)
	{
		read_buf[ret_val]='\0';
		write(1,read_buf,strlen(read_buf));
	}
		
	return 0;
}
