//Autogenerated code by util.build.BuildDalvikSuite; do not edit.
package dot.junit.opcodes.return_object;
import dot.junit.opcodes.return_object.d.*;
import dot.junit.*;
public class Main_testN8 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        //@uses dot.junit.opcodes.return_object.d.T_return_object_13
        //@uses dot.junit.opcodes.return_object.d.TChild
        //@uses dot.junit.opcodes.return_object.d.TSuper
        //@uses dot.junit.opcodes.return_object.d.TInterface
        T_return_object_13 t = new T_return_object_13();
        assertTrue(t.run());
    }
}
