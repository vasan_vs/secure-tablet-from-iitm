//Autogenerated code by util.build.BuildDalvikSuite; do not edit.
package dot.junit.opcodes.invoke_super;
import dot.junit.opcodes.invoke_super.d.*;
import dot.junit.*;
public class Main_testVFE16 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        //@uses dot.junit.opcodes.invoke_super.d.T_invoke_super_13
        //@uses dot.junit.opcodes.invoke_super.d.TSuper
         try {
             Class.forName("dot.junit.opcodes.invoke_super.d.T_invoke_super_13");
             fail("expected a verification exception");
         } catch (Throwable t) {
             DxUtil.checkVerifyException(t);
         }
    }
}
