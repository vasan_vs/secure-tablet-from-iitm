//Autogenerated code by util.build.BuildDalvikSuite; do not edit.
package dot.junit.opcodes.rem_long;
import dot.junit.opcodes.rem_long.d.*;
import dot.junit.*;
public class Main_testN6 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_rem_long_1 t = new T_rem_long_1();
        assertEquals(-2000000000l, t.run(-10000000000l, -4000000000l));
    }
}
