//Autogenerated code by util.build.BuildDalvikSuite; do not edit.
package dot.junit.opcodes.sput_byte;
import dot.junit.opcodes.sput_byte.d.*;
import dot.junit.*;
public class Main_testN1 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_sput_byte_1 t = new T_sput_byte_1();
        assertEquals(0, T_sput_byte_1.st_i1);
        t.run();
        assertEquals(77, T_sput_byte_1.st_i1);
    }
}
