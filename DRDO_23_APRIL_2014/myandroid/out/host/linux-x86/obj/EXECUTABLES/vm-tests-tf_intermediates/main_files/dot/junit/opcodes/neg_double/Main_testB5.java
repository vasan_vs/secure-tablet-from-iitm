//Autogenerated code by util.build.BuildDalvikSuite; do not edit.
package dot.junit.opcodes.neg_double;
import dot.junit.opcodes.neg_double.d.*;
import dot.junit.*;
public class Main_testB5 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_neg_double_1 t = new T_neg_double_1();
        assertEquals(-4.9E-324d, t.run(Double.MIN_VALUE));
    }
}
