//Autogenerated code by util.build.BuildDalvikSuite; do not edit.
package dot.junit.opcodes.add_double_2addr;
import dot.junit.opcodes.add_double_2addr.d.*;
import dot.junit.*;
public class Main_testB8 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_add_double_2addr_1 t = new T_add_double_2addr_1();
        assertEquals(Double.POSITIVE_INFINITY, t.run(Double.MAX_VALUE,
                Double.MAX_VALUE));
    }
}
