//Autogenerated code by util.build.BuildDalvikSuite; do not edit.
package dot.junit.opcodes.sget;
import dot.junit.opcodes.sget.d.*;
import dot.junit.*;
public class Main_testN2 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_sget_2 t = new T_sget_2();
        assertEquals(123f, t.run());
    }
}
