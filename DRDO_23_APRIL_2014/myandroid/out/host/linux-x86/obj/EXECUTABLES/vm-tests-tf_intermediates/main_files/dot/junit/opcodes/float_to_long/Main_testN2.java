//Autogenerated code by util.build.BuildDalvikSuite; do not edit.
package dot.junit.opcodes.float_to_long;
import dot.junit.opcodes.float_to_long.d.*;
import dot.junit.*;
public class Main_testN2 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
         T_float_to_long_1 t = new T_float_to_long_1();
         assertEquals(1l, t.run(1));
    }
}
