//Autogenerated code by util.build.BuildDalvikSuite; do not edit.
package dot.junit.opcodes.long_to_int;
import dot.junit.opcodes.long_to_int.d.*;
import dot.junit.*;
public class Main_testN2 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_long_to_int_1 t = new T_long_to_int_1();
        assertEquals(-123456789, t.run(-123456789l));
    }
}
