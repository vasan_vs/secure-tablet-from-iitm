//Autogenerated code by util.build.BuildDalvikSuite; do not edit.
package dot.junit.opcodes.aget_object;
import dot.junit.opcodes.aget_object.d.*;
import dot.junit.*;
public class Main_testN2 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_aget_object_1 t = new T_aget_object_1();
        String[] arr = new String[] {"a", "b"};
        assertEquals("b", t.run(arr, 1));
    }
}
