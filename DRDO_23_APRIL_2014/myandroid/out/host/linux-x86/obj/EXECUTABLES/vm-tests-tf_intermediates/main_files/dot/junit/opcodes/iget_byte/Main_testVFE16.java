//Autogenerated code by util.build.BuildDalvikSuite; do not edit.
package dot.junit.opcodes.iget_byte;
import dot.junit.opcodes.iget_byte.d.*;
import dot.junit.*;
public class Main_testVFE16 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        //@uses dot.junit.opcodes.iget_byte.d.T_iget_byte_5
        //@uses dot.junit.opcodes.iget_byte.TestStubs        
        try {
            new T_iget_byte_5().run();
            fail("expected an IncompatibleClassChangeError exception");
        }  catch (IncompatibleClassChangeError e) {
            // expected
        }
    }
}
