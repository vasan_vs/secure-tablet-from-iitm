//autogenerated by util.build.BuildDalvikSuite, do not change
package dot.junit.opcodes.monitor_exit;
import dot.junit.opcodes.monitor_exit.d.*;
import dot.junit.*;
public class Main_testE1 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        //@uses dot.junit.opcodes.monitor_exit.TestRunnable
        final T_monitor_exit_1 t = new T_monitor_exit_1();
        final Object o = new Object();

        Runnable r = new TestRunnable(t, o);
        synchronized (o) {
            Thread th = new Thread(r);
            th.start();
            th.join();
        }
        if (t.result == false) {
            fail("expected IllegalMonitorStateException");
        }
    }
}
