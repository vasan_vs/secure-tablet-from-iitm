//autogenerated by util.build.BuildDalvikSuite, do not change
package dot.junit.opcodes.shl_int;
import dot.junit.opcodes.shl_int.d.*;
import dot.junit.*;
public class Main_testN2 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_shl_int_1 t = new T_shl_int_1();
        assertEquals(132, t.run(33, 2));
    }
}
