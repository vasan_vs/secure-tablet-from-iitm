//autogenerated by util.build.BuildDalvikSuite, do not change
package dot.junit.opcodes.rsub_int;
import dot.junit.opcodes.rsub_int.d.*;
import dot.junit.*;
public class Main_testB7 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_rsub_int_5 t = new T_rsub_int_5();
        assertEquals(2147450881, t.run(Integer.MAX_VALUE));
        assertEquals(2147450880, t.run(Integer.MIN_VALUE));
        assertEquals(-65535, t.run(Short.MAX_VALUE));
    }
}
