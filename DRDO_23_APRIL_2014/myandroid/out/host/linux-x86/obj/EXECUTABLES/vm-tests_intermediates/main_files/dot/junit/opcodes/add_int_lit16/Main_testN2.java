//autogenerated by util.build.BuildDalvikSuite, do not change
package dot.junit.opcodes.add_int_lit16;
import dot.junit.opcodes.add_int_lit16.d.*;
import dot.junit.*;
public class Main_testN2 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_add_int_lit16_2 t = new T_add_int_lit16_2();
        assertEquals(255, t.run());
    }
}
