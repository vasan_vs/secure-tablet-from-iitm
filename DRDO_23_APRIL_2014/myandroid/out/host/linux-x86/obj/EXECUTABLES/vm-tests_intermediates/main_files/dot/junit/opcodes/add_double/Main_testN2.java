//autogenerated by util.build.BuildDalvikSuite, do not change
package dot.junit.opcodes.add_double;
import dot.junit.opcodes.add_double.d.*;
import dot.junit.*;
public class Main_testN2 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_add_double_1 t = new T_add_double_1();
        assertEquals(-3.14d, t.run(0, -3.14d));
    }
}
