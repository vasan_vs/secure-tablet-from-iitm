//autogenerated by util.build.BuildDalvikSuite, do not change
package dot.junit.opcodes.mul_double;
import dot.junit.opcodes.mul_double.d.*;
import dot.junit.*;
public class Main_testN2 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_mul_double_1 t = new T_mul_double_1();
        assertEquals(-0d, t.run(0, -3.14d));
    }
}
