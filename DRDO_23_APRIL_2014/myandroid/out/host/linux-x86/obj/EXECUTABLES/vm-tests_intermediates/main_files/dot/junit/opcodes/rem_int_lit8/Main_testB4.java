//autogenerated by util.build.BuildDalvikSuite, do not change
package dot.junit.opcodes.rem_int_lit8;
import dot.junit.opcodes.rem_int_lit8.d.*;
import dot.junit.*;
public class Main_testB4 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_rem_int_lit8_7 t = new T_rem_int_lit8_7();
        assertEquals(-2, t.run(Short.MIN_VALUE));
    }
}
