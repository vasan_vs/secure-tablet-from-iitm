//autogenerated by util.build.BuildDalvikSuite, do not change
package dot.junit.opcodes.rem_float_2addr;
import dot.junit.opcodes.rem_float_2addr.d.*;
import dot.junit.*;
public class Main_testB8 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_rem_float_2addr_1 t = new T_rem_float_2addr_1();
        assertEquals(0f, t.run(1, Float.MIN_VALUE));
    }
}
