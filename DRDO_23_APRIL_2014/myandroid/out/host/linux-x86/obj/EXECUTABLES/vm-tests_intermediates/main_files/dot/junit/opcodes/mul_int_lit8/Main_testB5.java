//autogenerated by util.build.BuildDalvikSuite, do not change
package dot.junit.opcodes.mul_int_lit8;
import dot.junit.opcodes.mul_int_lit8.d.*;
import dot.junit.*;
public class Main_testB5 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_mul_int_lit8_6 t = new T_mul_int_lit8_6();
        assertEquals(-4161536, t.run(Short.MIN_VALUE));
    }
}
