//autogenerated by util.build.BuildDalvikSuite, do not change
package dot.junit.opcodes.shr_long;
import dot.junit.opcodes.shr_long.d.*;
import dot.junit.*;
public class Main_testN1 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_shr_long_1 t = new T_shr_long_1();
        assertEquals(5000000000l, t.run(40000000000l, 3));
    }
}
