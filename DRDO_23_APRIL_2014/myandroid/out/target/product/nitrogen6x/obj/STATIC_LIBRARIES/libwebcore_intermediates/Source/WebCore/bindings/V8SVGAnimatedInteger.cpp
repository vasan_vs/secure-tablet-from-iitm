/*
    This file is part of the WebKit open source project.
    This file has been generated by generate-bindings.pl. DO NOT MODIFY!

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.
*/

#include "config.h"
#include "V8SVGAnimatedInteger.h"

#if ENABLE(SVG)

#include "RuntimeEnabledFeatures.h"
#include "V8Binding.h"
#include "V8BindingMacros.h"
#include "V8BindingState.h"
#include "V8DOMWrapper.h"
#include "V8IsolatedContext.h"
#include "V8Proxy.h"
#include <wtf/UnusedParam.h>

namespace WebCore {

WrapperTypeInfo V8SVGAnimatedInteger::info = { V8SVGAnimatedInteger::GetTemplate, V8SVGAnimatedInteger::derefObject, 0, 0 };

namespace SVGAnimatedIntegerInternal {

template <typename T> void V8_USE(T) { }

static v8::Handle<v8::Value> baseValAttrGetter(v8::Local<v8::String> name, const v8::AccessorInfo& info)
{
    INC_STATS("DOM.SVGAnimatedInteger.baseVal._get");
    SVGAnimatedInteger* imp = V8SVGAnimatedInteger::toNative(info.Holder());
    return v8::Integer::New(imp->baseVal());
}

static void baseValAttrSetter(v8::Local<v8::String> name, v8::Local<v8::Value> value, const v8::AccessorInfo& info)
{
    INC_STATS("DOM.SVGAnimatedInteger.baseVal._set");
    SVGAnimatedInteger* imp = V8SVGAnimatedInteger::toNative(info.Holder());
    int v = toInt32(value);
    imp->setBaseVal(v);
    return;
}

static v8::Handle<v8::Value> animValAttrGetter(v8::Local<v8::String> name, const v8::AccessorInfo& info)
{
    INC_STATS("DOM.SVGAnimatedInteger.animVal._get");
    SVGAnimatedInteger* imp = V8SVGAnimatedInteger::toNative(info.Holder());
    return v8::Integer::New(imp->animVal());
}

} // namespace SVGAnimatedIntegerInternal

static const BatchedAttribute SVGAnimatedIntegerAttrs[] = {
    // Attribute 'baseVal' (Type: 'attribute' ExtAttr: 'StrictTypeChecking')
    {"baseVal", SVGAnimatedIntegerInternal::baseValAttrGetter, SVGAnimatedIntegerInternal::baseValAttrSetter, 0 /* no data */, static_cast<v8::AccessControl>(v8::DEFAULT), static_cast<v8::PropertyAttribute>(v8::None), 0 /* on instance */},
    // Attribute 'animVal' (Type: 'readonly attribute' ExtAttr: '')
    {"animVal", SVGAnimatedIntegerInternal::animValAttrGetter, 0, 0 /* no data */, static_cast<v8::AccessControl>(v8::DEFAULT), static_cast<v8::PropertyAttribute>(v8::None), 0 /* on instance */},
};
static v8::Persistent<v8::FunctionTemplate> ConfigureV8SVGAnimatedIntegerTemplate(v8::Persistent<v8::FunctionTemplate> desc)
{
    v8::Local<v8::Signature> defaultSignature = configureTemplate(desc, "SVGAnimatedInteger", v8::Persistent<v8::FunctionTemplate>(), V8SVGAnimatedInteger::internalFieldCount,
        SVGAnimatedIntegerAttrs, WTF_ARRAY_LENGTH(SVGAnimatedIntegerAttrs),
        0, 0);
    UNUSED_PARAM(defaultSignature); // In some cases, it will not be used.
    

    // Custom toString template
    desc->Set(getToStringName(), getToStringTemplate());
    return desc;
}

v8::Persistent<v8::FunctionTemplate> V8SVGAnimatedInteger::GetRawTemplate()
{
    static v8::Persistent<v8::FunctionTemplate> V8SVGAnimatedIntegerRawCache = createRawTemplate();
    return V8SVGAnimatedIntegerRawCache;
}

v8::Persistent<v8::FunctionTemplate> V8SVGAnimatedInteger::GetTemplate()
{
    static v8::Persistent<v8::FunctionTemplate> V8SVGAnimatedIntegerCache = ConfigureV8SVGAnimatedIntegerTemplate(GetRawTemplate());
    return V8SVGAnimatedIntegerCache;
}

bool V8SVGAnimatedInteger::HasInstance(v8::Handle<v8::Value> value)
{
    return GetRawTemplate()->HasInstance(value);
}


v8::Handle<v8::Object> V8SVGAnimatedInteger::wrapSlow(SVGAnimatedInteger* impl)
{
    v8::Handle<v8::Object> wrapper;
    V8Proxy* proxy = 0;
    wrapper = V8DOMWrapper::instantiateV8Object(proxy, &info, impl);
    if (wrapper.IsEmpty())
        return wrapper;

    impl->ref();
    v8::Persistent<v8::Object> wrapperHandle = v8::Persistent<v8::Object>::New(wrapper);
    getDOMObjectMap().set(impl, wrapperHandle);
    return wrapper;
}

void V8SVGAnimatedInteger::derefObject(void* object)
{
    static_cast<SVGAnimatedInteger*>(object)->deref();
}

} // namespace WebCore

#endif // ENABLE(SVG)
