/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include "rsContext.h"
#include "rsFifo.h"

namespace android {
namespace renderscript {
typedef struct RS_CMD_ContextDestroy_rec RS_CMD_ContextDestroy;
typedef struct RS_CMD_ContextGetMessage_rec RS_CMD_ContextGetMessage;
typedef struct RS_CMD_ContextPeekMessage_rec RS_CMD_ContextPeekMessage;
typedef struct RS_CMD_ContextInitToClient_rec RS_CMD_ContextInitToClient;
typedef struct RS_CMD_ContextDeinitToClient_rec RS_CMD_ContextDeinitToClient;
typedef struct RS_CMD_TypeCreate_rec RS_CMD_TypeCreate;
typedef struct RS_CMD_AllocationCreateTyped_rec RS_CMD_AllocationCreateTyped;
typedef struct RS_CMD_AllocationCreateFromBitmap_rec RS_CMD_AllocationCreateFromBitmap;
typedef struct RS_CMD_AllocationCubeCreateFromBitmap_rec RS_CMD_AllocationCubeCreateFromBitmap;
typedef struct RS_CMD_ContextFinish_rec RS_CMD_ContextFinish;
typedef struct RS_CMD_ContextBindRootScript_rec RS_CMD_ContextBindRootScript;
typedef struct RS_CMD_ContextBindProgramStore_rec RS_CMD_ContextBindProgramStore;
typedef struct RS_CMD_ContextBindProgramFragment_rec RS_CMD_ContextBindProgramFragment;
typedef struct RS_CMD_ContextBindProgramVertex_rec RS_CMD_ContextBindProgramVertex;
typedef struct RS_CMD_ContextBindProgramRaster_rec RS_CMD_ContextBindProgramRaster;
typedef struct RS_CMD_ContextBindFont_rec RS_CMD_ContextBindFont;
typedef struct RS_CMD_ContextPause_rec RS_CMD_ContextPause;
typedef struct RS_CMD_ContextResume_rec RS_CMD_ContextResume;
typedef struct RS_CMD_ContextSetSurface_rec RS_CMD_ContextSetSurface;
typedef struct RS_CMD_ContextDump_rec RS_CMD_ContextDump;
typedef struct RS_CMD_ContextSetPriority_rec RS_CMD_ContextSetPriority;
typedef struct RS_CMD_ContextDestroyWorker_rec RS_CMD_ContextDestroyWorker;
typedef struct RS_CMD_AssignName_rec RS_CMD_AssignName;
typedef struct RS_CMD_ObjDestroy_rec RS_CMD_ObjDestroy;
typedef struct RS_CMD_ElementCreate_rec RS_CMD_ElementCreate;
typedef struct RS_CMD_ElementCreate2_rec RS_CMD_ElementCreate2;
typedef struct RS_CMD_AllocationCopyToBitmap_rec RS_CMD_AllocationCopyToBitmap;
typedef struct RS_CMD_Allocation1DData_rec RS_CMD_Allocation1DData;
typedef struct RS_CMD_Allocation1DElementData_rec RS_CMD_Allocation1DElementData;
typedef struct RS_CMD_Allocation2DData_rec RS_CMD_Allocation2DData;
typedef struct RS_CMD_Allocation2DElementData_rec RS_CMD_Allocation2DElementData;
typedef struct RS_CMD_AllocationGenerateMipmaps_rec RS_CMD_AllocationGenerateMipmaps;
typedef struct RS_CMD_AllocationRead_rec RS_CMD_AllocationRead;
typedef struct RS_CMD_AllocationSyncAll_rec RS_CMD_AllocationSyncAll;
typedef struct RS_CMD_AllocationResize1D_rec RS_CMD_AllocationResize1D;
typedef struct RS_CMD_AllocationResize2D_rec RS_CMD_AllocationResize2D;
typedef struct RS_CMD_AllocationCopy2DRange_rec RS_CMD_AllocationCopy2DRange;
typedef struct RS_CMD_SamplerCreate_rec RS_CMD_SamplerCreate;
typedef struct RS_CMD_ScriptBindAllocation_rec RS_CMD_ScriptBindAllocation;
typedef struct RS_CMD_ScriptSetTimeZone_rec RS_CMD_ScriptSetTimeZone;
typedef struct RS_CMD_ScriptInvoke_rec RS_CMD_ScriptInvoke;
typedef struct RS_CMD_ScriptInvokeV_rec RS_CMD_ScriptInvokeV;
typedef struct RS_CMD_ScriptForEach_rec RS_CMD_ScriptForEach;
typedef struct RS_CMD_ScriptSetVarI_rec RS_CMD_ScriptSetVarI;
typedef struct RS_CMD_ScriptSetVarObj_rec RS_CMD_ScriptSetVarObj;
typedef struct RS_CMD_ScriptSetVarJ_rec RS_CMD_ScriptSetVarJ;
typedef struct RS_CMD_ScriptSetVarF_rec RS_CMD_ScriptSetVarF;
typedef struct RS_CMD_ScriptSetVarD_rec RS_CMD_ScriptSetVarD;
typedef struct RS_CMD_ScriptSetVarV_rec RS_CMD_ScriptSetVarV;
typedef struct RS_CMD_ScriptCCreate_rec RS_CMD_ScriptCCreate;
typedef struct RS_CMD_ProgramStoreCreate_rec RS_CMD_ProgramStoreCreate;
typedef struct RS_CMD_ProgramRasterCreate_rec RS_CMD_ProgramRasterCreate;
typedef struct RS_CMD_ProgramBindConstants_rec RS_CMD_ProgramBindConstants;
typedef struct RS_CMD_ProgramBindTexture_rec RS_CMD_ProgramBindTexture;
typedef struct RS_CMD_ProgramBindSampler_rec RS_CMD_ProgramBindSampler;
typedef struct RS_CMD_ProgramFragmentCreate_rec RS_CMD_ProgramFragmentCreate;
typedef struct RS_CMD_ProgramVertexCreate_rec RS_CMD_ProgramVertexCreate;
typedef struct RS_CMD_FontCreateFromFile_rec RS_CMD_FontCreateFromFile;
typedef struct RS_CMD_FontCreateFromMemory_rec RS_CMD_FontCreateFromMemory;
typedef struct RS_CMD_MeshCreate_rec RS_CMD_MeshCreate;

#define RS_CMD_ID_ContextDestroy 1
struct RS_CMD_ContextDestroy_rec {
};

#define RS_CMD_ID_ContextGetMessage 2
struct RS_CMD_ContextGetMessage_rec {
    void * data;
    size_t data_length;
    size_t * receiveLen;
    size_t receiveLen_length;
    uint32_t * usrID;
    size_t usrID_length;
};

#define RS_CMD_ID_ContextPeekMessage 3
struct RS_CMD_ContextPeekMessage_rec {
    size_t * receiveLen;
    size_t receiveLen_length;
    uint32_t * usrID;
    size_t usrID_length;
};

#define RS_CMD_ID_ContextInitToClient 4
struct RS_CMD_ContextInitToClient_rec {
};

#define RS_CMD_ID_ContextDeinitToClient 5
struct RS_CMD_ContextDeinitToClient_rec {
};

#define RS_CMD_ID_TypeCreate 6
struct RS_CMD_TypeCreate_rec {
    RsElement e;
    uint32_t dimX;
    uint32_t dimY;
    uint32_t dimZ;
    bool mips;
    bool faces;
};

#define RS_CMD_ID_AllocationCreateTyped 7
struct RS_CMD_AllocationCreateTyped_rec {
    RsType vtype;
    RsAllocationMipmapControl mips;
    uint32_t usages;
};

#define RS_CMD_ID_AllocationCreateFromBitmap 8
struct RS_CMD_AllocationCreateFromBitmap_rec {
    RsType vtype;
    RsAllocationMipmapControl mips;
    const void * data;
    size_t data_length;
    uint32_t usages;
};

#define RS_CMD_ID_AllocationCubeCreateFromBitmap 9
struct RS_CMD_AllocationCubeCreateFromBitmap_rec {
    RsType vtype;
    RsAllocationMipmapControl mips;
    const void * data;
    size_t data_length;
    uint32_t usages;
};

#define RS_CMD_ID_ContextFinish 10
struct RS_CMD_ContextFinish_rec {
};

#define RS_CMD_ID_ContextBindRootScript 11
struct RS_CMD_ContextBindRootScript_rec {
    RsScript sampler;
};

#define RS_CMD_ID_ContextBindProgramStore 12
struct RS_CMD_ContextBindProgramStore_rec {
    RsProgramStore pgm;
};

#define RS_CMD_ID_ContextBindProgramFragment 13
struct RS_CMD_ContextBindProgramFragment_rec {
    RsProgramFragment pgm;
};

#define RS_CMD_ID_ContextBindProgramVertex 14
struct RS_CMD_ContextBindProgramVertex_rec {
    RsProgramVertex pgm;
};

#define RS_CMD_ID_ContextBindProgramRaster 15
struct RS_CMD_ContextBindProgramRaster_rec {
    RsProgramRaster pgm;
};

#define RS_CMD_ID_ContextBindFont 16
struct RS_CMD_ContextBindFont_rec {
    RsFont pgm;
};

#define RS_CMD_ID_ContextPause 17
struct RS_CMD_ContextPause_rec {
};

#define RS_CMD_ID_ContextResume 18
struct RS_CMD_ContextResume_rec {
};

#define RS_CMD_ID_ContextSetSurface 19
struct RS_CMD_ContextSetSurface_rec {
    uint32_t width;
    uint32_t height;
    RsNativeWindow sur;
};

#define RS_CMD_ID_ContextDump 20
struct RS_CMD_ContextDump_rec {
    int32_t bits;
};

#define RS_CMD_ID_ContextSetPriority 21
struct RS_CMD_ContextSetPriority_rec {
    int32_t priority;
};

#define RS_CMD_ID_ContextDestroyWorker 22
struct RS_CMD_ContextDestroyWorker_rec {
};

#define RS_CMD_ID_AssignName 23
struct RS_CMD_AssignName_rec {
    RsObjectBase obj;
    const char * name;
    size_t name_length;
};

#define RS_CMD_ID_ObjDestroy 24
struct RS_CMD_ObjDestroy_rec {
    RsAsyncVoidPtr objPtr;
};

#define RS_CMD_ID_ElementCreate 25
struct RS_CMD_ElementCreate_rec {
    RsDataType mType;
    RsDataKind mKind;
    bool mNormalized;
    uint32_t mVectorSize;
};

#define RS_CMD_ID_ElementCreate2 26
struct RS_CMD_ElementCreate2_rec {
    const RsElement * elements;
    size_t elements_length;
    const char ** names;
    size_t names_length_length;
    const size_t * names_length;
    const uint32_t * arraySize;
    size_t arraySize_length;
};

#define RS_CMD_ID_AllocationCopyToBitmap 27
struct RS_CMD_AllocationCopyToBitmap_rec {
    RsAllocation alloc;
    void * data;
    size_t data_length;
};

#define RS_CMD_ID_Allocation1DData 28
struct RS_CMD_Allocation1DData_rec {
    RsAllocation va;
    uint32_t xoff;
    uint32_t lod;
    uint32_t count;
    const void * data;
    size_t data_length;
};

#define RS_CMD_ID_Allocation1DElementData 29
struct RS_CMD_Allocation1DElementData_rec {
    RsAllocation va;
    uint32_t x;
    uint32_t lod;
    const void * data;
    size_t data_length;
    uint32_t comp_offset;
};

#define RS_CMD_ID_Allocation2DData 30
struct RS_CMD_Allocation2DData_rec {
    RsAllocation va;
    uint32_t xoff;
    uint32_t yoff;
    uint32_t lod;
    RsAllocationCubemapFace face;
    uint32_t w;
    uint32_t h;
    const void * data;
    size_t data_length;
};

#define RS_CMD_ID_Allocation2DElementData 31
struct RS_CMD_Allocation2DElementData_rec {
    RsAllocation va;
    uint32_t x;
    uint32_t y;
    uint32_t lod;
    RsAllocationCubemapFace face;
    const void * data;
    size_t data_length;
    uint32_t element_offset;
};

#define RS_CMD_ID_AllocationGenerateMipmaps 32
struct RS_CMD_AllocationGenerateMipmaps_rec {
    RsAllocation va;
};

#define RS_CMD_ID_AllocationRead 33
struct RS_CMD_AllocationRead_rec {
    RsAllocation va;
    void * data;
    size_t data_length;
};

#define RS_CMD_ID_AllocationSyncAll 34
struct RS_CMD_AllocationSyncAll_rec {
    RsAllocation va;
    RsAllocationUsageType src;
};

#define RS_CMD_ID_AllocationResize1D 35
struct RS_CMD_AllocationResize1D_rec {
    RsAllocation va;
    uint32_t dimX;
};

#define RS_CMD_ID_AllocationResize2D 36
struct RS_CMD_AllocationResize2D_rec {
    RsAllocation va;
    uint32_t dimX;
    uint32_t dimY;
};

#define RS_CMD_ID_AllocationCopy2DRange 37
struct RS_CMD_AllocationCopy2DRange_rec {
    RsAllocation dest;
    uint32_t destXoff;
    uint32_t destYoff;
    uint32_t destMip;
    uint32_t destFace;
    uint32_t width;
    uint32_t height;
    RsAllocation src;
    uint32_t srcXoff;
    uint32_t srcYoff;
    uint32_t srcMip;
    uint32_t srcFace;
};

#define RS_CMD_ID_SamplerCreate 38
struct RS_CMD_SamplerCreate_rec {
    RsSamplerValue magFilter;
    RsSamplerValue minFilter;
    RsSamplerValue wrapS;
    RsSamplerValue wrapT;
    RsSamplerValue wrapR;
    float mAniso;
};

#define RS_CMD_ID_ScriptBindAllocation 39
struct RS_CMD_ScriptBindAllocation_rec {
    RsScript vtm;
    RsAllocation va;
    uint32_t slot;
};

#define RS_CMD_ID_ScriptSetTimeZone 40
struct RS_CMD_ScriptSetTimeZone_rec {
    RsScript s;
    const char * timeZone;
    size_t timeZone_length;
};

#define RS_CMD_ID_ScriptInvoke 41
struct RS_CMD_ScriptInvoke_rec {
    RsScript s;
    uint32_t slot;
};

#define RS_CMD_ID_ScriptInvokeV 42
struct RS_CMD_ScriptInvokeV_rec {
    RsScript s;
    uint32_t slot;
    const void * data;
    size_t data_length;
};

#define RS_CMD_ID_ScriptForEach 43
struct RS_CMD_ScriptForEach_rec {
    RsScript s;
    uint32_t slot;
    RsAllocation ain;
    RsAllocation aout;
    const void * usr;
    size_t usr_length;
};

#define RS_CMD_ID_ScriptSetVarI 44
struct RS_CMD_ScriptSetVarI_rec {
    RsScript s;
    uint32_t slot;
    int value;
};

#define RS_CMD_ID_ScriptSetVarObj 45
struct RS_CMD_ScriptSetVarObj_rec {
    RsScript s;
    uint32_t slot;
    RsObjectBase value;
};

#define RS_CMD_ID_ScriptSetVarJ 46
struct RS_CMD_ScriptSetVarJ_rec {
    RsScript s;
    uint32_t slot;
    int64_t value;
};

#define RS_CMD_ID_ScriptSetVarF 47
struct RS_CMD_ScriptSetVarF_rec {
    RsScript s;
    uint32_t slot;
    float value;
};

#define RS_CMD_ID_ScriptSetVarD 48
struct RS_CMD_ScriptSetVarD_rec {
    RsScript s;
    uint32_t slot;
    double value;
};

#define RS_CMD_ID_ScriptSetVarV 49
struct RS_CMD_ScriptSetVarV_rec {
    RsScript s;
    uint32_t slot;
    const void * data;
    size_t data_length;
};

#define RS_CMD_ID_ScriptCCreate 50
struct RS_CMD_ScriptCCreate_rec {
    const char * resName;
    size_t resName_length;
    const char * cacheDir;
    size_t cacheDir_length;
    const char * text;
    size_t text_length;
};

#define RS_CMD_ID_ProgramStoreCreate 51
struct RS_CMD_ProgramStoreCreate_rec {
    bool colorMaskR;
    bool colorMaskG;
    bool colorMaskB;
    bool colorMaskA;
    bool depthMask;
    bool ditherEnable;
    RsBlendSrcFunc srcFunc;
    RsBlendDstFunc destFunc;
    RsDepthFunc depthFunc;
};

#define RS_CMD_ID_ProgramRasterCreate 52
struct RS_CMD_ProgramRasterCreate_rec {
    bool pointSprite;
    RsCullMode cull;
};

#define RS_CMD_ID_ProgramBindConstants 53
struct RS_CMD_ProgramBindConstants_rec {
    RsProgram vp;
    uint32_t slot;
    RsAllocation constants;
};

#define RS_CMD_ID_ProgramBindTexture 54
struct RS_CMD_ProgramBindTexture_rec {
    RsProgramFragment pf;
    uint32_t slot;
    RsAllocation a;
};

#define RS_CMD_ID_ProgramBindSampler 55
struct RS_CMD_ProgramBindSampler_rec {
    RsProgramFragment pf;
    uint32_t slot;
    RsSampler s;
};

#define RS_CMD_ID_ProgramFragmentCreate 56
struct RS_CMD_ProgramFragmentCreate_rec {
    const char * shaderText;
    size_t shaderText_length;
    const uint32_t * params;
    size_t params_length;
};

#define RS_CMD_ID_ProgramVertexCreate 57
struct RS_CMD_ProgramVertexCreate_rec {
    const char * shaderText;
    size_t shaderText_length;
    const uint32_t * params;
    size_t params_length;
};

#define RS_CMD_ID_FontCreateFromFile 58
struct RS_CMD_FontCreateFromFile_rec {
    const char * name;
    size_t name_length;
    float fontSize;
    uint32_t dpi;
};

#define RS_CMD_ID_FontCreateFromMemory 59
struct RS_CMD_FontCreateFromMemory_rec {
    const char * name;
    size_t name_length;
    float fontSize;
    uint32_t dpi;
    const void * data;
    size_t data_length;
};

#define RS_CMD_ID_MeshCreate 60
struct RS_CMD_MeshCreate_rec {
    RsAllocation * vtx;
    size_t vtx_length;
    RsAllocation * idx;
    size_t idx_length;
    uint32_t * primType;
    size_t primType_length;
};

void rsi_ContextDestroy (Context *);
RsMessageToClientType rsi_ContextGetMessage (Context *, void * data, size_t data_length, size_t * receiveLen, size_t receiveLen_length, uint32_t * usrID, size_t usrID_length);
RsMessageToClientType rsi_ContextPeekMessage (Context *, size_t * receiveLen, size_t receiveLen_length, uint32_t * usrID, size_t usrID_length);
void rsi_ContextInitToClient (Context *);
void rsi_ContextDeinitToClient (Context *);
RsType rsi_TypeCreate (Context *, RsElement e, uint32_t dimX, uint32_t dimY, uint32_t dimZ, bool mips, bool faces);
RsAllocation rsi_AllocationCreateTyped (Context *, RsType vtype, RsAllocationMipmapControl mips, uint32_t usages);
RsAllocation rsi_AllocationCreateFromBitmap (Context *, RsType vtype, RsAllocationMipmapControl mips, const void * data, size_t data_length, uint32_t usages);
RsAllocation rsi_AllocationCubeCreateFromBitmap (Context *, RsType vtype, RsAllocationMipmapControl mips, const void * data, size_t data_length, uint32_t usages);
void rsi_ContextFinish (Context *);
void rsi_ContextBindRootScript (Context *, RsScript sampler);
void rsi_ContextBindProgramStore (Context *, RsProgramStore pgm);
void rsi_ContextBindProgramFragment (Context *, RsProgramFragment pgm);
void rsi_ContextBindProgramVertex (Context *, RsProgramVertex pgm);
void rsi_ContextBindProgramRaster (Context *, RsProgramRaster pgm);
void rsi_ContextBindFont (Context *, RsFont pgm);
void rsi_ContextPause (Context *);
void rsi_ContextResume (Context *);
void rsi_ContextSetSurface (Context *, uint32_t width, uint32_t height, RsNativeWindow sur);
void rsi_ContextDump (Context *, int32_t bits);
void rsi_ContextSetPriority (Context *, int32_t priority);
void rsi_ContextDestroyWorker (Context *);
void rsi_AssignName (Context *, RsObjectBase obj, const char * name, size_t name_length);
void rsi_ObjDestroy (Context *, RsAsyncVoidPtr objPtr);
RsElement rsi_ElementCreate (Context *, RsDataType mType, RsDataKind mKind, bool mNormalized, uint32_t mVectorSize);
RsElement rsi_ElementCreate2 (Context *, const RsElement * elements, size_t elements_length, const char ** names, size_t names_length_length, const size_t * names_length, const uint32_t * arraySize, size_t arraySize_length);
void rsi_AllocationCopyToBitmap (Context *, RsAllocation alloc, void * data, size_t data_length);
void rsi_Allocation1DData (Context *, RsAllocation va, uint32_t xoff, uint32_t lod, uint32_t count, const void * data, size_t data_length);
void rsi_Allocation1DElementData (Context *, RsAllocation va, uint32_t x, uint32_t lod, const void * data, size_t data_length, uint32_t comp_offset);
void rsi_Allocation2DData (Context *, RsAllocation va, uint32_t xoff, uint32_t yoff, uint32_t lod, RsAllocationCubemapFace face, uint32_t w, uint32_t h, const void * data, size_t data_length);
void rsi_Allocation2DElementData (Context *, RsAllocation va, uint32_t x, uint32_t y, uint32_t lod, RsAllocationCubemapFace face, const void * data, size_t data_length, uint32_t element_offset);
void rsi_AllocationGenerateMipmaps (Context *, RsAllocation va);
void rsi_AllocationRead (Context *, RsAllocation va, void * data, size_t data_length);
void rsi_AllocationSyncAll (Context *, RsAllocation va, RsAllocationUsageType src);
void rsi_AllocationResize1D (Context *, RsAllocation va, uint32_t dimX);
void rsi_AllocationResize2D (Context *, RsAllocation va, uint32_t dimX, uint32_t dimY);
void rsi_AllocationCopy2DRange (Context *, RsAllocation dest, uint32_t destXoff, uint32_t destYoff, uint32_t destMip, uint32_t destFace, uint32_t width, uint32_t height, RsAllocation src, uint32_t srcXoff, uint32_t srcYoff, uint32_t srcMip, uint32_t srcFace);
RsSampler rsi_SamplerCreate (Context *, RsSamplerValue magFilter, RsSamplerValue minFilter, RsSamplerValue wrapS, RsSamplerValue wrapT, RsSamplerValue wrapR, float mAniso);
void rsi_ScriptBindAllocation (Context *, RsScript vtm, RsAllocation va, uint32_t slot);
void rsi_ScriptSetTimeZone (Context *, RsScript s, const char * timeZone, size_t timeZone_length);
void rsi_ScriptInvoke (Context *, RsScript s, uint32_t slot);
void rsi_ScriptInvokeV (Context *, RsScript s, uint32_t slot, const void * data, size_t data_length);
void rsi_ScriptForEach (Context *, RsScript s, uint32_t slot, RsAllocation ain, RsAllocation aout, const void * usr, size_t usr_length);
void rsi_ScriptSetVarI (Context *, RsScript s, uint32_t slot, int value);
void rsi_ScriptSetVarObj (Context *, RsScript s, uint32_t slot, RsObjectBase value);
void rsi_ScriptSetVarJ (Context *, RsScript s, uint32_t slot, int64_t value);
void rsi_ScriptSetVarF (Context *, RsScript s, uint32_t slot, float value);
void rsi_ScriptSetVarD (Context *, RsScript s, uint32_t slot, double value);
void rsi_ScriptSetVarV (Context *, RsScript s, uint32_t slot, const void * data, size_t data_length);
RsScript rsi_ScriptCCreate (Context *, const char * resName, size_t resName_length, const char * cacheDir, size_t cacheDir_length, const char * text, size_t text_length);
RsProgramStore rsi_ProgramStoreCreate (Context *, bool colorMaskR, bool colorMaskG, bool colorMaskB, bool colorMaskA, bool depthMask, bool ditherEnable, RsBlendSrcFunc srcFunc, RsBlendDstFunc destFunc, RsDepthFunc depthFunc);
RsProgramRaster rsi_ProgramRasterCreate (Context *, bool pointSprite, RsCullMode cull);
void rsi_ProgramBindConstants (Context *, RsProgram vp, uint32_t slot, RsAllocation constants);
void rsi_ProgramBindTexture (Context *, RsProgramFragment pf, uint32_t slot, RsAllocation a);
void rsi_ProgramBindSampler (Context *, RsProgramFragment pf, uint32_t slot, RsSampler s);
RsProgramFragment rsi_ProgramFragmentCreate (Context *, const char * shaderText, size_t shaderText_length, const uint32_t * params, size_t params_length);
RsProgramVertex rsi_ProgramVertexCreate (Context *, const char * shaderText, size_t shaderText_length, const uint32_t * params, size_t params_length);
RsFont rsi_FontCreateFromFile (Context *, const char * name, size_t name_length, float fontSize, uint32_t dpi);
RsFont rsi_FontCreateFromMemory (Context *, const char * name, size_t name_length, float fontSize, uint32_t dpi, const void * data, size_t data_length);
RsMesh rsi_MeshCreate (Context *, RsAllocation * vtx, size_t vtx_length, RsAllocation * idx, size_t idx_length, uint32_t * primType, size_t primType_length);


void rsp_ContextFinish (Context *, const void *);
void rsp_ContextBindRootScript (Context *, const void *);
void rsp_ContextBindProgramStore (Context *, const void *);
void rsp_ContextBindProgramFragment (Context *, const void *);
void rsp_ContextBindProgramVertex (Context *, const void *);
void rsp_ContextBindProgramRaster (Context *, const void *);
void rsp_ContextBindFont (Context *, const void *);
void rsp_ContextPause (Context *, const void *);
void rsp_ContextResume (Context *, const void *);
void rsp_ContextSetSurface (Context *, const void *);
void rsp_ContextDump (Context *, const void *);
void rsp_ContextSetPriority (Context *, const void *);
void rsp_ContextDestroyWorker (Context *, const void *);
void rsp_AssignName (Context *, const void *);
void rsp_ObjDestroy (Context *, const void *);
void rsp_AllocationCopyToBitmap (Context *, const void *);
void rsp_Allocation1DData (Context *, const void *);
void rsp_Allocation1DElementData (Context *, const void *);
void rsp_Allocation2DData (Context *, const void *);
void rsp_Allocation2DElementData (Context *, const void *);
void rsp_AllocationGenerateMipmaps (Context *, const void *);
void rsp_AllocationRead (Context *, const void *);
void rsp_AllocationSyncAll (Context *, const void *);
void rsp_AllocationResize1D (Context *, const void *);
void rsp_AllocationResize2D (Context *, const void *);
void rsp_AllocationCopy2DRange (Context *, const void *);
void rsp_ScriptBindAllocation (Context *, const void *);
void rsp_ScriptSetTimeZone (Context *, const void *);
void rsp_ScriptInvoke (Context *, const void *);
void rsp_ScriptInvokeV (Context *, const void *);
void rsp_ScriptForEach (Context *, const void *);
void rsp_ScriptSetVarI (Context *, const void *);
void rsp_ScriptSetVarObj (Context *, const void *);
void rsp_ScriptSetVarJ (Context *, const void *);
void rsp_ScriptSetVarF (Context *, const void *);
void rsp_ScriptSetVarD (Context *, const void *);
void rsp_ScriptSetVarV (Context *, const void *);
void rsp_ScriptCCreate (Context *, const void *);
void rsp_ProgramBindConstants (Context *, const void *);
void rsp_ProgramBindTexture (Context *, const void *);
void rsp_ProgramBindSampler (Context *, const void *);
void rsp_FontCreateFromFile (Context *, const void *);
void rsp_FontCreateFromMemory (Context *, const void *);
void rsp_MeshCreate (Context *, const void *);


typedef struct RsPlaybackRemoteHeaderRec {
    uint32_t command;
    uint32_t size;
} RsPlaybackRemoteHeader;

typedef void (*RsPlaybackLocalFunc)(Context *, const void *, size_t sizeBytes);
typedef void (*RsPlaybackRemoteFunc)(Context *, Fifo *, uint8_t *scratch, size_t scratchSize);
extern RsPlaybackLocalFunc gPlaybackFuncs[61];
extern RsPlaybackRemoteFunc gPlaybackRemoteFuncs[61];
}
}
