package android.animation;
public interface TimeInterpolator
{
public abstract  float getInterpolation(float input);
}
