package android.view.inputmethod;
public final class CorrectionInfo
  implements android.os.Parcelable
{
public  CorrectionInfo(int offset, java.lang.CharSequence oldText, java.lang.CharSequence newText) { throw new RuntimeException("Stub!"); }
public  int getOffset() { throw new RuntimeException("Stub!"); }
public  java.lang.CharSequence getOldText() { throw new RuntimeException("Stub!"); }
public  java.lang.CharSequence getNewText() { throw new RuntimeException("Stub!"); }
public  java.lang.String toString() { throw new RuntimeException("Stub!"); }
public  void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }
public  int describeContents() { throw new RuntimeException("Stub!"); }
public static final android.os.Parcelable.Creator<android.view.inputmethod.CorrectionInfo> CREATOR;
static { CREATOR = null; }
}
