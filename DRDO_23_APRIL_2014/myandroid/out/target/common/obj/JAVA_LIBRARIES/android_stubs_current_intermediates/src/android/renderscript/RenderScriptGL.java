package android.renderscript;
public class RenderScriptGL
  extends android.renderscript.RenderScript
{
public static class SurfaceConfig
{
public  SurfaceConfig() { throw new RuntimeException("Stub!"); }
public  SurfaceConfig(android.renderscript.RenderScriptGL.SurfaceConfig sc) { throw new RuntimeException("Stub!"); }
public  void setColor(int minimum, int preferred) { throw new RuntimeException("Stub!"); }
public  void setAlpha(int minimum, int preferred) { throw new RuntimeException("Stub!"); }
public  void setDepth(int minimum, int preferred) { throw new RuntimeException("Stub!"); }
public  void setSamples(int minimum, int preferred, float Q) { throw new RuntimeException("Stub!"); }
}
public  RenderScriptGL(android.content.Context ctx, android.renderscript.RenderScriptGL.SurfaceConfig sc) { throw new RuntimeException("Stub!"); }
public  void setSurface(android.view.SurfaceHolder sur, int w, int h) { throw new RuntimeException("Stub!"); }
public  void setSurfaceTexture(android.graphics.SurfaceTexture sur, int w, int h) { throw new RuntimeException("Stub!"); }
public  int getHeight() { throw new RuntimeException("Stub!"); }
public  int getWidth() { throw new RuntimeException("Stub!"); }
public  void pause() { throw new RuntimeException("Stub!"); }
public  void resume() { throw new RuntimeException("Stub!"); }
public  void bindRootScript(android.renderscript.Script s) { throw new RuntimeException("Stub!"); }
public  void bindProgramStore(android.renderscript.ProgramStore p) { throw new RuntimeException("Stub!"); }
public  void bindProgramFragment(android.renderscript.ProgramFragment p) { throw new RuntimeException("Stub!"); }
public  void bindProgramRaster(android.renderscript.ProgramRaster p) { throw new RuntimeException("Stub!"); }
public  void bindProgramVertex(android.renderscript.ProgramVertex p) { throw new RuntimeException("Stub!"); }
}
