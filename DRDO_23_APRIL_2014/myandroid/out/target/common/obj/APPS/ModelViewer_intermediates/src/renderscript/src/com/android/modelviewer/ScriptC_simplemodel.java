/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * This file is auto-generated. DO NOT MODIFY!
 * The source Renderscript file: frameworks/base/tests/RenderScriptTests/ModelViewer/src/com/android/modelviewer/simplemodel.rs
 */
package com.android.modelviewer;

import android.renderscript.*;
import android.content.res.Resources;

/**
 * @hide
 */
public class ScriptC_simplemodel extends ScriptC {
    // Constructor
    public  ScriptC_simplemodel(RenderScript rs, Resources resources, int id) {
        super(rs, resources, id);
    }

    private final static int mExportVarIdx_gPVBackground = 0;
    private ProgramVertex mExportVar_gPVBackground;
    public void set_gPVBackground(ProgramVertex v) {
        mExportVar_gPVBackground = v;
        setVar(mExportVarIdx_gPVBackground, v);
    }

    public ProgramVertex get_gPVBackground() {
        return mExportVar_gPVBackground;
    }

    private final static int mExportVarIdx_gPFBackground = 1;
    private ProgramFragment mExportVar_gPFBackground;
    public void set_gPFBackground(ProgramFragment v) {
        mExportVar_gPFBackground = v;
        setVar(mExportVarIdx_gPFBackground, v);
    }

    public ProgramFragment get_gPFBackground() {
        return mExportVar_gPFBackground;
    }

    private final static int mExportVarIdx_gTGrid = 2;
    private Allocation mExportVar_gTGrid;
    public void set_gTGrid(Allocation v) {
        mExportVar_gTGrid = v;
        setVar(mExportVarIdx_gTGrid, v);
    }

    public Allocation get_gTGrid() {
        return mExportVar_gTGrid;
    }

    private final static int mExportVarIdx_gPFSBackground = 3;
    private ProgramStore mExportVar_gPFSBackground;
    public void set_gPFSBackground(ProgramStore v) {
        mExportVar_gPFSBackground = v;
        setVar(mExportVarIdx_gPFSBackground, v);
    }

    public ProgramStore get_gPFSBackground() {
        return mExportVar_gPFSBackground;
    }

    private final static int mExportVarIdx_gItalic = 4;
    private Font mExportVar_gItalic;
    public void set_gItalic(Font v) {
        mExportVar_gItalic = v;
        setVar(mExportVarIdx_gItalic, v);
    }

    public Font get_gItalic() {
        return mExportVar_gItalic;
    }

    private final static int mExportVarIdx_gTextAlloc = 5;
    private Allocation mExportVar_gTextAlloc;
    public void set_gTextAlloc(Allocation v) {
        mExportVar_gTextAlloc = v;
        setVar(mExportVarIdx_gTextAlloc, v);
    }

    public Allocation get_gTextAlloc() {
        return mExportVar_gTextAlloc;
    }

    private final static int mExportVarIdx_gPostureMatrix = 6;
    private Matrix4f mExportVar_gPostureMatrix;
    public void set_gPostureMatrix(Matrix4f v) {
        mExportVar_gPostureMatrix = v;
        FieldPacker fp = new FieldPacker(64);
        fp.addMatrix(v);
        setVar(mExportVarIdx_gPostureMatrix, fp);
    }

    public Matrix4f get_gPostureMatrix() {
        return mExportVar_gPostureMatrix;
    }

    private final static int mExportVarIdx_gMeshes = 7;
    private ScriptField_MeshInfo mExportVar_gMeshes;
    public void bind_gMeshes(ScriptField_MeshInfo v) {
        mExportVar_gMeshes = v;
        if (v == null) bindAllocation(null, mExportVarIdx_gMeshes);
        else bindAllocation(v.getAllocation(), mExportVarIdx_gMeshes);
    }

    public ScriptField_MeshInfo get_gMeshes() {
        return mExportVar_gMeshes;
    }

    private final static int mExportFuncIdx_onActionDown = 0;
    public void invoke_onActionDown(float x, float y) {
        FieldPacker onActionDown_fp = new FieldPacker(8);
        onActionDown_fp.addF32(x);
        onActionDown_fp.addF32(y);
        invoke(mExportFuncIdx_onActionDown, onActionDown_fp);
    }

    private final static int mExportFuncIdx_onActionScale = 1;
    public void invoke_onActionScale(float scale) {
        FieldPacker onActionScale_fp = new FieldPacker(4);
        onActionScale_fp.addF32(scale);
        invoke(mExportFuncIdx_onActionScale, onActionScale_fp);
    }

    private final static int mExportFuncIdx_onActionMove = 2;
    public void invoke_onActionMove(float x, float y) {
        FieldPacker onActionMove_fp = new FieldPacker(8);
        onActionMove_fp.addF32(x);
        onActionMove_fp.addF32(y);
        invoke(mExportFuncIdx_onActionMove, onActionMove_fp);
    }

    private final static int mExportFuncIdx_updateMeshInfo = 3;
    public void invoke_updateMeshInfo() {
        invoke(mExportFuncIdx_updateMeshInfo);
    }

    private final static int mExportFuncIdx_drawDescription = 4;
    public void invoke_drawDescription() {
        invoke(mExportFuncIdx_drawDescription);
    }

}

