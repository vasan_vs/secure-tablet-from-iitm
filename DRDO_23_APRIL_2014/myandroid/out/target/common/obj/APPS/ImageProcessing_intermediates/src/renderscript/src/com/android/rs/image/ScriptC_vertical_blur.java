/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * This file is auto-generated. DO NOT MODIFY!
 * The source Renderscript file: frameworks/base/tests/RenderScriptTests/ImageProcessing/src/com/android/rs/image/vertical_blur.rs
 */
package com.android.rs.image;

import android.renderscript.*;
import android.content.res.Resources;

/**
 * @hide
 */
public class ScriptC_vertical_blur extends ScriptC {
    // Constructor
    public  ScriptC_vertical_blur(RenderScript rs, Resources resources, int id) {
        super(rs, resources, id);
        __U8_4 = Element.U8_4(rs);
    }

    private Element __U8_4;
    private final static int mExportForEachIdx_root = 0;
    public void forEach_root(Allocation aout) {
        // check aout
        if (!aout.getType().getElement().isCompatible(__U8_4)) {
            throw new RSRuntimeException("Type mismatch with U8_4!");
        }
        forEach(mExportForEachIdx_root, null, aout, null);
    }

    private final static int mExportFuncIdx_setLevels = 0;
    public void invoke_setLevels(float iBlk, float oBlk, float iWht, float oWht) {
        FieldPacker setLevels_fp = new FieldPacker(16);
        setLevels_fp.addF32(iBlk);
        setLevels_fp.addF32(oBlk);
        setLevels_fp.addF32(iWht);
        setLevels_fp.addF32(oWht);
        invoke(mExportFuncIdx_setLevels, setLevels_fp);
    }

    private final static int mExportFuncIdx_setSaturation = 1;
    public void invoke_setSaturation(float sat) {
        FieldPacker setSaturation_fp = new FieldPacker(4);
        setSaturation_fp.addF32(sat);
        invoke(mExportFuncIdx_setSaturation, setSaturation_fp);
    }

    private final static int mExportFuncIdx_setGamma = 2;
    public void invoke_setGamma(float g) {
        FieldPacker setGamma_fp = new FieldPacker(4);
        setGamma_fp.addF32(g);
        invoke(mExportFuncIdx_setGamma, setGamma_fp);
    }

}

