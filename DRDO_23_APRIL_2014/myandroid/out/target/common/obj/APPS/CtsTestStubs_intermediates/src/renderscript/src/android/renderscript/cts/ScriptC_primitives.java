/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * This file is auto-generated. DO NOT MODIFY!
 * The source Renderscript file: cts/tests/src/android/renderscript/cts/primitives.rs
 */
package android.renderscript.cts;

import android.renderscript.*;
import android.content.res.Resources;

/**
 * @hide
 */
public class ScriptC_primitives extends ScriptC {
    // Constructor
    public  ScriptC_primitives(RenderScript rs, Resources resources, int id) {
        super(rs, resources, id);
        mExportVar_floatTest = 1.99f;
        mExportVar_doubleTest = 2.05;
        mExportVar_charTest = -8;
        mExportVar_shortTest = -16;
        mExportVar_intTest = -32;
        mExportVar_longTest = 17179869184L;
        mExportVar_longlongTest = 68719476736L;
        mExportVar_boolTest = false;
        mExportVar_ucharTest = 8;
        mExportVar_ushortTest = 16;
        mExportVar_uintTest = 32;
        mExportVar_ulongTest = 4611686018427387904L;
        mExportVar_int64_tTest = -17179869184L;
        mExportVar_uint64_tTest = 117179869184L;
    }

    private final static int mExportVarIdx_c1 = 0;
    private ScriptField_ConstMatrix mExportVar_c1;
    public void bind_c1(ScriptField_ConstMatrix v) {
        mExportVar_c1 = v;
        if (v == null) bindAllocation(null, mExportVarIdx_c1);
        else bindAllocation(v.getAllocation(), mExportVarIdx_c1);
    }

    public ScriptField_ConstMatrix get_c1() {
        return mExportVar_c1;
    }

    private final static int mExportVarIdx_c2 = 1;
    private ScriptField_ConstComplex mExportVar_c2;
    public void bind_c2(ScriptField_ConstComplex v) {
        mExportVar_c2 = v;
        if (v == null) bindAllocation(null, mExportVarIdx_c2);
        else bindAllocation(v.getAllocation(), mExportVarIdx_c2);
    }

    public ScriptField_ConstComplex get_c2() {
        return mExportVar_c2;
    }

    private final static int mExportVarIdx_c3 = 2;
    private ScriptField_ConstExtra mExportVar_c3;
    public void bind_c3(ScriptField_ConstExtra v) {
        mExportVar_c3 = v;
        if (v == null) bindAllocation(null, mExportVarIdx_c3);
        else bindAllocation(v.getAllocation(), mExportVarIdx_c3);
    }

    public ScriptField_ConstExtra get_c3() {
        return mExportVar_c3;
    }

    private final static int mExportVarIdx_avt = 3;
    private ScriptField_AllVectorTypes mExportVar_avt;
    public void bind_avt(ScriptField_AllVectorTypes v) {
        mExportVar_avt = v;
        if (v == null) bindAllocation(null, mExportVarIdx_avt);
        else bindAllocation(v.getAllocation(), mExportVarIdx_avt);
    }

    public ScriptField_AllVectorTypes get_avt() {
        return mExportVar_avt;
    }

    private final static int mExportVarIdx_floatTest = 4;
    private float mExportVar_floatTest;
    public void set_floatTest(float v) {
        mExportVar_floatTest = v;
        setVar(mExportVarIdx_floatTest, v);
    }

    public float get_floatTest() {
        return mExportVar_floatTest;
    }

    private final static int mExportVarIdx_doubleTest = 5;
    private double mExportVar_doubleTest;
    public void set_doubleTest(double v) {
        mExportVar_doubleTest = v;
        setVar(mExportVarIdx_doubleTest, v);
    }

    public double get_doubleTest() {
        return mExportVar_doubleTest;
    }

    private final static int mExportVarIdx_charTest = 6;
    private byte mExportVar_charTest;
    public void set_charTest(byte v) {
        mExportVar_charTest = v;
        setVar(mExportVarIdx_charTest, v);
    }

    public byte get_charTest() {
        return mExportVar_charTest;
    }

    private final static int mExportVarIdx_shortTest = 7;
    private short mExportVar_shortTest;
    public void set_shortTest(short v) {
        mExportVar_shortTest = v;
        setVar(mExportVarIdx_shortTest, v);
    }

    public short get_shortTest() {
        return mExportVar_shortTest;
    }

    private final static int mExportVarIdx_intTest = 8;
    private int mExportVar_intTest;
    public void set_intTest(int v) {
        mExportVar_intTest = v;
        setVar(mExportVarIdx_intTest, v);
    }

    public int get_intTest() {
        return mExportVar_intTest;
    }

    private final static int mExportVarIdx_longTest = 9;
    private long mExportVar_longTest;
    public void set_longTest(long v) {
        mExportVar_longTest = v;
        setVar(mExportVarIdx_longTest, v);
    }

    public long get_longTest() {
        return mExportVar_longTest;
    }

    private final static int mExportVarIdx_longlongTest = 10;
    private long mExportVar_longlongTest;
    public void set_longlongTest(long v) {
        mExportVar_longlongTest = v;
        setVar(mExportVarIdx_longlongTest, v);
    }

    public long get_longlongTest() {
        return mExportVar_longlongTest;
    }

    private final static int mExportVarIdx_boolTest = 11;
    private boolean mExportVar_boolTest;
    public void set_boolTest(boolean v) {
        mExportVar_boolTest = v;
        setVar(mExportVarIdx_boolTest, v);
    }

    public boolean get_boolTest() {
        return mExportVar_boolTest;
    }

    private final static int mExportVarIdx_structTest = 12;
    private ScriptField_myStruct.Item mExportVar_structTest;
    public void set_structTest(ScriptField_myStruct.Item v) {
        mExportVar_structTest = v;
        FieldPacker fp = new FieldPacker(4);
        fp.addI32(v.i);
        setVar(mExportVarIdx_structTest, fp);
    }

    public ScriptField_myStruct.Item get_structTest() {
        return mExportVar_structTest;
    }

    private final static int mExportVarIdx_allocationTest = 13;
    private Allocation mExportVar_allocationTest;
    public void set_allocationTest(Allocation v) {
        mExportVar_allocationTest = v;
        setVar(mExportVarIdx_allocationTest, v);
    }

    public Allocation get_allocationTest() {
        return mExportVar_allocationTest;
    }

    private final static int mExportVarIdx_intPtrTest = 14;
    private Allocation mExportVar_intPtrTest;
    public void bind_intPtrTest(Allocation v) {
        mExportVar_intPtrTest = v;
        if (v == null) bindAllocation(null, mExportVarIdx_intPtrTest);
        else bindAllocation(v, mExportVarIdx_intPtrTest);
    }

    public Allocation get_intPtrTest() {
        return mExportVar_intPtrTest;
    }

    private final static int mExportVarIdx_ucharTest = 15;
    private short mExportVar_ucharTest;
    public void set_ucharTest(short v) {
        mExportVar_ucharTest = v;
        setVar(mExportVarIdx_ucharTest, v);
    }

    public short get_ucharTest() {
        return mExportVar_ucharTest;
    }

    private final static int mExportVarIdx_ushortTest = 16;
    private int mExportVar_ushortTest;
    public void set_ushortTest(int v) {
        mExportVar_ushortTest = v;
        setVar(mExportVarIdx_ushortTest, v);
    }

    public int get_ushortTest() {
        return mExportVar_ushortTest;
    }

    private final static int mExportVarIdx_uintTest = 17;
    private long mExportVar_uintTest;
    public void set_uintTest(long v) {
        mExportVar_uintTest = v;
        setVar(mExportVarIdx_uintTest, v);
    }

    public long get_uintTest() {
        return mExportVar_uintTest;
    }

    private final static int mExportVarIdx_ulongTest = 18;
    private long mExportVar_ulongTest;
    public void set_ulongTest(long v) {
        mExportVar_ulongTest = v;
        setVar(mExportVarIdx_ulongTest, v);
    }

    public long get_ulongTest() {
        return mExportVar_ulongTest;
    }

    private final static int mExportVarIdx_int64_tTest = 19;
    private long mExportVar_int64_tTest;
    public void set_int64_tTest(long v) {
        mExportVar_int64_tTest = v;
        setVar(mExportVarIdx_int64_tTest, v);
    }

    public long get_int64_tTest() {
        return mExportVar_int64_tTest;
    }

    private final static int mExportVarIdx_uint64_tTest = 20;
    private long mExportVar_uint64_tTest;
    public void set_uint64_tTest(long v) {
        mExportVar_uint64_tTest = v;
        setVar(mExportVarIdx_uint64_tTest, v);
    }

    public long get_uint64_tTest() {
        return mExportVar_uint64_tTest;
    }

    private final static int mExportFuncIdx_test = 0;
    public void invoke_test() {
        invoke(mExportFuncIdx_test);
    }

}

