/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * This file is auto-generated. DO NOT MODIFY!
 * The source Renderscript file: packages/wallpapers/NoiseField/src/com/android/noisefield/noisefield.rs
 */
package com.android.noisefield;

import android.renderscript.*;
import android.content.res.Resources;

/**
 * @hide
 */
public class ScriptC_noisefield extends ScriptC {
    // Constructor
    public  ScriptC_noisefield(RenderScript rs, Resources resources, int id) {
        super(rs, resources, id);
        mExportVar_touchDown = false;
    }

    private final static int mExportVarIdx_textureDot = 0;
    private Allocation mExportVar_textureDot;
    public void set_textureDot(Allocation v) {
        mExportVar_textureDot = v;
        setVar(mExportVarIdx_textureDot, v);
    }

    public Allocation get_textureDot() {
        return mExportVar_textureDot;
    }

    private final static int mExportVarIdx_textureVignette = 1;
    private Allocation mExportVar_textureVignette;
    public void set_textureVignette(Allocation v) {
        mExportVar_textureVignette = v;
        setVar(mExportVarIdx_textureVignette, v);
    }

    public Allocation get_textureVignette() {
        return mExportVar_textureVignette;
    }

    private final static int mExportVarIdx_vertBg = 2;
    private ProgramVertex mExportVar_vertBg;
    public void set_vertBg(ProgramVertex v) {
        mExportVar_vertBg = v;
        setVar(mExportVarIdx_vertBg, v);
    }

    public ProgramVertex get_vertBg() {
        return mExportVar_vertBg;
    }

    private final static int mExportVarIdx_fragBg = 3;
    private ProgramFragment mExportVar_fragBg;
    public void set_fragBg(ProgramFragment v) {
        mExportVar_fragBg = v;
        setVar(mExportVarIdx_fragBg, v);
    }

    public ProgramFragment get_fragBg() {
        return mExportVar_fragBg;
    }

    private final static int mExportVarIdx_vertDots = 4;
    private ProgramVertex mExportVar_vertDots;
    public void set_vertDots(ProgramVertex v) {
        mExportVar_vertDots = v;
        setVar(mExportVarIdx_vertDots, v);
    }

    public ProgramVertex get_vertDots() {
        return mExportVar_vertDots;
    }

    private final static int mExportVarIdx_fragDots = 5;
    private ProgramFragment mExportVar_fragDots;
    public void set_fragDots(ProgramFragment v) {
        mExportVar_fragDots = v;
        setVar(mExportVarIdx_fragDots, v);
    }

    public ProgramFragment get_fragDots() {
        return mExportVar_fragDots;
    }

    private final static int mExportVarIdx_storeAlpha = 6;
    private ProgramStore mExportVar_storeAlpha;
    public void set_storeAlpha(ProgramStore v) {
        mExportVar_storeAlpha = v;
        setVar(mExportVarIdx_storeAlpha, v);
    }

    public ProgramStore get_storeAlpha() {
        return mExportVar_storeAlpha;
    }

    private final static int mExportVarIdx_storeAdd = 7;
    private ProgramStore mExportVar_storeAdd;
    public void set_storeAdd(ProgramStore v) {
        mExportVar_storeAdd = v;
        setVar(mExportVarIdx_storeAdd, v);
    }

    public ProgramStore get_storeAdd() {
        return mExportVar_storeAdd;
    }

    private final static int mExportVarIdx_vpConstants = 8;
    private ScriptField_VpConsts mExportVar_vpConstants;
    public void bind_vpConstants(ScriptField_VpConsts v) {
        mExportVar_vpConstants = v;
        if (v == null) bindAllocation(null, mExportVarIdx_vpConstants);
        else bindAllocation(v.getAllocation(), mExportVarIdx_vpConstants);
    }

    public ScriptField_VpConsts get_vpConstants() {
        return mExportVar_vpConstants;
    }

    private final static int mExportVarIdx_dotParticles = 9;
    private ScriptField_Particle mExportVar_dotParticles;
    public void bind_dotParticles(ScriptField_Particle v) {
        mExportVar_dotParticles = v;
        if (v == null) bindAllocation(null, mExportVarIdx_dotParticles);
        else bindAllocation(v.getAllocation(), mExportVarIdx_dotParticles);
    }

    public ScriptField_Particle get_dotParticles() {
        return mExportVar_dotParticles;
    }

    private final static int mExportVarIdx_vertexColors = 10;
    private ScriptField_VertexColor_s mExportVar_vertexColors;
    public void bind_vertexColors(ScriptField_VertexColor_s v) {
        mExportVar_vertexColors = v;
        if (v == null) bindAllocation(null, mExportVarIdx_vertexColors);
        else bindAllocation(v.getAllocation(), mExportVarIdx_vertexColors);
    }

    public ScriptField_VertexColor_s get_vertexColors() {
        return mExportVar_vertexColors;
    }

    private final static int mExportVarIdx_dotMesh = 11;
    private Mesh mExportVar_dotMesh;
    public void set_dotMesh(Mesh v) {
        mExportVar_dotMesh = v;
        setVar(mExportVarIdx_dotMesh, v);
    }

    public Mesh get_dotMesh() {
        return mExportVar_dotMesh;
    }

    private final static int mExportVarIdx_gBackgroundMesh = 12;
    private Mesh mExportVar_gBackgroundMesh;
    public void set_gBackgroundMesh(Mesh v) {
        mExportVar_gBackgroundMesh = v;
        setVar(mExportVarIdx_gBackgroundMesh, v);
    }

    public Mesh get_gBackgroundMesh() {
        return mExportVar_gBackgroundMesh;
    }

    private final static int mExportVarIdx_densityDPI = 13;
    private float mExportVar_densityDPI;
    public void set_densityDPI(float v) {
        mExportVar_densityDPI = v;
        setVar(mExportVarIdx_densityDPI, v);
    }

    public float get_densityDPI() {
        return mExportVar_densityDPI;
    }

    private final static int mExportVarIdx_touchDown = 14;
    private boolean mExportVar_touchDown;
    public void set_touchDown(boolean v) {
        mExportVar_touchDown = v;
        setVar(mExportVarIdx_touchDown, v);
    }

    public boolean get_touchDown() {
        return mExportVar_touchDown;
    }

    private final static int mExportFuncIdx_positionParticles = 0;
    public void invoke_positionParticles() {
        invoke(mExportFuncIdx_positionParticles);
    }

    private final static int mExportFuncIdx_touch = 1;
    public void invoke_touch(float x, float y) {
        FieldPacker touch_fp = new FieldPacker(8);
        touch_fp.addF32(x);
        touch_fp.addF32(y);
        invoke(mExportFuncIdx_touch, touch_fp);
    }

}

