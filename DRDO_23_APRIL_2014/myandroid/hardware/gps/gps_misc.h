/*
**
** IMPORTANT LEGAL NOTICE:
** waiver of Liability: Please note that this software is freeware licensed
** under the Apache v. 2.0 license. This software might have been altered,
** modified, influenced or changed by third parties without knowledge and
** consent of Cinterion Wireless Modules GmbH (“Cinterion”).
** As the user of this freeware software you are fully and solely
** responsible for the compatibility of the software with your system and
** all its (software) components as well as any defects, bugs, malfunctions
** or damages arising from or in connection with its installation or use.
** Cinterion does not accept any liability whatsoever with respect to this
** software, namely, but not limited to, its functionality and its
** compatibility with any other hardware and/or software or parts thereof.
** Cinterion shall not be liable for any defects, bugs, malfunctions or
** damages contained in this software or resulting from or arising in
** connection with its installation or use.
**
** Copyright 2011, Cinterion Wireless Modules GmbH
** Copyright 2006, The Android Open Source Project
**
** Licensed under the Apache License, Version 2.0 (the "License");
** you may not use this file except in compliance with the License.
** You may obtain a copy of the License at
**
**     http://www.apache.org/licenses/LICENSE-2.0
**
** Unless required by applicable law or agreed to in writing, software
** distributed under the License is distributed on an "AS IS" BASIS,
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
** See the License for the specific language governing permissions and
** limitations under the License.
*/

//////////////////////////////////////////////////////////////////////
//
// gps_misc.h
// miscellaneous functions
//
//////////////////////////////////////////////////////////////////////

// android version
#define ANDROID_VER_2_1   210
#define ANDROID_VER_2_2   220
#define ANDROID_VER_2_3   230

// #define __ANDROID_VERSION__   ANDROID_VER_2_1


// Debug zones
#define ZONE_ERROR    1
#define ZONE_FUNC     2
#define ZONE_GPS_INT  4
#define ZONE_INFO     8

// #define DEFAULT_ZONE   (ZONE_ERROR | ZONE_FUNC | ZONE_GPS_INT | ZONE_INFO)
#define DEFAULT_ZONE   ZONE_ERROR


void msleep ( unsigned long dwMilliSecs );
int epoll_register ( int epoll_fd, int fd );
int epoll_deregister( int  epoll_fd, int  fd );
int misc_OpenGPSPort( void );
int misc_CloseGPSPort(int fd);
int misc_StartGPS ( void );
int misc_StopGPS ( void );
void GPSTrace( int zone, const char *format, ... );
void misc_VersionInfo( void );


