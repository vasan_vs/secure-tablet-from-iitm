#include "tamper.h"
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

int get_values(int fd, int reg )
{
	int val;
	
	val=i2c_smbus_read_byte_data(fd, reg);
	//val=i2c_smbus_read_block_data(fd, reg, data);

	if (val<0)
        {
                perror ("Failed to read from slave:");
                exit(EXIT_FAILURE);
        }
	
	return val;
}
	
int check_tamper(int fd)
{
        /*The slave address is given to the IOCTL of the adapter to open tamper device*/
	int tamp_det;
        if (ioctl(fd, I2C_SLAVE, SLAVE_ADDR) <0)
        {
                fprintf (stderr, "Failed to set slave address: %m\n");
                return -1;
        }
	
	tamp_det = get_values(fd, FLAG_REGISTER);

	if ( (tamp_det & TB1) || (tamp_det & TB2) || (tamp_det & OF))
		return BOARD_TAMPERED;
	else
		return BOARD_NOT_TAMPERED;
}

		
int main(void)
{

	int fd_i2c,fd_con;
	

	fd_i2c= open("/dev/i2c-0", O_RDWR);
	if(fd_i2c<0)
	{
		printf("Failed in opening /dev/i2c-0\n");
		exit(0);	
	}
	fd_con= open("/dev/console", O_RDWR);
	if(fd_con<0)
	{
		write(1,"Failed in opening /dev/console\n",30);
		exit(0);	
	}

	//while (1)
	{	
		if (check_tamper(fd_i2c)==BOARD_TAMPERED)	
		{
			 printf("Board is Tampered unable to UNLOCK");
			//	system("rm /dev/input/event0");
			//	system("rm /dev/input/event1");
				
			//	write(fd_con,"Board is Tampered\n",18);
			//	sleep(60);
			//	system("echo 1 > /sys/class/graphics/fb0/blank");
			//	sleep(20);
			//	execlp("reboot","reboot",NULL);
		}
	 	if (check_tamper(fd_i2c)!=BOARD_TAMPERED)
		{
		

			int count=0;			
			char buff1[2],buff2[7];
			char va;
			int i,size;
			buff1[0]=i2c_smbus_read_byte_data(fd_i2c, (USER_DATA));
			buff1[1]=i2c_smbus_read_byte_data(fd_i2c, (USER_DATA+1));
			size=((buff1[0]-48)*10+buff1[1]-48);
			for(i=0;i<size;i++)
			{		
				va=i2c_smbus_read_byte_data(fd_i2c, (USER_DATA+i+2));
				sprintf(buff2, "%c", va);	
				int nb=strlen(buff2);
				printf("%c",buff2[0]);
				count++;
				if(count==2 && i!=size)
				{
				printf("  ");
				count=0;
				
				}
			}
		}

		
	}
	return 1;
}
