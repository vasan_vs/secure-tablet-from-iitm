#include <linux/i2c-dev.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>

#define SLAVE_ADDR      0x68
#define TAMPER_SET_1    0
#define TAMPER_SET_2    1 
#define FLAG_REGISTER   0x0f
#define TAMPER_REG_1    0x14
#define TAMPER_REG_2    0x15
#define USER_DATA 	0x20

#define BOARD_TAMPERED		1
#define BOARD_NOT_TAMPERED	0

#define TEB1    (1 << 7)
#define TEB2    (1 << 7)
#define TB1     (1 << 1)
#define TB2     (1 << 0)
#define BL      (1 << 4)
#define OF      (1 << 2)
#define TIE1    (1 << 6)
#define TIE2    (1 << 6)
#define TCM1    (1 << 5)
#define TCM2    (1 << 5)
#define TPM1    (1 << 4)
#define TPM2    (1 << 4)
#define CLR1    (1 << 0)
#define CLR2    (1 << 0)
#define RST1    (1 << 1)
#define RST2    (1 << 1)

void write_register(int fd, int reg, int value);
int get_values(int fd, int reg);
void unset_bit(int fd, int reg, int bit_pos);
void set_bit(int fd, int reg, int bit_pos);
int check_tamper(int fd);
