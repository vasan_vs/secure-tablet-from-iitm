#include <linux/i2c-dev.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#define SLAVE_ADDR 0x68

int get_values(int fd, int reg)
{
	int val;
	
	/* Read the register values, retry is Alert (7th) bit is 1, which denoted that the value in the register is being updated"*/
	val=i2c_smbus_read_byte_data(fd, reg);

	if (val<0)
        {
                perror ("Failed to read from slave:");
                exit(EXIT_FAILURE);
        }
	
	return val;
}
	
		
int main(int argc, char *argv[])

{

	int fd, tamper;

	fd = open("/dev/i2c-0", O_RDWR);

	/*The slave address is given to the IOCTL of the adapter to open tamper device*/
	if (ioctl(fd, I2C_SLAVE, SLAVE_ADDR) <0)
	{
		fprintf (stderr, "Failed to set slave address: %m\n");
		return 1;
	}

	tamper=get_values(fd, 0x20);
	if (tamper!=2)
		printf("Board is Tampered");
	else
		printf("Board is safe ( Not Tampered)");

	return 1;
}



