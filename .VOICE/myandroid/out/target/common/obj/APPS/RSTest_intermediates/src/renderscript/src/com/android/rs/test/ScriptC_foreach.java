/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * This file is auto-generated. DO NOT MODIFY!
 * The source Renderscript file: frameworks/base/tests/RenderScriptTests/tests/src/com/android/rs/test/foreach.rs
 */
package com.android.rs.test;

import android.renderscript.*;
import android.content.res.Resources;

/**
 * @hide
 */
public class ScriptC_foreach extends ScriptC {
    // Constructor
    public  ScriptC_foreach(RenderScript rs, Resources resources, int id) {
        super(rs, resources, id);
        __I32 = Element.I32(rs);
    }

    private Element __I32;
    private final static int mExportVarIdx_a = 0;
    private Allocation mExportVar_a;
    public void bind_a(Allocation v) {
        mExportVar_a = v;
        if (v == null) bindAllocation(null, mExportVarIdx_a);
        else bindAllocation(v, mExportVarIdx_a);
    }

    public Allocation get_a() {
        return mExportVar_a;
    }

    private final static int mExportVarIdx_dimX = 1;
    private int mExportVar_dimX;
    public void set_dimX(int v) {
        mExportVar_dimX = v;
        setVar(mExportVarIdx_dimX, v);
    }

    public int get_dimX() {
        return mExportVar_dimX;
    }

    private final static int mExportVarIdx_dimY = 2;
    private int mExportVar_dimY;
    public void set_dimY(int v) {
        mExportVar_dimY = v;
        setVar(mExportVarIdx_dimY, v);
    }

    public int get_dimY() {
        return mExportVar_dimY;
    }

    private final static int mExportForEachIdx_root = 0;
    public void forEach_root(Allocation aout) {
        // check aout
        if (!aout.getType().getElement().isCompatible(__I32)) {
            throw new RSRuntimeException("Type mismatch with I32!");
        }
        forEach(mExportForEachIdx_root, null, aout, null);
    }

    private final static int mExportFuncIdx_foreach_test = 0;
    public void invoke_foreach_test() {
        invoke(mExportFuncIdx_foreach_test);
    }

}

