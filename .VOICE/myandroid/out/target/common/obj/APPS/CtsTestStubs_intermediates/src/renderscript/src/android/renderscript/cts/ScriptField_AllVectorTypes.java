/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * This file is auto-generated. DO NOT MODIFY!
 * The source Renderscript file: cts/tests/src/android/renderscript/cts/graphics_runner.rs
 */
package android.renderscript.cts;

import android.renderscript.*;
import android.content.res.Resources;

/**
 * @hide
 */
public class ScriptField_AllVectorTypes extends android.renderscript.Script.FieldBase {
    static public class Item {
        public static final int sizeof = 128;

        Byte2 b2;
        Byte3 b3;
        Byte4 b4;
        Short2 s2;
        Short3 s3;
        Short4 s4;
        Int2 i2;
        Int3 i3;
        Int4 i4;
        Float2 f2;
        Float3 f3;
        Float4 f4;

        Item() {
            b2 = new Byte2();
            b3 = new Byte3();
            b4 = new Byte4();
            s2 = new Short2();
            s3 = new Short3();
            s4 = new Short4();
            i2 = new Int2();
            i3 = new Int3();
            i4 = new Int4();
            f2 = new Float2();
            f3 = new Float3();
            f4 = new Float4();
        }

    }

    private Item mItemArray[];
    private FieldPacker mIOBuffer;
    public static Element createElement(RenderScript rs) {
        Element.Builder eb = new Element.Builder(rs);
        eb.add(Element.createVector(rs, Element.DataType.SIGNED_8, 2), "b2");
        eb.add(Element.U16(rs), "#padding_1");
        eb.add(Element.createVector(rs, Element.DataType.SIGNED_8, 3), "b3");
        eb.add(Element.U8(rs), "#padding_2");
        eb.add(Element.createVector(rs, Element.DataType.SIGNED_8, 4), "b4");
        eb.add(Element.createVector(rs, Element.DataType.SIGNED_16, 2), "s2");
        eb.add(Element.createVector(rs, Element.DataType.SIGNED_16, 3), "s3");
        eb.add(Element.U16(rs), "#padding_3");
        eb.add(Element.createVector(rs, Element.DataType.SIGNED_16, 4), "s4");
        eb.add(Element.createVector(rs, Element.DataType.SIGNED_32, 2), "i2");
        eb.add(Element.U32(rs), "#padding_4");
        eb.add(Element.U32(rs), "#padding_5");
        eb.add(Element.createVector(rs, Element.DataType.SIGNED_32, 3), "i3");
        eb.add(Element.U32(rs), "#padding_6");
        eb.add(Element.createVector(rs, Element.DataType.SIGNED_32, 4), "i4");
        eb.add(Element.F32_2(rs), "f2");
        eb.add(Element.U32(rs), "#padding_7");
        eb.add(Element.U32(rs), "#padding_8");
        eb.add(Element.F32_3(rs), "f3");
        eb.add(Element.U32(rs), "#padding_9");
        eb.add(Element.F32_4(rs), "f4");
        return eb.create();
    }

    public  ScriptField_AllVectorTypes(RenderScript rs, int count) {
        mItemArray = null;
        mIOBuffer = null;
        mElement = createElement(rs);
        init(rs, count);
    }

    public  ScriptField_AllVectorTypes(RenderScript rs, int count, int usages) {
        mItemArray = null;
        mIOBuffer = null;
        mElement = createElement(rs);
        init(rs, count, usages);
    }

    private void copyToArrayLocal(Item i, FieldPacker fp) {
        fp.addI8(i.b2);
        fp.skip(2);
        fp.addI8(i.b3);
        fp.skip(1);
        fp.addI8(i.b4);
        fp.addI16(i.s2);
        fp.addI16(i.s3);
        fp.skip(2);
        fp.addI16(i.s4);
        fp.addI32(i.i2);
        fp.skip(8);
        fp.addI32(i.i3);
        fp.skip(4);
        fp.addI32(i.i4);
        fp.addF32(i.f2);
        fp.skip(8);
        fp.addF32(i.f3);
        fp.skip(4);
        fp.addF32(i.f4);
    }

    private void copyToArray(Item i, int index) {
        if (mIOBuffer == null) mIOBuffer = new FieldPacker(Item.sizeof * getType().getX()/* count */);
        mIOBuffer.reset(index * Item.sizeof);
        copyToArrayLocal(i, mIOBuffer);
    }

    public synchronized void set(Item i, int index, boolean copyNow) {
        if (mItemArray == null) mItemArray = new Item[getType().getX() /* count */];
        mItemArray[index] = i;
        if (copyNow)  {
            copyToArray(i, index);
            FieldPacker fp = new FieldPacker(Item.sizeof);
            copyToArrayLocal(i, fp);
            mAllocation.setFromFieldPacker(index, fp);
        }

    }

    public synchronized Item get(int index) {
        if (mItemArray == null) return null;
        return mItemArray[index];
    }

    public synchronized void set_b2(int index, Byte2 v, boolean copyNow) {
        if (mIOBuffer == null) mIOBuffer = new FieldPacker(Item.sizeof * getType().getX()/* count */);
        if (mItemArray == null) mItemArray = new Item[getType().getX() /* count */];
        if (mItemArray[index] == null) mItemArray[index] = new Item();
        mItemArray[index].b2 = v;
        if (copyNow)  {
            mIOBuffer.reset(index * Item.sizeof);
            mIOBuffer.addI8(v);
            FieldPacker fp = new FieldPacker(2);
            fp.addI8(v);
            mAllocation.setFromFieldPacker(index, 0, fp);
        }

    }

    public synchronized void set_b3(int index, Byte3 v, boolean copyNow) {
        if (mIOBuffer == null) mIOBuffer = new FieldPacker(Item.sizeof * getType().getX()/* count */);
        if (mItemArray == null) mItemArray = new Item[getType().getX() /* count */];
        if (mItemArray[index] == null) mItemArray[index] = new Item();
        mItemArray[index].b3 = v;
        if (copyNow)  {
            mIOBuffer.reset(index * Item.sizeof + 4);
            mIOBuffer.addI8(v);
            FieldPacker fp = new FieldPacker(3);
            fp.addI8(v);
            mAllocation.setFromFieldPacker(index, 2, fp);
        }

    }

    public synchronized void set_b4(int index, Byte4 v, boolean copyNow) {
        if (mIOBuffer == null) mIOBuffer = new FieldPacker(Item.sizeof * getType().getX()/* count */);
        if (mItemArray == null) mItemArray = new Item[getType().getX() /* count */];
        if (mItemArray[index] == null) mItemArray[index] = new Item();
        mItemArray[index].b4 = v;
        if (copyNow)  {
            mIOBuffer.reset(index * Item.sizeof + 8);
            mIOBuffer.addI8(v);
            FieldPacker fp = new FieldPacker(4);
            fp.addI8(v);
            mAllocation.setFromFieldPacker(index, 4, fp);
        }

    }

    public synchronized void set_s2(int index, Short2 v, boolean copyNow) {
        if (mIOBuffer == null) mIOBuffer = new FieldPacker(Item.sizeof * getType().getX()/* count */);
        if (mItemArray == null) mItemArray = new Item[getType().getX() /* count */];
        if (mItemArray[index] == null) mItemArray[index] = new Item();
        mItemArray[index].s2 = v;
        if (copyNow)  {
            mIOBuffer.reset(index * Item.sizeof + 12);
            mIOBuffer.addI16(v);
            FieldPacker fp = new FieldPacker(4);
            fp.addI16(v);
            mAllocation.setFromFieldPacker(index, 5, fp);
        }

    }

    public synchronized void set_s3(int index, Short3 v, boolean copyNow) {
        if (mIOBuffer == null) mIOBuffer = new FieldPacker(Item.sizeof * getType().getX()/* count */);
        if (mItemArray == null) mItemArray = new Item[getType().getX() /* count */];
        if (mItemArray[index] == null) mItemArray[index] = new Item();
        mItemArray[index].s3 = v;
        if (copyNow)  {
            mIOBuffer.reset(index * Item.sizeof + 16);
            mIOBuffer.addI16(v);
            FieldPacker fp = new FieldPacker(6);
            fp.addI16(v);
            mAllocation.setFromFieldPacker(index, 6, fp);
        }

    }

    public synchronized void set_s4(int index, Short4 v, boolean copyNow) {
        if (mIOBuffer == null) mIOBuffer = new FieldPacker(Item.sizeof * getType().getX()/* count */);
        if (mItemArray == null) mItemArray = new Item[getType().getX() /* count */];
        if (mItemArray[index] == null) mItemArray[index] = new Item();
        mItemArray[index].s4 = v;
        if (copyNow)  {
            mIOBuffer.reset(index * Item.sizeof + 24);
            mIOBuffer.addI16(v);
            FieldPacker fp = new FieldPacker(8);
            fp.addI16(v);
            mAllocation.setFromFieldPacker(index, 8, fp);
        }

    }

    public synchronized void set_i2(int index, Int2 v, boolean copyNow) {
        if (mIOBuffer == null) mIOBuffer = new FieldPacker(Item.sizeof * getType().getX()/* count */);
        if (mItemArray == null) mItemArray = new Item[getType().getX() /* count */];
        if (mItemArray[index] == null) mItemArray[index] = new Item();
        mItemArray[index].i2 = v;
        if (copyNow)  {
            mIOBuffer.reset(index * Item.sizeof + 32);
            mIOBuffer.addI32(v);
            FieldPacker fp = new FieldPacker(8);
            fp.addI32(v);
            mAllocation.setFromFieldPacker(index, 9, fp);
        }

    }

    public synchronized void set_i3(int index, Int3 v, boolean copyNow) {
        if (mIOBuffer == null) mIOBuffer = new FieldPacker(Item.sizeof * getType().getX()/* count */);
        if (mItemArray == null) mItemArray = new Item[getType().getX() /* count */];
        if (mItemArray[index] == null) mItemArray[index] = new Item();
        mItemArray[index].i3 = v;
        if (copyNow)  {
            mIOBuffer.reset(index * Item.sizeof + 48);
            mIOBuffer.addI32(v);
            FieldPacker fp = new FieldPacker(12);
            fp.addI32(v);
            mAllocation.setFromFieldPacker(index, 12, fp);
        }

    }

    public synchronized void set_i4(int index, Int4 v, boolean copyNow) {
        if (mIOBuffer == null) mIOBuffer = new FieldPacker(Item.sizeof * getType().getX()/* count */);
        if (mItemArray == null) mItemArray = new Item[getType().getX() /* count */];
        if (mItemArray[index] == null) mItemArray[index] = new Item();
        mItemArray[index].i4 = v;
        if (copyNow)  {
            mIOBuffer.reset(index * Item.sizeof + 64);
            mIOBuffer.addI32(v);
            FieldPacker fp = new FieldPacker(16);
            fp.addI32(v);
            mAllocation.setFromFieldPacker(index, 14, fp);
        }

    }

    public synchronized void set_f2(int index, Float2 v, boolean copyNow) {
        if (mIOBuffer == null) mIOBuffer = new FieldPacker(Item.sizeof * getType().getX()/* count */);
        if (mItemArray == null) mItemArray = new Item[getType().getX() /* count */];
        if (mItemArray[index] == null) mItemArray[index] = new Item();
        mItemArray[index].f2 = v;
        if (copyNow)  {
            mIOBuffer.reset(index * Item.sizeof + 80);
            mIOBuffer.addF32(v);
            FieldPacker fp = new FieldPacker(8);
            fp.addF32(v);
            mAllocation.setFromFieldPacker(index, 15, fp);
        }

    }

    public synchronized void set_f3(int index, Float3 v, boolean copyNow) {
        if (mIOBuffer == null) mIOBuffer = new FieldPacker(Item.sizeof * getType().getX()/* count */);
        if (mItemArray == null) mItemArray = new Item[getType().getX() /* count */];
        if (mItemArray[index] == null) mItemArray[index] = new Item();
        mItemArray[index].f3 = v;
        if (copyNow)  {
            mIOBuffer.reset(index * Item.sizeof + 96);
            mIOBuffer.addF32(v);
            FieldPacker fp = new FieldPacker(12);
            fp.addF32(v);
            mAllocation.setFromFieldPacker(index, 18, fp);
        }

    }

    public synchronized void set_f4(int index, Float4 v, boolean copyNow) {
        if (mIOBuffer == null) mIOBuffer = new FieldPacker(Item.sizeof * getType().getX()/* count */);
        if (mItemArray == null) mItemArray = new Item[getType().getX() /* count */];
        if (mItemArray[index] == null) mItemArray[index] = new Item();
        mItemArray[index].f4 = v;
        if (copyNow)  {
            mIOBuffer.reset(index * Item.sizeof + 112);
            mIOBuffer.addF32(v);
            FieldPacker fp = new FieldPacker(16);
            fp.addF32(v);
            mAllocation.setFromFieldPacker(index, 20, fp);
        }

    }

    public synchronized Byte2 get_b2(int index) {
        if (mItemArray == null) return null;
        return mItemArray[index].b2;
    }

    public synchronized Byte3 get_b3(int index) {
        if (mItemArray == null) return null;
        return mItemArray[index].b3;
    }

    public synchronized Byte4 get_b4(int index) {
        if (mItemArray == null) return null;
        return mItemArray[index].b4;
    }

    public synchronized Short2 get_s2(int index) {
        if (mItemArray == null) return null;
        return mItemArray[index].s2;
    }

    public synchronized Short3 get_s3(int index) {
        if (mItemArray == null) return null;
        return mItemArray[index].s3;
    }

    public synchronized Short4 get_s4(int index) {
        if (mItemArray == null) return null;
        return mItemArray[index].s4;
    }

    public synchronized Int2 get_i2(int index) {
        if (mItemArray == null) return null;
        return mItemArray[index].i2;
    }

    public synchronized Int3 get_i3(int index) {
        if (mItemArray == null) return null;
        return mItemArray[index].i3;
    }

    public synchronized Int4 get_i4(int index) {
        if (mItemArray == null) return null;
        return mItemArray[index].i4;
    }

    public synchronized Float2 get_f2(int index) {
        if (mItemArray == null) return null;
        return mItemArray[index].f2;
    }

    public synchronized Float3 get_f3(int index) {
        if (mItemArray == null) return null;
        return mItemArray[index].f3;
    }

    public synchronized Float4 get_f4(int index) {
        if (mItemArray == null) return null;
        return mItemArray[index].f4;
    }

    public synchronized void copyAll() {
        for (int ct = 0; ct < mItemArray.length; ct++) copyToArray(mItemArray[ct], ct);
        mAllocation.setFromFieldPacker(0, mIOBuffer);
    }

    public synchronized void resize(int newSize) {
        if (mItemArray != null)  {
            int oldSize = mItemArray.length;
            int copySize = Math.min(oldSize, newSize);
            if (newSize == oldSize) return;
            Item ni[] = new Item[newSize];
            System.arraycopy(mItemArray, 0, ni, 0, copySize);
            mItemArray = ni;
        }

        mAllocation.resize(newSize);
        if (mIOBuffer != null) mIOBuffer = new FieldPacker(Item.sizeof * getType().getX()/* count */);
    }

}

