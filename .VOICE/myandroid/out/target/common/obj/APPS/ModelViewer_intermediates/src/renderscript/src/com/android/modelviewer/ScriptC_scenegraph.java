/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * This file is auto-generated. DO NOT MODIFY!
 * The source Renderscript file: frameworks/base/tests/RenderScriptTests/ModelViewer/src/com/android/modelviewer/scenegraph.rs
 */
package com.android.modelviewer;

import android.renderscript.*;
import android.content.res.Resources;

/**
 * @hide
 */
public class ScriptC_scenegraph extends ScriptC {
    // Constructor
    public  ScriptC_scenegraph(RenderScript rs, Resources resources, int id) {
        super(rs, resources, id);
    }

    private final static int mExportVarIdx_gPVBackground = 0;
    private ProgramVertex mExportVar_gPVBackground;
    public void set_gPVBackground(ProgramVertex v) {
        mExportVar_gPVBackground = v;
        setVar(mExportVarIdx_gPVBackground, v);
    }

    public ProgramVertex get_gPVBackground() {
        return mExportVar_gPVBackground;
    }

    private final static int mExportVarIdx_gPFBackground = 1;
    private ProgramFragment mExportVar_gPFBackground;
    public void set_gPFBackground(ProgramFragment v) {
        mExportVar_gPFBackground = v;
        setVar(mExportVarIdx_gPFBackground, v);
    }

    public ProgramFragment get_gPFBackground() {
        return mExportVar_gPFBackground;
    }

    private final static int mExportVarIdx_gTGrid = 2;
    private Allocation mExportVar_gTGrid;
    public void set_gTGrid(Allocation v) {
        mExportVar_gTGrid = v;
        setVar(mExportVarIdx_gTGrid, v);
    }

    public Allocation get_gTGrid() {
        return mExportVar_gTGrid;
    }

    private final static int mExportVarIdx_gTestMesh = 3;
    private Mesh mExportVar_gTestMesh;
    public void set_gTestMesh(Mesh v) {
        mExportVar_gTestMesh = v;
        setVar(mExportVarIdx_gTestMesh, v);
    }

    public Mesh get_gTestMesh() {
        return mExportVar_gTestMesh;
    }

    private final static int mExportVarIdx_gPFSBackground = 4;
    private ProgramStore mExportVar_gPFSBackground;
    public void set_gPFSBackground(ProgramStore v) {
        mExportVar_gPFSBackground = v;
        setVar(mExportVarIdx_gPFSBackground, v);
    }

    public ProgramStore get_gPFSBackground() {
        return mExportVar_gPFSBackground;
    }

    private final static int mExportVarIdx_gRotate = 5;
    private float mExportVar_gRotate;
    public void set_gRotate(float v) {
        mExportVar_gRotate = v;
        setVar(mExportVarIdx_gRotate, v);
    }

    public float get_gRotate() {
        return mExportVar_gRotate;
    }

    private final static int mExportVarIdx_gItalic = 6;
    private Font mExportVar_gItalic;
    public void set_gItalic(Font v) {
        mExportVar_gItalic = v;
        setVar(mExportVarIdx_gItalic, v);
    }

    public Font get_gItalic() {
        return mExportVar_gItalic;
    }

    private final static int mExportVarIdx_gTextAlloc = 7;
    private Allocation mExportVar_gTextAlloc;
    public void set_gTextAlloc(Allocation v) {
        mExportVar_gTextAlloc = v;
        setVar(mExportVarIdx_gTextAlloc, v);
    }

    public Allocation get_gTextAlloc() {
        return mExportVar_gTextAlloc;
    }

    private final static int mExportVarIdx_gTransformRS = 8;
    private Script mExportVar_gTransformRS;
    public void set_gTransformRS(Script v) {
        mExportVar_gTransformRS = v;
        setVar(mExportVarIdx_gTransformRS, v);
    }

    public Script get_gTransformRS() {
        return mExportVar_gTransformRS;
    }

    private final static int mExportVarIdx_gGroup = 9;
    private ScriptField_SgTransform mExportVar_gGroup;
    public void bind_gGroup(ScriptField_SgTransform v) {
        mExportVar_gGroup = v;
        if (v == null) bindAllocation(null, mExportVarIdx_gGroup);
        else bindAllocation(v.getAllocation(), mExportVarIdx_gGroup);
    }

    public ScriptField_SgTransform get_gGroup() {
        return mExportVar_gGroup;
    }

    private final static int mExportVarIdx_gRobot1 = 10;
    private ScriptField_SgTransform mExportVar_gRobot1;
    public void bind_gRobot1(ScriptField_SgTransform v) {
        mExportVar_gRobot1 = v;
        if (v == null) bindAllocation(null, mExportVarIdx_gRobot1);
        else bindAllocation(v.getAllocation(), mExportVarIdx_gRobot1);
    }

    public ScriptField_SgTransform get_gRobot1() {
        return mExportVar_gRobot1;
    }

    private final static int mExportVarIdx_gRobot1Index = 11;
    private int mExportVar_gRobot1Index;
    public void set_gRobot1Index(int v) {
        mExportVar_gRobot1Index = v;
        setVar(mExportVarIdx_gRobot1Index, v);
    }

    public int get_gRobot1Index() {
        return mExportVar_gRobot1Index;
    }

    private final static int mExportVarIdx_gRobot2 = 12;
    private ScriptField_SgTransform mExportVar_gRobot2;
    public void bind_gRobot2(ScriptField_SgTransform v) {
        mExportVar_gRobot2 = v;
        if (v == null) bindAllocation(null, mExportVarIdx_gRobot2);
        else bindAllocation(v.getAllocation(), mExportVarIdx_gRobot2);
    }

    public ScriptField_SgTransform get_gRobot2() {
        return mExportVar_gRobot2;
    }

    private final static int mExportVarIdx_gRobot2Index = 13;
    private int mExportVar_gRobot2Index;
    public void set_gRobot2Index(int v) {
        mExportVar_gRobot2Index = v;
        setVar(mExportVarIdx_gRobot2Index, v);
    }

    public int get_gRobot2Index() {
        return mExportVar_gRobot2Index;
    }

    private final static int mExportVarIdx_gRootNode = 14;
    private ScriptField_SgTransform mExportVar_gRootNode;
    public void bind_gRootNode(ScriptField_SgTransform v) {
        mExportVar_gRootNode = v;
        if (v == null) bindAllocation(null, mExportVarIdx_gRootNode);
        else bindAllocation(v.getAllocation(), mExportVarIdx_gRootNode);
    }

    public ScriptField_SgTransform get_gRootNode() {
        return mExportVar_gRootNode;
    }

}

