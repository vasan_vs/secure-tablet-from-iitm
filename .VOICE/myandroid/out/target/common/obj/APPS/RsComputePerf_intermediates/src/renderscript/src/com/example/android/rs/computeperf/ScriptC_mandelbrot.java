/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * This file is auto-generated. DO NOT MODIFY!
 * The source Renderscript file: frameworks/base/tests/RenderScriptTests/ComputePerf/src/com/example/android/rs/computeperf/mandelbrot.rs
 */
package com.example.android.rs.computeperf;

import android.renderscript.*;
import android.content.res.Resources;

/**
 * @hide
 */
public class ScriptC_mandelbrot extends ScriptC {
    // Constructor
    public  ScriptC_mandelbrot(RenderScript rs, Resources resources, int id) {
        super(rs, resources, id);
        mExportVar_gMaxIteration = 500;
        mExportVar_gDimX = 1024;
        mExportVar_gDimY = 1024;
        __U8_4 = Element.U8_4(rs);
    }

    private Element __U8_4;
    private final static int mExportVarIdx_gMaxIteration = 0;
    private int mExportVar_gMaxIteration;
    public int get_gMaxIteration() {
        return mExportVar_gMaxIteration;
    }

    private final static int mExportVarIdx_gDimX = 1;
    private int mExportVar_gDimX;
    public int get_gDimX() {
        return mExportVar_gDimX;
    }

    private final static int mExportVarIdx_gDimY = 2;
    private int mExportVar_gDimY;
    public int get_gDimY() {
        return mExportVar_gDimY;
    }

    private final static int mExportForEachIdx_root = 0;
    public void forEach_root(Allocation aout) {
        // check aout
        if (!aout.getType().getElement().isCompatible(__U8_4)) {
            throw new RSRuntimeException("Type mismatch with U8_4!");
        }
        forEach(mExportForEachIdx_root, null, aout, null);
    }

}

