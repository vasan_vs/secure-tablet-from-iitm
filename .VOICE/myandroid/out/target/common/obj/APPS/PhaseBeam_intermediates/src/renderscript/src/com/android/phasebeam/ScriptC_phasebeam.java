/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * This file is auto-generated. DO NOT MODIFY!
 * The source Renderscript file: packages/wallpapers/PhaseBeam/src/com/android/phasebeam/phasebeam.rs
 */
package com.android.phasebeam;

import android.renderscript.*;
import android.content.res.Resources;

/**
 * @hide
 */
public class ScriptC_phasebeam extends ScriptC {
    // Constructor
    public  ScriptC_phasebeam(RenderScript rs, Resources resources, int id) {
        super(rs, resources, id);
        mExportVar_xOffset = 0.5f;
    }

    private final static int mExportVarIdx_textureDot = 0;
    private Allocation mExportVar_textureDot;
    public void set_textureDot(Allocation v) {
        mExportVar_textureDot = v;
        setVar(mExportVarIdx_textureDot, v);
    }

    public Allocation get_textureDot() {
        return mExportVar_textureDot;
    }

    private final static int mExportVarIdx_textureBeam = 1;
    private Allocation mExportVar_textureBeam;
    public void set_textureBeam(Allocation v) {
        mExportVar_textureBeam = v;
        setVar(mExportVarIdx_textureBeam, v);
    }

    public Allocation get_textureBeam() {
        return mExportVar_textureBeam;
    }

    private final static int mExportVarIdx_vertBg = 2;
    private ProgramVertex mExportVar_vertBg;
    public void set_vertBg(ProgramVertex v) {
        mExportVar_vertBg = v;
        setVar(mExportVarIdx_vertBg, v);
    }

    public ProgramVertex get_vertBg() {
        return mExportVar_vertBg;
    }

    private final static int mExportVarIdx_fragBg = 3;
    private ProgramFragment mExportVar_fragBg;
    public void set_fragBg(ProgramFragment v) {
        mExportVar_fragBg = v;
        setVar(mExportVarIdx_fragBg, v);
    }

    public ProgramFragment get_fragBg() {
        return mExportVar_fragBg;
    }

    private final static int mExportVarIdx_vertDots = 4;
    private ProgramVertex mExportVar_vertDots;
    public void set_vertDots(ProgramVertex v) {
        mExportVar_vertDots = v;
        setVar(mExportVarIdx_vertDots, v);
    }

    public ProgramVertex get_vertDots() {
        return mExportVar_vertDots;
    }

    private final static int mExportVarIdx_fragDots = 5;
    private ProgramFragment mExportVar_fragDots;
    public void set_fragDots(ProgramFragment v) {
        mExportVar_fragDots = v;
        setVar(mExportVarIdx_fragDots, v);
    }

    public ProgramFragment get_fragDots() {
        return mExportVar_fragDots;
    }

    private final static int mExportVarIdx_vpConstants = 6;
    private ScriptField_VpConsts mExportVar_vpConstants;
    public void bind_vpConstants(ScriptField_VpConsts v) {
        mExportVar_vpConstants = v;
        if (v == null) bindAllocation(null, mExportVarIdx_vpConstants);
        else bindAllocation(v.getAllocation(), mExportVarIdx_vpConstants);
    }

    public ScriptField_VpConsts get_vpConstants() {
        return mExportVar_vpConstants;
    }

    private final static int mExportVarIdx_vertexColors = 7;
    private ScriptField_VertexColor_s mExportVar_vertexColors;
    public void bind_vertexColors(ScriptField_VertexColor_s v) {
        mExportVar_vertexColors = v;
        if (v == null) bindAllocation(null, mExportVarIdx_vertexColors);
        else bindAllocation(v.getAllocation(), mExportVarIdx_vertexColors);
    }

    public ScriptField_VertexColor_s get_vertexColors() {
        return mExportVar_vertexColors;
    }

    private final static int mExportVarIdx_dotParticles = 8;
    private ScriptField_Particle mExportVar_dotParticles;
    public void bind_dotParticles(ScriptField_Particle v) {
        mExportVar_dotParticles = v;
        if (v == null) bindAllocation(null, mExportVarIdx_dotParticles);
        else bindAllocation(v.getAllocation(), mExportVarIdx_dotParticles);
    }

    public ScriptField_Particle get_dotParticles() {
        return mExportVar_dotParticles;
    }

    private final static int mExportVarIdx_beamParticles = 9;
    private ScriptField_Particle mExportVar_beamParticles;
    public void bind_beamParticles(ScriptField_Particle v) {
        mExportVar_beamParticles = v;
        if (v == null) bindAllocation(null, mExportVarIdx_beamParticles);
        else bindAllocation(v.getAllocation(), mExportVarIdx_beamParticles);
    }

    public ScriptField_Particle get_beamParticles() {
        return mExportVar_beamParticles;
    }

    private final static int mExportVarIdx_dotMesh = 10;
    private Mesh mExportVar_dotMesh;
    public void set_dotMesh(Mesh v) {
        mExportVar_dotMesh = v;
        setVar(mExportVarIdx_dotMesh, v);
    }

    public Mesh get_dotMesh() {
        return mExportVar_dotMesh;
    }

    private final static int mExportVarIdx_beamMesh = 11;
    private Mesh mExportVar_beamMesh;
    public void set_beamMesh(Mesh v) {
        mExportVar_beamMesh = v;
        setVar(mExportVarIdx_beamMesh, v);
    }

    public Mesh get_beamMesh() {
        return mExportVar_beamMesh;
    }

    private final static int mExportVarIdx_gBackgroundMesh = 12;
    private Mesh mExportVar_gBackgroundMesh;
    public void set_gBackgroundMesh(Mesh v) {
        mExportVar_gBackgroundMesh = v;
        setVar(mExportVarIdx_gBackgroundMesh, v);
    }

    public Mesh get_gBackgroundMesh() {
        return mExportVar_gBackgroundMesh;
    }

    private final static int mExportVarIdx_densityDPI = 13;
    private float mExportVar_densityDPI;
    public void set_densityDPI(float v) {
        mExportVar_densityDPI = v;
        setVar(mExportVarIdx_densityDPI, v);
    }

    public float get_densityDPI() {
        return mExportVar_densityDPI;
    }

    private final static int mExportVarIdx_xOffset = 14;
    private float mExportVar_xOffset;
    public void set_xOffset(float v) {
        mExportVar_xOffset = v;
        setVar(mExportVarIdx_xOffset, v);
    }

    public float get_xOffset() {
        return mExportVar_xOffset;
    }

    private final static int mExportFuncIdx_positionParticles = 0;
    public void invoke_positionParticles() {
        invoke(mExportFuncIdx_positionParticles);
    }

}

