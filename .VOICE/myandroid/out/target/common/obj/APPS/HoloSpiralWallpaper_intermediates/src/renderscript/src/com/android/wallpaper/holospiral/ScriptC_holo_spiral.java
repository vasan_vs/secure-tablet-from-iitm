/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * This file is auto-generated. DO NOT MODIFY!
 * The source Renderscript file: packages/wallpapers/HoloSpiral/src/com/android/wallpaper/holospiral/holo_spiral.rs
 */
package com.android.wallpaper.holospiral;

import android.renderscript.*;
import android.content.res.Resources;

/**
 * @hide
 */
public class ScriptC_holo_spiral extends ScriptC {
    // Constructor
    public  ScriptC_holo_spiral(RenderScript rs, Resources resources, int id) {
        super(rs, resources, id);
    }

    private final static int mExportVarIdx_gPVBackground = 0;
    private ProgramVertex mExportVar_gPVBackground;
    public void set_gPVBackground(ProgramVertex v) {
        mExportVar_gPVBackground = v;
        setVar(mExportVarIdx_gPVBackground, v);
    }

    public ProgramVertex get_gPVBackground() {
        return mExportVar_gPVBackground;
    }

    private final static int mExportVarIdx_gPVGeometry = 1;
    private ProgramVertex mExportVar_gPVGeometry;
    public void set_gPVGeometry(ProgramVertex v) {
        mExportVar_gPVGeometry = v;
        setVar(mExportVarIdx_gPVGeometry, v);
    }

    public ProgramVertex get_gPVGeometry() {
        return mExportVar_gPVGeometry;
    }

    private final static int mExportVarIdx_gPFBackground = 2;
    private ProgramFragment mExportVar_gPFBackground;
    public void set_gPFBackground(ProgramFragment v) {
        mExportVar_gPFBackground = v;
        setVar(mExportVarIdx_gPFBackground, v);
    }

    public ProgramFragment get_gPFBackground() {
        return mExportVar_gPFBackground;
    }

    private final static int mExportVarIdx_gPFGeometry = 3;
    private ProgramFragment mExportVar_gPFGeometry;
    public void set_gPFGeometry(ProgramFragment v) {
        mExportVar_gPFGeometry = v;
        setVar(mExportVarIdx_gPFGeometry, v);
    }

    public ProgramFragment get_gPFGeometry() {
        return mExportVar_gPFGeometry;
    }

    private final static int mExportVarIdx_gPSBackground = 4;
    private ProgramStore mExportVar_gPSBackground;
    public void set_gPSBackground(ProgramStore v) {
        mExportVar_gPSBackground = v;
        setVar(mExportVarIdx_gPSBackground, v);
    }

    public ProgramStore get_gPSBackground() {
        return mExportVar_gPSBackground;
    }

    private final static int mExportVarIdx_gPSGeometry = 5;
    private ProgramStore mExportVar_gPSGeometry;
    public void set_gPSGeometry(ProgramStore v) {
        mExportVar_gPSGeometry = v;
        setVar(mExportVarIdx_gPSGeometry, v);
    }

    public ProgramStore get_gPSGeometry() {
        return mExportVar_gPSGeometry;
    }

    private final static int mExportVarIdx_gInnerGeometry = 6;
    private Mesh mExportVar_gInnerGeometry;
    public void set_gInnerGeometry(Mesh v) {
        mExportVar_gInnerGeometry = v;
        setVar(mExportVarIdx_gInnerGeometry, v);
    }

    public Mesh get_gInnerGeometry() {
        return mExportVar_gInnerGeometry;
    }

    private final static int mExportVarIdx_gOuterGeometry = 7;
    private Mesh mExportVar_gOuterGeometry;
    public void set_gOuterGeometry(Mesh v) {
        mExportVar_gOuterGeometry = v;
        setVar(mExportVarIdx_gOuterGeometry, v);
    }

    public Mesh get_gOuterGeometry() {
        return mExportVar_gOuterGeometry;
    }

    private final static int mExportVarIdx_gBackgroundMesh = 8;
    private Mesh mExportVar_gBackgroundMesh;
    public void set_gBackgroundMesh(Mesh v) {
        mExportVar_gBackgroundMesh = v;
        setVar(mExportVarIdx_gBackgroundMesh, v);
    }

    public Mesh get_gBackgroundMesh() {
        return mExportVar_gBackgroundMesh;
    }

    private final static int mExportVarIdx_gPointTexture = 9;
    private Allocation mExportVar_gPointTexture;
    public void set_gPointTexture(Allocation v) {
        mExportVar_gPointTexture = v;
        setVar(mExportVarIdx_gPointTexture, v);
    }

    public Allocation get_gPointTexture() {
        return mExportVar_gPointTexture;
    }

    private final static int mExportVarIdx_gXOffset = 10;
    private float mExportVar_gXOffset;
    public void set_gXOffset(float v) {
        mExportVar_gXOffset = v;
        setVar(mExportVarIdx_gXOffset, v);
    }

    public float get_gXOffset() {
        return mExportVar_gXOffset;
    }

    private final static int mExportVarIdx_gNearPlane = 11;
    private float mExportVar_gNearPlane;
    public void set_gNearPlane(float v) {
        mExportVar_gNearPlane = v;
        setVar(mExportVarIdx_gNearPlane, v);
    }

    public float get_gNearPlane() {
        return mExportVar_gNearPlane;
    }

    private final static int mExportVarIdx_gFarPlane = 12;
    private float mExportVar_gFarPlane;
    public void set_gFarPlane(float v) {
        mExportVar_gFarPlane = v;
        setVar(mExportVarIdx_gFarPlane, v);
    }

    public float get_gFarPlane() {
        return mExportVar_gFarPlane;
    }

    private final static int mExportVarIdx_gVSConstants = 13;
    private ScriptField_VertexShaderConstants_s mExportVar_gVSConstants;
    public void bind_gVSConstants(ScriptField_VertexShaderConstants_s v) {
        mExportVar_gVSConstants = v;
        if (v == null) bindAllocation(null, mExportVarIdx_gVSConstants);
        else bindAllocation(v.getAllocation(), mExportVarIdx_gVSConstants);
    }

    public ScriptField_VertexShaderConstants_s get_gVSConstants() {
        return mExportVar_gVSConstants;
    }

    private final static int mExportVarIdx_VertexColor_s_dummy = 14;
    private ScriptField_VertexColor_s mExportVar_VertexColor_s_dummy;
    public void bind_VertexColor_s_dummy(ScriptField_VertexColor_s v) {
        mExportVar_VertexColor_s_dummy = v;
        if (v == null) bindAllocation(null, mExportVarIdx_VertexColor_s_dummy);
        else bindAllocation(v.getAllocation(), mExportVarIdx_VertexColor_s_dummy);
    }

    public ScriptField_VertexColor_s get_VertexColor_s_dummy() {
        return mExportVar_VertexColor_s_dummy;
    }

    private final static int mExportFuncIdx_resize = 0;
    public void invoke_resize(float width, float height) {
        FieldPacker resize_fp = new FieldPacker(8);
        resize_fp.addF32(width);
        resize_fp.addF32(height);
        invoke(mExportFuncIdx_resize, resize_fp);
    }

}

