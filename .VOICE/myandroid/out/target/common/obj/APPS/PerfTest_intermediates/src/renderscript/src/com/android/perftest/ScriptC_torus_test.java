/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * This file is auto-generated. DO NOT MODIFY!
 * The source Renderscript file: frameworks/base/tests/RenderScriptTests/PerfTest/src/com/android/perftest/torus_test.rs
 */
package com.android.perftest;

import android.renderscript.*;
import android.content.res.Resources;

/**
 * @hide
 */
public class ScriptC_torus_test extends ScriptC {
    // Constructor
    public  ScriptC_torus_test(RenderScript rs, Resources resources, int id) {
        super(rs, resources, id);
        mExportVar_gLight0Rotation = 0f;
        mExportVar_gLight1Rotation = 0f;
    }

    private final static int mExportVarIdx_gProgVertex = 0;
    private ProgramVertex mExportVar_gProgVertex;
    public void set_gProgVertex(ProgramVertex v) {
        mExportVar_gProgVertex = v;
        setVar(mExportVarIdx_gProgVertex, v);
    }

    public ProgramVertex get_gProgVertex() {
        return mExportVar_gProgVertex;
    }

    private final static int mExportVarIdx_gProgFragmentColor = 1;
    private ProgramFragment mExportVar_gProgFragmentColor;
    public void set_gProgFragmentColor(ProgramFragment v) {
        mExportVar_gProgFragmentColor = v;
        setVar(mExportVarIdx_gProgFragmentColor, v);
    }

    public ProgramFragment get_gProgFragmentColor() {
        return mExportVar_gProgFragmentColor;
    }

    private final static int mExportVarIdx_gProgFragmentTexture = 2;
    private ProgramFragment mExportVar_gProgFragmentTexture;
    public void set_gProgFragmentTexture(ProgramFragment v) {
        mExportVar_gProgFragmentTexture = v;
        setVar(mExportVarIdx_gProgFragmentTexture, v);
    }

    public ProgramFragment get_gProgFragmentTexture() {
        return mExportVar_gProgFragmentTexture;
    }

    private final static int mExportVarIdx_gProgStoreBlendNoneDepth = 3;
    private ProgramStore mExportVar_gProgStoreBlendNoneDepth;
    public void set_gProgStoreBlendNoneDepth(ProgramStore v) {
        mExportVar_gProgStoreBlendNoneDepth = v;
        setVar(mExportVarIdx_gProgStoreBlendNoneDepth, v);
    }

    public ProgramStore get_gProgStoreBlendNoneDepth() {
        return mExportVar_gProgStoreBlendNoneDepth;
    }

    private final static int mExportVarIdx_gTorusMesh = 4;
    private Mesh mExportVar_gTorusMesh;
    public void set_gTorusMesh(Mesh v) {
        mExportVar_gTorusMesh = v;
        setVar(mExportVarIdx_gTorusMesh, v);
    }

    public Mesh get_gTorusMesh() {
        return mExportVar_gTorusMesh;
    }

    private final static int mExportVarIdx_gCullBack = 5;
    private ProgramRaster mExportVar_gCullBack;
    public void set_gCullBack(ProgramRaster v) {
        mExportVar_gCullBack = v;
        setVar(mExportVarIdx_gCullBack, v);
    }

    public ProgramRaster get_gCullBack() {
        return mExportVar_gCullBack;
    }

    private final static int mExportVarIdx_gCullFront = 6;
    private ProgramRaster mExportVar_gCullFront;
    public void set_gCullFront(ProgramRaster v) {
        mExportVar_gCullFront = v;
        setVar(mExportVarIdx_gCullFront, v);
    }

    public ProgramRaster get_gCullFront() {
        return mExportVar_gCullFront;
    }

    private final static int mExportVarIdx_gVSConstants = 7;
    private ScriptField_VertexShaderConstants_s mExportVar_gVSConstants;
    public void bind_gVSConstants(ScriptField_VertexShaderConstants_s v) {
        mExportVar_gVSConstants = v;
        if (v == null) bindAllocation(null, mExportVarIdx_gVSConstants);
        else bindAllocation(v.getAllocation(), mExportVarIdx_gVSConstants);
    }

    public ScriptField_VertexShaderConstants_s get_gVSConstants() {
        return mExportVar_gVSConstants;
    }

    private final static int mExportVarIdx_gFSConstants = 8;
    private ScriptField_FragentShaderConstants_s mExportVar_gFSConstants;
    public void bind_gFSConstants(ScriptField_FragentShaderConstants_s v) {
        mExportVar_gFSConstants = v;
        if (v == null) bindAllocation(null, mExportVarIdx_gFSConstants);
        else bindAllocation(v.getAllocation(), mExportVarIdx_gFSConstants);
    }

    public ScriptField_FragentShaderConstants_s get_gFSConstants() {
        return mExportVar_gFSConstants;
    }

    private final static int mExportVarIdx_gVSConstPixel = 9;
    private ScriptField_VertexShaderConstants3_s mExportVar_gVSConstPixel;
    public void bind_gVSConstPixel(ScriptField_VertexShaderConstants3_s v) {
        mExportVar_gVSConstPixel = v;
        if (v == null) bindAllocation(null, mExportVarIdx_gVSConstPixel);
        else bindAllocation(v.getAllocation(), mExportVarIdx_gVSConstPixel);
    }

    public ScriptField_VertexShaderConstants3_s get_gVSConstPixel() {
        return mExportVar_gVSConstPixel;
    }

    private final static int mExportVarIdx_gFSConstPixel = 10;
    private ScriptField_FragentShaderConstants3_s mExportVar_gFSConstPixel;
    public void bind_gFSConstPixel(ScriptField_FragentShaderConstants3_s v) {
        mExportVar_gFSConstPixel = v;
        if (v == null) bindAllocation(null, mExportVarIdx_gFSConstPixel);
        else bindAllocation(v.getAllocation(), mExportVarIdx_gFSConstPixel);
    }

    public ScriptField_FragentShaderConstants3_s get_gFSConstPixel() {
        return mExportVar_gFSConstPixel;
    }

    private final static int mExportVarIdx_gProgVertexCustom = 11;
    private ProgramVertex mExportVar_gProgVertexCustom;
    public void set_gProgVertexCustom(ProgramVertex v) {
        mExportVar_gProgVertexCustom = v;
        setVar(mExportVarIdx_gProgVertexCustom, v);
    }

    public ProgramVertex get_gProgVertexCustom() {
        return mExportVar_gProgVertexCustom;
    }

    private final static int mExportVarIdx_gProgFragmentCustom = 12;
    private ProgramFragment mExportVar_gProgFragmentCustom;
    public void set_gProgFragmentCustom(ProgramFragment v) {
        mExportVar_gProgFragmentCustom = v;
        setVar(mExportVarIdx_gProgFragmentCustom, v);
    }

    public ProgramFragment get_gProgFragmentCustom() {
        return mExportVar_gProgFragmentCustom;
    }

    private final static int mExportVarIdx_gLinearClamp = 13;
    private Sampler mExportVar_gLinearClamp;
    public void set_gLinearClamp(Sampler v) {
        mExportVar_gLinearClamp = v;
        setVar(mExportVarIdx_gLinearClamp, v);
    }

    public Sampler get_gLinearClamp() {
        return mExportVar_gLinearClamp;
    }

    private final static int mExportVarIdx_gTexTorus = 14;
    private Allocation mExportVar_gTexTorus;
    public void set_gTexTorus(Allocation v) {
        mExportVar_gTexTorus = v;
        setVar(mExportVarIdx_gTexTorus, v);
    }

    public Allocation get_gTexTorus() {
        return mExportVar_gTexTorus;
    }

    private final static int mExportVarIdx_gProgVertexPixelLight = 15;
    private ProgramVertex mExportVar_gProgVertexPixelLight;
    public void set_gProgVertexPixelLight(ProgramVertex v) {
        mExportVar_gProgVertexPixelLight = v;
        setVar(mExportVarIdx_gProgVertexPixelLight, v);
    }

    public ProgramVertex get_gProgVertexPixelLight() {
        return mExportVar_gProgVertexPixelLight;
    }

    private final static int mExportVarIdx_gProgVertexPixelLightMove = 16;
    private ProgramVertex mExportVar_gProgVertexPixelLightMove;
    public void set_gProgVertexPixelLightMove(ProgramVertex v) {
        mExportVar_gProgVertexPixelLightMove = v;
        setVar(mExportVarIdx_gProgVertexPixelLightMove, v);
    }

    public ProgramVertex get_gProgVertexPixelLightMove() {
        return mExportVar_gProgVertexPixelLightMove;
    }

    private final static int mExportVarIdx_gProgFragmentPixelLight = 17;
    private ProgramFragment mExportVar_gProgFragmentPixelLight;
    public void set_gProgFragmentPixelLight(ProgramFragment v) {
        mExportVar_gProgFragmentPixelLight = v;
        setVar(mExportVarIdx_gProgFragmentPixelLight, v);
    }

    public ProgramFragment get_gProgFragmentPixelLight() {
        return mExportVar_gProgFragmentPixelLight;
    }

    private final static int mExportVarIdx_gLight0Rotation = 18;
    private float mExportVar_gLight0Rotation;
    public void set_gLight0Rotation(float v) {
        mExportVar_gLight0Rotation = v;
        setVar(mExportVarIdx_gLight0Rotation, v);
    }

    public float get_gLight0Rotation() {
        return mExportVar_gLight0Rotation;
    }

    private final static int mExportVarIdx_gLight1Rotation = 19;
    private float mExportVar_gLight1Rotation;
    public void set_gLight1Rotation(float v) {
        mExportVar_gLight1Rotation = v;
        setVar(mExportVarIdx_gLight1Rotation, v);
    }

    public float get_gLight1Rotation() {
        return mExportVar_gLight1Rotation;
    }

    private final static int mExportForEachIdx_root = 0;
    public void forEach_root(Allocation ain, Allocation aout) {
        // Verify dimensions
        Type tIn = ain.getType();
        Type tOut = aout.getType();
        if ((tIn.getCount() != tOut.getCount()) ||
            (tIn.getX() != tOut.getX()) ||
            (tIn.getY() != tOut.getY()) ||
            (tIn.getZ() != tOut.getZ()) ||
            (tIn.hasFaces() != tOut.hasFaces()) ||
            (tIn.hasMipmaps() != tOut.hasMipmaps())) {
            throw new RSRuntimeException("Dimension mismatch between input and output parameters!");
        }
        forEach(mExportForEachIdx_root, ain, aout, null);
    }

}

