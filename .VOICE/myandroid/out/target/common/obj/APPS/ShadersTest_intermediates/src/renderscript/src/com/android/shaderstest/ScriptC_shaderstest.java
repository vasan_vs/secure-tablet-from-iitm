/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * This file is auto-generated. DO NOT MODIFY!
 * The source Renderscript file: frameworks/base/tests/RenderScriptTests/ShadersTest/src/com/android/shaderstest/shaderstest.rs
 */
package com.android.shaderstest;

import android.renderscript.*;
import android.content.res.Resources;

/**
 * @hide
 */
public class ScriptC_shaderstest extends ScriptC {
    // Constructor
    public  ScriptC_shaderstest(RenderScript rs, Resources resources, int id) {
        super(rs, resources, id);
    }

    private final static int mExportVarIdx_gPVBackground = 0;
    private ProgramVertex mExportVar_gPVBackground;
    public void set_gPVBackground(ProgramVertex v) {
        mExportVar_gPVBackground = v;
        setVar(mExportVarIdx_gPVBackground, v);
    }

    public ProgramVertex get_gPVBackground() {
        return mExportVar_gPVBackground;
    }

    private final static int mExportVarIdx_gPFBackground = 1;
    private ProgramFragment mExportVar_gPFBackground;
    public void set_gPFBackground(ProgramFragment v) {
        mExportVar_gPFBackground = v;
        setVar(mExportVarIdx_gPFBackground, v);
    }

    public ProgramFragment get_gPFBackground() {
        return mExportVar_gPFBackground;
    }

    private final static int mExportVarIdx_gFSVignetteConstants = 2;
    private ScriptField_VignetteConstants_s mExportVar_gFSVignetteConstants;
    public void bind_gFSVignetteConstants(ScriptField_VignetteConstants_s v) {
        mExportVar_gFSVignetteConstants = v;
        if (v == null) bindAllocation(null, mExportVarIdx_gFSVignetteConstants);
        else bindAllocation(v.getAllocation(), mExportVarIdx_gFSVignetteConstants);
    }

    public ScriptField_VignetteConstants_s get_gFSVignetteConstants() {
        return mExportVar_gFSVignetteConstants;
    }

    private final static int mExportVarIdx_gPFVignette = 3;
    private ProgramFragment mExportVar_gPFVignette;
    public void set_gPFVignette(ProgramFragment v) {
        mExportVar_gPFVignette = v;
        setVar(mExportVarIdx_gPFVignette, v);
    }

    public ProgramFragment get_gPFVignette() {
        return mExportVar_gPFVignette;
    }

    private final static int mExportVarIdx_gTMesh = 4;
    private Allocation mExportVar_gTMesh;
    public void set_gTMesh(Allocation v) {
        mExportVar_gTMesh = v;
        setVar(mExportVarIdx_gTMesh, v);
    }

    public Allocation get_gTMesh() {
        return mExportVar_gTMesh;
    }

    private final static int mExportVarIdx_gLinear = 5;
    private Sampler mExportVar_gLinear;
    public void set_gLinear(Sampler v) {
        mExportVar_gLinear = v;
        setVar(mExportVarIdx_gLinear, v);
    }

    public Sampler get_gLinear() {
        return mExportVar_gLinear;
    }

    private final static int mExportVarIdx_gNearest = 6;
    private Sampler mExportVar_gNearest;
    public void set_gNearest(Sampler v) {
        mExportVar_gNearest = v;
        setVar(mExportVarIdx_gNearest, v);
    }

    public Sampler get_gNearest() {
        return mExportVar_gNearest;
    }

    private final static int mExportVarIdx_gPFSBackground = 7;
    private ProgramStore mExportVar_gPFSBackground;
    public void set_gPFSBackground(ProgramStore v) {
        mExportVar_gPFSBackground = v;
        setVar(mExportVarIdx_gPFSBackground, v);
    }

    public ProgramStore get_gPFSBackground() {
        return mExportVar_gPFSBackground;
    }

    private final static int mExportVarIdx_gScreenDepth = 8;
    private Allocation mExportVar_gScreenDepth;
    public void set_gScreenDepth(Allocation v) {
        mExportVar_gScreenDepth = v;
        setVar(mExportVarIdx_gScreenDepth, v);
    }

    public Allocation get_gScreenDepth() {
        return mExportVar_gScreenDepth;
    }

    private final static int mExportVarIdx_gScreen = 9;
    private Allocation mExportVar_gScreen;
    public void set_gScreen(Allocation v) {
        mExportVar_gScreen = v;
        setVar(mExportVarIdx_gScreen, v);
    }

    public Allocation get_gScreen() {
        return mExportVar_gScreen;
    }

    private final static int mExportVarIdx_gMeshes = 10;
    private ScriptField_MeshInfo mExportVar_gMeshes;
    public void bind_gMeshes(ScriptField_MeshInfo v) {
        mExportVar_gMeshes = v;
        if (v == null) bindAllocation(null, mExportVarIdx_gMeshes);
        else bindAllocation(v.getAllocation(), mExportVarIdx_gMeshes);
    }

    public ScriptField_MeshInfo get_gMeshes() {
        return mExportVar_gMeshes;
    }

    private final static int mExportFuncIdx_onActionDown = 0;
    public void invoke_onActionDown(float x, float y) {
        FieldPacker onActionDown_fp = new FieldPacker(8);
        onActionDown_fp.addF32(x);
        onActionDown_fp.addF32(y);
        invoke(mExportFuncIdx_onActionDown, onActionDown_fp);
    }

    private final static int mExportFuncIdx_onActionScale = 1;
    public void invoke_onActionScale(float scale) {
        FieldPacker onActionScale_fp = new FieldPacker(4);
        onActionScale_fp.addF32(scale);
        invoke(mExportFuncIdx_onActionScale, onActionScale_fp);
    }

    private final static int mExportFuncIdx_onActionMove = 2;
    public void invoke_onActionMove(float x, float y) {
        FieldPacker onActionMove_fp = new FieldPacker(8);
        onActionMove_fp.addF32(x);
        onActionMove_fp.addF32(y);
        invoke(mExportFuncIdx_onActionMove, onActionMove_fp);
    }

    private final static int mExportFuncIdx_updateMeshInfo = 3;
    public void invoke_updateMeshInfo() {
        invoke(mExportFuncIdx_updateMeshInfo);
    }

}

