/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * This file is auto-generated. DO NOT MODIFY!
 * The source Renderscript file: development/samples/RenderScript/MiscSamples/src/com/example/android/rs/miscsamples/rsrenderstates.rs
 */
package com.example.android.rs.miscsamples;

import android.renderscript.*;
import android.content.res.Resources;

/**
 * @hide
 */
public class ScriptField_VertexShaderConstants2_s extends android.renderscript.Script.FieldBase {
    static public class Item {
        public static final int sizeof = 256;

        Matrix4f[] model;
        Matrix4f proj;
        Float4[] light_Posision;
        float[] light_Diffuse;
        float[] light_Specular;
        float[] light_CosinePower;

        Item() {
            model = new Matrix4f[2];
            for (int $ct = 0; $ct < 2; $ct++) {
                model[$ct] = new Matrix4f();
            }

            proj = new Matrix4f();
            light_Posision = new Float4[2];
            for (int $ct = 0; $ct < 2; $ct++) {
                light_Posision[$ct] = new Float4();
            }

            light_Diffuse = new float[2];
            light_Specular = new float[2];
            light_CosinePower = new float[2];
        }

    }

    private Item mItemArray[];
    private FieldPacker mIOBuffer;
    public static Element createElement(RenderScript rs) {
        Element.Builder eb = new Element.Builder(rs);
        eb.add(Element.MATRIX_4X4(rs), "model", 2);
        eb.add(Element.MATRIX_4X4(rs), "proj");
        eb.add(Element.F32_4(rs), "light_Posision", 2);
        eb.add(Element.F32(rs), "light_Diffuse", 2);
        eb.add(Element.F32(rs), "light_Specular", 2);
        eb.add(Element.F32(rs), "light_CosinePower", 2);
        eb.add(Element.U32(rs), "#padding_1");
        eb.add(Element.U32(rs), "#padding_2");
        return eb.create();
    }

    public  ScriptField_VertexShaderConstants2_s(RenderScript rs, int count) {
        mItemArray = null;
        mIOBuffer = null;
        mElement = createElement(rs);
        init(rs, count);
    }

    public  ScriptField_VertexShaderConstants2_s(RenderScript rs, int count, int usages) {
        mItemArray = null;
        mIOBuffer = null;
        mElement = createElement(rs);
        init(rs, count, usages);
    }

    private void copyToArrayLocal(Item i, FieldPacker fp) {
        for (int ct2 = 0; ct2 < 2; ct2++) {
            fp.addMatrix(i.model[ct2]);
        }

        fp.addMatrix(i.proj);
        for (int ct2 = 0; ct2 < 2; ct2++) {
            fp.addF32(i.light_Posision[ct2]);
        }

        for (int ct2 = 0; ct2 < 2; ct2++) {
            fp.addF32(i.light_Diffuse[ct2]);
        }

        for (int ct2 = 0; ct2 < 2; ct2++) {
            fp.addF32(i.light_Specular[ct2]);
        }

        for (int ct2 = 0; ct2 < 2; ct2++) {
            fp.addF32(i.light_CosinePower[ct2]);
        }

        fp.skip(8);
    }

    private void copyToArray(Item i, int index) {
        if (mIOBuffer == null) mIOBuffer = new FieldPacker(Item.sizeof * getType().getX()/* count */);
        mIOBuffer.reset(index * Item.sizeof);
        copyToArrayLocal(i, mIOBuffer);
    }

    public synchronized void set(Item i, int index, boolean copyNow) {
        if (mItemArray == null) mItemArray = new Item[getType().getX() /* count */];
        mItemArray[index] = i;
        if (copyNow)  {
            copyToArray(i, index);
            FieldPacker fp = new FieldPacker(Item.sizeof);
            copyToArrayLocal(i, fp);
            mAllocation.setFromFieldPacker(index, fp);
        }

    }

    public synchronized Item get(int index) {
        if (mItemArray == null) return null;
        return mItemArray[index];
    }

    public synchronized void set_model(int index, Matrix4f[] v, boolean copyNow) {
        if (mIOBuffer == null) mIOBuffer = new FieldPacker(Item.sizeof * getType().getX()/* count */);
        if (mItemArray == null) mItemArray = new Item[getType().getX() /* count */];
        if (mItemArray[index] == null) mItemArray[index] = new Item();
        mItemArray[index].model = v;
        if (copyNow)  {
            mIOBuffer.reset(index * Item.sizeof);
            for (int ct1 = 0; ct1 < 2; ct1++) {
                mIOBuffer.addMatrix(v[ct1]);
            }

            FieldPacker fp = new FieldPacker(128);
            for (int ct1 = 0; ct1 < 2; ct1++) {
                fp.addMatrix(v[ct1]);
            }

            mAllocation.setFromFieldPacker(index, 0, fp);
        }

    }

    public synchronized void set_proj(int index, Matrix4f v, boolean copyNow) {
        if (mIOBuffer == null) mIOBuffer = new FieldPacker(Item.sizeof * getType().getX()/* count */);
        if (mItemArray == null) mItemArray = new Item[getType().getX() /* count */];
        if (mItemArray[index] == null) mItemArray[index] = new Item();
        mItemArray[index].proj = v;
        if (copyNow)  {
            mIOBuffer.reset(index * Item.sizeof + 128);
            mIOBuffer.addMatrix(v);
            FieldPacker fp = new FieldPacker(64);
            fp.addMatrix(v);
            mAllocation.setFromFieldPacker(index, 1, fp);
        }

    }

    public synchronized void set_light_Posision(int index, Float4[] v, boolean copyNow) {
        if (mIOBuffer == null) mIOBuffer = new FieldPacker(Item.sizeof * getType().getX()/* count */);
        if (mItemArray == null) mItemArray = new Item[getType().getX() /* count */];
        if (mItemArray[index] == null) mItemArray[index] = new Item();
        mItemArray[index].light_Posision = v;
        if (copyNow)  {
            mIOBuffer.reset(index * Item.sizeof + 192);
            for (int ct1 = 0; ct1 < 2; ct1++) {
                mIOBuffer.addF32(v[ct1]);
            }

            FieldPacker fp = new FieldPacker(32);
            for (int ct1 = 0; ct1 < 2; ct1++) {
                fp.addF32(v[ct1]);
            }

            mAllocation.setFromFieldPacker(index, 2, fp);
        }

    }

    public synchronized void set_light_Diffuse(int index, float[] v, boolean copyNow) {
        if (mIOBuffer == null) mIOBuffer = new FieldPacker(Item.sizeof * getType().getX()/* count */);
        if (mItemArray == null) mItemArray = new Item[getType().getX() /* count */];
        if (mItemArray[index] == null) mItemArray[index] = new Item();
        mItemArray[index].light_Diffuse = v;
        if (copyNow)  {
            mIOBuffer.reset(index * Item.sizeof + 224);
            for (int ct1 = 0; ct1 < 2; ct1++) {
                mIOBuffer.addF32(v[ct1]);
            }

            FieldPacker fp = new FieldPacker(8);
            for (int ct1 = 0; ct1 < 2; ct1++) {
                fp.addF32(v[ct1]);
            }

            mAllocation.setFromFieldPacker(index, 3, fp);
        }

    }

    public synchronized void set_light_Specular(int index, float[] v, boolean copyNow) {
        if (mIOBuffer == null) mIOBuffer = new FieldPacker(Item.sizeof * getType().getX()/* count */);
        if (mItemArray == null) mItemArray = new Item[getType().getX() /* count */];
        if (mItemArray[index] == null) mItemArray[index] = new Item();
        mItemArray[index].light_Specular = v;
        if (copyNow)  {
            mIOBuffer.reset(index * Item.sizeof + 232);
            for (int ct1 = 0; ct1 < 2; ct1++) {
                mIOBuffer.addF32(v[ct1]);
            }

            FieldPacker fp = new FieldPacker(8);
            for (int ct1 = 0; ct1 < 2; ct1++) {
                fp.addF32(v[ct1]);
            }

            mAllocation.setFromFieldPacker(index, 4, fp);
        }

    }

    public synchronized void set_light_CosinePower(int index, float[] v, boolean copyNow) {
        if (mIOBuffer == null) mIOBuffer = new FieldPacker(Item.sizeof * getType().getX()/* count */);
        if (mItemArray == null) mItemArray = new Item[getType().getX() /* count */];
        if (mItemArray[index] == null) mItemArray[index] = new Item();
        mItemArray[index].light_CosinePower = v;
        if (copyNow)  {
            mIOBuffer.reset(index * Item.sizeof + 240);
            for (int ct1 = 0; ct1 < 2; ct1++) {
                mIOBuffer.addF32(v[ct1]);
            }

            FieldPacker fp = new FieldPacker(8);
            for (int ct1 = 0; ct1 < 2; ct1++) {
                fp.addF32(v[ct1]);
            }

            mAllocation.setFromFieldPacker(index, 5, fp);
        }

    }

    public synchronized Matrix4f[] get_model(int index) {
        if (mItemArray == null) return null;
        return mItemArray[index].model;
    }

    public synchronized Matrix4f get_proj(int index) {
        if (mItemArray == null) return null;
        return mItemArray[index].proj;
    }

    public synchronized Float4[] get_light_Posision(int index) {
        if (mItemArray == null) return null;
        return mItemArray[index].light_Posision;
    }

    public synchronized float[] get_light_Diffuse(int index) {
        if (mItemArray == null) return null;
        return mItemArray[index].light_Diffuse;
    }

    public synchronized float[] get_light_Specular(int index) {
        if (mItemArray == null) return null;
        return mItemArray[index].light_Specular;
    }

    public synchronized float[] get_light_CosinePower(int index) {
        if (mItemArray == null) return null;
        return mItemArray[index].light_CosinePower;
    }

    public synchronized void copyAll() {
        for (int ct = 0; ct < mItemArray.length; ct++) copyToArray(mItemArray[ct], ct);
        mAllocation.setFromFieldPacker(0, mIOBuffer);
    }

    public synchronized void resize(int newSize) {
        if (mItemArray != null)  {
            int oldSize = mItemArray.length;
            int copySize = Math.min(oldSize, newSize);
            if (newSize == oldSize) return;
            Item ni[] = new Item[newSize];
            System.arraycopy(mItemArray, 0, ni, 0, copySize);
            mItemArray = ni;
        }

        mAllocation.resize(newSize);
        if (mIOBuffer != null) mIOBuffer = new FieldPacker(Item.sizeof * getType().getX()/* count */);
    }

}

