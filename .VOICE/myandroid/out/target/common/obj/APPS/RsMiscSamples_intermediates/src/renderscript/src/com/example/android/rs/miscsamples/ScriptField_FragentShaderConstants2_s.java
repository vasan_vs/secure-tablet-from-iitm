/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * This file is auto-generated. DO NOT MODIFY!
 * The source Renderscript file: development/samples/RenderScript/MiscSamples/src/com/example/android/rs/miscsamples/rsrenderstates.rs
 */
package com.example.android.rs.miscsamples;

import android.renderscript.*;
import android.content.res.Resources;

/**
 * @hide
 */
public class ScriptField_FragentShaderConstants2_s extends android.renderscript.Script.FieldBase {
    static public class Item {
        public static final int sizeof = 64;

        Float4[] light_DiffuseColor;
        Float4[] light_SpecularColor;

        Item() {
            light_DiffuseColor = new Float4[2];
            for (int $ct = 0; $ct < 2; $ct++) {
                light_DiffuseColor[$ct] = new Float4();
            }

            light_SpecularColor = new Float4[2];
            for (int $ct = 0; $ct < 2; $ct++) {
                light_SpecularColor[$ct] = new Float4();
            }

        }

    }

    private Item mItemArray[];
    private FieldPacker mIOBuffer;
    public static Element createElement(RenderScript rs) {
        Element.Builder eb = new Element.Builder(rs);
        eb.add(Element.F32_4(rs), "light_DiffuseColor", 2);
        eb.add(Element.F32_4(rs), "light_SpecularColor", 2);
        return eb.create();
    }

    public  ScriptField_FragentShaderConstants2_s(RenderScript rs, int count) {
        mItemArray = null;
        mIOBuffer = null;
        mElement = createElement(rs);
        init(rs, count);
    }

    public  ScriptField_FragentShaderConstants2_s(RenderScript rs, int count, int usages) {
        mItemArray = null;
        mIOBuffer = null;
        mElement = createElement(rs);
        init(rs, count, usages);
    }

    private void copyToArrayLocal(Item i, FieldPacker fp) {
        for (int ct2 = 0; ct2 < 2; ct2++) {
            fp.addF32(i.light_DiffuseColor[ct2]);
        }

        for (int ct2 = 0; ct2 < 2; ct2++) {
            fp.addF32(i.light_SpecularColor[ct2]);
        }

    }

    private void copyToArray(Item i, int index) {
        if (mIOBuffer == null) mIOBuffer = new FieldPacker(Item.sizeof * getType().getX()/* count */);
        mIOBuffer.reset(index * Item.sizeof);
        copyToArrayLocal(i, mIOBuffer);
    }

    public synchronized void set(Item i, int index, boolean copyNow) {
        if (mItemArray == null) mItemArray = new Item[getType().getX() /* count */];
        mItemArray[index] = i;
        if (copyNow)  {
            copyToArray(i, index);
            FieldPacker fp = new FieldPacker(Item.sizeof);
            copyToArrayLocal(i, fp);
            mAllocation.setFromFieldPacker(index, fp);
        }

    }

    public synchronized Item get(int index) {
        if (mItemArray == null) return null;
        return mItemArray[index];
    }

    public synchronized void set_light_DiffuseColor(int index, Float4[] v, boolean copyNow) {
        if (mIOBuffer == null) mIOBuffer = new FieldPacker(Item.sizeof * getType().getX()/* count */);
        if (mItemArray == null) mItemArray = new Item[getType().getX() /* count */];
        if (mItemArray[index] == null) mItemArray[index] = new Item();
        mItemArray[index].light_DiffuseColor = v;
        if (copyNow)  {
            mIOBuffer.reset(index * Item.sizeof);
            for (int ct1 = 0; ct1 < 2; ct1++) {
                mIOBuffer.addF32(v[ct1]);
            }

            FieldPacker fp = new FieldPacker(32);
            for (int ct1 = 0; ct1 < 2; ct1++) {
                fp.addF32(v[ct1]);
            }

            mAllocation.setFromFieldPacker(index, 0, fp);
        }

    }

    public synchronized void set_light_SpecularColor(int index, Float4[] v, boolean copyNow) {
        if (mIOBuffer == null) mIOBuffer = new FieldPacker(Item.sizeof * getType().getX()/* count */);
        if (mItemArray == null) mItemArray = new Item[getType().getX() /* count */];
        if (mItemArray[index] == null) mItemArray[index] = new Item();
        mItemArray[index].light_SpecularColor = v;
        if (copyNow)  {
            mIOBuffer.reset(index * Item.sizeof + 32);
            for (int ct1 = 0; ct1 < 2; ct1++) {
                mIOBuffer.addF32(v[ct1]);
            }

            FieldPacker fp = new FieldPacker(32);
            for (int ct1 = 0; ct1 < 2; ct1++) {
                fp.addF32(v[ct1]);
            }

            mAllocation.setFromFieldPacker(index, 1, fp);
        }

    }

    public synchronized Float4[] get_light_DiffuseColor(int index) {
        if (mItemArray == null) return null;
        return mItemArray[index].light_DiffuseColor;
    }

    public synchronized Float4[] get_light_SpecularColor(int index) {
        if (mItemArray == null) return null;
        return mItemArray[index].light_SpecularColor;
    }

    public synchronized void copyAll() {
        for (int ct = 0; ct < mItemArray.length; ct++) copyToArray(mItemArray[ct], ct);
        mAllocation.setFromFieldPacker(0, mIOBuffer);
    }

    public synchronized void resize(int newSize) {
        if (mItemArray != null)  {
            int oldSize = mItemArray.length;
            int copySize = Math.min(oldSize, newSize);
            if (newSize == oldSize) return;
            Item ni[] = new Item[newSize];
            System.arraycopy(mItemArray, 0, ni, 0, copySize);
            mItemArray = ni;
        }

        mAllocation.resize(newSize);
        if (mIOBuffer != null) mIOBuffer = new FieldPacker(Item.sizeof * getType().getX()/* count */);
    }

}

