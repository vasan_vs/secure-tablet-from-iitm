package android.hardware.usb;
public class UsbAccessory
  implements android.os.Parcelable
{
UsbAccessory() { throw new RuntimeException("Stub!"); }
public  java.lang.String getManufacturer() { throw new RuntimeException("Stub!"); }
public  java.lang.String getModel() { throw new RuntimeException("Stub!"); }
public  java.lang.String getDescription() { throw new RuntimeException("Stub!"); }
public  java.lang.String getVersion() { throw new RuntimeException("Stub!"); }
public  java.lang.String getUri() { throw new RuntimeException("Stub!"); }
public  java.lang.String getSerial() { throw new RuntimeException("Stub!"); }
public  boolean equals(java.lang.Object obj) { throw new RuntimeException("Stub!"); }
public  int hashCode() { throw new RuntimeException("Stub!"); }
public  java.lang.String toString() { throw new RuntimeException("Stub!"); }
public  int describeContents() { throw new RuntimeException("Stub!"); }
public  void writeToParcel(android.os.Parcel parcel, int flags) { throw new RuntimeException("Stub!"); }
public static final android.os.Parcelable.Creator<android.hardware.usb.UsbAccessory> CREATOR;
static { CREATOR = null; }
}
