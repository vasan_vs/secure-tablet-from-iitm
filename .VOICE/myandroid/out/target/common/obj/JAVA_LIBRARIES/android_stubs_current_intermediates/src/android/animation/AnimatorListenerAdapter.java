package android.animation;
public abstract class AnimatorListenerAdapter
  implements android.animation.Animator.AnimatorListener
{
public  AnimatorListenerAdapter() { throw new RuntimeException("Stub!"); }
public  void onAnimationCancel(android.animation.Animator animation) { throw new RuntimeException("Stub!"); }
public  void onAnimationEnd(android.animation.Animator animation) { throw new RuntimeException("Stub!"); }
public  void onAnimationRepeat(android.animation.Animator animation) { throw new RuntimeException("Stub!"); }
public  void onAnimationStart(android.animation.Animator animation) { throw new RuntimeException("Stub!"); }
}
