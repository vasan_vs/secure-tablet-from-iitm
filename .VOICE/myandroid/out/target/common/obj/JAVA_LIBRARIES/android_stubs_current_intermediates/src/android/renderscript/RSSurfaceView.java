package android.renderscript;
public class RSSurfaceView
  extends android.view.SurfaceView
  implements android.view.SurfaceHolder.Callback
{
public  RSSurfaceView(android.content.Context context) { super((android.content.Context)null,(android.util.AttributeSet)null,0); throw new RuntimeException("Stub!"); }
public  RSSurfaceView(android.content.Context context, android.util.AttributeSet attrs) { super((android.content.Context)null,(android.util.AttributeSet)null,0); throw new RuntimeException("Stub!"); }
public  void surfaceCreated(android.view.SurfaceHolder holder) { throw new RuntimeException("Stub!"); }
public  void surfaceDestroyed(android.view.SurfaceHolder holder) { throw new RuntimeException("Stub!"); }
public  void surfaceChanged(android.view.SurfaceHolder holder, int format, int w, int h) { throw new RuntimeException("Stub!"); }
public  void pause() { throw new RuntimeException("Stub!"); }
public  void resume() { throw new RuntimeException("Stub!"); }
public  android.renderscript.RenderScriptGL createRenderScriptGL(android.renderscript.RenderScriptGL.SurfaceConfig sc) { throw new RuntimeException("Stub!"); }
public  void destroyRenderScriptGL() { throw new RuntimeException("Stub!"); }
public  void setRenderScriptGL(android.renderscript.RenderScriptGL rs) { throw new RuntimeException("Stub!"); }
public  android.renderscript.RenderScriptGL getRenderScriptGL() { throw new RuntimeException("Stub!"); }
}
