package android.renderscript;
public class FieldPacker
{
public  FieldPacker(int len) { throw new RuntimeException("Stub!"); }
public  void align(int v) { throw new RuntimeException("Stub!"); }
public  void reset() { throw new RuntimeException("Stub!"); }
public  void reset(int i) { throw new RuntimeException("Stub!"); }
public  void skip(int i) { throw new RuntimeException("Stub!"); }
public  void addI8(byte v) { throw new RuntimeException("Stub!"); }
public  void addI16(short v) { throw new RuntimeException("Stub!"); }
public  void addI32(int v) { throw new RuntimeException("Stub!"); }
public  void addI64(long v) { throw new RuntimeException("Stub!"); }
public  void addU8(short v) { throw new RuntimeException("Stub!"); }
public  void addU16(int v) { throw new RuntimeException("Stub!"); }
public  void addU32(long v) { throw new RuntimeException("Stub!"); }
public  void addU64(long v) { throw new RuntimeException("Stub!"); }
public  void addF32(float v) { throw new RuntimeException("Stub!"); }
public  void addF64(double v) { throw new RuntimeException("Stub!"); }
public  void addObj(android.renderscript.BaseObj obj) { throw new RuntimeException("Stub!"); }
public  void addF32(android.renderscript.Float2 v) { throw new RuntimeException("Stub!"); }
public  void addF32(android.renderscript.Float3 v) { throw new RuntimeException("Stub!"); }
public  void addF32(android.renderscript.Float4 v) { throw new RuntimeException("Stub!"); }
public  void addF64(android.renderscript.Double2 v) { throw new RuntimeException("Stub!"); }
public  void addF64(android.renderscript.Double3 v) { throw new RuntimeException("Stub!"); }
public  void addF64(android.renderscript.Double4 v) { throw new RuntimeException("Stub!"); }
public  void addI8(android.renderscript.Byte2 v) { throw new RuntimeException("Stub!"); }
public  void addI8(android.renderscript.Byte3 v) { throw new RuntimeException("Stub!"); }
public  void addI8(android.renderscript.Byte4 v) { throw new RuntimeException("Stub!"); }
public  void addU8(android.renderscript.Short2 v) { throw new RuntimeException("Stub!"); }
public  void addU8(android.renderscript.Short3 v) { throw new RuntimeException("Stub!"); }
public  void addU8(android.renderscript.Short4 v) { throw new RuntimeException("Stub!"); }
public  void addI16(android.renderscript.Short2 v) { throw new RuntimeException("Stub!"); }
public  void addI16(android.renderscript.Short3 v) { throw new RuntimeException("Stub!"); }
public  void addI16(android.renderscript.Short4 v) { throw new RuntimeException("Stub!"); }
public  void addU16(android.renderscript.Int2 v) { throw new RuntimeException("Stub!"); }
public  void addU16(android.renderscript.Int3 v) { throw new RuntimeException("Stub!"); }
public  void addU16(android.renderscript.Int4 v) { throw new RuntimeException("Stub!"); }
public  void addI32(android.renderscript.Int2 v) { throw new RuntimeException("Stub!"); }
public  void addI32(android.renderscript.Int3 v) { throw new RuntimeException("Stub!"); }
public  void addI32(android.renderscript.Int4 v) { throw new RuntimeException("Stub!"); }
public  void addU32(android.renderscript.Long2 v) { throw new RuntimeException("Stub!"); }
public  void addU32(android.renderscript.Long3 v) { throw new RuntimeException("Stub!"); }
public  void addU32(android.renderscript.Long4 v) { throw new RuntimeException("Stub!"); }
public  void addI64(android.renderscript.Long2 v) { throw new RuntimeException("Stub!"); }
public  void addI64(android.renderscript.Long3 v) { throw new RuntimeException("Stub!"); }
public  void addI64(android.renderscript.Long4 v) { throw new RuntimeException("Stub!"); }
public  void addU64(android.renderscript.Long2 v) { throw new RuntimeException("Stub!"); }
public  void addU64(android.renderscript.Long3 v) { throw new RuntimeException("Stub!"); }
public  void addU64(android.renderscript.Long4 v) { throw new RuntimeException("Stub!"); }
public  void addMatrix(android.renderscript.Matrix4f v) { throw new RuntimeException("Stub!"); }
public  void addMatrix(android.renderscript.Matrix3f v) { throw new RuntimeException("Stub!"); }
public  void addMatrix(android.renderscript.Matrix2f v) { throw new RuntimeException("Stub!"); }
public  void addBoolean(boolean v) { throw new RuntimeException("Stub!"); }
public final  byte[] getData() { throw new RuntimeException("Stub!"); }
}
