package android.graphics;
public class Point
  implements android.os.Parcelable
{
public  Point() { throw new RuntimeException("Stub!"); }
public  Point(int x, int y) { throw new RuntimeException("Stub!"); }
public  Point(android.graphics.Point src) { throw new RuntimeException("Stub!"); }
public  void set(int x, int y) { throw new RuntimeException("Stub!"); }
public final  void negate() { throw new RuntimeException("Stub!"); }
public final  void offset(int dx, int dy) { throw new RuntimeException("Stub!"); }
public final  boolean equals(int x, int y) { throw new RuntimeException("Stub!"); }
public  boolean equals(java.lang.Object o) { throw new RuntimeException("Stub!"); }
public  int hashCode() { throw new RuntimeException("Stub!"); }
public  java.lang.String toString() { throw new RuntimeException("Stub!"); }
public  int describeContents() { throw new RuntimeException("Stub!"); }
public  void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }
public  void readFromParcel(android.os.Parcel in) { throw new RuntimeException("Stub!"); }
public int x;
public int y;
public static final android.os.Parcelable.Creator<android.graphics.Point> CREATOR;
static { CREATOR = null; }
}
