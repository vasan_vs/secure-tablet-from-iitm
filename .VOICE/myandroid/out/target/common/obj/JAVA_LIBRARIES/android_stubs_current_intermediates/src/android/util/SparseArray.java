package android.util;
public class SparseArray<E>
  implements java.lang.Cloneable
{
public  SparseArray() { throw new RuntimeException("Stub!"); }
public  SparseArray(int initialCapacity) { throw new RuntimeException("Stub!"); }
@java.lang.SuppressWarnings(value={"unchecked"})
public  android.util.SparseArray<E> clone() { throw new RuntimeException("Stub!"); }
public  E get(int key) { throw new RuntimeException("Stub!"); }
@java.lang.SuppressWarnings(value={"unchecked"})
public  E get(int key, E valueIfKeyNotFound) { throw new RuntimeException("Stub!"); }
public  void delete(int key) { throw new RuntimeException("Stub!"); }
public  void remove(int key) { throw new RuntimeException("Stub!"); }
public  void removeAt(int index) { throw new RuntimeException("Stub!"); }
public  void put(int key, E value) { throw new RuntimeException("Stub!"); }
public  int size() { throw new RuntimeException("Stub!"); }
public  int keyAt(int index) { throw new RuntimeException("Stub!"); }
@java.lang.SuppressWarnings(value={"unchecked"})
public  E valueAt(int index) { throw new RuntimeException("Stub!"); }
public  void setValueAt(int index, E value) { throw new RuntimeException("Stub!"); }
public  int indexOfKey(int key) { throw new RuntimeException("Stub!"); }
public  int indexOfValue(E value) { throw new RuntimeException("Stub!"); }
public  void clear() { throw new RuntimeException("Stub!"); }
public  void append(int key, E value) { throw new RuntimeException("Stub!"); }
}
