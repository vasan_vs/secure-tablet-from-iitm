//autogenerated by util.build.BuildDalvikSuite, do not change
package dot.junit.opcodes.iget_char;
import dot.junit.opcodes.iget_char.d.*;
import dot.junit.*;
public class Main_testVFE3 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        try {
            new T_iget_char_13().run();
            fail("expected a NoSuchFieldError exception");
        } catch (NoSuchFieldError t) {
            // expected
        }
    }
}
