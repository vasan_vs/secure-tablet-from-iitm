//autogenerated by util.build.BuildDalvikSuite, do not change
package dot.junit.opcodes.add_double_2addr;
import dot.junit.opcodes.add_double_2addr.d.*;
import dot.junit.*;
public class Main_testN1 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_add_double_2addr_1 t = new T_add_double_2addr_1();
        assertEquals(5.84d, t.run(2.7d, 3.14d));
    }
}
