//autogenerated by util.build.BuildDalvikSuite, do not change
package dot.junit.opcodes.sget_byte;
import dot.junit.opcodes.sget_byte.d.*;
import dot.junit.*;
public class Main_testVFE7 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        //@uses dot.junit.opcodes.sget_byte.d.T_sget_byte_12
        //@uses dot.junit.opcodes.sget_byte.d.T_sget_byte_1
        try {
            new T_sget_byte_12().run();
            fail("expected a verification exception");
        } catch (IllegalAccessError t) {
        }
    }
}
