//autogenerated by util.build.BuildDalvikSuite, do not change
package dot.junit.opcodes.iput;
import dot.junit.opcodes.iput.d.*;
import dot.junit.*;
public class Main_testN2 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_iput_19 t = new T_iput_19();
        assertEquals(0.0f, t.st_f1);
        t.run();
        assertEquals(3.14f, t.st_f1);
    }
}
