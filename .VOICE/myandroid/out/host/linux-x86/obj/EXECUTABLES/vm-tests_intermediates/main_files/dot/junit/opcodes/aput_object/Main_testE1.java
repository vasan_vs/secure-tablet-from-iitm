//autogenerated by util.build.BuildDalvikSuite, do not change
package dot.junit.opcodes.aput_object;
import dot.junit.opcodes.aput_object.d.*;
import dot.junit.*;
public class Main_testE1 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_aput_object_1 t = new T_aput_object_1();
        String[] arr = new String[2];
        try {
            t.run(arr, arr.length, "abc");
            fail("expected ArrayIndexOutOfBoundsException");
        } catch (ArrayIndexOutOfBoundsException aie) {
            // expected
        }
    }
}
