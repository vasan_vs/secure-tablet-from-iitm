//autogenerated by util.build.BuildDalvikSuite, do not change
package dot.junit.opcodes.const_wide_16;
import dot.junit.opcodes.const_wide_16.d.*;
import dot.junit.*;
public class Main_testN1 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_const_wide_16_1 t = new T_const_wide_16_1();
         long a = 10000l;
         long b = 10000l;
        assertEquals(a + b, t.run());
    }
}
