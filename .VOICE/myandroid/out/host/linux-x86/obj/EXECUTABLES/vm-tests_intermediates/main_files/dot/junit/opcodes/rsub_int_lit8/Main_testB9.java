//autogenerated by util.build.BuildDalvikSuite, do not change
package dot.junit.opcodes.rsub_int_lit8;
import dot.junit.opcodes.rsub_int_lit8.d.*;
import dot.junit.*;
public class Main_testB9 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_rsub_int_lit8_6 t = new T_rsub_int_lit8_6();
        assertEquals(Integer.MIN_VALUE, t.run(Integer.MAX_VALUE));
        assertEquals(Byte.MIN_VALUE, t.run(Byte.MAX_VALUE));
    }
}
