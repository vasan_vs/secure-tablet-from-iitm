//autogenerated by util.build.BuildDalvikSuite, do not change
package dot.junit.opcodes.iget_wide;
import dot.junit.opcodes.iget_wide.d.*;
import dot.junit.*;
public class Main_testN1 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_iget_wide_1 t = new T_iget_wide_1();
        assertEquals(12345679890123l, t.run());
    }
}
