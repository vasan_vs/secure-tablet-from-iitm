//autogenerated by util.build.BuildDalvikSuite, do not change
package dot.junit.opcodes.iget;
import dot.junit.opcodes.iget.d.*;
import dot.junit.*;
public class Main_testN3 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        //@uses dot.junit.opcodes.iget.d.T_iget_1
        //@uses dot.junit.opcodes.iget.d.T_iget_11
        T_iget_11 t = new T_iget_11();
        assertEquals(10, t.run());
    }
}
