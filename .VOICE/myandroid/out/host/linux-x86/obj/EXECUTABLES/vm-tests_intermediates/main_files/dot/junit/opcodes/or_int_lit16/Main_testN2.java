//autogenerated by util.build.BuildDalvikSuite, do not change
package dot.junit.opcodes.or_int_lit16;
import dot.junit.opcodes.or_int_lit16.d.*;
import dot.junit.*;
public class Main_testN2 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_or_int_lit16_2 t = new T_or_int_lit16_2();
        assertEquals(0x7ff7, t.run(0x5ff5));
    }
}
