//autogenerated by util.build.BuildDalvikSuite, do not change
package dot.junit.opcodes.cmp_long;
import dot.junit.opcodes.cmp_long.d.*;
import dot.junit.*;
public class Main_testB3 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_cmp_long_1 t = new T_cmp_long_1();
        assertEquals(1, t.run(1l, 0l));
    }
}
