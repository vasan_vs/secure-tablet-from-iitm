//Autogenerated code by util.build.BuildDalvikSuite; do not edit.
package dot.junit.opcodes.div_double;
import dot.junit.opcodes.div_double.d.*;
import dot.junit.*;
public class Main_testN4 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {

        T_div_double_1 t = new T_div_double_1();
        assertEquals(-1.162962962962963d, t.run(-3.14d, 2.7d));
    }
}
