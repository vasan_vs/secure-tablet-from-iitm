//Autogenerated code by util.build.BuildDalvikSuite; do not edit.
package dot.junit.opcodes.and_int;
import dot.junit.opcodes.and_int.d.*;
import dot.junit.*;
public class Main_testVFE4 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        try {
            Class.forName("dot.junit.opcodes.and_int.d.T_and_int_4");
            fail("expected a verification exception");
        } catch (Throwable t) {
            DxUtil.checkVerifyException(t);
        }
    }
}
