//Autogenerated code by util.build.BuildDalvikSuite; do not edit.
package dot.junit.opcodes.iget;
import dot.junit.opcodes.iget.d.*;
import dot.junit.*;
public class Main_testVFE9 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        try {
            Class.forName("dot.junit.opcodes.iget.d.T_iget_15");
            fail("expected a verification exception");
        } catch (Throwable t) {
            DxUtil.checkVerifyException(t);
        }
    }
}
