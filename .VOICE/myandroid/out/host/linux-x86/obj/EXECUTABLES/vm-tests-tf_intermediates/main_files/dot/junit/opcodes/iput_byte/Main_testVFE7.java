//Autogenerated code by util.build.BuildDalvikSuite; do not edit.
package dot.junit.opcodes.iput_byte;
import dot.junit.opcodes.iput_byte.d.*;
import dot.junit.*;
public class Main_testVFE7 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        try {
            Class.forName("dot.junit.opcodes.iput_byte.d.T_iput_byte_18");
            fail("expected a verification exception");
        } catch (Throwable t) {
            DxUtil.checkVerifyException(t);
        }
    }
}
