//Autogenerated code by util.build.BuildDalvikSuite; do not edit.
package dot.junit.opcodes.rem_float;
import dot.junit.opcodes.rem_float.d.*;
import dot.junit.*;
public class Main_testB3 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_rem_float_1 t = new T_rem_float_1();
        assertEquals(Float.NaN, t.run(Float.POSITIVE_INFINITY, -2.7f));
    }
}
