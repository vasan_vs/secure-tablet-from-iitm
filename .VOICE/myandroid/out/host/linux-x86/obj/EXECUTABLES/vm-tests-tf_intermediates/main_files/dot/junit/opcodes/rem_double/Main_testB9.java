//Autogenerated code by util.build.BuildDalvikSuite; do not edit.
package dot.junit.opcodes.rem_double;
import dot.junit.opcodes.rem_double.d.*;
import dot.junit.*;
public class Main_testB9 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_rem_double_1 t = new T_rem_double_1();

        assertEquals(1.543905285031139E-10d, t.run(Double.MAX_VALUE, -1E-9d));
    }
}
