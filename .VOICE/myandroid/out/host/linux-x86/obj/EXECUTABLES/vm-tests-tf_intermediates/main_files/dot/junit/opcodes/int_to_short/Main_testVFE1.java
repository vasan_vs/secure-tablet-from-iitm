//Autogenerated code by util.build.BuildDalvikSuite; do not edit.
package dot.junit.opcodes.int_to_short;
import dot.junit.opcodes.int_to_short.d.*;
import dot.junit.*;
public class Main_testVFE1 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        try {
            Class.forName("dot.junit.opcodes.int_to_short.d.T_int_to_short_2");
            fail("expected a verification exception");
        } catch (Throwable t) {
            DxUtil.checkVerifyException(t);
        }
    }
}
