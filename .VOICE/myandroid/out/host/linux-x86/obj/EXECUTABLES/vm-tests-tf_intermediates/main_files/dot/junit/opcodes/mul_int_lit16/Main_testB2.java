//Autogenerated code by util.build.BuildDalvikSuite; do not edit.
package dot.junit.opcodes.mul_int_lit16;
import dot.junit.opcodes.mul_int_lit16.d.*;
import dot.junit.*;
public class Main_testB2 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_mul_int_lit16_4 t = new T_mul_int_lit16_4();
        assertEquals(0, t.run(Short.MAX_VALUE));
    }
}
