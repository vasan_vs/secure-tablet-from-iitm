//Autogenerated code by util.build.BuildDalvikSuite; do not edit.
package dot.junit.opcodes.sub_int;
import dot.junit.opcodes.sub_int.d.*;
import dot.junit.*;
public class Main_testB8 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_sub_int_1 t = new T_sub_int_1();
        assertEquals(-2147483647, t.run(1, Integer.MIN_VALUE));
    }
}
