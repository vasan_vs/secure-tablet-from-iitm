
static int max98089_dai1_hw_params(struct snd_pcm_substream *substream,
				   struct snd_pcm_hw_params *params,
				   struct snd_soc_dai *dai)
{
	struct snd_soc_codec *codec = dai->codec;
	struct max98089_priv *max98089 = snd_soc_codec_get_drvdata(codec);
	struct max98089_cdata *cdata;
	unsigned long long ni;
	unsigned int rate;
	u8 regval;

	cdata = &max98089->dai[0];

	rate = params_rate(params);

	switch (params_format(params)) {
	case SNDRV_PCM_FORMAT_S16_LE:
		snd_soc_update_bits(codec, M98089_REG_14_DAI1_FORMAT,
			M98089_DAI_WS, 0);
		break;
	case SNDRV_PCM_FORMAT_S24_LE:
		snd_soc_update_bits(codec, M98089_REG_14_DAI1_FORMAT,
			M98089_DAI_WS, M98089_DAI_WS);
		break;
	default:
		return -EINVAL;
	}

	snd_soc_update_bits(codec, M98089_REG_51_PWR_SYS, M98089_SHDNRUN, 0);

	if (rate_value(rate, &regval))
		return -EINVAL;

	snd_soc_update_bits(codec, M98089_REG_11_DAI1_CLKMODE,
		M98089_CLKMODE_MASK, regval); 
	cdata->rate = rate; 

	/* Configure NI when operating as master */ 
	
	if (snd_soc_read(codec, M98089_REG_14_DAI1_FORMAT)
		& M98089_DAI_MAS) {
		if (max98089->sysclk == 0) {
			dev_err(codec->dev, "Invalid system clock frequency\n");
			return -EINVAL;
		}
		ni = 65536ULL * (rate < 50000 ? 96ULL : 48ULL)
				* (unsigned long long int)rate;
		do_div(ni, (unsigned long long int)max98089->sysclk);
		snd_soc_write(codec, M98089_REG_12_DAI1_CLKCFG_HI,
			(ni >> 8) & 0x7F);
		snd_soc_write(codec, M98089_REG_13_DAI1_CLKCFG_LO,
			ni & 0xFF);
	}

	/* Update sample rate mode */
	if (rate < 50000)
		snd_soc_update_bits(codec, M98089_REG_18_DAI1_FILTERS,
			M98089_DAI_DHF, 0);
	else
		snd_soc_update_bits(codec, M98089_REG_18_DAI1_FILTERS,
			M98089_DAI_DHF, 0x47);

	/* Hardcoding the values corressponding to widgets. */
	snd_soc_write (codec, M98089_REG_39_LVL_HP_L, 0x1c);
	snd_soc_write (codec, M98089_REG_3A_LVL_HP_R, 0x1c); 
	snd_soc_write (codec, M98089_REG_4D_PWR_EN_OUT, 0xc3);
	snd_soc_write (codec, M98089_REG_18_DAI1_FILTERS,0x44);
	 snd_soc_write (codec, M98089_REG_2F_LVL_DAI1_PLAY,0x60);
	snd_soc_update_bits(codec, M98089_REG_51_PWR_SYS, M98089_SHDNRUN,
		M98089_SHDNRUN);
       
	 snd_soc_write(codec, M98089_REG_4E_BIAS_CNTL, 0xF0);
        snd_soc_write(codec, M98089_REG_4B_CFG_JACKDET, 0x80);
        snd_soc_write(codec, M98089_REG_4E_BIAS_CNTL, 0xF1);

        printk(KERN_ALERT "****************** %s: JACK Status setting = %x\n", __func__, snd_soc_read(codec, 0x02));

//snd_soc_write (codec, M98089_REG_49_CFG_LEVEL, 0x2);
//snd_soc_write (codec, 0x30, 0x00);

#define DIG_MIC
	int i1;

#ifdef DIG_MIC
//	for(i1 = 3; i1 < 0x52; i1++)		
//		snd_soc_write(codec, i1, 0x0);
	snd_soc_write(codec, M98089_REG_10_SYS_CLK, 0x10);
	snd_soc_write(codec, M98089_REG_11_DAI1_CLKMODE, 0x1c);
	snd_soc_write(codec, M98089_REG_12_DAI1_CLKCFG_HI, 0x00);
	snd_soc_write(codec, M98089_REG_13_DAI1_CLKCFG_LO, 0x00);
	snd_soc_write(codec, M98089_REG_14_DAI1_FORMAT, 0xa6);/*MAS=1 (for now) WCI1=1 (for PCM) BCI1=0 (FOR PCM) DLY1=0 (FOR PCM) TDM1=1 (FOR PCM) FSW1=0 WS1=0*/
//	snd_soc_write(codec, M98089_REG_1C_DAI2_FORMAT, 0x24);
	snd_soc_write(codec, M98089_REG_15_DAI1_CLOCK, 0x46);

	snd_soc_write(codec, M98089_REG_16_DAI1_IOCFG, 0x43);
	snd_soc_write(codec, M98089_REG_1E_DAI2_IOCFG, 0x83);


	snd_soc_write(codec, M98089_REG_17_DAI1_TDM, 0x00); /*SLOTL1=0 AND SLOTR1=O (FOR PCM)*/
/*	snd_soc_write(codec, M98089_REG_22_MIX_DAC, 0x84);
	snd_soc_write(codec, M98089_REG_35_LVL_MIC1, 0x60);
	snd_soc_write(codec, M98089_REG_48_CFG_MIC, 0x30);
	snd_soc_write(codec, M98089_REG_4D_PWR_EN_OUT, 0xc3);
	snd_soc_write(codec, M98089_REG_4E_BIAS_CNTL, 0x30);
*/
	snd_soc_write(codec, M98089_REG_4F_DAC_BIAS1, 0x00);
/*
	snd_soc_write(codec, M98089_REG_50_DAC_BIAS2, 0x0f);
	snd_soc_write(codec, M98089_REG_39_LVL_HP_L, 0x1c);
	snd_soc_write(codec, M98089_REG_3A_LVL_HP_R, 0x1c);
	snd_soc_write(codec, M98089_REG_51_PWR_SYS, 0x80);
*/
#endif
	printk(KERN_ALERT "Register settings in dai1 hw_params*********\n");
//	for(i1 = 0; i1 < 0x52; i1++)			/*COMMENTED AS THEY ARE CLEAR ON READ REGISTERS.*/
//	  printk(KERN_ALERT "Register %x : %x\n", i1, snd_soc_read(codec, i1));

	return 0;
}



static int max98089_dai2_hw_params(struct snd_pcm_substream *substream,
				   struct snd_pcm_hw_params *params,
				   struct snd_soc_dai *dai)
{
	struct snd_soc_codec *codec = dai->codec;
	struct max98089_priv *max98089 = snd_soc_codec_get_drvdata(codec);
	struct max98089_cdata *cdata;
	unsigned long long ni;
	unsigned int rate;
	u8 regval;

	cdata = &max98089->dai[1];

	/* Gets the frequency of the sudio file. */
	rate = params_rate(params);

	/* Sets WS2. Right now its only 16 because TDM2 = 0. Incase of TDM2 = 1, it can be either 16 or 24. */
	switch (params_format(params)) {
	case SNDRV_PCM_FORMAT_S16_LE:
		snd_soc_update_bits(codec, M98089_REG_1C_DAI2_FORMAT,
			M98089_DAI_WS, 0);
		break;
	case SNDRV_PCM_FORMAT_S24_LE:
		snd_soc_update_bits(codec, M98089_REG_1C_DAI2_FORMAT,
			M98089_DAI_WS, M98089_DAI_WS);
		break;
	default:
		return -EINVAL;
	}

	/* The codec is switched off before further changes are made. */
	snd_soc_update_bits(codec, M98089_REG_51_PWR_SYS, M98089_SHDNRUN, 0);

	/* regval is set according to the rate. It is the register value for SR2 used by ALC. */
	if (rate_value(rate, &regval))
		return -EINVAL;

	/* Updates SR2 wih regval. */
	snd_soc_update_bits(codec, M98089_REG_19_DAI2_CLKMODE,
		M98089_CLKMODE_MASK, regval);
	cdata->rate = rate;

	/* Irrelevant in slave mode. */
	/* Configure NI when operating as master */
	if (snd_soc_read(codec, M98089_REG_1C_DAI2_FORMAT)
		& M98089_DAI_MAS) {
		if (max98089->sysclk == 0) {
			dev_err(codec->dev, "Invalid system clock frequency\n");
			return -EINVAL;
		}
		ni = 65536ULL * (rate < 50000 ? 96ULL : 48ULL)
				* (unsigned long long int)rate;
		do_div(ni, (unsigned long long int)max98089->sysclk);
		snd_soc_write(codec, M98089_REG_1A_DAI2_CLKCFG_HI,
			(ni >> 8) & 0x7F);
		snd_soc_write(codec, M98089_REG_1B_DAI2_CLKCFG_LO,
			ni & 0xFF);
	}
	
	/* Update sample rate mode */
	if (rate < 50000)
		snd_soc_update_bits(codec, M98089_REG_20_DAI2_FILTERS,
			M98089_DAI_DHF, 0);
	else
		snd_soc_update_bits(codec, M98089_REG_20_DAI2_FILTERS,
			M98089_DAI_DHF, M98089_DAI_DHF);

	/* Hardcoding the values corressponding to widgets. */
	snd_soc_write (codec, M98089_REG_39_LVL_HP_L, 0x1c);
	snd_soc_write (codec, M98089_REG_3A_LVL_HP_R, 0x1c); 
	snd_soc_write (codec, M98089_REG_4D_PWR_EN_OUT, 0xc3); 

	/* Switch on the codec. */
	snd_soc_update_bits(codec, M98089_REG_51_PWR_SYS, M98089_SHDNRUN,
		M98089_SHDNRUN);
        snd_soc_write(codec, M98089_REG_4E_BIAS_CNTL, 0xF0);
        snd_soc_write(codec, M98089_REG_4B_CFG_JACKDET, 0x80);
        snd_soc_write(codec, M98089_REG_4E_BIAS_CNTL, 0xF1);

       printk(KERN_ALERT "****************** %s: JACK Status setting = %x\n", __func__, snd_soc_read(codec, 0x02));


	int i1;
       
	snd_soc_write(codec, M98089_REG_10_SYS_CLK, 0x10);
	snd_soc_write(codec, M98089_REG_11_DAI1_CLKMODE, 0x1c);
	snd_soc_write(codec, M98089_REG_12_DAI1_CLKCFG_HI, 0x00);
	snd_soc_write(codec, M98089_REG_13_DAI1_CLKCFG_LO, 0x00);
	snd_soc_write(codec, M98089_REG_14_DAI1_FORMAT, 0xa6);/*MAS=1 (for now) WCI1=1 (for PCM) BCI1=0 (FOR PCM) DLY1=0 (FOR PCM) TDM1=1 (FOR PCM) FSW1=0 WS1=0*/
//	snd_soc_write(codec, M98089_REG_1C_DAI2_FORMAT, 0x24);
	snd_soc_write(codec, M98089_REG_15_DAI1_CLOCK, 0x46);
	printk(KERN_ALERT "Register settings in dai2 hw_params\n");
//	for(i1 = 0; i1 < 0x52; i1++ )
//		printk(KERN_ALERT "Register %x : %x\n", i1, snd_soc_read(codec, i1));

	return 0;
}



static int max98089_dai1_set_fmt(struct snd_soc_dai *codec_dai,
				 unsigned int fmt)
{
	struct snd_soc_codec *codec = codec_dai->codec;
	struct max98089_priv *max98089 = snd_soc_codec_get_drvdata(codec);
	struct max98089_cdata *cdata;
	u8 reg15val;
	u8 reg14val = 0;

	cdata = &max98089->dai[0];

	if (fmt != cdata->fmt) {
		cdata->fmt = fmt;

		switch (fmt & SND_SOC_DAIFMT_MASTER_MASK) {
		case SND_SOC_DAIFMT_CBS_CFS:
			/* Slave mode PLL */
			snd_soc_write(codec, M98089_REG_12_DAI1_CLKCFG_HI,
				0x80);
			snd_soc_write(codec, M98089_REG_13_DAI1_CLKCFG_LO,
				0x00);
			break;
		case SND_SOC_DAIFMT_CBM_CFM:
			/* Set to master mode */
			reg14val |= M98089_DAI_MAS;
			break;
		case SND_SOC_DAIFMT_CBS_CFM:
		case SND_SOC_DAIFMT_CBM_CFS:
		default:
			dev_err(codec->dev, "Clock mode unsupported");
			return -EINVAL;
		}

		switch (fmt & SND_SOC_DAIFMT_FORMAT_MASK) {
		case SND_SOC_DAIFMT_I2S:
			reg14val |= M98089_DAI_DLY;
			break;
		case SND_SOC_DAIFMT_LEFT_J:
			break;
		default:
			return -EINVAL;
		}

		switch (fmt & SND_SOC_DAIFMT_INV_MASK) {
		case SND_SOC_DAIFMT_NB_NF:
			break;
		case SND_SOC_DAIFMT_NB_IF:
			reg14val |= M98089_DAI_WCI;
			break;
		case SND_SOC_DAIFMT_IB_NF:
			reg14val |= M98089_DAI_BCI;
			break;
		case SND_SOC_DAIFMT_IB_IF:
			reg14val |= M98089_DAI_BCI|M98089_DAI_WCI;
			break;
		default:
			return -EINVAL;
		}

		snd_soc_update_bits(codec, M98089_REG_14_DAI1_FORMAT,
			M98089_DAI_MAS | M98089_DAI_DLY | M98089_DAI_BCI |
			M98089_DAI_WCI, reg14val);

		reg15val = M98089_DAI_BSEL64;
		#ifndef TEST_MIC
		if (max98089->digmic)
		#endif
			reg15val |= M98089_DAI_OSR64;
		printk(KERN_INFO "reg_15 val being updated as %d \n", reg15val);
		snd_soc_write(codec, M98089_REG_15_DAI1_CLOCK, reg15val);
	}

	/* Hardcoding the values corressponding to widgets. */
	snd_soc_write (codec, M98089_REG_39_LVL_HP_L, 0x1c);
	snd_soc_write (codec, M98089_REG_3A_LVL_HP_R, 0x1c); 
	snd_soc_write (codec, M98089_REG_4D_PWR_EN_OUT, 0xc3); 
        snd_soc_write (codec, M98089_REG_18_DAI1_FILTERS,0x47);
  snd_soc_write (codec, M98089_REG_2F_LVL_DAI1_PLAY,0x60);
//snd_soc_write (codec, M98089_REG_49_CFG_LEVEL, 0x2);
//snd_soc_write (codec, 0x30, 0x00);

        snd_soc_write(codec, M98089_REG_4E_BIAS_CNTL, 0xF0);
        snd_soc_write(codec, M98089_REG_4B_CFG_JACKDET, 0x80);
        snd_soc_write(codec, M98089_REG_4E_BIAS_CNTL, 0xF1);

       printk(KERN_ALERT "****************** %s: JACK Status setting = %x\n", __func__, snd_soc_read(codec, 0x02));

	int i1;
#define DIG_MIC       
#ifdef DIG_MIC
	snd_soc_write(codec, M98089_REG_10_SYS_CLK, 0x10);
	snd_soc_write(codec, M98089_REG_11_DAI1_CLKMODE, 0x1c);
	snd_soc_write(codec, M98089_REG_12_DAI1_CLKCFG_HI, 0x00);
	snd_soc_write(codec, M98089_REG_13_DAI1_CLKCFG_LO, 0x00);
	snd_soc_write(codec, M98089_REG_14_DAI1_FORMAT, 0xa6);/*MAS=1 (for now) WCI1=1 (for PCM) BCI1=0 (FOR PCM) DLY1=0 (FOR PCM) TDM1=1 (FOR PCM) FSW1=0 WS1=0*/
//	snd_soc_write(codec, M98089_REG_1C_DAI2_FORMAT, 0x24);
	snd_soc_write(codec, M98089_REG_15_DAI1_CLOCK, 0x46);

	snd_soc_write(codec, M98089_REG_16_DAI1_IOCFG, 0x43);
	snd_soc_write(codec, M98089_REG_1E_DAI2_IOCFG, 0x83);


	snd_soc_write(codec, M98089_REG_17_DAI1_TDM, 0x00); /*SLOTL1=0 AND SLOTR1=O (FOR PCM)*/
/*	snd_soc_write(codec, M98089_REG_22_MIX_DAC, 0x84);
	snd_soc_write(codec, M98089_REG_35_LVL_MIC1, 0x60);
	snd_soc_write(codec, M98089_REG_48_CFG_MIC, 0x30);
	snd_soc_write(codec, M98089_REG_4D_PWR_EN_OUT, 0xc3);
	snd_soc_write(codec, M98089_REG_4E_BIAS_CNTL, 0x30);
*/
	snd_soc_write(codec, M98089_REG_4F_DAC_BIAS1, 0x00);
/*
	snd_soc_write(codec, M98089_REG_50_DAC_BIAS2, 0x0f);
	snd_soc_write(codec, M98089_REG_39_LVL_HP_L, 0x1c);
	snd_soc_write(codec, M98089_REG_3A_LVL_HP_R, 0x1c);
	snd_soc_write(codec, M98089_REG_51_PWR_SYS, 0x80);*/
#endif


	printk(KERN_ALERT "Register settings in set_fmt in dai1 #########################\n");
//	for(i1 = 0; i1 < 0x52; i1++ )
//		printk(KERN_ALERT "Register %x : %x\n", i1, snd_soc_read(codec, i1));

	return 0;
}



static int max98089_dai2_set_fmt(struct snd_soc_dai *codec_dai,
				 unsigned int fmt)
{
	struct snd_soc_codec *codec = codec_dai->codec;
	struct max98089_priv *max98089 = snd_soc_codec_get_drvdata(codec);
	struct max98089_cdata *cdata;
	u8 reg1Cval = 0;

	cdata = &max98089->dai[1];

	if (fmt != cdata->fmt) {
		cdata->fmt = fmt;

		switch (fmt & SND_SOC_DAIFMT_MASTER_MASK) {
		case SND_SOC_DAIFMT_CBS_CFS:
			/* Slave mode PLL */
			snd_soc_write(codec, M98089_REG_1A_DAI2_CLKCFG_HI,
				0x80);
			snd_soc_write(codec, M98089_REG_1B_DAI2_CLKCFG_LO,
				0x00);
			break;
		case SND_SOC_DAIFMT_CBM_CFM:
			/* Set to master mode */
			reg1Cval |= M98089_DAI_MAS;
			break;
		case SND_SOC_DAIFMT_CBS_CFM:
		case SND_SOC_DAIFMT_CBM_CFS:
		default:
			dev_err(codec->dev, "Clock mode unsupported");
			return -EINVAL;
		}

		switch (fmt & SND_SOC_DAIFMT_FORMAT_MASK) {
		case SND_SOC_DAIFMT_I2S:
			reg1Cval |= M98089_DAI_DLY;
			break;
		case SND_SOC_DAIFMT_LEFT_J:
			break;
		default:
			return -EINVAL;
		}

		switch (fmt & SND_SOC_DAIFMT_INV_MASK) {
		case SND_SOC_DAIFMT_NB_NF:
			break;
		case SND_SOC_DAIFMT_NB_IF:
			reg1Cval |= M98089_DAI_WCI;
			break;
		case SND_SOC_DAIFMT_IB_NF:
			reg1Cval |= M98089_DAI_BCI;
			break;
		case SND_SOC_DAIFMT_IB_IF:
			reg1Cval |= M98089_DAI_BCI|M98089_DAI_WCI;
			break;
		default:
			return -EINVAL;
		}

		snd_soc_update_bits(codec, M98089_REG_1C_DAI2_FORMAT,
			M98089_DAI_MAS | M98089_DAI_DLY | M98089_DAI_BCI |
			M98089_DAI_WCI, reg1Cval);

		snd_soc_write(codec, M98089_REG_1D_DAI2_CLOCK,
			M98089_DAI_BSEL64);
	}

	/* Hardcoding the values corressponding to widgets. */
	snd_soc_write (codec, M98089_REG_39_LVL_HP_L, 0x1c);
	snd_soc_write (codec, M98089_REG_3A_LVL_HP_R, 0x1c); 
	snd_soc_write (codec, M98089_REG_4D_PWR_EN_OUT, 0xc3); 

       snd_soc_write(codec, M98089_REG_4E_BIAS_CNTL, 0xF0);
       snd_soc_write(codec, M98089_REG_4B_CFG_JACKDET, 0x80);
       snd_soc_write(codec, M98089_REG_4E_BIAS_CNTL, 0xF1);

       printk(KERN_ALERT "****************** %s: JACK Status setting = %x\n", __func__, snd_soc_read(codec, 0x02));

	int i1;
	snd_soc_write(codec, M98089_REG_10_SYS_CLK, 0x10);
	snd_soc_write(codec, M98089_REG_11_DAI1_CLKMODE, 0x1c);
	snd_soc_write(codec, M98089_REG_12_DAI1_CLKCFG_HI, 0x00);
	snd_soc_write(codec, M98089_REG_13_DAI1_CLKCFG_LO, 0x00);
	snd_soc_write(codec, M98089_REG_14_DAI1_FORMAT, 0xa6);/*MAS=1 (for now) WCI1=1 (for PCM) BCI1=0 (FOR PCM) DLY1=0 (FOR PCM) TDM1=1 (FOR PCM) FSW1=0 WS1=0*/
//	snd_soc_write(codec, M98089_REG_1C_DAI2_FORMAT, 0x24);
	snd_soc_write(codec, M98089_REG_15_DAI1_CLOCK, 0x46);
	printk(KERN_ALERT "Register settings in dai2 set_fmt\n");
	snd_soc_write(codec, M98089_REG_1E_DAI2_IOCFG, 0x83);
//	for(i1 = 0; i1 < 0x52; i1++ )
//		printk(KERN_ALERT "Register %x : %x\n", i1, snd_soc_read(codec, i1));

       
	return 0;
}
