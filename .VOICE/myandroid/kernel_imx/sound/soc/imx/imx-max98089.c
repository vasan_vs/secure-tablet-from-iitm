/*
 * sound/soc/imx/3ds-max98089.c --  SoC audio for i.MX 3ds boards with
 *                                  max98089 codec
 *
 * Copyright 2009 Sascha Hauer, Pengutronix <s.hauer@pengutronix.de>
 * Copyright (C) 2011 Freescale Semiconductor, Inc.
 *
 * This program is free software; you can redistribute  it and/or modify it
 * under  the terms of  the GNU General  Public License as published by the
 * Free Software Foundation;  either version 2 of the  License, or (at your
 * option) any later version.
 */

#include <linux/clk.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/device.h>
#include <linux/i2c.h>
#include <linux/fsl_devices.h>
#include <linux/gpio.h>
#include <sound/core.h>
#include <sound/pcm.h>
#include <sound/pcm_params.h>
#include <sound/soc.h>
#include <sound/soc-dai.h>
#include <sound/initval.h>
#include <sound/jack.h>
#include <sound/soc-dapm.h>
#include <asm/mach-types.h>
#include <mach/audmux.h>
#include <mach/ssi.h>

#include "../codecs/max98089.h"
#include "imx-ssi.h"
#include "i2s.h"

static struct imx_max98089_priv {
	int sysclk;
	int codec_sysclk;
	int dai_hifi;
	int hw;
	struct platform_device *pdev;
} card_priv;

static struct snd_soc_jack hs_jack;
struct clk *codec_mclk;
static struct snd_soc_card imx_max98089;

/* Headphones jack detection DAPM pins */
static struct snd_soc_jack_pin hs_jack_pins[] = {
	{
		.pin = "Headphone Jack",
		.mask = SND_JACK_HEADPHONE,
	},
};

/* Headphones jack detection gpios */
static struct snd_soc_jack_gpio hs_jack_gpios[] = {
	[0] = {
		/* gpio is set on per-platform basis */
		.name           = "hp-gpio",
		.report         = SND_JACK_HEADPHONE,
		.debounce_time	= 200,
	},
};

static int imx_hifi_startup(struct snd_pcm_substream *substream)
{
	struct snd_soc_pcm_runtime *rtd = substream->private_data;
	struct snd_soc_dai *codec_dai = rtd->codec_dai;
printk(KERN_ERR "***********************************imx_hifi_startup*******\n");
	if (!codec_dai->active)
			clk_enable(codec_mclk);

	return 0;
}

static void imx_hifi_shutdown(struct snd_pcm_substream *substream)
{
 	struct snd_soc_pcm_runtime *rtd = substream->private_data;
	struct snd_soc_dai *codec_dai = rtd->codec_dai;
printk(KERN_ERR "***********************************imx_hifi_shutdown*******\n");
	if (!codec_dai->active)
		clk_disable(codec_mclk);

	return;
}

static const struct {
	u32 rate;
	u8  sr;
} rate_table[] = {
	{8000,  0x10},
	{11025, 0x20},
	{16000, 0x30},
	{22050, 0x40},
	{24000, 0x50},
	{32000, 0x60},
	{44100, 0x70},
	{48000, 0x80},
	{88200, 0x90},
	{96000, 0xA0},
};

static inline int rate_value(int rate, u8 *value)
{
	int i;

	for (i = 0; i < ARRAY_SIZE(rate_table); i++) {
		if (rate_table[i].rate >= rate) {
			*value = rate_table[i].sr;
			return 0;
		}
	}
	*value = rate_table[0].sr;
	return -EINVAL;
}

/*	-Run the code once and check the printk statement which prints the sysclk set in card_priv structure. Verify whether it is same as the ssi syslk using the logic
	analyzer. - it is 0;
	-SSI provides an ssi_sysclk. First of all dtermine from where can we get the frquency of this clk. See the last comment.
	-Next check what is the role of MCLK is in max98089's functioning and where is it being set, if at all it is being set. (It is mostly used for I2C)
	-The ssi clock dividers are being set here and two clks - INT_BIT_CLK and FRAME_SYNC_CLK - are being obatined and fed into the codec.
	-Check whether they corresspond to the BCLK and LRCLK of the codec respectively. (Mostly yes)
	-If the frquency of SSI's sysclk can be obtained then we can get a good accuracy for the FRAME_SYNC_CLK which is supposed to be the same
	as the sampling frequency of the audio.
	-There is a set_sysclk function call in this function. Check what sysclk is being set by it. Is it the SSI sysclk(Mostly yes). It sets it to zero. Is this done
	because we are changing the clock dividers and once the clock dividers are set it is again set to its original value?
*/
static int max98089_params(struct snd_pcm_substream *substream,
	struct snd_pcm_hw_params *params)
{
	struct snd_soc_pcm_runtime *rtd = substream->private_data;
	struct snd_soc_codec *codec = rtd->codec;
	struct snd_soc_dai *codec_dai = rtd->codec_dai;
	struct snd_soc_dai *cpu_dai = rtd->cpu_dai;

	struct imx_ssi *ssi_mode = snd_soc_dai_get_drvdata(cpu_dai);

	int ret;
	u32 dai_format;

	unsigned int channels = params_channels(params);

	ssi_mode->flags |= IMX_SSI_SYN;

	int wl = 32;
	/*
	 * Getting the word length of the input audio.
	 */  
	switch (params_format(params)) {
		case SNDRV_PCM_FORMAT_U24:
		case SNDRV_PCM_FORMAT_S24:
			wl = 24;
			break;
		case SNDRV_PCM_FORMAT_U16_LE:
		case SNDRV_PCM_FORMAT_S16_LE:
			wl = 16;
			break;
		default:
			return -EINVAL;
	}

	int div_pm = 0, div_2 = 0, div_psr = 0;

	/* Check what system clock is being provided here.
	 * On the basis of that set the registers.
	 */

	printk(KERN_ALERT "**************************System Clock = %d\n", card_priv.sysclk);	// does this sysclk refers to the ssi sysclk or max98089's sysclk
	printk(KERN_ALERT "**************************465");

	/* Getting the LRCLK form the SYSCLK ny setting
	 * the clkdiv registers such that LRCLK is nearly
	 * equal to the sampling rate of the audio signal.
	 */

	int rate = params_rate(params);

	switch (rate) {
	case 16000:
	case 22050:
	case 24000:
	case 32000:
	case 44100:
	case 48000:
	case 88200:
	case 96000:
	case 64000:
	case 8000:
	case 11025:
	case 12000:
		// TODO : Add support for 24-bit media files
		div_pm = ( int )( 480 / ( rate / 1000 ) );
		if(channels == 1)
			div_pm = 2 * div_pm + 1;
		break;
	default:
		return -EINVAL;
	}

	dai_format = SND_SOC_DAIFMT_I2S | SND_SOC_DAIFMT_NB_NF |
		SND_SOC_DAIFMT_CBS_CFS;

	/* set codec DAI configuration */
	/* Does this go to the set_fmt function of max98089? */
	ret = snd_soc_dai_set_fmt(codec_dai, dai_format);
	if (ret < 0)
		return ret;

	/* TODO: The SSI driver should figure this out for us */
	switch (channels) {
	case 1:
		snd_soc_dai_set_tdm_slot(cpu_dai, 0xfffffffe, 0xfffffffe, 2, 32);
		break;
	case 2:
		snd_soc_dai_set_tdm_slot(cpu_dai, 0xfffffffc, 0xfffffffc, 2, 32);
		break;
	default:
		return -EINVAL;
	}

	//printk(KERN_ALERT "================================No of channels = %d", channels);

	/* set cpu DAI configuration */
	dai_format = SND_SOC_DAIFMT_I2S | SND_SOC_DAIFMT_NB_NF |
		SND_SOC_DAIFMT_CBS_CFS;
	/* Does this go to the set_fmt function of max98089? */
	ret = snd_soc_dai_set_fmt(cpu_dai, dai_format);
	if (ret < 0)
		return ret;

	/* Is the sys clk - used to obtain the LRCLK - being set here?
	 */
	/* Uncomment the commented line and remove the original line to make the code general.
	 * It basically sets the PCLK according to the frequency of audio input and sets the sysclk of
	 * max98089 equal to the sampling rate of the imput audio. We are assuming that the sysclk is MCLK in the schematics.
	 */
	ret = snd_soc_dai_set_sysclk(codec_dai, IMX_SSP_SYS_CLK, 0, SND_SOC_CLOCK_IN);
//	ret = snd_soc_dai_set_sysclk(codec_dai, IMX_SSP_SYS_CLK, rate, SND_SOC_CLOCK_IN);

	ret = snd_soc_dai_set_clkdiv(cpu_dai, IMX_SSI_TX_DIV_PM, div_pm);		/*We are only setting the Tx registers and not the Rx registers*/
	if (ret < 0) printk (KERN_INFO "Error setting DIV_PM\n");
	ret = snd_soc_dai_set_clkdiv(cpu_dai, IMX_SSI_TX_DIV_2, div_2);
	if (ret < 0) printk (KERN_INFO "Error setting TX_DIV_2\n");
	ret = snd_soc_dai_set_clkdiv(cpu_dai, IMX_SSI_TX_DIV_PSR, div_psr);
	if (ret < 0) printk (KERN_INFO "Error setting TX_DIV_PSR\n");

	return 0;
}

static struct snd_soc_ops imx_max98089_hifi_ops = {
	.startup = imx_hifi_startup,
	.shutdown = imx_hifi_shutdown,
	.hw_params = max98089_params,
};

static int max98089_jack_func;
static int max98089_spk_func;
static int max98089_line_in_func;

static const char *jack_function[] = { "off", "on"};

static const char *spk_function[] = { "off", "on" };

static const char *line_in_function[] = { "off", "on" };

static const struct soc_enum max98089_enum[] = {
	SOC_ENUM_SINGLE_EXT(2, jack_function),
	SOC_ENUM_SINGLE_EXT(2, spk_function),
	SOC_ENUM_SINGLE_EXT(2, line_in_function),
};

static int max98089_get_jack(struct snd_kcontrol *kcontrol,
			     struct snd_ctl_elem_value *ucontrol)
{
printk(KERN_ERR "***********************************get_jack*******\n");
	ucontrol->value.enumerated.item[0] = max98089_jack_func;
	return 0;
}

static int max98089_set_jack(struct snd_kcontrol *kcontrol,
			     struct snd_ctl_elem_value *ucontrol)
{
	struct snd_soc_codec *codec = snd_kcontrol_chip(kcontrol);
printk(KERN_ERR "***********************************set_jack*******\n");
	if (max98089_jack_func == ucontrol->value.enumerated.item[0])
		return 0;

	max98089_jack_func = ucontrol->value.enumerated.item[0];
	if (max98089_jack_func)
		snd_soc_dapm_enable_pin(&codec->dapm, "Headphone Jack");
	else
		snd_soc_dapm_disable_pin(&codec->dapm, "Headphone Jack");

	snd_soc_dapm_sync(&codec->dapm);
	return 1;
}

static int max98089_get_spk(struct snd_kcontrol *kcontrol,
			    struct snd_ctl_elem_value *ucontrol)
{
printk(KERN_ERR "***********************************get_spk*******\n");
	ucontrol->value.enumerated.item[0] = max98089_spk_func;
	return 0;
}

static int max98089_set_spk(struct snd_kcontrol *kcontrol,
			    struct snd_ctl_elem_value *ucontrol)
{
	struct snd_soc_codec *codec = snd_kcontrol_chip(kcontrol);
printk(KERN_ERR "***********************************set_spk*******\n");
	if (max98089_spk_func == ucontrol->value.enumerated.item[0])
		return 0;

	max98089_spk_func = ucontrol->value.enumerated.item[0];
	if (max98089_spk_func)
		snd_soc_dapm_enable_pin(&codec->dapm, "Ext Spk");
	else
		snd_soc_dapm_disable_pin(&codec->dapm, "Ext Spk");

	snd_soc_dapm_sync(&codec->dapm);
	return 1;
}

static int max98089_get_line_in(struct snd_kcontrol *kcontrol,
			     struct snd_ctl_elem_value *ucontrol)
{
printk(KERN_ERR "***********************************get_linein*******\n");
	ucontrol->value.enumerated.item[0] = max98089_line_in_func;
	return 0;
}

static int max98089_set_line_in(struct snd_kcontrol *kcontrol,
			     struct snd_ctl_elem_value *ucontrol)
{
	struct snd_soc_codec *codec = snd_kcontrol_chip(kcontrol);
printk(KERN_ERR "***********************************set_linein*******\n");
	if (max98089_line_in_func == ucontrol->value.enumerated.item[0])
		return 0;

	max98089_line_in_func = ucontrol->value.enumerated.item[0];
	if (max98089_line_in_func)
		snd_soc_dapm_enable_pin(&codec->dapm, "Line In Jack");
	else
		snd_soc_dapm_disable_pin(&codec->dapm, "Line In Jack");

	snd_soc_dapm_sync(&codec->dapm);
	return 1;
}

/* imx_3stack card dapm widgets */
static const struct snd_soc_dapm_widget imx_3stack_dapm_widgets[] = {
	SND_SOC_DAPM_MIC("Mic Jack", NULL),
	SND_SOC_DAPM_LINE("Line In Jack", NULL),
	SND_SOC_DAPM_SPK("Ext Spk", NULL),
	SND_SOC_DAPM_HP("Headphone Jack", NULL),
	SND_SOC_DAPM_HP("HP_OUT", NULL),
};

static const struct snd_kcontrol_new max98089_machine_controls[] = {
	SOC_ENUM_EXT("Jack Function", max98089_enum[0], max98089_get_jack,
		     max98089_set_jack),
	SOC_ENUM_EXT("Speaker Function", max98089_enum[1], max98089_get_spk,
		     max98089_set_spk),
	SOC_ENUM_EXT("Line In Function", max98089_enum[1], max98089_get_line_in,
		     max98089_set_line_in),
};

#if 1
/* imx_3stack machine connections to the codec pins */
static const struct snd_soc_dapm_route audio_map[] = {

	/* Mic Jack --> MIC_IN (with automatic bias) */
//	{"MIC_IN", NULL, "Mic Jack"},

	/* Line in Jack --> LINE_IN */
//	{"LINE_IN", NULL, "Line In Jack"},

	/* HP_OUT --> Headphone Jack */
	{"Headphone Jack", NULL, "HP_OUT"},

	/* LINE_OUT --> Ext Speaker */
//	{"Ext Spk", NULL, "LINE_OUT"},
};
#endif 

static int CnT = 0;

static int imx_3stack_max98089_init(struct snd_soc_pcm_runtime *rtd)
{
	struct snd_soc_codec *codec = rtd->codec;
	int ret;
printk(KERN_ERR "***********************************imx_3stack_init*******\n");
	if(CnT++ == 0){
		printk (KERN_WARNING "************ Inside the loop for calling machine controls ***************\n");
		ret = snd_soc_add_controls(codec, max98089_machine_controls,
			ARRAY_SIZE(max98089_machine_controls));
		if (ret)
			return ret;
	}
	/* Add imx_3stack specific widgets */
	snd_soc_dapm_new_controls(&codec->dapm, imx_3stack_dapm_widgets,
				  ARRAY_SIZE(imx_3stack_dapm_widgets));

	/* Set up imx_3stack specific audio path audio_map */
	snd_soc_dapm_add_routes(&codec->dapm, audio_map, ARRAY_SIZE(audio_map));

	snd_soc_dapm_disable_pin(&codec->dapm, "Line In Jack");
	snd_soc_dapm_enable_pin(&codec->dapm, "Headphone Jack");
	snd_soc_dapm_sync(&codec->dapm);

	if (hs_jack_gpios[0].gpio != -1) {
		/* Jack detection API stuff */
		ret = snd_soc_jack_new(codec, "Headphone Jack",
				       SND_JACK_HEADPHONE, &hs_jack);
		printk(KERN_WARNING "***********************************jck_detect_api******* Return value : %d\n", ret);
		if (ret)
			return ret;

		ret = snd_soc_jack_add_pins(&hs_jack, ARRAY_SIZE(hs_jack_pins),
					hs_jack_pins);
		if (ret) {
			printk(KERN_WARNING "failed to call  snd_soc_jack_add_pins *********** Return value: %d\n", ret);
			return ret;
		}

		ret = snd_soc_jack_add_gpios(&hs_jack,
					ARRAY_SIZE(hs_jack_gpios), hs_jack_gpios);
		if (ret)
			printk(KERN_WARNING "failed to call snd_soc_jack_add_gpios\n");
	}

	return 0;
}

static struct snd_soc_dai_link imx_max98089_dai[] = {
	{//dai2
		.name		= "HiFi2",
		.stream_name	= "HiFi",
		.codec_dai_name	= "max98089.1",
		.codec_name	= "max98089.0-0010",		// 0-0020 to 1-0010
		.cpu_dai_name	= "imx-ssi.1",			
		.platform_name	= "imx-pcm-audio.1",
		.init		= imx_3stack_max98089_init,
		.ops		= &imx_max98089_hifi_ops,
	},
	{//dai1
		.name		= "HiFi1",
		.stream_name	= "HiFi",
		.codec_dai_name	= "max98089.0",
		.codec_name	= "max98089.0-0010",
		.cpu_dai_name	= "imx-ssi.1",			
		.platform_name	= "imx-pcm-audio.1",
//		.init		= imx_3stack_max98089_init,
		.ops		= &imx_max98089_hifi_ops,
	}
};

static struct snd_soc_card imx_max98089 = {
	.name		= "max98089-audio",
	.dai_link	= imx_max98089_dai,
	.num_links	= ARRAY_SIZE(imx_max98089_dai),
};

static struct platform_device *imx_max98089_snd_device;

static int imx_audmux_config(int slave, int master)
{
	unsigned int ptcr, pdcr;
	slave = slave - 1;
	master = master - 1;
printk(KERN_ERR "***********************************imx_audmux_config*******\n");
	/* SSI0 mastered by port 5 */
	ptcr = MXC_AUDMUX_V2_PTCR_SYN |
		MXC_AUDMUX_V2_PTCR_TFSDIR |
		MXC_AUDMUX_V2_PTCR_TFSEL(master) |
		MXC_AUDMUX_V2_PTCR_TCLKDIR |
		MXC_AUDMUX_V2_PTCR_TCSEL(master);
	pdcr = MXC_AUDMUX_V2_PDCR_RXDSEL(master);
	mxc_audmux_v2_configure_port(slave, ptcr, pdcr);

	ptcr = MXC_AUDMUX_V2_PTCR_SYN;
	pdcr = MXC_AUDMUX_V2_PDCR_RXDSEL(slave);
	mxc_audmux_v2_configure_port(master, ptcr, pdcr);

	return 0;
}

static int __devinit imx_max98089_probe(struct platform_device *pdev)
{
	struct mxc_audio_platform_data *plat = pdev->dev.platform_data;

	int ret = 0;
printk(KERN_ERR "***********************************imx_max_probe*******\n");
	card_priv.sysclk = plat->sysclk;
	card_priv.pdev = pdev;

	imx_audmux_config(plat->ext_port, plat->src_port);

	ret = -EINVAL;
	if (plat->init && plat->init())
		return ret;

	hs_jack_gpios[0].gpio = plat->hp_gpio;
	hs_jack_gpios[0].invert = plat->hp_active_low;

	return 0;
}

static int imx_max98089_remove(struct platform_device *pdev)
{
	struct mxc_audio_platform_data *plat = pdev->dev.platform_data;

	if (plat->finit)
		plat->finit();

	return 0;
}

static struct platform_driver imx_max98089_audio_driver = {
	.probe = imx_max98089_probe,
	.remove = imx_max98089_remove,
	.driver = {
		   .name = "imx-max98089",
		   },
};

static int __init imx_max98089_init(void)
{
	int ret;
	printk(KERN_ERR "***********************************imx_max98089_init*******\n");
	ret = platform_driver_register(&imx_max98089_audio_driver);
	if (ret)
		return -ENOMEM;

	if (machine_is_mx35_3ds() || machine_is_mx6q_sabrelite())
	{
		imx_max98089_dai[0].codec_name = "max98089.0-0010";
	}
	else
	{
		imx_max98089_dai[0].codec_name = "max98089.1-0010";
	}
	imx_max98089_snd_device = platform_device_alloc("soc-audio", 1);
	if (!imx_max98089_snd_device)
		return -ENOMEM;

	platform_set_drvdata(imx_max98089_snd_device, &imx_max98089);

	ret = platform_device_add(imx_max98089_snd_device);
	if (ret) {
		printk(KERN_ERR "ASoC: Platform device allocation failed\n");
		platform_device_put(imx_max98089_snd_device);
	}

	return ret;
}

static void __exit imx_max98089_exit(void)
{
	platform_driver_unregister(&imx_max98089_audio_driver);
	platform_device_unregister(imx_max98089_snd_device);
}

module_init(imx_max98089_init);
module_exit(imx_max98089_exit);

MODULE_AUTHOR("Sascha Hauer <s.hauer@pengutronix.de>");
MODULE_DESCRIPTION("PhyCORE ALSA SoC driver");
MODULE_LICENSE("GPL");
