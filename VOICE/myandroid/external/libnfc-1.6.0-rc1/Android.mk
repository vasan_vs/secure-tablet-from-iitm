LOCAL_PATH:= $(call my-dir)

#
# libnfc-1.6
#

include $(CLEAR_VARS)

LOCAL_ARM_MODE := arm

LOCAL_SRC_FILES := libnfc/iso14443-subr.c libnfc/mirror-subr.c libnfc/nfc.c libnfc/nfc-device.c libnfc/nfc-emulation.c libnfc/nfc-internal.c libnfc/log-printf.c
LOCAL_SRC_FILES += libnfc/buses/uart.c 
LOCAL_SRC_FILES += libnfc/chips/pn53x.c 
LOCAL_SRC_FILES += libnfc/drivers/pn532_uart.c 

#LOCAL_SRC_FILES += drivers/pn53x_usb.c drivers/acr122.c drivers/acr122s.c drivers/arygon.c

LOCAL_CFLAGS := -O2 -Wall -pedantic -Wextra -std=c99 -DSERIAL_AUTOPROBE_ENABLED
LOCAL_CFLAGS += -DHAVE_CONFIG_H -DDRIVER_PN532_UART_ENABLED -DDEBUG -DIIT_DEBUG

# -DDRIVER_PN53X_USB_ENABLED -DHAVE_LIBUSB -DDRIVER_ACR122_ENABLED -DDRIVER_ACR122S_ENABLED -DDRIVER_ARYGON_ENABLED 

$(info $(LOCAL_PATH))

LOCAL_CFLAGS += -I$(LOCAL_PATH)
LOCAL_CFLAGS += -I$(LOCAL_PATH)/include
LOCAL_CFLAGS += -I$(LOCAL_PATH)/libnfc
LOCAL_CFLAGS += -I$(LOCAL_PATH)/libnfc/buses
LOCAL_CFLAGS += -I$(LOCAL_PATH)/libnfc/chips
LOCAL_CFLAGS += -I$(LOCAL_PATH)/libnfc/drivers

LOCAL_MODULE:= libnfc-1.6
LOCAL_MODULE_TAGS := optional
#LOCAL_SHARED_LIBRARIES := libusb-1.0

#LOCAL_SHARED_LIBRARIES := libc libcutils

include $(BUILD_SHARED_LIBRARY)

#
# nfc-list
#

include $(CLEAR_VARS)

LOCAL_SRC_FILES := utils/nfc-utils.c utils/nfc-list.c

LOCAL_CFLAGS:= -g -O2 -Wall -pedantic -Wextra -std=c99 -DDEBUG -DIIT_DEBUG

LOCAL_CFLAGS += -I$(LOCAL_PATH) \
	-I$(LOCAL_PATH)/include

LOCAL_MODULE:= nfc-list
LOCAL_MODULE_TAGS := debug
LOCAL_SHARED_LIBRARIES := libnfc-1.6 libc libcutils libusb-1.0

include $(BUILD_EXECUTABLE)

#
#
#

include $(CLEAR_VARS)

LOCAL_SRC_FILES := examples/nfc-anticol.c  utils/nfc-utils.c

LOCAL_CFLAGS:= -g -O2 -Wall -pedantic -Wextra -std=c99 -DDEBUG -DIIT_DEBUG

LOCAL_CFLAGS += -I$(LOCAL_PATH) \
	-I$(LOCAL_PATH)/include

LOCAL_MODULE:= nfc-anticol
LOCAL_MODULE_TAGS := debug
LOCAL_SHARED_LIBRARIES := libnfc-1.6 libc libcutils libusb-1.0

include $(BUILD_EXECUTABLE)


include $(CLEAR_VARS)

LOCAL_SRC_FILES := utils/nfc-probe.c

LOCAL_CFLAGS:= -g -O2 -Wall -pedantic -Wextra -std=c99 -DDEBUG -DIIT_DEBUG

LOCAL_CFLAGS += -I$(LOCAL_PATH) \
	-I$(LOCAL_PATH)/include

LOCAL_MODULE:= nfc-probe
LOCAL_MODULE_TAGS := debug
LOCAL_SHARED_LIBRARIES := libnfc-1.6 libc libcutils libusb-1.0

include $(BUILD_EXECUTABLE)
