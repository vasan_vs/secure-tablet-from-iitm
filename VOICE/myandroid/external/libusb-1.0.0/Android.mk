LOCAL_PATH:= $(call my-dir)

#
# libusb-1.0
#

include $(CLEAR_VARS)

LOCAL_ARM_MODE := arm

LOCAL_SRC_FILES := \
		core.c \
		descriptor.c \
		io.c \
		sync.c \
		os/linux_usbfs.c

LOCAL_CFLAGS := -W -Wall -fPIC -DPIC -DHAVE_CONFIG_H -DLIBUSB_DESCRIBE

LOCAL_CFLAGS += -I$(LOCAL_PATH)
LOCAL_CFLAGS += -I$(LOCAL_PATH)/os

LOCAL_MODULE:= libusb-1.0
LOCAL_MODULE_TAGS := optional


include $(BUILD_SHARED_LIBRARY)

#
# lsusb
#
include $(CLEAR_VARS)

LOCAL_ARM_MODE := arm

LOCAL_SRC_FILES:= examples/lsusb.c

LOCAL_CFLAGS += -W -Wall
LOCAL_CFLAGS += -fPIC -DPIC
LOCAL_CFLAGS += -I$(LOCAL_PATH)
LOCAL_CFLAGS += -I$(LOCAL_PATH)/os


LOCAL_MODULE:= lsusb
LOCAL_MODULE_TAGS := optional
LOCAL_SHARED_LIBRARIES := libusb-1.0

LOCAL_PRELINK_MODULE := false

include $(BUILD_EXECUTABLE)
