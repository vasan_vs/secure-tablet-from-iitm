/*
**
** IMPORTANT LEGAL NOTICE:
** waiver of Liability: Please note that this software is freeware licensed
** under the Apache v. 2.0 license. This software might have been altered,
** modified, influenced or changed by third parties without knowledge and
** consent of Cinterion Wireless Modules GmbH (“Cinterion”).
** As the user of this freeware software you are fully and solely
** responsible for the compatibility of the software with your system and
** all its (software) components as well as any defects, bugs, malfunctions
** or damages arising from or in connection with its installation or use.
** Cinterion does not accept any liability whatsoever with respect to this
** software, namely, but not limited to, its functionality and its
** compatibility with any other hardware and/or software or parts thereof.
** Cinterion shall not be liable for any defects, bugs, malfunctions or
** damages contained in this software or resulting from or arising in
** connection with its installation or use.
**
** Copyright 2011, Cinterion Wireless Modules GmbH
** Copyright 2006, The Android Open Source Project
**
** Licensed under the Apache License, Version 2.0 (the "License");
** you may not use this file except in compliance with the License.
** You may obtain a copy of the License at
**
**     http://www.apache.org/licenses/LICENSE-2.0
**
** Unless required by applicable law or agreed to in writing, software
** distributed under the License is distributed on an "AS IS" BASIS,
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
** See the License for the specific language governing permissions and
** limitations under the License.
*/


#include <errno.h>
#include <pthread.h>
#include <termios.h>
#include <fcntl.h>
#include <sys/epoll.h>
#include <math.h>
#include <time.h>
#include <semaphore.h>
#include <signal.h>
#include <unistd.h>
#include <cutils/sockets.h>

#define  LOG_NDEBUG 0
#define  LOG_TAG  "gps_cmw"
#include <cutils/log.h>

#include <gps_misc.h>

#if defined (__ANDROID_VERSION__) &&  __ANDROID_VERSION__ < ANDROID_VER_2_3
  #include <hardware_legacy/gps.h>
  #include <hardware/hardware.h>
#else
  #include <hardware/gps.h>
#endif

#include <gps_nmea.h>
#include <gps_version.h>



#define GPS_STATUS_CB(_cb, _s)    \
  if ((_cb).status_cb) {          \
    GpsStatus gps_status;         \
    gps_status.status = (_s);     \
    (_cb).status_cb(&gps_status); \
    GPSTrace (ZONE_INFO, "gps status callback: 0x%x", _s); \
  }

#define GPSPORT_OPEN_POLLING        1000  // msec

enum {
    STATE_QUIT  = 0,
    STATE_INIT  = 1,
    STATE_START = 2
};

/* commands sent to the gps thread */
enum {
    CMD_QUIT  = 0,
    CMD_START = 1,
    CMD_STOP  = 2,
    CMD_GPS_READ  = 3
};

/* Since NMEA parser requires lcoks */
#define GPS_STATE_LOCK_FIX(_s)         \
{                                      \
  int ret;                             \
  do {                                 \
    ret = sem_wait(&(_s)->fix_sem);    \
  } while (ret < 0 && errno == EINTR);   \
}
#define GPS_STATE_UNLOCK_FIX(_s)       \
  sem_post(&(_s)->fix_sem)

/* this is the state of our connection to the qemu_gpsd daemon */
typedef struct {
    int                     state;
    int                     GpsOpenPolling;
    int                     fd;
    GpsCallbacks            callbacks;
    pthread_t               cmd_thread;
    pthread_t               tmr_thread;
    int                     tmr_thread_runs;
    pthread_t               openport_thread;
    int                     control[2];
    int                     fix_freq;
    sem_t                   fix_sem;
    int                     first_fix;
    NmeaReader              reader;

} GpsInst;

static GpsInst  g_gps_inst;
static GpsInst  *g_pGpsInst = &g_gps_inst;




/*****************************************************************/
/*****************************************************************/
/**
 *
 */
void *
gps_OpenGPSPort_thread ( void*  arg )
{
    GpsInst *inst = (GpsInst *)arg;
    int   fd = -1;
    char  cmd = CMD_GPS_READ;
    int   ret;

    while ((fd < 0) && (inst->state == STATE_START)) {
        fd = misc_OpenGPSPort();
        if (fd < 0) {
            msleep(GPSPORT_OPEN_POLLING);
            continue;
        }
    }

    if (inst->state == STATE_START) {
        inst->fd = fd;
        misc_StartGPS ();

        GPSTrace (ZONE_FUNC, "-> %s: send CMD_GPS_READ", __FUNCTION__);

        do {
            ret=write( inst->control[0], &cmd, 1 );
        } while (ret < 0 && errno == EINTR);

        if (ret != 1) {
            GPSTrace (ZONE_ERROR, "%s: could not send CMD_GPS_READ command: ret=%d: %s",
              __FUNCTION__, ret, strerror(errno));
        }
    } else {
        // if port is opened and state not  STATE_START
        if (fd >= 0) {
            misc_CloseGPSPort(fd);
        }
    }
    return NULL;
}

/**
 *
 */
void gps_OpenGPSPort (GpsInst* inst) {
    GPSTrace (ZONE_FUNC, "-> %s:", __FUNCTION__);
    
    // wait for previous instance, if exist
    pthread_join (inst->openport_thread, NULL);
    inst->fd = misc_CloseGPSPort(inst->fd);

    if ( pthread_create( &inst->openport_thread, NULL, gps_OpenGPSPort_thread, inst ) != 0 ) {
        GPSTrace (ZONE_ERROR, "%s: fatal error -> could not create openport_thread: %s", __FUNCTION__, strerror(errno));
    }

    GPSTrace (ZONE_FUNC, "<- %s:", __FUNCTION__);
}

/**
 *
 */
#if defined (__ANDROID_VERSION__) &&  __ANDROID_VERSION__ < ANDROID_VER_2_3
  void *
#else
  void
#endif
gps_timer_thread( void*  arg )
{
    GpsInst *inst = (GpsInst *)arg;

    GPSTrace (ZONE_INFO, "gps entered timer thread");
    inst->tmr_thread_runs = 1;

    do {
        GPSTrace (ZONE_INFO, "gps timer exp");

        GPS_STATE_LOCK_FIX(inst);

        if (inst->reader.fix.flags != 0) {
            GPSTrace (ZONE_INFO, "gps fix cb: 0x%x", inst->reader.fix.flags);
            if (inst->callbacks.location_cb) {
                inst->callbacks.location_cb( &inst->reader.fix );
                inst->reader.fix.flags = 0;
                inst->first_fix = 1;
            }
        }

        if (inst->reader.sv_status_changed != 0) {
            GPSTrace (ZONE_INFO, "gps sv status callback");
            if (inst->callbacks.sv_status_cb) {
                inst->callbacks.sv_status_cb( &inst->reader.sv_status );
                inst->reader.sv_status_changed = 0;
            }
        }

        GPS_STATE_UNLOCK_FIX(inst);
        msleep(inst->fix_freq);

    } while(inst->state == STATE_START);
    
    inst->tmr_thread_runs = 0;

    GPSTrace (ZONE_INFO, "gps timer thread destroyed");

#if defined (__ANDROID_VERSION__) &&  __ANDROID_VERSION__ < ANDROID_VER_2_3
    return NULL;
#else
    return;
#endif

}

/**
 *
 */
static void stop_timer_thread (GpsInst*  inst) {

    GPSTrace (ZONE_FUNC, "-> %s:", __FUNCTION__);

    do {
        GPSTrace (ZONE_INFO, "gps timer thread stopping");
        msleep(100);
    } while(inst->tmr_thread_runs == 1);
    GPSTrace (ZONE_FUNC, "<- %s:", __FUNCTION__);
}


/**
 *
 * this is the main thread, it waits for commands from gps_state_start/stop and,
 * when started, messages from the GPS driver. these are simple NMEA sentences
 * that must be parsed to be converted into GPS fixes sent to the framework
 */
#if defined (__ANDROID_VERSION__) &&  __ANDROID_VERSION__ < ANDROID_VER_2_3
  void *
#else
  void
#endif
gps_cmd_thread( void*  arg )
{
    GpsInst*    inst = (GpsInst*) arg;
    NmeaReader  *reader;
    int         epoll_fd   = epoll_create(2);
    int         started    = 0;
    int         control_fd = inst->control[1];

    reader = &inst->reader;

    // register control file descriptors for polling
    epoll_register( epoll_fd, control_fd );

    GPSTrace (ZONE_INFO, "gps thread running");

    GPS_STATUS_CB(inst->callbacks, GPS_STATUS_ENGINE_ON);

    // now loop
    for (;;) {
        struct epoll_event   events[2];
        int    ne, nevents;

        GPSTrace (ZONE_INFO, "gps thread wait for events");
        nevents = epoll_wait( epoll_fd, events, 2, -1 );
        if (nevents < 0) {
            if (errno != EINTR)
                GPSTrace (ZONE_ERROR, "epoll_wait() unexpected error: %s", strerror(errno));
            continue;
        }
        GPSTrace (ZONE_INFO, "gps thread received %d events", nevents);
        for (ne = 0; ne < nevents; ne++) {
            int  fd = events[ne].data.fd;

            // data from CMD port
            if (fd == control_fd) {
                char  cmd = 255;
                int   ret;

                GPSTrace (ZONE_INFO, "gps control fd event");

                if ((events[ne].events & (EPOLLERR|EPOLLHUP)) != 0) {
                    GPSTrace (ZONE_ERROR, "%s: fatal error -> EPOLLERR or EPOLLHUP after epoll_wait() for control_fd(%d)!?", __FUNCTION__, events[ne].data.fd);
                    if (started) {
                        GPSTrace (ZONE_INFO, "gps thread stopping");
                        started = 0;
                        epoll_deregister( epoll_fd, inst->fd );
                        close (inst->fd);
                        inst->fd = -1;
                        inst->state = STATE_QUIT;
                        stop_timer_thread(inst);
                        GPS_STATUS_CB(inst->callbacks, GPS_STATUS_SESSION_END);
                    }

                    goto Exit;
                }

                // read control cmd
                do {
                    ret = read( fd, &cmd, 1 );
                } while (ret < 0 && errno == EINTR);


                switch (cmd) {
                  case CMD_QUIT:
                      GPSTrace (ZONE_INFO, "gps thread cmd CMD_QUIT");
                      goto Exit;

                  case CMD_START:
                      GPSTrace (ZONE_INFO, "gps thread cmd CMD_START");
                      if (!started) {
                          nmea_reader_init( reader );

                          inst->state = STATE_START;
                          started = 1;
                          gps_OpenGPSPort(inst);

                          GPS_STATUS_CB(inst->callbacks, GPS_STATUS_SESSION_BEGIN);
#if defined (__ANDROID_VERSION__) &&  __ANDROID_VERSION__ < ANDROID_VER_2_3
                          if ( pthread_create( &inst->tmr_thread, NULL, gps_timer_thread, inst ) != 0 ) {
#else
                          if ( (inst->tmr_thread = inst->callbacks.create_thread_cb ("gps_timer_thread", gps_timer_thread, inst)) == 0 ) {
#endif
                              GPSTrace (ZONE_ERROR, "%s: fatal error -> could not create gps timer thread: %s", __FUNCTION__, strerror(errno));
                          } else {
                              GPSTrace (ZONE_INFO, "%s: tmr_thread = %d", __FUNCTION__, inst->tmr_thread);
                          }
                      }
                      break;

                  case CMD_STOP:
                      GPSTrace (ZONE_INFO, "gps thread cmd CMD_STOP");
                      if (started) {
                          started = 0;
                          inst->state = STATE_INIT;

                          // if exits
                          pthread_join (inst->openport_thread, NULL);

                          misc_StopGPS ();
                          epoll_deregister( epoll_fd, inst->fd );
                          inst->fd = misc_CloseGPSPort(inst->fd);
                          stop_timer_thread(inst);
                          GPS_STATUS_CB(inst->callbacks, GPS_STATUS_SESSION_END);
                      }
                      break;

                    case CMD_GPS_READ:
                      GPSTrace (ZONE_INFO, "gps thread cmd CMD_GPS_READ");
                      if (started) {
                          if (inst->fd >= 0) {
                              epoll_register( epoll_fd, inst->fd );
                          }
                      }
                      break;
                      
                  default:
                      GPSTrace (ZONE_ERROR, "%s: unknown cmd %d", __FUNCTION__, cmd);
                      break;
                }
            }

            // data from GPS port
            else if (fd == inst->fd) {
                if ((events[ne].events & (EPOLLERR|EPOLLHUP)) != 0) {
                    // unexpected close from gps device
                      GPSTrace (ZONE_ERROR, "%s: unexpected close from gps device (%d) -> EPOLLERR or EPOLLHUP after epoll_wait()!?", __FUNCTION__, events[ne].data.fd);
                      if (started) {
                          misc_StopGPS ();
                          epoll_deregister( epoll_fd, inst->fd );
                          inst->fd = misc_CloseGPSPort(inst->fd);

                          // reopen GPS Port
                          gps_OpenGPSPort(inst);
                      }
                } else {
                    // read data from gps device
                    char buf[512];
                    int  num, ret = 0;

                    do {
                        ret = read( fd, buf, sizeof(buf) );
                    } while (ret < 0 && errno == EINTR);

                    if (ret > 0) {
                        for (num = 0; num < ret; num++) {
                            if (0 == nmea_reader_addc( reader, buf[num] )) {

                                if (0 == nmea_reader_parse( reader )) {

                                    GPS_STATE_LOCK_FIX(inst);
                                    if (  !inst->first_fix
                                        && inst->state == STATE_INIT
                                        && (reader->fix.flags & GPS_LOCATION_HAS_LAT_LONG)) {

                                        if (inst->callbacks.location_cb) {
                                            GPSTrace (ZONE_INFO, "%s: report location", __FUNCTION__);
                                            inst->callbacks.location_cb( &reader->fix );
                                            reader->fix.flags = 0;
                                        }
                                        inst->first_fix = 1;
                                    }
                                    GPS_STATE_UNLOCK_FIX(inst);
                                }
                                // reset nmea buffer
                                reader->pos = 0;
                            }
                        }
                    } else {
                        GPSTrace (ZONE_ERROR, "gps fd read error");
                    }
                }
            }
            else {
                GPSTrace (ZONE_ERROR, "epoll_wait() returned unkown fd %d ?", fd);
            }
        }
    }
Exit:
    GPS_STATUS_CB(inst->callbacks, GPS_STATUS_ENGINE_OFF);

#if defined (__ANDROID_VERSION__) &&  __ANDROID_VERSION__ < ANDROID_VER_2_3
    return NULL;
#else
    return;
#endif

}

/*****************************************************************/
/*****************************************************************/
/*****                                                       *****/
/*****       C O N N E C T I O N   S T A T E                 *****/
/*****                                                       *****/
/*****************************************************************/
/*****************************************************************/
/**
 *
 */
static void
gps_state_done( GpsInst*  inst )
{
    // tell the thread to quit, and wait for it
    char   cmd = CMD_QUIT;

    GPSTrace (ZONE_FUNC, "-> %s: send CMD_QUIT", __FUNCTION__);

    inst->state = STATE_QUIT;
    write( inst->control[0], &cmd, 1 );
    pthread_join(inst->cmd_thread, NULL);

    // close the control socket pair
    close( inst->control[0] ); inst->control[0] = -1;
    close( inst->control[1] ); inst->control[1] = -1;

    // close connection to the GPS device
    misc_StopGPS ();
    inst->fd = misc_CloseGPSPort( inst->fd);

    pthread_join (inst->openport_thread, NULL);
    stop_timer_thread(inst);

    sem_destroy(&inst->fix_sem);
    memset(inst, 0, sizeof(*inst));

    GPSTrace (ZONE_FUNC, "<- %s:", __FUNCTION__);
}

/**
 *
 */
static void
gps_state_init( GpsInst*  inst )
{
    struct sigevent tmr_event;

    GPSTrace (ZONE_FUNC, "-> %s:", __FUNCTION__);

    inst->state      = STATE_INIT;
    inst->GpsOpenPolling = 0;
    inst->control[0] = -1;
    inst->control[1] = -1;
    inst->fd         = -1;
    inst->fix_freq   = 1000;
    inst->first_fix  = 0;

    if (sem_init(&inst->fix_sem, 0, 1) != 0) {
      GPSTrace (ZONE_ERROR, "gps semaphore initialization failed! errno = %d", errno);
      goto Fail;
    }

    if ( socketpair( AF_LOCAL, SOCK_STREAM, 0, inst->control ) < 0 ) {
        GPSTrace (ZONE_ERROR, "could not create thread control socket pair: %s", strerror(errno));
        goto Fail;
    }
#if defined (__ANDROID_VERSION__) &&  __ANDROID_VERSION__ < ANDROID_VER_2_3
    if ( pthread_create( &inst->cmd_thread, NULL, gps_cmd_thread, inst ) != 0 ) {
#else
    if ( (inst->cmd_thread = inst->callbacks.create_thread_cb ("gps_cmd_thread", gps_cmd_thread, inst)) == 0 ) {
#endif
        GPSTrace (ZONE_ERROR, "could not create gps thread: %s", strerror(errno));
        goto Fail;
    }


    GPSTrace (ZONE_FUNC, "<- %s: gps state initialized", __FUNCTION__);
    return;

Fail:
    gps_state_done( inst );
    GPSTrace (ZONE_FUNC, "<- %s: (Fail)", __FUNCTION__);
}

/**
 *
 */
static void
gps_state_start( GpsInst*  inst )
{
    char  cmd = CMD_START;
    int   ret;

    GPSTrace (ZONE_FUNC, "-> %s: send CMD_START", __FUNCTION__);

    do {
      ret = write( inst->control[0], &cmd, 1 );
    } while (ret < 0 && errno == EINTR);

    if (ret != 1) {
        GPSTrace (ZONE_ERROR, "%s: could not send CMD_START command: ret=%d: %s",
          __FUNCTION__, ret, strerror(errno));
    }
    GPSTrace (ZONE_FUNC, "<- %s:" , __FUNCTION__);
}

/**
 *
 */
static void
gps_state_stop( GpsInst*  inst )
{
    char  cmd = CMD_STOP;
    int   ret;

    GPSTrace (ZONE_FUNC, "-> %s: send CMD_STOP", __FUNCTION__);

    do {
        ret=write( inst->control[0], &cmd, 1 );
    } while (ret < 0 && errno == EINTR);

    if (ret != 1) {
        GPSTrace (ZONE_ERROR, "%s: could not send CMD_START command: ret=%d: %s",
          __FUNCTION__, ret, strerror(errno));
    }
    
    GPSTrace (ZONE_FUNC, "<- %s:" , __FUNCTION__);
}



/*****************************************************************/
/*****************************************************************/
/*****                                                       *****/
/*****       I N T E R F A C E                               *****/
/*****                                                       *****/
/*****************************************************************/
/*****************************************************************/
/**
 *
 */
static int
cwm_gps_init(GpsCallbacks* callbacks)
{
    GpsInst*  inst = g_pGpsInst;
    
    GPSTrace (ZONE_GPS_INT, "%s:", __FUNCTION__);

    inst->callbacks = *callbacks;

    if (inst->state == STATE_QUIT)
        gps_state_init(inst);

    return 0;
}

/**
 *
 */
static void
cwm_gps_cleanup(void)
{
    GpsInst*  inst = g_pGpsInst;
    
    GPSTrace (ZONE_GPS_INT, "%s:", __FUNCTION__);

    if (inst->state != STATE_QUIT)
        gps_state_done(inst);
}

/**
 *
 */
static int
cwm_gps_start()
{
    GpsInst*  inst = g_pGpsInst;
    
    GPSTrace (ZONE_GPS_INT, "%s:", __FUNCTION__);
    
    inst->first_fix  = 0;

    if (inst->state == STATE_QUIT) {
        GPSTrace (ZONE_ERROR, "%s: called with uninitialized state !!", __FUNCTION__);
        return -1;
    }

    gps_state_start(inst);
    return 0;
}

/**
 *
 */
static int
cwm_gps_stop()
{
    GpsInst*  inst = g_pGpsInst;

    GPSTrace (ZONE_GPS_INT, "%s:", __FUNCTION__);

    if (inst->state == STATE_QUIT) {
        GPSTrace (ZONE_ERROR, "%s: called with uninitialized state !!", __FUNCTION__);
        return -1;
    }
    
    gps_state_stop(inst);
    return 0;
}

/**
 *
 */
static int
cwm_gps_inject_time(GpsUtcTime time, int64_t timeReference, int uncertainty)
{
    GPSTrace (ZONE_GPS_INT, "%s: do nothing", __FUNCTION__);
    return 0;
}

/**
 *
 */
static int
cwm_gps_inject_location(double latitude, double longitude, float accuracy)
{
    GPSTrace (ZONE_GPS_INT, "%s: do nothing", __FUNCTION__);
    return 0;
}

/**
 *
 */
static void
cwm_gps_delete_aiding_data(GpsAidingData flags)
{
    GPSTrace (ZONE_GPS_INT, "%s: do nothing", __FUNCTION__);
}

#if defined (__ANDROID_VERSION__) &&  __ANDROID_VERSION__ < ANDROID_VER_2_3
  #include <hardware_legacy/gps.h>
#else
  #include <hardware/gps.h>
#endif

static int cwm_gps_set_position_mode(
    GpsPositionMode mode,
#if defined (__ANDROID_VERSION__) &&  __ANDROID_VERSION__ < ANDROID_VER_2_3
    int min_interval)
#else
    GpsPositionRecurrence recurrence,
    uint32_t min_interval,
    uint32_t preferred_accuracy,
    uint32_t preferred_time)
#endif
{
    GpsInst*  inst = g_pGpsInst;
    
    GPSTrace (ZONE_GPS_INT, "%s:", __FUNCTION__);
    GPSTrace (ZONE_INFO, "    mode = %d", mode);
    GPSTrace (ZONE_INFO, "    min_interval = %d", (uint32_t)min_interval);
    
#if defined (__ANDROID_VERSION__) &&  __ANDROID_VERSION__ < ANDROID_VER_2_3
    min_interval = min_interval * 1000;
#else
    GPSTrace (ZONE_INFO, "    recurrence = %d", recurrence);
    GPSTrace (ZONE_INFO, "    preferred_accuracy = %d", preferred_accuracy);
    GPSTrace (ZONE_INFO, "    preferred_time = %d", preferred_time);
#endif

    if (inst->state == STATE_QUIT) {
        GPSTrace (ZONE_ERROR, "%s: called with uninitialized state !!", __FUNCTION__);
        return -1;
    }

    inst->fix_freq = (uint32_t)min_interval;

    return 0;
}

/**
 *
 */
static const void*
cwm_gps_get_extension(const char* name)
{
    GPSTrace (ZONE_GPS_INT, "%s: do nothing", __FUNCTION__);
    // no extensions supported
    return NULL;
}

/**
 *
 */
static const GpsInterface  cwmGpsInterface = {
#if defined (__ANDROID_VERSION__) &&  __ANDROID_VERSION__ < ANDROID_VER_2_3
#else
    sizeof(GpsInterface),
#endif
    cwm_gps_init,
    cwm_gps_start,
    cwm_gps_stop,
    cwm_gps_cleanup,
    cwm_gps_inject_time,
    cwm_gps_inject_location,
    cwm_gps_delete_aiding_data,
    cwm_gps_set_position_mode,
    cwm_gps_get_extension,
};

/**
 *
 */
#if defined (__ANDROID_VERSION__) &&  __ANDROID_VERSION__ < ANDROID_VER_2_3
const GpsInterface* gps_get_hardware_interface()
#else
const GpsInterface* gps_get_interface(struct gps_device_t* dev)
#endif
{
    GPSTrace (ZONE_GPS_INT, "%s:", __FUNCTION__);
    misc_VersionInfo();

    return &cwmGpsInterface;
}

#if defined (__ANDROID_VERSION__) &&  __ANDROID_VERSION__ < ANDROID_VER_2_3
#else

static int open_gps(const struct hw_module_t* module, char const* name,
        struct hw_device_t** device)
{
    struct gps_device_t *dev = malloc(sizeof(struct gps_device_t));
    memset(dev, 0, sizeof(*dev));

    GPSTrace (ZONE_GPS_INT, "%s:", __FUNCTION__);
    
    misc_VersionInfo();

    dev->common.tag = HARDWARE_DEVICE_TAG;
    dev->common.version = 0;
    dev->common.module = (struct hw_module_t*)module;
//    dev->common.close = (int (*)(struct hw_device_t*))close_lights;
    dev->get_gps_interface = gps_get_interface;

    *device = (struct hw_device_t*)dev;
    return 0;
}


/**
 *
 */
static struct hw_module_methods_t gps_module_methods = {
    .open = open_gps
};

/**
 *
 */
const struct hw_module_t HAL_MODULE_INFO_SYM = {
    .tag = HARDWARE_MODULE_TAG,
    .version_major = GPS_VERSION_1,
    .version_minor = GPS_VERSION_2,
    .id = GPS_HARDWARE_MODULE_ID,
    .name = "Cinterion GPS Module",
    .author = "The Android Open Source Project",
    .methods = &gps_module_methods,
};
#endif
