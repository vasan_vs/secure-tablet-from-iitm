#include <linux/i2c-dev.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>

#define SLAVE_ADDR 0x4c

void help(void);
void write_mma7660fc(int fd, int reg, int value);
int get_values(int fd, int reg);
int conv_axis_values(int val);

void help(void)
{

	printf("Please Enter the command as follows:\n\ncommand : accel [options]\n\n[options]:\n\n1. a - Prints the axis values\n2. s - Prints whether the board is shaken or not\n3. h - Prints the help for this command\n\n");
}


/*This function is used to write data in to the registers of mma7660fc*/	
void write_mma7660fc(int fd, int reg, int value)
{
	int res;
	
	if ((i2c_smbus_read_byte_data(fd, reg))!=value)
		res=i2c_smbus_write_byte_data(fd, reg, value);
	else
		res=1;

	/*Return error when the read i2c client device fails*/
	if (res<0)
	{	
		perror ("Failed to write to slave:");
		exit(EXIT_FAILURE);
	}
}	

/* Function get_values is used to read values from the registers of mma7660fc*/
int get_values(int fd, int reg)
{
	int val;
	
	/* Read the register values, retry is Alert (7th) bit is 1, which denoted that the value in the register is being updated"*/
	do
	{

		val=i2c_smbus_read_byte_data(fd, reg);

	}while(val&64);		/* Check the 7th bit for Alert sign*/


	if (val<0)
        {
                perror ("Failed to read from slave:");
                exit(EXIT_FAILURE);
        }
	
	return val;
}

int conv_axis_values(int val)
{
	
	/* If the 6th bit is 1, the value is negative, when the 6th bit is 0, the value is positive*/
	if (val&32)

		/* The first three bits val[8], val[7] ( Alert bit ), val[6] ( Sign bit ), are being masked and value is obtained*/

		return -(val&31);	/* The value is negative when Alert bit is set*/ 

	else
		return (val&31);	/* The value is positive when Alert bit is set*/
}
	
	
		
int main(int argc, char *argv[])

{

	int fd, x, y, z, tilt;

	fd = open("/dev/i2c-1", O_RDWR);	/* The device special file for I2C in calixto AM335x is /dev/i2c-1*/

	/*The slave address is given to the IOCTL of the adapter to open mma7660fc device*/
	if (ioctl(fd, I2C_SLAVE, SLAVE_ADDR) <0)
	{
		fprintf (stderr, "Failed to set slave address: %m\n");
		return 1;
	}

	/*The slave address is given to the IOCTL of the adapter to open mma7660fc device
	To know more about the registers and the meaning of their values, please visit
	http://cache.freescale.com/files/sensors/doc/data_sheet/MMA7660FC.pdf*/


	write_mma7660fc(fd, 0x07, 0xe9);	/* 0x07 is the mode register*/
	write_mma7660fc(fd, 0x04, 0x03);	/*0x04 is used to set sampling rate status*/
	write_mma7660fc(fd, 0x06, 0xe7);	/*0x06 used to setup interupt and Shake parameters*/
	write_mma7660fc(fd, 0x08, 0xe0);	/*0x08 used to set auto-wake, auto-sleep and samples per second*/
	write_mma7660fc(fd, 0x09, 0x00);	/*0x09 used to set Tap/pulse detection register*/

	if (argc!=2)
	{
		help();
		return 1;
	}
	
	switch(argv[1][0]){

		case 'a':
		
			/* Get the values from x,y,z axis and print them on the screen*/

			x=get_values(fd, 0x00);
			y=get_values(fd, 0x01);
			z=get_values(fd, 0x02);
			
			printf ("\n%d\t%d\t%d\n",conv_axis_values(x),conv_axis_values(y),conv_axis_values(z));
			break;
		
		case 's':
			
			/* Find whether a shake is detected*/

			tilt=get_values(fd, 0x03);
			
			/* The Last bit of TILT STATUS register is checked for Shake detection : TILT[8]*/
		
			if (tilt&128)
				printf ("\nSHAKEN\n");
			else
				printf ("\nSTATIC\n");
			break;

		default:
			help();
			break;
	}
	return 1;
}



