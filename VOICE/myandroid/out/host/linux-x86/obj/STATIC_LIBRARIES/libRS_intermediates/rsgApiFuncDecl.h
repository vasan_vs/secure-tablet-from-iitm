/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

void rsContextDestroy (RsContext rsc);
RsMessageToClientType rsContextGetMessage (RsContext rsc, void * data, size_t data_length, size_t * receiveLen, size_t receiveLen_length, uint32_t * usrID, size_t usrID_length);
RsMessageToClientType rsContextPeekMessage (RsContext rsc, size_t * receiveLen, size_t receiveLen_length, uint32_t * usrID, size_t usrID_length);
void rsContextInitToClient (RsContext rsc);
void rsContextDeinitToClient (RsContext rsc);
RsType rsTypeCreate (RsContext rsc, RsElement e, uint32_t dimX, uint32_t dimY, uint32_t dimZ, bool mips, bool faces);
RsAllocation rsAllocationCreateTyped (RsContext rsc, RsType vtype, RsAllocationMipmapControl mips, uint32_t usages);
RsAllocation rsAllocationCreateFromBitmap (RsContext rsc, RsType vtype, RsAllocationMipmapControl mips, const void * data, size_t data_length, uint32_t usages);
RsAllocation rsAllocationCubeCreateFromBitmap (RsContext rsc, RsType vtype, RsAllocationMipmapControl mips, const void * data, size_t data_length, uint32_t usages);
void rsContextFinish (RsContext rsc);
void rsContextBindRootScript (RsContext rsc, RsScript sampler);
void rsContextBindProgramStore (RsContext rsc, RsProgramStore pgm);
void rsContextBindProgramFragment (RsContext rsc, RsProgramFragment pgm);
void rsContextBindProgramVertex (RsContext rsc, RsProgramVertex pgm);
void rsContextBindProgramRaster (RsContext rsc, RsProgramRaster pgm);
void rsContextBindFont (RsContext rsc, RsFont pgm);
void rsContextPause (RsContext rsc);
void rsContextResume (RsContext rsc);
void rsContextSetSurface (RsContext rsc, uint32_t width, uint32_t height, RsNativeWindow sur);
void rsContextDump (RsContext rsc, int32_t bits);
void rsContextSetPriority (RsContext rsc, int32_t priority);
void rsContextDestroyWorker (RsContext rsc);
void rsAssignName (RsContext rsc, RsObjectBase obj, const char * name, size_t name_length);
void rsObjDestroy (RsContext rsc, RsAsyncVoidPtr objPtr);
RsElement rsElementCreate (RsContext rsc, RsDataType mType, RsDataKind mKind, bool mNormalized, uint32_t mVectorSize);
RsElement rsElementCreate2 (RsContext rsc, const RsElement * elements, size_t elements_length, const char ** names, size_t names_length_length, const size_t * names_length, const uint32_t * arraySize, size_t arraySize_length);
void rsAllocationCopyToBitmap (RsContext rsc, RsAllocation alloc, void * data, size_t data_length);
void rsAllocation1DData (RsContext rsc, RsAllocation va, uint32_t xoff, uint32_t lod, uint32_t count, const void * data, size_t data_length);
void rsAllocation1DElementData (RsContext rsc, RsAllocation va, uint32_t x, uint32_t lod, const void * data, size_t data_length, uint32_t comp_offset);
void rsAllocation2DData (RsContext rsc, RsAllocation va, uint32_t xoff, uint32_t yoff, uint32_t lod, RsAllocationCubemapFace face, uint32_t w, uint32_t h, const void * data, size_t data_length);
void rsAllocation2DElementData (RsContext rsc, RsAllocation va, uint32_t x, uint32_t y, uint32_t lod, RsAllocationCubemapFace face, const void * data, size_t data_length, uint32_t element_offset);
void rsAllocationGenerateMipmaps (RsContext rsc, RsAllocation va);
void rsAllocationRead (RsContext rsc, RsAllocation va, void * data, size_t data_length);
void rsAllocationSyncAll (RsContext rsc, RsAllocation va, RsAllocationUsageType src);
void rsAllocationResize1D (RsContext rsc, RsAllocation va, uint32_t dimX);
void rsAllocationResize2D (RsContext rsc, RsAllocation va, uint32_t dimX, uint32_t dimY);
void rsAllocationCopy2DRange (RsContext rsc, RsAllocation dest, uint32_t destXoff, uint32_t destYoff, uint32_t destMip, uint32_t destFace, uint32_t width, uint32_t height, RsAllocation src, uint32_t srcXoff, uint32_t srcYoff, uint32_t srcMip, uint32_t srcFace);
RsSampler rsSamplerCreate (RsContext rsc, RsSamplerValue magFilter, RsSamplerValue minFilter, RsSamplerValue wrapS, RsSamplerValue wrapT, RsSamplerValue wrapR, float mAniso);
void rsScriptBindAllocation (RsContext rsc, RsScript vtm, RsAllocation va, uint32_t slot);
void rsScriptSetTimeZone (RsContext rsc, RsScript s, const char * timeZone, size_t timeZone_length);
void rsScriptInvoke (RsContext rsc, RsScript s, uint32_t slot);
void rsScriptInvokeV (RsContext rsc, RsScript s, uint32_t slot, const void * data, size_t data_length);
void rsScriptForEach (RsContext rsc, RsScript s, uint32_t slot, RsAllocation ain, RsAllocation aout, const void * usr, size_t usr_length);
void rsScriptSetVarI (RsContext rsc, RsScript s, uint32_t slot, int value);
void rsScriptSetVarObj (RsContext rsc, RsScript s, uint32_t slot, RsObjectBase value);
void rsScriptSetVarJ (RsContext rsc, RsScript s, uint32_t slot, int64_t value);
void rsScriptSetVarF (RsContext rsc, RsScript s, uint32_t slot, float value);
void rsScriptSetVarD (RsContext rsc, RsScript s, uint32_t slot, double value);
void rsScriptSetVarV (RsContext rsc, RsScript s, uint32_t slot, const void * data, size_t data_length);
RsScript rsScriptCCreate (RsContext rsc, const char * resName, size_t resName_length, const char * cacheDir, size_t cacheDir_length, const char * text, size_t text_length);
RsProgramStore rsProgramStoreCreate (RsContext rsc, bool colorMaskR, bool colorMaskG, bool colorMaskB, bool colorMaskA, bool depthMask, bool ditherEnable, RsBlendSrcFunc srcFunc, RsBlendDstFunc destFunc, RsDepthFunc depthFunc);
RsProgramRaster rsProgramRasterCreate (RsContext rsc, bool pointSprite, RsCullMode cull);
void rsProgramBindConstants (RsContext rsc, RsProgram vp, uint32_t slot, RsAllocation constants);
void rsProgramBindTexture (RsContext rsc, RsProgramFragment pf, uint32_t slot, RsAllocation a);
void rsProgramBindSampler (RsContext rsc, RsProgramFragment pf, uint32_t slot, RsSampler s);
RsProgramFragment rsProgramFragmentCreate (RsContext rsc, const char * shaderText, size_t shaderText_length, const uint32_t * params, size_t params_length);
RsProgramVertex rsProgramVertexCreate (RsContext rsc, const char * shaderText, size_t shaderText_length, const uint32_t * params, size_t params_length);
RsFont rsFontCreateFromFile (RsContext rsc, const char * name, size_t name_length, float fontSize, uint32_t dpi);
RsFont rsFontCreateFromMemory (RsContext rsc, const char * name, size_t name_length, float fontSize, uint32_t dpi, const void * data, size_t data_length);
RsMesh rsMeshCreate (RsContext rsc, RsAllocation * vtx, size_t vtx_length, RsAllocation * idx, size_t idx_length, uint32_t * primType, size_t primType_length);


