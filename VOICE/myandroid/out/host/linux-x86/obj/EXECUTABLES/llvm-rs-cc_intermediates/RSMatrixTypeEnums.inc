ENUM_RS_MATRIX_TYPE(RSMatrix2x2, "rs_matrix2x2", 2)
ENUM_RS_MATRIX_TYPE(RSMatrix3x3, "rs_matrix3x3", 3)
ENUM_RS_MATRIX_TYPE(RSMatrix4x4, "rs_matrix4x4", 4)
#undef ENUM_RS_MATRIX_TYPE
