ENUM_RS_DATA_ELEMENT("rs_pixel_l", PixelL, Unsigned8, true, 1)
ENUM_RS_DATA_ELEMENT("rs_pixel_a", PixelA, Unsigned8, true, 1)
ENUM_RS_DATA_ELEMENT("rs_pixel_la", PixelLA, Unsigned8, true, 2)
ENUM_RS_DATA_ELEMENT("rs_pixel_rgb", PixelRGB, Unsigned8, true, 3)
ENUM_RS_DATA_ELEMENT("rs_pixel_rgba", PixelRGB, Unsigned8, true, 4)
ENUM_RS_DATA_ELEMENT("rs_pixel_rgb565", PixelRGB, Unsigned8, true, 3)
ENUM_RS_DATA_ELEMENT("rs_pixel_rgb5551", PixelRGBA, Unsigned8, true, 4)
ENUM_RS_DATA_ELEMENT("rs_pixel_rgb4444", PixelRGBA, Unsigned8, true, 4)
#undef ENUM_RS_DATA_ELEMENT
