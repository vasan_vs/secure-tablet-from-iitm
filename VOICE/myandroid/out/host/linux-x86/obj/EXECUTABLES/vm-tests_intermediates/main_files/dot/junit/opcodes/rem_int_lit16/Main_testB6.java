//autogenerated by util.build.BuildDalvikSuite, do not change
package dot.junit.opcodes.rem_int_lit16;
import dot.junit.opcodes.rem_int_lit16.d.*;
import dot.junit.*;
public class Main_testB6 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_rem_int_lit16_8 t = new T_rem_int_lit16_8();
        assertEquals(1, t.run(1));
    }
}
