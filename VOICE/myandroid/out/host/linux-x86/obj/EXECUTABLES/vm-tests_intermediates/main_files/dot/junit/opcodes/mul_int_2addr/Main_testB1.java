//autogenerated by util.build.BuildDalvikSuite, do not change
package dot.junit.opcodes.mul_int_2addr;
import dot.junit.opcodes.mul_int_2addr.d.*;
import dot.junit.*;
public class Main_testB1 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_mul_int_2addr_1 t = new T_mul_int_2addr_1();
        assertEquals(0, t.run(0, Integer.MAX_VALUE));
    }
}
