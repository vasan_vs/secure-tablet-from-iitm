//autogenerated by util.build.BuildDalvikSuite, do not change
package dot.junit.opcodes.div_float_2addr;
import dot.junit.opcodes.div_float_2addr.d.*;
import dot.junit.*;
public class Main_testN3 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_div_float_2addr_1 t = new T_div_float_2addr_1();
        assertEquals(-1.162963f, t.run(-3.14f, 2.7f));
    }
}
