//autogenerated by util.build.BuildDalvikSuite, do not change
package dot.junit.opcodes.float_to_int;
import dot.junit.opcodes.float_to_int.d.*;
import dot.junit.*;
public class Main_testB2 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_float_to_int_1 t = new T_float_to_int_1();
        assertEquals(Integer.MAX_VALUE, t.run(Float.MAX_VALUE));
    }
}
