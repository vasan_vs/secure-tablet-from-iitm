//autogenerated by util.build.BuildDalvikSuite, do not change
package dot.junit.opcodes.double_to_int;
import dot.junit.opcodes.double_to_int.d.*;
import dot.junit.*;
public class Main_testN3 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_double_to_int_1 t = new T_double_to_int_1();
        assertEquals(-1, t.run(-1d));
    }
}
