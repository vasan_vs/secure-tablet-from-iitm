//autogenerated by util.build.BuildDalvikSuite, do not change
package dot.junit.opcodes.add_int_2addr;
import dot.junit.opcodes.add_int_2addr.d.*;
import dot.junit.*;
public class Main_testN4 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_add_int_2addr_1 t = new T_add_int_2addr_1();
        assertEquals(-2147483647, t.run(0, -2147483647));
    }
}
