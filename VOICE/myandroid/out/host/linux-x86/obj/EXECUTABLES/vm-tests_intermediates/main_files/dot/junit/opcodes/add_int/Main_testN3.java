//autogenerated by util.build.BuildDalvikSuite, do not change
package dot.junit.opcodes.add_int;
import dot.junit.opcodes.add_int.d.*;
import dot.junit.*;
public class Main_testN3 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_add_int_1 t = new T_add_int_1();
        assertEquals(-65536, t.run(0, -65536));
    }
}
