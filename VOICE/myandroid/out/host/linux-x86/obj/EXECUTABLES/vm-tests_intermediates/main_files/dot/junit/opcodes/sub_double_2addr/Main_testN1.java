//autogenerated by util.build.BuildDalvikSuite, do not change
package dot.junit.opcodes.sub_double_2addr;
import dot.junit.opcodes.sub_double_2addr.d.*;
import dot.junit.*;
public class Main_testN1 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_sub_double_2addr_1 t = new T_sub_double_2addr_1();
        assertEquals(-0.43999999999999995d, t.run(2.7d, 3.14d));
    }
}
