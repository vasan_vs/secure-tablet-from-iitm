//autogenerated by util.build.BuildDalvikSuite, do not change
package dot.junit.opcodes.sput_char;
import dot.junit.opcodes.sput_char.d.*;
import dot.junit.*;
public class Main_testN1 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_sput_char_1 t = new T_sput_char_1();
        assertEquals(0, T_sput_char_1.st_i1);
        t.run();
        assertEquals(77, T_sput_char_1.st_i1);
    }
}
