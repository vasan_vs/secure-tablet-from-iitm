//autogenerated by util.build.BuildDalvikSuite, do not change
package dot.junit.opcodes.iput_short;
import dot.junit.opcodes.iput_short.d.*;
import dot.junit.*;
public class Main_testN4 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        //@uses dot.junit.opcodes.iput_short.d.T_iput_short_1
        //@uses dot.junit.opcodes.iput_short.d.T_iput_short_14
        T_iput_short_14 t = new T_iput_short_14();
        assertEquals(0, t.getProtectedField());
        t.run();
        assertEquals(77, t.getProtectedField());
    }
}
