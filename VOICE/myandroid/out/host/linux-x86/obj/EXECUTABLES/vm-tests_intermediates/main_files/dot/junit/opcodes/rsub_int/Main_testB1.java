//autogenerated by util.build.BuildDalvikSuite, do not change
package dot.junit.opcodes.rsub_int;
import dot.junit.opcodes.rsub_int.d.*;
import dot.junit.*;
public class Main_testB1 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_rsub_int_3 t = new T_rsub_int_3();
        assertEquals(-Integer.MAX_VALUE, t.run(Integer.MAX_VALUE));
        assertEquals(-Short.MAX_VALUE, t.run(Short.MAX_VALUE));
    }
}
