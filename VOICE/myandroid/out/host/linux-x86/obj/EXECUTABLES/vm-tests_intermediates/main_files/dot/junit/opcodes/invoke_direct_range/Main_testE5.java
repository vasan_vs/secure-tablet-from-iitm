//autogenerated by util.build.BuildDalvikSuite, do not change
package dot.junit.opcodes.invoke_direct_range;
import dot.junit.opcodes.invoke_direct_range.d.*;
import dot.junit.*;
public class Main_testE5 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_invoke_direct_range_9 t = new T_invoke_direct_range_9();
        try {
            assertEquals(5, t.run());
            fail("expected UnsatisfiedLinkError");
        } catch (UnsatisfiedLinkError e) {
            // expected
        }
    }
}
