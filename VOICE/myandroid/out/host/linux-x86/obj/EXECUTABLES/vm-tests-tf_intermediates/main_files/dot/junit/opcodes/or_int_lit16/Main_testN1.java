//Autogenerated code by util.build.BuildDalvikSuite; do not edit.
package dot.junit.opcodes.or_int_lit16;
import dot.junit.opcodes.or_int_lit16.d.*;
import dot.junit.*;
public class Main_testN1 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_or_int_lit16_1 t = new T_or_int_lit16_1();
        assertEquals(15, t.run(15));
    }
}
