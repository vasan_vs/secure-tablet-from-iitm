//Autogenerated code by util.build.BuildDalvikSuite; do not edit.
package dot.junit.opcodes.iget_char;
import dot.junit.opcodes.iget_char.d.*;
import dot.junit.*;
public class Main_testN3 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        //@uses dot.junit.opcodes.iget_char.d.T_iget_char_1
        //@uses dot.junit.opcodes.iget_char.d.T_iget_char_11
        T_iget_char_11 t = new T_iget_char_11();
        assertEquals(77, t.run());
    }
}
