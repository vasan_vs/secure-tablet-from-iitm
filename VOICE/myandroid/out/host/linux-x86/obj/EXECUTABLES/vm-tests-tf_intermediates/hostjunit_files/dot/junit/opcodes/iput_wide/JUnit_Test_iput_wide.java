//Autogenerated code by util.build.BuildDalvikSuite; do not edit.
package dot.junit.opcodes.iput_wide;
import java.io.IOException;
import com.android.tradefed.testtype.DeviceTestCase;

public class JUnit_Test_iput_wide extends DeviceTestCase {
public void testE2() throws Exception {
    String res = getDevice().executeShellCommand("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/Main_testE2.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/d/T_iput_wide_13.jar dot.junit.opcodes.iput_wide.Main_testE2");
// A sucessful adb shell command returns an empty string.
assertEquals("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/Main_testE2.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/d/T_iput_wide_13.jar dot.junit.opcodes.iput_wide.Main_testE2", "", res);}

public void testE5() throws Exception {
    String res = getDevice().executeShellCommand("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/Main_testE5.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/d/T_iput_wide_11.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/TestStubs.jar dot.junit.opcodes.iput_wide.Main_testE5");
// A sucessful adb shell command returns an empty string.
assertEquals("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/Main_testE5.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/d/T_iput_wide_11.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/TestStubs.jar dot.junit.opcodes.iput_wide.Main_testE5", "", res);}

public void testN1() throws Exception {
    String res = getDevice().executeShellCommand("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/Main_testN1.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/d/T_iput_wide_1.jar dot.junit.opcodes.iput_wide.Main_testN1");
// A sucessful adb shell command returns an empty string.
assertEquals("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/Main_testN1.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/d/T_iput_wide_1.jar dot.junit.opcodes.iput_wide.Main_testN1", "", res);}

public void testN2() throws Exception {
    String res = getDevice().executeShellCommand("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/Main_testN2.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/d/T_iput_wide_5.jar dot.junit.opcodes.iput_wide.Main_testN2");
// A sucessful adb shell command returns an empty string.
assertEquals("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/Main_testN2.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/d/T_iput_wide_5.jar dot.junit.opcodes.iput_wide.Main_testN2", "", res);}

public void testN3() throws Exception {
    String res = getDevice().executeShellCommand("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/Main_testN3.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/d/T_iput_wide_12.jar dot.junit.opcodes.iput_wide.Main_testN3");
// A sucessful adb shell command returns an empty string.
assertEquals("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/Main_testN3.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/d/T_iput_wide_12.jar dot.junit.opcodes.iput_wide.Main_testN3", "", res);}

public void testN4() throws Exception {
    String res = getDevice().executeShellCommand("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/Main_testN4.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/d/T_iput_wide_1.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/d/T_iput_wide_14.jar dot.junit.opcodes.iput_wide.Main_testN4");
// A sucessful adb shell command returns an empty string.
assertEquals("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/Main_testN4.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/d/T_iput_wide_1.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/d/T_iput_wide_14.jar dot.junit.opcodes.iput_wide.Main_testN4", "", res);}

public void testVFE1() throws Exception {
    String res = getDevice().executeShellCommand("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/Main_testVFE1.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/d/T_iput_wide_3.jar dot.junit.opcodes.iput_wide.Main_testVFE1");
// A sucessful adb shell command returns an empty string.
assertEquals("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/Main_testVFE1.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/d/T_iput_wide_3.jar dot.junit.opcodes.iput_wide.Main_testVFE1", "", res);}

public void testVFE10() throws Exception {
    String res = getDevice().executeShellCommand("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/Main_testVFE10.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/d/T_iput_wide_9.jar dot.junit.opcodes.iput_wide.Main_testVFE10");
// A sucessful adb shell command returns an empty string.
assertEquals("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/Main_testVFE10.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/d/T_iput_wide_9.jar dot.junit.opcodes.iput_wide.Main_testVFE10", "", res);}

public void testVFE11() throws Exception {
    String res = getDevice().executeShellCommand("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/Main_testVFE11.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/d/T_iput_wide_10.jar dot.junit.opcodes.iput_wide.Main_testVFE11");
// A sucessful adb shell command returns an empty string.
assertEquals("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/Main_testVFE11.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/d/T_iput_wide_10.jar dot.junit.opcodes.iput_wide.Main_testVFE11", "", res);}

public void testVFE12() throws Exception {
    String res = getDevice().executeShellCommand("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/Main_testVFE12.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/d/T_iput_wide_15.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/d/T_iput_wide_1.jar dot.junit.opcodes.iput_wide.Main_testVFE12");
// A sucessful adb shell command returns an empty string.
assertEquals("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/Main_testVFE12.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/d/T_iput_wide_15.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/d/T_iput_wide_1.jar dot.junit.opcodes.iput_wide.Main_testVFE12", "", res);}

public void testVFE13() throws Exception {
    String res = getDevice().executeShellCommand("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/Main_testVFE13.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/d/T_iput_wide_2.jar dot.junit.opcodes.iput_wide.Main_testVFE13");
// A sucessful adb shell command returns an empty string.
assertEquals("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/Main_testVFE13.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/d/T_iput_wide_2.jar dot.junit.opcodes.iput_wide.Main_testVFE13", "", res);}

public void testVFE14() throws Exception {
    String res = getDevice().executeShellCommand("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/Main_testVFE14.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/d/T_iput_wide_20.jar dot.junit.opcodes.iput_wide.Main_testVFE14");
// A sucessful adb shell command returns an empty string.
assertEquals("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/Main_testVFE14.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/d/T_iput_wide_20.jar dot.junit.opcodes.iput_wide.Main_testVFE14", "", res);}

public void testVFE15() throws Exception {
    String res = getDevice().executeShellCommand("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/Main_testVFE15.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/d/T_iput_wide_21.jar dot.junit.opcodes.iput_wide.Main_testVFE15");
// A sucessful adb shell command returns an empty string.
assertEquals("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/Main_testVFE15.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/d/T_iput_wide_21.jar dot.junit.opcodes.iput_wide.Main_testVFE15", "", res);}

public void testVFE16() throws Exception {
    String res = getDevice().executeShellCommand("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/Main_testVFE16.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/d/T_iput_wide_22.jar dot.junit.opcodes.iput_wide.Main_testVFE16");
// A sucessful adb shell command returns an empty string.
assertEquals("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/Main_testVFE16.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/d/T_iput_wide_22.jar dot.junit.opcodes.iput_wide.Main_testVFE16", "", res);}

public void testVFE17() throws Exception {
    String res = getDevice().executeShellCommand("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/Main_testVFE17.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/d/T_iput_wide_23.jar dot.junit.opcodes.iput_wide.Main_testVFE17");
// A sucessful adb shell command returns an empty string.
assertEquals("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/Main_testVFE17.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/d/T_iput_wide_23.jar dot.junit.opcodes.iput_wide.Main_testVFE17", "", res);}

public void testVFE18() throws Exception {
    String res = getDevice().executeShellCommand("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/Main_testVFE18.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/d/T_iput_wide_24.jar dot.junit.opcodes.iput_wide.Main_testVFE18");
// A sucessful adb shell command returns an empty string.
assertEquals("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/Main_testVFE18.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/d/T_iput_wide_24.jar dot.junit.opcodes.iput_wide.Main_testVFE18", "", res);}

public void testVFE2() throws Exception {
    String res = getDevice().executeShellCommand("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/Main_testVFE2.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/d/T_iput_wide_4.jar dot.junit.opcodes.iput_wide.Main_testVFE2");
// A sucessful adb shell command returns an empty string.
assertEquals("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/Main_testVFE2.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/d/T_iput_wide_4.jar dot.junit.opcodes.iput_wide.Main_testVFE2", "", res);}

public void testVFE30() throws Exception {
    String res = getDevice().executeShellCommand("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/Main_testVFE30.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/d/T_iput_wide_30.jar dot.junit.opcodes.iput_wide.Main_testVFE30");
// A sucessful adb shell command returns an empty string.
assertEquals("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/Main_testVFE30.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/d/T_iput_wide_30.jar dot.junit.opcodes.iput_wide.Main_testVFE30", "", res);}

public void testVFE5() throws Exception {
    String res = getDevice().executeShellCommand("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/Main_testVFE5.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/d/T_iput_wide_17.jar dot.junit.opcodes.iput_wide.Main_testVFE5");
// A sucessful adb shell command returns an empty string.
assertEquals("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/Main_testVFE5.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/d/T_iput_wide_17.jar dot.junit.opcodes.iput_wide.Main_testVFE5", "", res);}

public void testVFE6() throws Exception {
    String res = getDevice().executeShellCommand("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/Main_testVFE6.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/d/T_iput_wide_6.jar dot.junit.opcodes.iput_wide.Main_testVFE6");
// A sucessful adb shell command returns an empty string.
assertEquals("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/Main_testVFE6.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/d/T_iput_wide_6.jar dot.junit.opcodes.iput_wide.Main_testVFE6", "", res);}

public void testVFE7() throws Exception {
    String res = getDevice().executeShellCommand("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/Main_testVFE7.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/d/T_iput_wide_18.jar dot.junit.opcodes.iput_wide.Main_testVFE7");
// A sucessful adb shell command returns an empty string.
assertEquals("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/Main_testVFE7.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/d/T_iput_wide_18.jar dot.junit.opcodes.iput_wide.Main_testVFE7", "", res);}

public void testVFE8() throws Exception {
    String res = getDevice().executeShellCommand("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/Main_testVFE8.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/d/T_iput_wide_7.jar dot.junit.opcodes.iput_wide.Main_testVFE8");
// A sucessful adb shell command returns an empty string.
assertEquals("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/Main_testVFE8.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/d/T_iput_wide_7.jar dot.junit.opcodes.iput_wide.Main_testVFE8", "", res);}

public void testVFE9() throws Exception {
    String res = getDevice().executeShellCommand("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/Main_testVFE9.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/d/T_iput_wide_8.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/TestStubs.jar dot.junit.opcodes.iput_wide.Main_testVFE9");
// A sucessful adb shell command returns an empty string.
assertEquals("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/Main_testVFE9.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/d/T_iput_wide_8.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/iput_wide/TestStubs.jar dot.junit.opcodes.iput_wide.Main_testVFE9", "", res);}


}
