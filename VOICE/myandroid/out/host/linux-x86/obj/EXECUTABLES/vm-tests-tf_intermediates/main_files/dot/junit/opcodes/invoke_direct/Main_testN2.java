//Autogenerated code by util.build.BuildDalvikSuite; do not edit.
package dot.junit.opcodes.invoke_direct;
import dot.junit.opcodes.invoke_direct.d.*;
import dot.junit.*;
public class Main_testN2 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_invoke_direct_2 t = new T_invoke_direct_2();
        assertEquals(345, t.run());
    }
}
