//Autogenerated code by util.build.BuildDalvikSuite; do not edit.
package dot.junit.opcodes.mul_long;
import dot.junit.opcodes.mul_long.d.*;
import dot.junit.*;
public class Main_testN1 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_mul_long_1 t = new T_mul_long_1();
        assertEquals(3195355577426903040l, t.run(222000000000l, 5000000000l));
    }
}
