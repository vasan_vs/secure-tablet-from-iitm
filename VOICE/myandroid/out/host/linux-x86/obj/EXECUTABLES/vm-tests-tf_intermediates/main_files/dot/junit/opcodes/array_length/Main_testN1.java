//Autogenerated code by util.build.BuildDalvikSuite; do not edit.
package dot.junit.opcodes.array_length;
import dot.junit.opcodes.array_length.d.*;
import dot.junit.*;
public class Main_testN1 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_array_length_1 t = new T_array_length_1();
        String[] a = new String[5];
        assertEquals(5, t.run(a));
    }
}
