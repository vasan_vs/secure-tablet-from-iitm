//Autogenerated code by util.build.BuildDalvikSuite; do not edit.
package dot.junit.opcodes.mul_int_lit8;
import dot.junit.opcodes.mul_int_lit8.d.*;
import dot.junit.*;
public class Main_testN2 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_mul_int_lit8_1 t = new T_mul_int_lit8_1();
        assertEquals(-250, t.run(-25));
    }
}
