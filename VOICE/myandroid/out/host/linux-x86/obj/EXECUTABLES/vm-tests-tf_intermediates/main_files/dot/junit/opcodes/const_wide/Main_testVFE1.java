//Autogenerated code by util.build.BuildDalvikSuite; do not edit.
package dot.junit.opcodes.const_wide;
import dot.junit.opcodes.const_wide.d.*;
import dot.junit.*;
public class Main_testVFE1 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        try {
            Class.forName("dot.junit.opcodes.const_wide.d.T_const_wide_3");
            fail("expected a verification exception");
        } catch (Throwable t) {
            DxUtil.checkVerifyException(t);
        }
    }
}
