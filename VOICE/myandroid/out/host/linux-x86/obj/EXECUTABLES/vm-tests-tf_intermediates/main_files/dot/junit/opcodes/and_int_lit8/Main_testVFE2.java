//Autogenerated code by util.build.BuildDalvikSuite; do not edit.
package dot.junit.opcodes.and_int_lit8;
import dot.junit.opcodes.and_int_lit8.d.*;
import dot.junit.*;
public class Main_testVFE2 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        try {
            Class.forName("dot.junit.opcodes.and_int_lit8.d.T_and_int_lit8_7");
            fail("expected a verification exception");
        } catch (Throwable t) {
            DxUtil.checkVerifyException(t);
        }
    }
}
