//Autogenerated code by util.build.BuildDalvikSuite; do not edit.
package dot.junit.opcodes.int_to_short;
import dot.junit.opcodes.int_to_short.d.*;
import dot.junit.*;
public class Main_testN7 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_int_to_short_1 t = new T_int_to_short_1();
        assertEquals(0xfffffedc, t.run(0x10fedc));
    }
}
