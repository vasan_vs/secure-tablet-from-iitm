//Autogenerated code by util.build.BuildDalvikSuite; do not edit.
package dot.junit.opcodes.move_object;
import dot.junit.opcodes.move_object.d.*;
import dot.junit.*;
public class Main_testVFE4 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        try {
            Class.forName("dot.junit.opcodes.move_object.d.T_move_object_5");
            fail("expected a verification exception");
        } catch (Throwable t) {
            DxUtil.checkVerifyException(t);
        }
    }
}
