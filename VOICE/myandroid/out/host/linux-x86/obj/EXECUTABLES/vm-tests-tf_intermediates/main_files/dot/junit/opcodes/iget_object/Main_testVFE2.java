//Autogenerated code by util.build.BuildDalvikSuite; do not edit.
package dot.junit.opcodes.iget_object;
import dot.junit.opcodes.iget_object.d.*;
import dot.junit.*;
public class Main_testVFE2 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        try {
            Class.forName("dot.junit.opcodes.iget_object.d.T_iget_object_3");
            fail("expected a verification exception");
        } catch (Throwable t) {
            DxUtil.checkVerifyException(t);
        }
    }
}
