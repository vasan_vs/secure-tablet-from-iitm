//Autogenerated code by util.build.BuildDalvikSuite; do not edit.
package dot.junit.opcodes.or_int_2addr;
import dot.junit.opcodes.or_int_2addr.d.*;
import dot.junit.*;
public class Main_testB2 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_or_int_2addr_1 t = new T_or_int_2addr_1();
        assertEquals(0xffffffff, t.run(Integer.MAX_VALUE, Integer.MIN_VALUE));
    }
}
