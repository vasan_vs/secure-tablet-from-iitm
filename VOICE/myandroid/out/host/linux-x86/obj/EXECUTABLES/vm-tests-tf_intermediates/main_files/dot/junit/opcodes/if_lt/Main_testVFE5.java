//Autogenerated code by util.build.BuildDalvikSuite; do not edit.
package dot.junit.opcodes.if_lt;
import dot.junit.opcodes.if_lt.d.*;
import dot.junit.*;
public class Main_testVFE5 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        try {
            Class.forName("dot.junit.opcodes.if_lt.d.T_if_lt_9");
            fail("expected a verification exception");
        } catch (Throwable t) {
            DxUtil.checkVerifyException(t);
        }
    }
}
