//Autogenerated code by util.build.BuildDalvikSuite; do not edit.
package dot.junit.opcodes.aget_byte;
import dot.junit.opcodes.aget_byte.d.*;
import dot.junit.*;
public class Main_testVFE6 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        try {
            Class.forName("dot.junit.opcodes.aget_byte.d.T_aget_byte_7");
            fail("expected a verification exception");
        } catch (Throwable t) {
            DxUtil.checkVerifyException(t);
        }
    }
}
