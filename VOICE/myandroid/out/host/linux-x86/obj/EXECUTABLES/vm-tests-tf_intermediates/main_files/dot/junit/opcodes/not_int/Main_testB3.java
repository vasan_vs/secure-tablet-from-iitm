//Autogenerated code by util.build.BuildDalvikSuite; do not edit.
package dot.junit.opcodes.not_int;
import dot.junit.opcodes.not_int.d.*;
import dot.junit.*;
public class Main_testB3 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_not_int_1 t = new T_not_int_1();
        assertEquals(-2, t.run(1));
    }
}
