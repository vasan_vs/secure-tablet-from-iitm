//Autogenerated code by util.build.BuildDalvikSuite; do not edit.
package dot.junit.opcodes.aget_byte;
import dot.junit.opcodes.aget_byte.d.*;
import dot.junit.*;
public class Main_testE2 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_aget_byte_1 t = new T_aget_byte_1();
        try {
            t.run(null, 2);
            fail("expected NullPointerException");
        } catch (NullPointerException aie) {
            // expected
        }
    }
}
