//Autogenerated code by util.build.BuildDalvikSuite; do not edit.
package dot.junit.opcodes.opc_const;
import java.io.IOException;
import com.android.tradefed.testtype.DeviceTestCase;

public class JUnit_Test_opc_const extends DeviceTestCase {
public void testN1() throws Exception {
    String res = getDevice().executeShellCommand("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/opc_const/Main_testN1.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/opc_const/d/T_opc_const_1.jar dot.junit.opcodes.opc_const.Main_testN1");
// A sucessful adb shell command returns an empty string.
assertEquals("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/opc_const/Main_testN1.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/opc_const/d/T_opc_const_1.jar dot.junit.opcodes.opc_const.Main_testN1", "", res);}

public void testN2() throws Exception {
    String res = getDevice().executeShellCommand("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/opc_const/Main_testN2.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/opc_const/d/T_opc_const_2.jar dot.junit.opcodes.opc_const.Main_testN2");
// A sucessful adb shell command returns an empty string.
assertEquals("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/opc_const/Main_testN2.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/opc_const/d/T_opc_const_2.jar dot.junit.opcodes.opc_const.Main_testN2", "", res);}

public void testVFE1() throws Exception {
    String res = getDevice().executeShellCommand("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/opc_const/Main_testVFE1.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/opc_const/d/T_opc_const_3.jar dot.junit.opcodes.opc_const.Main_testVFE1");
// A sucessful adb shell command returns an empty string.
assertEquals("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/opc_const/Main_testVFE1.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/opc_const/d/T_opc_const_3.jar dot.junit.opcodes.opc_const.Main_testVFE1", "", res);}

public void testVFE2() throws Exception {
    String res = getDevice().executeShellCommand("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/opc_const/Main_testVFE2.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/opc_const/d/T_opc_const_4.jar dot.junit.opcodes.opc_const.Main_testVFE2");
// A sucessful adb shell command returns an empty string.
assertEquals("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/opc_const/Main_testVFE2.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/opc_const/d/T_opc_const_4.jar dot.junit.opcodes.opc_const.Main_testVFE2", "", res);}


}
