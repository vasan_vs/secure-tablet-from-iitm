//Autogenerated code by util.build.BuildDalvikSuite; do not edit.
package dot.junit.format.f1;
import dot.junit.format.f1.d.*;
import dot.junit.*;
public class Main_testVFE9 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        try {
            Class.forName("dot.junit.format.f1.d.T_f1_9");
            fail("expected a verification exception but this test may fail if this check is not enforced");
        } catch (Throwable t) {
            DxUtil.checkVerifyException(t);
        }
    }
}
