//Autogenerated code by util.build.BuildDalvikSuite; do not edit.
package dot.junit.opcodes.if_gt;
import dot.junit.opcodes.if_gt.d.*;
import dot.junit.*;
public class Main_testVFE4 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        try {
            Class.forName("dot.junit.opcodes.if_gt.d.T_if_gt_7");
            fail("expected a verification exception");
        } catch (Throwable t) {
            DxUtil.checkVerifyException(t);
        }
    }
}
