//Autogenerated code by util.build.BuildDalvikSuite; do not edit.
package dot.junit.opcodes.shr_int_2addr;
import dot.junit.opcodes.shr_int_2addr.d.*;
import dot.junit.*;
public class Main_testB3 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_shr_int_2addr_1 t = new T_shr_int_2addr_1();
        assertEquals(0xc0000000, t.run(Integer.MIN_VALUE, 1));
    }
}
