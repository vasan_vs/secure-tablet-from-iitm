/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * This file is auto-generated. DO NOT MODIFY!
 * The source Renderscript file: packages/wallpapers/MusicVisualization/src/com/android/musicvis/vis5/many.rs
 */
package com.android.musicvis.vis5;

import android.renderscript.*;
import android.content.res.Resources;

/**
 * @hide
 */
public class ScriptC_many extends ScriptC {
    // Constructor
    public  ScriptC_many(RenderScript rs, Resources resources, int id) {
        super(rs, resources, id);
        mExportVar_fadeoutcounter = 0;
        mExportVar_fadeincounter = 0;
        mExportVar_wave1pos = 0;
        mExportVar_wave1amp = 0;
        mExportVar_wave2pos = 0;
        mExportVar_wave2amp = 0;
        mExportVar_wave3pos = 0;
        mExportVar_wave3amp = 0;
        mExportVar_wave4pos = 0;
        mExportVar_wave4amp = 0;
        mExportVar_waveCounter = 0;
        mExportVar_lastuptime = 0;
        mExportVar_autorotation = 0f;
    }

    private final static int mExportVarIdx_gAngle = 0;
    private float mExportVar_gAngle;
    public void set_gAngle(float v) {
        mExportVar_gAngle = v;
        setVar(mExportVarIdx_gAngle, v);
    }

    public float get_gAngle() {
        return mExportVar_gAngle;
    }

    private final static int mExportVarIdx_gPeak = 1;
    private int mExportVar_gPeak;
    public void set_gPeak(int v) {
        mExportVar_gPeak = v;
        setVar(mExportVarIdx_gPeak, v);
    }

    public int get_gPeak() {
        return mExportVar_gPeak;
    }

    private final static int mExportVarIdx_gRotate = 2;
    private float mExportVar_gRotate;
    public void set_gRotate(float v) {
        mExportVar_gRotate = v;
        setVar(mExportVarIdx_gRotate, v);
    }

    public float get_gRotate() {
        return mExportVar_gRotate;
    }

    private final static int mExportVarIdx_gTilt = 3;
    private float mExportVar_gTilt;
    public void set_gTilt(float v) {
        mExportVar_gTilt = v;
        setVar(mExportVarIdx_gTilt, v);
    }

    public float get_gTilt() {
        return mExportVar_gTilt;
    }

    private final static int mExportVarIdx_gIdle = 4;
    private int mExportVar_gIdle;
    public void set_gIdle(int v) {
        mExportVar_gIdle = v;
        setVar(mExportVarIdx_gIdle, v);
    }

    public int get_gIdle() {
        return mExportVar_gIdle;
    }

    private final static int mExportVarIdx_gWaveCounter = 5;
    private int mExportVar_gWaveCounter;
    public void set_gWaveCounter(int v) {
        mExportVar_gWaveCounter = v;
        setVar(mExportVarIdx_gWaveCounter, v);
    }

    public int get_gWaveCounter() {
        return mExportVar_gWaveCounter;
    }

    private final static int mExportVarIdx_gPVBackground = 6;
    private ProgramVertex mExportVar_gPVBackground;
    public void set_gPVBackground(ProgramVertex v) {
        mExportVar_gPVBackground = v;
        setVar(mExportVarIdx_gPVBackground, v);
    }

    public ProgramVertex get_gPVBackground() {
        return mExportVar_gPVBackground;
    }

    private final static int mExportVarIdx_gPFBackgroundMip = 7;
    private ProgramFragment mExportVar_gPFBackgroundMip;
    public void set_gPFBackgroundMip(ProgramFragment v) {
        mExportVar_gPFBackgroundMip = v;
        setVar(mExportVarIdx_gPFBackgroundMip, v);
    }

    public ProgramFragment get_gPFBackgroundMip() {
        return mExportVar_gPFBackgroundMip;
    }

    private final static int mExportVarIdx_gPFBackgroundNoMip = 8;
    private ProgramFragment mExportVar_gPFBackgroundNoMip;
    public void set_gPFBackgroundNoMip(ProgramFragment v) {
        mExportVar_gPFBackgroundNoMip = v;
        setVar(mExportVarIdx_gPFBackgroundNoMip, v);
    }

    public ProgramFragment get_gPFBackgroundNoMip() {
        return mExportVar_gPFBackgroundNoMip;
    }

    private final static int mExportVarIdx_gPR = 9;
    private ProgramRaster mExportVar_gPR;
    public void set_gPR(ProgramRaster v) {
        mExportVar_gPR = v;
        setVar(mExportVarIdx_gPR, v);
    }

    public ProgramRaster get_gPR() {
        return mExportVar_gPR;
    }

    private final static int mExportVarIdx_gTvumeter_background = 10;
    private Allocation mExportVar_gTvumeter_background;
    public void set_gTvumeter_background(Allocation v) {
        mExportVar_gTvumeter_background = v;
        setVar(mExportVarIdx_gTvumeter_background, v);
    }

    public Allocation get_gTvumeter_background() {
        return mExportVar_gTvumeter_background;
    }

    private final static int mExportVarIdx_gTvumeter_peak_on = 11;
    private Allocation mExportVar_gTvumeter_peak_on;
    public void set_gTvumeter_peak_on(Allocation v) {
        mExportVar_gTvumeter_peak_on = v;
        setVar(mExportVarIdx_gTvumeter_peak_on, v);
    }

    public Allocation get_gTvumeter_peak_on() {
        return mExportVar_gTvumeter_peak_on;
    }

    private final static int mExportVarIdx_gTvumeter_peak_off = 12;
    private Allocation mExportVar_gTvumeter_peak_off;
    public void set_gTvumeter_peak_off(Allocation v) {
        mExportVar_gTvumeter_peak_off = v;
        setVar(mExportVarIdx_gTvumeter_peak_off, v);
    }

    public Allocation get_gTvumeter_peak_off() {
        return mExportVar_gTvumeter_peak_off;
    }

    private final static int mExportVarIdx_gTvumeter_needle = 13;
    private Allocation mExportVar_gTvumeter_needle;
    public void set_gTvumeter_needle(Allocation v) {
        mExportVar_gTvumeter_needle = v;
        setVar(mExportVarIdx_gTvumeter_needle, v);
    }

    public Allocation get_gTvumeter_needle() {
        return mExportVar_gTvumeter_needle;
    }

    private final static int mExportVarIdx_gTvumeter_black = 14;
    private Allocation mExportVar_gTvumeter_black;
    public void set_gTvumeter_black(Allocation v) {
        mExportVar_gTvumeter_black = v;
        setVar(mExportVarIdx_gTvumeter_black, v);
    }

    public Allocation get_gTvumeter_black() {
        return mExportVar_gTvumeter_black;
    }

    private final static int mExportVarIdx_gTvumeter_frame = 15;
    private Allocation mExportVar_gTvumeter_frame;
    public void set_gTvumeter_frame(Allocation v) {
        mExportVar_gTvumeter_frame = v;
        setVar(mExportVarIdx_gTvumeter_frame, v);
    }

    public Allocation get_gTvumeter_frame() {
        return mExportVar_gTvumeter_frame;
    }

    private final static int mExportVarIdx_gTvumeter_album = 16;
    private Allocation mExportVar_gTvumeter_album;
    public void set_gTvumeter_album(Allocation v) {
        mExportVar_gTvumeter_album = v;
        setVar(mExportVarIdx_gTvumeter_album, v);
    }

    public Allocation get_gTvumeter_album() {
        return mExportVar_gTvumeter_album;
    }

    private final static int mExportVarIdx_gPFSBackground = 17;
    private ProgramStore mExportVar_gPFSBackground;
    public void set_gPFSBackground(ProgramStore v) {
        mExportVar_gPFSBackground = v;
        setVar(mExportVarIdx_gPFSBackground, v);
    }

    public ProgramStore get_gPFSBackground() {
        return mExportVar_gPFSBackground;
    }

    private final static int mExportVarIdx_gPoints = 18;
    private ScriptField_Vertex mExportVar_gPoints;
    public void bind_gPoints(ScriptField_Vertex v) {
        mExportVar_gPoints = v;
        if (v == null) bindAllocation(null, mExportVarIdx_gPoints);
        else bindAllocation(v.getAllocation(), mExportVarIdx_gPoints);
    }

    public ScriptField_Vertex get_gPoints() {
        return mExportVar_gPoints;
    }

    private final static int mExportVarIdx_gPointBuffer = 19;
    private Allocation mExportVar_gPointBuffer;
    public void set_gPointBuffer(Allocation v) {
        mExportVar_gPointBuffer = v;
        setVar(mExportVarIdx_gPointBuffer, v);
    }

    public Allocation get_gPointBuffer() {
        return mExportVar_gPointBuffer;
    }

    private final static int mExportVarIdx_gTlinetexture = 20;
    private Allocation mExportVar_gTlinetexture;
    public void set_gTlinetexture(Allocation v) {
        mExportVar_gTlinetexture = v;
        setVar(mExportVarIdx_gTlinetexture, v);
    }

    public Allocation get_gTlinetexture() {
        return mExportVar_gTlinetexture;
    }

    private final static int mExportVarIdx_gCubeMesh = 21;
    private Mesh mExportVar_gCubeMesh;
    public void set_gCubeMesh(Mesh v) {
        mExportVar_gCubeMesh = v;
        setVar(mExportVarIdx_gCubeMesh, v);
    }

    public Mesh get_gCubeMesh() {
        return mExportVar_gCubeMesh;
    }

    private final static int mExportVarIdx_fadeoutcounter = 22;
    private int mExportVar_fadeoutcounter;
    public void set_fadeoutcounter(int v) {
        mExportVar_fadeoutcounter = v;
        setVar(mExportVarIdx_fadeoutcounter, v);
    }

    public int get_fadeoutcounter() {
        return mExportVar_fadeoutcounter;
    }

    private final static int mExportVarIdx_fadeincounter = 23;
    private int mExportVar_fadeincounter;
    public void set_fadeincounter(int v) {
        mExportVar_fadeincounter = v;
        setVar(mExportVarIdx_fadeincounter, v);
    }

    public int get_fadeincounter() {
        return mExportVar_fadeincounter;
    }

    private final static int mExportVarIdx_wave1pos = 24;
    private int mExportVar_wave1pos;
    public void set_wave1pos(int v) {
        mExportVar_wave1pos = v;
        setVar(mExportVarIdx_wave1pos, v);
    }

    public int get_wave1pos() {
        return mExportVar_wave1pos;
    }

    private final static int mExportVarIdx_wave1amp = 25;
    private int mExportVar_wave1amp;
    public void set_wave1amp(int v) {
        mExportVar_wave1amp = v;
        setVar(mExportVarIdx_wave1amp, v);
    }

    public int get_wave1amp() {
        return mExportVar_wave1amp;
    }

    private final static int mExportVarIdx_wave2pos = 26;
    private int mExportVar_wave2pos;
    public void set_wave2pos(int v) {
        mExportVar_wave2pos = v;
        setVar(mExportVarIdx_wave2pos, v);
    }

    public int get_wave2pos() {
        return mExportVar_wave2pos;
    }

    private final static int mExportVarIdx_wave2amp = 27;
    private int mExportVar_wave2amp;
    public void set_wave2amp(int v) {
        mExportVar_wave2amp = v;
        setVar(mExportVarIdx_wave2amp, v);
    }

    public int get_wave2amp() {
        return mExportVar_wave2amp;
    }

    private final static int mExportVarIdx_wave3pos = 28;
    private int mExportVar_wave3pos;
    public void set_wave3pos(int v) {
        mExportVar_wave3pos = v;
        setVar(mExportVarIdx_wave3pos, v);
    }

    public int get_wave3pos() {
        return mExportVar_wave3pos;
    }

    private final static int mExportVarIdx_wave3amp = 29;
    private int mExportVar_wave3amp;
    public void set_wave3amp(int v) {
        mExportVar_wave3amp = v;
        setVar(mExportVarIdx_wave3amp, v);
    }

    public int get_wave3amp() {
        return mExportVar_wave3amp;
    }

    private final static int mExportVarIdx_wave4pos = 30;
    private int mExportVar_wave4pos;
    public void set_wave4pos(int v) {
        mExportVar_wave4pos = v;
        setVar(mExportVarIdx_wave4pos, v);
    }

    public int get_wave4pos() {
        return mExportVar_wave4pos;
    }

    private final static int mExportVarIdx_wave4amp = 31;
    private int mExportVar_wave4amp;
    public void set_wave4amp(int v) {
        mExportVar_wave4amp = v;
        setVar(mExportVarIdx_wave4amp, v);
    }

    public int get_wave4amp() {
        return mExportVar_wave4amp;
    }

    private final static int mExportVarIdx_idle = 32;
    private float[] mExportVar_idle;
    public void set_idle(float[] v) {
        mExportVar_idle = v;
        FieldPacker fp = new FieldPacker(16384);
        for (int ct1 = 0; ct1 < 4096; ct1++) {
            fp.addF32(v[ct1]);
        }

        setVar(mExportVarIdx_idle, fp);
    }

    public float[] get_idle() {
        return mExportVar_idle;
    }

    private final static int mExportVarIdx_waveCounter = 33;
    private int mExportVar_waveCounter;
    public void set_waveCounter(int v) {
        mExportVar_waveCounter = v;
        setVar(mExportVarIdx_waveCounter, v);
    }

    public int get_waveCounter() {
        return mExportVar_waveCounter;
    }

    private final static int mExportVarIdx_lastuptime = 34;
    private int mExportVar_lastuptime;
    public void set_lastuptime(int v) {
        mExportVar_lastuptime = v;
        setVar(mExportVarIdx_lastuptime, v);
    }

    public int get_lastuptime() {
        return mExportVar_lastuptime;
    }

    private final static int mExportVarIdx_autorotation = 35;
    private float mExportVar_autorotation;
    public void set_autorotation(float v) {
        mExportVar_autorotation = v;
        setVar(mExportVarIdx_autorotation, v);
    }

    public float get_autorotation() {
        return mExportVar_autorotation;
    }

}

