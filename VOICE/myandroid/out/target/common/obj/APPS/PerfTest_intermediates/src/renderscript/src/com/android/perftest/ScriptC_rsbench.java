/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * This file is auto-generated. DO NOT MODIFY!
 * The source Renderscript file: frameworks/base/tests/RenderScriptTests/PerfTest/src/com/android/perftest/rsbench.rs
 */
package com.android.perftest;

import android.renderscript.*;
import android.content.res.Resources;

/**
 * @hide
 */
public class ScriptC_rsbench extends ScriptC {
    // Constructor
    public  ScriptC_rsbench(RenderScript rs, Resources resources, int id) {
        super(rs, resources, id);
        mExportVar_RS_MSG_TEST_DONE = 100;
        mExportVar_RS_MSG_RESULTS_READY = 101;
        mExportVar_gMaxModes = 31;
        mExportVar_gXOffset = 0.5f;
    }

    private final static int mExportVarIdx_RS_MSG_TEST_DONE = 0;
    private int mExportVar_RS_MSG_TEST_DONE;
    public int get_RS_MSG_TEST_DONE() {
        return mExportVar_RS_MSG_TEST_DONE;
    }

    private final static int mExportVarIdx_RS_MSG_RESULTS_READY = 1;
    private int mExportVar_RS_MSG_RESULTS_READY;
    public int get_RS_MSG_RESULTS_READY() {
        return mExportVar_RS_MSG_RESULTS_READY;
    }

    private final static int mExportVarIdx_gMaxModes = 2;
    private int mExportVar_gMaxModes;
    public int get_gMaxModes() {
        return mExportVar_gMaxModes;
    }

    private final static int mExportVarIdx_gMaxLoops = 3;
    private int mExportVar_gMaxLoops;
    public void set_gMaxLoops(int v) {
        mExportVar_gMaxLoops = v;
        setVar(mExportVarIdx_gMaxLoops, v);
    }

    public int get_gMaxLoops() {
        return mExportVar_gMaxLoops;
    }

    private final static int mExportVarIdx_gTSpace = 4;
    private Allocation mExportVar_gTSpace;
    public void set_gTSpace(Allocation v) {
        mExportVar_gTSpace = v;
        setVar(mExportVarIdx_gTSpace, v);
    }

    public Allocation get_gTSpace() {
        return mExportVar_gTSpace;
    }

    private final static int mExportVarIdx_gTLight1 = 5;
    private Allocation mExportVar_gTLight1;
    public void set_gTLight1(Allocation v) {
        mExportVar_gTLight1 = v;
        setVar(mExportVarIdx_gTLight1, v);
    }

    public Allocation get_gTLight1() {
        return mExportVar_gTLight1;
    }

    private final static int mExportVarIdx_gTFlares = 6;
    private Allocation mExportVar_gTFlares;
    public void set_gTFlares(Allocation v) {
        mExportVar_gTFlares = v;
        setVar(mExportVarIdx_gTFlares, v);
    }

    public Allocation get_gTFlares() {
        return mExportVar_gTFlares;
    }

    private final static int mExportVarIdx_gParticlesMesh = 7;
    private Mesh mExportVar_gParticlesMesh;
    public void set_gParticlesMesh(Mesh v) {
        mExportVar_gParticlesMesh = v;
        setVar(mExportVarIdx_gParticlesMesh, v);
    }

    public Mesh get_gParticlesMesh() {
        return mExportVar_gParticlesMesh;
    }

    private final static int mExportVarIdx_gPFBackground = 8;
    private ProgramFragment mExportVar_gPFBackground;
    public void set_gPFBackground(ProgramFragment v) {
        mExportVar_gPFBackground = v;
        setVar(mExportVarIdx_gPFBackground, v);
    }

    public ProgramFragment get_gPFBackground() {
        return mExportVar_gPFBackground;
    }

    private final static int mExportVarIdx_gPFStars = 9;
    private ProgramFragment mExportVar_gPFStars;
    public void set_gPFStars(ProgramFragment v) {
        mExportVar_gPFStars = v;
        setVar(mExportVarIdx_gPFStars, v);
    }

    public ProgramFragment get_gPFStars() {
        return mExportVar_gPFStars;
    }

    private final static int mExportVarIdx_gPVStars = 10;
    private ProgramVertex mExportVar_gPVStars;
    public void set_gPVStars(ProgramVertex v) {
        mExportVar_gPVStars = v;
        setVar(mExportVarIdx_gPVStars, v);
    }

    public ProgramVertex get_gPVStars() {
        return mExportVar_gPVStars;
    }

    private final static int mExportVarIdx_gPVBkProj = 11;
    private ProgramVertex mExportVar_gPVBkProj;
    public void set_gPVBkProj(ProgramVertex v) {
        mExportVar_gPVBkProj = v;
        setVar(mExportVarIdx_gPVBkProj, v);
    }

    public ProgramVertex get_gPVBkProj() {
        return mExportVar_gPVBkProj;
    }

    private final static int mExportVarIdx_gPSLights = 12;
    private ProgramStore mExportVar_gPSLights;
    public void set_gPSLights(ProgramStore v) {
        mExportVar_gPSLights = v;
        setVar(mExportVarIdx_gPSLights, v);
    }

    public ProgramStore get_gPSLights() {
        return mExportVar_gPSLights;
    }

    private final static int mExportVarIdx_gXOffset = 13;
    private float mExportVar_gXOffset;
    public void set_gXOffset(float v) {
        mExportVar_gXOffset = v;
        setVar(mExportVarIdx_gXOffset, v);
    }

    public float get_gXOffset() {
        return mExportVar_gXOffset;
    }

    private final static int mExportVarIdx_Particles = 14;
    private ScriptField_Particle mExportVar_Particles;
    public void bind_Particles(ScriptField_Particle v) {
        mExportVar_Particles = v;
        if (v == null) bindAllocation(null, mExportVarIdx_Particles);
        else bindAllocation(v.getAllocation(), mExportVarIdx_Particles);
    }

    public ScriptField_Particle get_Particles() {
        return mExportVar_Particles;
    }

    private final static int mExportVarIdx_vpConstants = 15;
    private ScriptField_VpConsts mExportVar_vpConstants;
    public void bind_vpConstants(ScriptField_VpConsts v) {
        mExportVar_vpConstants = v;
        if (v == null) bindAllocation(null, mExportVarIdx_vpConstants);
        else bindAllocation(v.getAllocation(), mExportVarIdx_vpConstants);
    }

    public ScriptField_VpConsts get_vpConstants() {
        return mExportVar_vpConstants;
    }

    private final static int mExportVarIdx_gStringBuffer = 16;
    private Allocation mExportVar_gStringBuffer;
    public void bind_gStringBuffer(Allocation v) {
        mExportVar_gStringBuffer = v;
        if (v == null) bindAllocation(null, mExportVarIdx_gStringBuffer);
        else bindAllocation(v, mExportVarIdx_gStringBuffer);
    }

    public Allocation get_gStringBuffer() {
        return mExportVar_gStringBuffer;
    }

    private final static int mExportVarIdx_gProgVertex = 17;
    private ProgramVertex mExportVar_gProgVertex;
    public void set_gProgVertex(ProgramVertex v) {
        mExportVar_gProgVertex = v;
        setVar(mExportVarIdx_gProgVertex, v);
    }

    public ProgramVertex get_gProgVertex() {
        return mExportVar_gProgVertex;
    }

    private final static int mExportVarIdx_gProgFragmentColor = 18;
    private ProgramFragment mExportVar_gProgFragmentColor;
    public void set_gProgFragmentColor(ProgramFragment v) {
        mExportVar_gProgFragmentColor = v;
        setVar(mExportVarIdx_gProgFragmentColor, v);
    }

    public ProgramFragment get_gProgFragmentColor() {
        return mExportVar_gProgFragmentColor;
    }

    private final static int mExportVarIdx_gProgFragmentTexture = 19;
    private ProgramFragment mExportVar_gProgFragmentTexture;
    public void set_gProgFragmentTexture(ProgramFragment v) {
        mExportVar_gProgFragmentTexture = v;
        setVar(mExportVarIdx_gProgFragmentTexture, v);
    }

    public ProgramFragment get_gProgFragmentTexture() {
        return mExportVar_gProgFragmentTexture;
    }

    private final static int mExportVarIdx_gProgStoreBlendNoneDepth = 20;
    private ProgramStore mExportVar_gProgStoreBlendNoneDepth;
    public void set_gProgStoreBlendNoneDepth(ProgramStore v) {
        mExportVar_gProgStoreBlendNoneDepth = v;
        setVar(mExportVarIdx_gProgStoreBlendNoneDepth, v);
    }

    public ProgramStore get_gProgStoreBlendNoneDepth() {
        return mExportVar_gProgStoreBlendNoneDepth;
    }

    private final static int mExportVarIdx_gProgStoreBlendNone = 21;
    private ProgramStore mExportVar_gProgStoreBlendNone;
    public void set_gProgStoreBlendNone(ProgramStore v) {
        mExportVar_gProgStoreBlendNone = v;
        setVar(mExportVarIdx_gProgStoreBlendNone, v);
    }

    public ProgramStore get_gProgStoreBlendNone() {
        return mExportVar_gProgStoreBlendNone;
    }

    private final static int mExportVarIdx_gProgStoreBlendAlpha = 22;
    private ProgramStore mExportVar_gProgStoreBlendAlpha;
    public void set_gProgStoreBlendAlpha(ProgramStore v) {
        mExportVar_gProgStoreBlendAlpha = v;
        setVar(mExportVarIdx_gProgStoreBlendAlpha, v);
    }

    public ProgramStore get_gProgStoreBlendAlpha() {
        return mExportVar_gProgStoreBlendAlpha;
    }

    private final static int mExportVarIdx_gProgStoreBlendAdd = 23;
    private ProgramStore mExportVar_gProgStoreBlendAdd;
    public void set_gProgStoreBlendAdd(ProgramStore v) {
        mExportVar_gProgStoreBlendAdd = v;
        setVar(mExportVarIdx_gProgStoreBlendAdd, v);
    }

    public ProgramStore get_gProgStoreBlendAdd() {
        return mExportVar_gProgStoreBlendAdd;
    }

    private final static int mExportVarIdx_gTexOpaque = 24;
    private Allocation mExportVar_gTexOpaque;
    public void set_gTexOpaque(Allocation v) {
        mExportVar_gTexOpaque = v;
        setVar(mExportVarIdx_gTexOpaque, v);
    }

    public Allocation get_gTexOpaque() {
        return mExportVar_gTexOpaque;
    }

    private final static int mExportVarIdx_gTexTorus = 25;
    private Allocation mExportVar_gTexTorus;
    public void set_gTexTorus(Allocation v) {
        mExportVar_gTexTorus = v;
        setVar(mExportVarIdx_gTexTorus, v);
    }

    public Allocation get_gTexTorus() {
        return mExportVar_gTexTorus;
    }

    private final static int mExportVarIdx_gTexTransparent = 26;
    private Allocation mExportVar_gTexTransparent;
    public void set_gTexTransparent(Allocation v) {
        mExportVar_gTexTransparent = v;
        setVar(mExportVarIdx_gTexTransparent, v);
    }

    public Allocation get_gTexTransparent() {
        return mExportVar_gTexTransparent;
    }

    private final static int mExportVarIdx_gTexChecker = 27;
    private Allocation mExportVar_gTexChecker;
    public void set_gTexChecker(Allocation v) {
        mExportVar_gTexChecker = v;
        setVar(mExportVarIdx_gTexChecker, v);
    }

    public Allocation get_gTexChecker() {
        return mExportVar_gTexChecker;
    }

    private final static int mExportVarIdx_gTexGlobe = 28;
    private Allocation mExportVar_gTexGlobe;
    public void set_gTexGlobe(Allocation v) {
        mExportVar_gTexGlobe = v;
        setVar(mExportVarIdx_gTexGlobe, v);
    }

    public Allocation get_gTexGlobe() {
        return mExportVar_gTexGlobe;
    }

    private final static int mExportVarIdx_gTexList100 = 29;
    private ScriptField_ListAllocs_s mExportVar_gTexList100;
    public void bind_gTexList100(ScriptField_ListAllocs_s v) {
        mExportVar_gTexList100 = v;
        if (v == null) bindAllocation(null, mExportVarIdx_gTexList100);
        else bindAllocation(v.getAllocation(), mExportVarIdx_gTexList100);
    }

    public ScriptField_ListAllocs_s get_gTexList100() {
        return mExportVar_gTexList100;
    }

    private final static int mExportVarIdx_gSampleTextList100 = 30;
    private ScriptField_ListAllocs_s mExportVar_gSampleTextList100;
    public void bind_gSampleTextList100(ScriptField_ListAllocs_s v) {
        mExportVar_gSampleTextList100 = v;
        if (v == null) bindAllocation(null, mExportVarIdx_gSampleTextList100);
        else bindAllocation(v.getAllocation(), mExportVarIdx_gSampleTextList100);
    }

    public ScriptField_ListAllocs_s get_gSampleTextList100() {
        return mExportVar_gSampleTextList100;
    }

    private final static int mExportVarIdx_gListViewText = 31;
    private ScriptField_ListAllocs_s mExportVar_gListViewText;
    public void bind_gListViewText(ScriptField_ListAllocs_s v) {
        mExportVar_gListViewText = v;
        if (v == null) bindAllocation(null, mExportVarIdx_gListViewText);
        else bindAllocation(v.getAllocation(), mExportVarIdx_gListViewText);
    }

    public ScriptField_ListAllocs_s get_gListViewText() {
        return mExportVar_gListViewText;
    }

    private final static int mExportVarIdx_g10by10Mesh = 32;
    private Mesh mExportVar_g10by10Mesh;
    public void set_g10by10Mesh(Mesh v) {
        mExportVar_g10by10Mesh = v;
        setVar(mExportVarIdx_g10by10Mesh, v);
    }

    public Mesh get_g10by10Mesh() {
        return mExportVar_g10by10Mesh;
    }

    private final static int mExportVarIdx_g100by100Mesh = 33;
    private Mesh mExportVar_g100by100Mesh;
    public void set_g100by100Mesh(Mesh v) {
        mExportVar_g100by100Mesh = v;
        setVar(mExportVarIdx_g100by100Mesh, v);
    }

    public Mesh get_g100by100Mesh() {
        return mExportVar_g100by100Mesh;
    }

    private final static int mExportVarIdx_gWbyHMesh = 34;
    private Mesh mExportVar_gWbyHMesh;
    public void set_gWbyHMesh(Mesh v) {
        mExportVar_gWbyHMesh = v;
        setVar(mExportVarIdx_gWbyHMesh, v);
    }

    public Mesh get_gWbyHMesh() {
        return mExportVar_gWbyHMesh;
    }

    private final static int mExportVarIdx_gSingleMesh = 35;
    private Mesh mExportVar_gSingleMesh;
    public void set_gSingleMesh(Mesh v) {
        mExportVar_gSingleMesh = v;
        setVar(mExportVarIdx_gSingleMesh, v);
    }

    public Mesh get_gSingleMesh() {
        return mExportVar_gSingleMesh;
    }

    private final static int mExportVarIdx_gFontSans = 36;
    private Font mExportVar_gFontSans;
    public void set_gFontSans(Font v) {
        mExportVar_gFontSans = v;
        setVar(mExportVarIdx_gFontSans, v);
    }

    public Font get_gFontSans() {
        return mExportVar_gFontSans;
    }

    private final static int mExportVarIdx_gFontSerif = 37;
    private Font mExportVar_gFontSerif;
    public void set_gFontSerif(Font v) {
        mExportVar_gFontSerif = v;
        setVar(mExportVarIdx_gFontSerif, v);
    }

    public Font get_gFontSerif() {
        return mExportVar_gFontSerif;
    }

    private final static int mExportVarIdx_gDisplayMode = 38;
    private int mExportVar_gDisplayMode;
    public void set_gDisplayMode(int v) {
        mExportVar_gDisplayMode = v;
        setVar(mExportVarIdx_gDisplayMode, v);
    }

    public int get_gDisplayMode() {
        return mExportVar_gDisplayMode;
    }

    private final static int mExportVarIdx_gLinearClamp = 39;
    private Sampler mExportVar_gLinearClamp;
    public void set_gLinearClamp(Sampler v) {
        mExportVar_gLinearClamp = v;
        setVar(mExportVarIdx_gLinearClamp, v);
    }

    public Sampler get_gLinearClamp() {
        return mExportVar_gLinearClamp;
    }

    private final static int mExportVarIdx_gLinearWrap = 40;
    private Sampler mExportVar_gLinearWrap;
    public void set_gLinearWrap(Sampler v) {
        mExportVar_gLinearWrap = v;
        setVar(mExportVarIdx_gLinearWrap, v);
    }

    public Sampler get_gLinearWrap() {
        return mExportVar_gLinearWrap;
    }

    private final static int mExportVarIdx_gMipLinearWrap = 41;
    private Sampler mExportVar_gMipLinearWrap;
    public void set_gMipLinearWrap(Sampler v) {
        mExportVar_gMipLinearWrap = v;
        setVar(mExportVarIdx_gMipLinearWrap, v);
    }

    public Sampler get_gMipLinearWrap() {
        return mExportVar_gMipLinearWrap;
    }

    private final static int mExportVarIdx_gNearestClamp = 42;
    private Sampler mExportVar_gNearestClamp;
    public void set_gNearestClamp(Sampler v) {
        mExportVar_gNearestClamp = v;
        setVar(mExportVarIdx_gNearestClamp, v);
    }

    public Sampler get_gNearestClamp() {
        return mExportVar_gNearestClamp;
    }

    private final static int mExportVarIdx_gCullBack = 43;
    private ProgramRaster mExportVar_gCullBack;
    public void set_gCullBack(ProgramRaster v) {
        mExportVar_gCullBack = v;
        setVar(mExportVarIdx_gCullBack, v);
    }

    public ProgramRaster get_gCullBack() {
        return mExportVar_gCullBack;
    }

    private final static int mExportVarIdx_gCullFront = 44;
    private ProgramRaster mExportVar_gCullFront;
    public void set_gCullFront(ProgramRaster v) {
        mExportVar_gCullFront = v;
        setVar(mExportVarIdx_gCullFront, v);
    }

    public ProgramRaster get_gCullFront() {
        return mExportVar_gCullFront;
    }

    private final static int mExportVarIdx_gCullNone = 45;
    private ProgramRaster mExportVar_gCullNone;
    public void set_gCullNone(ProgramRaster v) {
        mExportVar_gCullNone = v;
        setVar(mExportVarIdx_gCullNone, v);
    }

    public ProgramRaster get_gCullNone() {
        return mExportVar_gCullNone;
    }

    private final static int mExportVarIdx_gVSInputs = 46;
    private ScriptField_VertexShaderInputs_s mExportVar_gVSInputs;
    public void bind_gVSInputs(ScriptField_VertexShaderInputs_s v) {
        mExportVar_gVSInputs = v;
        if (v == null) bindAllocation(null, mExportVarIdx_gVSInputs);
        else bindAllocation(v.getAllocation(), mExportVarIdx_gVSInputs);
    }

    public ScriptField_VertexShaderInputs_s get_gVSInputs() {
        return mExportVar_gVSInputs;
    }

    private final static int mExportVarIdx_gProgFragmentMultitex = 47;
    private ProgramFragment mExportVar_gProgFragmentMultitex;
    public void set_gProgFragmentMultitex(ProgramFragment v) {
        mExportVar_gProgFragmentMultitex = v;
        setVar(mExportVarIdx_gProgFragmentMultitex, v);
    }

    public ProgramFragment get_gProgFragmentMultitex() {
        return mExportVar_gProgFragmentMultitex;
    }

    private final static int mExportVarIdx_gRenderBufferColor = 48;
    private Allocation mExportVar_gRenderBufferColor;
    public void set_gRenderBufferColor(Allocation v) {
        mExportVar_gRenderBufferColor = v;
        setVar(mExportVarIdx_gRenderBufferColor, v);
    }

    public Allocation get_gRenderBufferColor() {
        return mExportVar_gRenderBufferColor;
    }

    private final static int mExportVarIdx_gRenderBufferDepth = 49;
    private Allocation mExportVar_gRenderBufferDepth;
    public void set_gRenderBufferDepth(Allocation v) {
        mExportVar_gRenderBufferDepth = v;
        setVar(mExportVarIdx_gRenderBufferDepth, v);
    }

    public Allocation get_gRenderBufferDepth() {
        return mExportVar_gRenderBufferDepth;
    }

    private final static int mExportVarIdx_gFontScript = 50;
    private Script mExportVar_gFontScript;
    public void set_gFontScript(Script v) {
        mExportVar_gFontScript = v;
        setVar(mExportVarIdx_gFontScript, v);
    }

    public Script get_gFontScript() {
        return mExportVar_gFontScript;
    }

    private final static int mExportVarIdx_gTorusScript = 51;
    private Script mExportVar_gTorusScript;
    public void set_gTorusScript(Script v) {
        mExportVar_gTorusScript = v;
        setVar(mExportVarIdx_gTorusScript, v);
    }

    public Script get_gTorusScript() {
        return mExportVar_gTorusScript;
    }

    private final static int mExportVarIdx_gDummyAlloc = 52;
    private Allocation mExportVar_gDummyAlloc;
    public void set_gDummyAlloc(Allocation v) {
        mExportVar_gDummyAlloc = v;
        setVar(mExportVarIdx_gDummyAlloc, v);
    }

    public Allocation get_gDummyAlloc() {
        return mExportVar_gDummyAlloc;
    }

    private final static int mExportFuncIdx_initParticles = 0;
    public void invoke_initParticles() {
        invoke(mExportFuncIdx_initParticles);
    }

    private final static int mExportFuncIdx_setDebugMode = 1;
    public void invoke_setDebugMode(int testNumber) {
        FieldPacker setDebugMode_fp = new FieldPacker(4);
        setDebugMode_fp.addI32(testNumber);
        invoke(mExportFuncIdx_setDebugMode, setDebugMode_fp);
    }

    private final static int mExportFuncIdx_setBenchmarkMode = 2;
    public void invoke_setBenchmarkMode() {
        invoke(mExportFuncIdx_setBenchmarkMode);
    }

    private final static int mExportFuncIdx_getTestName = 3;
    public void invoke_getTestName(int testIndex) {
        FieldPacker getTestName_fp = new FieldPacker(4);
        getTestName_fp.addI32(testIndex);
        invoke(mExportFuncIdx_getTestName, getTestName_fp);
    }

}

