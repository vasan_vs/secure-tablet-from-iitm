/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * This file is auto-generated. DO NOT MODIFY!
 * The source Renderscript file: development/samples/RenderScript/MiscSamples/src/com/example/android/rs/miscsamples/rsrenderstates.rs
 */
package com.example.android.rs.miscsamples;

import android.renderscript.*;
import android.content.res.Resources;

/**
 * @hide
 */
public class ScriptC_rsrenderstates extends ScriptC {
    // Constructor
    public  ScriptC_rsrenderstates(RenderScript rs, Resources resources, int id) {
        super(rs, resources, id);
        mExportVar_gMaxModes = 11;
        mExportVar_gDt = 0f;
    }

    private final static int mExportVarIdx_gMaxModes = 0;
    private int mExportVar_gMaxModes;
    public int get_gMaxModes() {
        return mExportVar_gMaxModes;
    }

    private final static int mExportVarIdx_gProgVertex = 1;
    private ProgramVertex mExportVar_gProgVertex;
    public void set_gProgVertex(ProgramVertex v) {
        mExportVar_gProgVertex = v;
        setVar(mExportVarIdx_gProgVertex, v);
    }

    public ProgramVertex get_gProgVertex() {
        return mExportVar_gProgVertex;
    }

    private final static int mExportVarIdx_gProgFragmentColor = 2;
    private ProgramFragment mExportVar_gProgFragmentColor;
    public void set_gProgFragmentColor(ProgramFragment v) {
        mExportVar_gProgFragmentColor = v;
        setVar(mExportVarIdx_gProgFragmentColor, v);
    }

    public ProgramFragment get_gProgFragmentColor() {
        return mExportVar_gProgFragmentColor;
    }

    private final static int mExportVarIdx_gProgFragmentTexture = 3;
    private ProgramFragment mExportVar_gProgFragmentTexture;
    public void set_gProgFragmentTexture(ProgramFragment v) {
        mExportVar_gProgFragmentTexture = v;
        setVar(mExportVarIdx_gProgFragmentTexture, v);
    }

    public ProgramFragment get_gProgFragmentTexture() {
        return mExportVar_gProgFragmentTexture;
    }

    private final static int mExportVarIdx_gProgStoreBlendNoneDepth = 4;
    private ProgramStore mExportVar_gProgStoreBlendNoneDepth;
    public void set_gProgStoreBlendNoneDepth(ProgramStore v) {
        mExportVar_gProgStoreBlendNoneDepth = v;
        setVar(mExportVarIdx_gProgStoreBlendNoneDepth, v);
    }

    public ProgramStore get_gProgStoreBlendNoneDepth() {
        return mExportVar_gProgStoreBlendNoneDepth;
    }

    private final static int mExportVarIdx_gProgStoreBlendNone = 5;
    private ProgramStore mExportVar_gProgStoreBlendNone;
    public void set_gProgStoreBlendNone(ProgramStore v) {
        mExportVar_gProgStoreBlendNone = v;
        setVar(mExportVarIdx_gProgStoreBlendNone, v);
    }

    public ProgramStore get_gProgStoreBlendNone() {
        return mExportVar_gProgStoreBlendNone;
    }

    private final static int mExportVarIdx_gProgStoreBlendAlpha = 6;
    private ProgramStore mExportVar_gProgStoreBlendAlpha;
    public void set_gProgStoreBlendAlpha(ProgramStore v) {
        mExportVar_gProgStoreBlendAlpha = v;
        setVar(mExportVarIdx_gProgStoreBlendAlpha, v);
    }

    public ProgramStore get_gProgStoreBlendAlpha() {
        return mExportVar_gProgStoreBlendAlpha;
    }

    private final static int mExportVarIdx_gProgStoreBlendAdd = 7;
    private ProgramStore mExportVar_gProgStoreBlendAdd;
    public void set_gProgStoreBlendAdd(ProgramStore v) {
        mExportVar_gProgStoreBlendAdd = v;
        setVar(mExportVarIdx_gProgStoreBlendAdd, v);
    }

    public ProgramStore get_gProgStoreBlendAdd() {
        return mExportVar_gProgStoreBlendAdd;
    }

    private final static int mExportVarIdx_gTexOpaque = 8;
    private Allocation mExportVar_gTexOpaque;
    public void set_gTexOpaque(Allocation v) {
        mExportVar_gTexOpaque = v;
        setVar(mExportVarIdx_gTexOpaque, v);
    }

    public Allocation get_gTexOpaque() {
        return mExportVar_gTexOpaque;
    }

    private final static int mExportVarIdx_gTexTorus = 9;
    private Allocation mExportVar_gTexTorus;
    public void set_gTexTorus(Allocation v) {
        mExportVar_gTexTorus = v;
        setVar(mExportVarIdx_gTexTorus, v);
    }

    public Allocation get_gTexTorus() {
        return mExportVar_gTexTorus;
    }

    private final static int mExportVarIdx_gTexTransparent = 10;
    private Allocation mExportVar_gTexTransparent;
    public void set_gTexTransparent(Allocation v) {
        mExportVar_gTexTransparent = v;
        setVar(mExportVarIdx_gTexTransparent, v);
    }

    public Allocation get_gTexTransparent() {
        return mExportVar_gTexTransparent;
    }

    private final static int mExportVarIdx_gTexChecker = 11;
    private Allocation mExportVar_gTexChecker;
    public void set_gTexChecker(Allocation v) {
        mExportVar_gTexChecker = v;
        setVar(mExportVarIdx_gTexChecker, v);
    }

    public Allocation get_gTexChecker() {
        return mExportVar_gTexChecker;
    }

    private final static int mExportVarIdx_gTexCube = 12;
    private Allocation mExportVar_gTexCube;
    public void set_gTexCube(Allocation v) {
        mExportVar_gTexCube = v;
        setVar(mExportVarIdx_gTexCube, v);
    }

    public Allocation get_gTexCube() {
        return mExportVar_gTexCube;
    }

    private final static int mExportVarIdx_gMbyNMesh = 13;
    private Mesh mExportVar_gMbyNMesh;
    public void set_gMbyNMesh(Mesh v) {
        mExportVar_gMbyNMesh = v;
        setVar(mExportVarIdx_gMbyNMesh, v);
    }

    public Mesh get_gMbyNMesh() {
        return mExportVar_gMbyNMesh;
    }

    private final static int mExportVarIdx_gTorusMesh = 14;
    private Mesh mExportVar_gTorusMesh;
    public void set_gTorusMesh(Mesh v) {
        mExportVar_gTorusMesh = v;
        setVar(mExportVarIdx_gTorusMesh, v);
    }

    public Mesh get_gTorusMesh() {
        return mExportVar_gTorusMesh;
    }

    private final static int mExportVarIdx_gFontSans = 15;
    private Font mExportVar_gFontSans;
    public void set_gFontSans(Font v) {
        mExportVar_gFontSans = v;
        setVar(mExportVarIdx_gFontSans, v);
    }

    public Font get_gFontSans() {
        return mExportVar_gFontSans;
    }

    private final static int mExportVarIdx_gFontSerif = 16;
    private Font mExportVar_gFontSerif;
    public void set_gFontSerif(Font v) {
        mExportVar_gFontSerif = v;
        setVar(mExportVarIdx_gFontSerif, v);
    }

    public Font get_gFontSerif() {
        return mExportVar_gFontSerif;
    }

    private final static int mExportVarIdx_gFontSerifBold = 17;
    private Font mExportVar_gFontSerifBold;
    public void set_gFontSerifBold(Font v) {
        mExportVar_gFontSerifBold = v;
        setVar(mExportVarIdx_gFontSerifBold, v);
    }

    public Font get_gFontSerifBold() {
        return mExportVar_gFontSerifBold;
    }

    private final static int mExportVarIdx_gFontSerifItalic = 18;
    private Font mExportVar_gFontSerifItalic;
    public void set_gFontSerifItalic(Font v) {
        mExportVar_gFontSerifItalic = v;
        setVar(mExportVarIdx_gFontSerifItalic, v);
    }

    public Font get_gFontSerifItalic() {
        return mExportVar_gFontSerifItalic;
    }

    private final static int mExportVarIdx_gFontSerifBoldItalic = 19;
    private Font mExportVar_gFontSerifBoldItalic;
    public void set_gFontSerifBoldItalic(Font v) {
        mExportVar_gFontSerifBoldItalic = v;
        setVar(mExportVarIdx_gFontSerifBoldItalic, v);
    }

    public Font get_gFontSerifBoldItalic() {
        return mExportVar_gFontSerifBoldItalic;
    }

    private final static int mExportVarIdx_gFontMono = 20;
    private Font mExportVar_gFontMono;
    public void set_gFontMono(Font v) {
        mExportVar_gFontMono = v;
        setVar(mExportVarIdx_gFontMono, v);
    }

    public Font get_gFontMono() {
        return mExportVar_gFontMono;
    }

    private final static int mExportVarIdx_gTextAlloc = 21;
    private Allocation mExportVar_gTextAlloc;
    public void set_gTextAlloc(Allocation v) {
        mExportVar_gTextAlloc = v;
        setVar(mExportVarIdx_gTextAlloc, v);
    }

    public Allocation get_gTextAlloc() {
        return mExportVar_gTextAlloc;
    }

    private final static int mExportVarIdx_gDisplayMode = 22;
    private int mExportVar_gDisplayMode;
    public void set_gDisplayMode(int v) {
        mExportVar_gDisplayMode = v;
        setVar(mExportVarIdx_gDisplayMode, v);
    }

    public int get_gDisplayMode() {
        return mExportVar_gDisplayMode;
    }

    private final static int mExportVarIdx_gLinearClamp = 23;
    private Sampler mExportVar_gLinearClamp;
    public void set_gLinearClamp(Sampler v) {
        mExportVar_gLinearClamp = v;
        setVar(mExportVarIdx_gLinearClamp, v);
    }

    public Sampler get_gLinearClamp() {
        return mExportVar_gLinearClamp;
    }

    private final static int mExportVarIdx_gLinearWrap = 24;
    private Sampler mExportVar_gLinearWrap;
    public void set_gLinearWrap(Sampler v) {
        mExportVar_gLinearWrap = v;
        setVar(mExportVarIdx_gLinearWrap, v);
    }

    public Sampler get_gLinearWrap() {
        return mExportVar_gLinearWrap;
    }

    private final static int mExportVarIdx_gMipLinearWrap = 25;
    private Sampler mExportVar_gMipLinearWrap;
    public void set_gMipLinearWrap(Sampler v) {
        mExportVar_gMipLinearWrap = v;
        setVar(mExportVarIdx_gMipLinearWrap, v);
    }

    public Sampler get_gMipLinearWrap() {
        return mExportVar_gMipLinearWrap;
    }

    private final static int mExportVarIdx_gMipLinearAniso8 = 26;
    private Sampler mExportVar_gMipLinearAniso8;
    public void set_gMipLinearAniso8(Sampler v) {
        mExportVar_gMipLinearAniso8 = v;
        setVar(mExportVarIdx_gMipLinearAniso8, v);
    }

    public Sampler get_gMipLinearAniso8() {
        return mExportVar_gMipLinearAniso8;
    }

    private final static int mExportVarIdx_gMipLinearAniso15 = 27;
    private Sampler mExportVar_gMipLinearAniso15;
    public void set_gMipLinearAniso15(Sampler v) {
        mExportVar_gMipLinearAniso15 = v;
        setVar(mExportVarIdx_gMipLinearAniso15, v);
    }

    public Sampler get_gMipLinearAniso15() {
        return mExportVar_gMipLinearAniso15;
    }

    private final static int mExportVarIdx_gNearestClamp = 28;
    private Sampler mExportVar_gNearestClamp;
    public void set_gNearestClamp(Sampler v) {
        mExportVar_gNearestClamp = v;
        setVar(mExportVarIdx_gNearestClamp, v);
    }

    public Sampler get_gNearestClamp() {
        return mExportVar_gNearestClamp;
    }

    private final static int mExportVarIdx_gCullBack = 29;
    private ProgramRaster mExportVar_gCullBack;
    public void set_gCullBack(ProgramRaster v) {
        mExportVar_gCullBack = v;
        setVar(mExportVarIdx_gCullBack, v);
    }

    public ProgramRaster get_gCullBack() {
        return mExportVar_gCullBack;
    }

    private final static int mExportVarIdx_gCullFront = 30;
    private ProgramRaster mExportVar_gCullFront;
    public void set_gCullFront(ProgramRaster v) {
        mExportVar_gCullFront = v;
        setVar(mExportVarIdx_gCullFront, v);
    }

    public ProgramRaster get_gCullFront() {
        return mExportVar_gCullFront;
    }

    private final static int mExportVarIdx_gCullNone = 31;
    private ProgramRaster mExportVar_gCullNone;
    public void set_gCullNone(ProgramRaster v) {
        mExportVar_gCullNone = v;
        setVar(mExportVarIdx_gCullNone, v);
    }

    public ProgramRaster get_gCullNone() {
        return mExportVar_gCullNone;
    }

    private final static int mExportVarIdx_gVSConstants = 32;
    private ScriptField_VertexShaderConstants_s mExportVar_gVSConstants;
    public void bind_gVSConstants(ScriptField_VertexShaderConstants_s v) {
        mExportVar_gVSConstants = v;
        if (v == null) bindAllocation(null, mExportVarIdx_gVSConstants);
        else bindAllocation(v.getAllocation(), mExportVarIdx_gVSConstants);
    }

    public ScriptField_VertexShaderConstants_s get_gVSConstants() {
        return mExportVar_gVSConstants;
    }

    private final static int mExportVarIdx_gVSConstants2 = 33;
    private ScriptField_VertexShaderConstants2_s mExportVar_gVSConstants2;
    public void bind_gVSConstants2(ScriptField_VertexShaderConstants2_s v) {
        mExportVar_gVSConstants2 = v;
        if (v == null) bindAllocation(null, mExportVarIdx_gVSConstants2);
        else bindAllocation(v.getAllocation(), mExportVarIdx_gVSConstants2);
    }

    public ScriptField_VertexShaderConstants2_s get_gVSConstants2() {
        return mExportVar_gVSConstants2;
    }

    private final static int mExportVarIdx_gFSConstants = 34;
    private ScriptField_FragentShaderConstants_s mExportVar_gFSConstants;
    public void bind_gFSConstants(ScriptField_FragentShaderConstants_s v) {
        mExportVar_gFSConstants = v;
        if (v == null) bindAllocation(null, mExportVarIdx_gFSConstants);
        else bindAllocation(v.getAllocation(), mExportVarIdx_gFSConstants);
    }

    public ScriptField_FragentShaderConstants_s get_gFSConstants() {
        return mExportVar_gFSConstants;
    }

    private final static int mExportVarIdx_gFSConstants2 = 35;
    private ScriptField_FragentShaderConstants2_s mExportVar_gFSConstants2;
    public void bind_gFSConstants2(ScriptField_FragentShaderConstants2_s v) {
        mExportVar_gFSConstants2 = v;
        if (v == null) bindAllocation(null, mExportVarIdx_gFSConstants2);
        else bindAllocation(v.getAllocation(), mExportVarIdx_gFSConstants2);
    }

    public ScriptField_FragentShaderConstants2_s get_gFSConstants2() {
        return mExportVar_gFSConstants2;
    }

    private final static int mExportVarIdx_gVSInputs = 36;
    private ScriptField_VertexShaderInputs_s mExportVar_gVSInputs;
    public void bind_gVSInputs(ScriptField_VertexShaderInputs_s v) {
        mExportVar_gVSInputs = v;
        if (v == null) bindAllocation(null, mExportVarIdx_gVSInputs);
        else bindAllocation(v.getAllocation(), mExportVarIdx_gVSInputs);
    }

    public ScriptField_VertexShaderInputs_s get_gVSInputs() {
        return mExportVar_gVSInputs;
    }

    private final static int mExportVarIdx_gProgVertexCustom = 37;
    private ProgramVertex mExportVar_gProgVertexCustom;
    public void set_gProgVertexCustom(ProgramVertex v) {
        mExportVar_gProgVertexCustom = v;
        setVar(mExportVarIdx_gProgVertexCustom, v);
    }

    public ProgramVertex get_gProgVertexCustom() {
        return mExportVar_gProgVertexCustom;
    }

    private final static int mExportVarIdx_gProgFragmentCustom = 38;
    private ProgramFragment mExportVar_gProgFragmentCustom;
    public void set_gProgFragmentCustom(ProgramFragment v) {
        mExportVar_gProgFragmentCustom = v;
        setVar(mExportVarIdx_gProgFragmentCustom, v);
    }

    public ProgramFragment get_gProgFragmentCustom() {
        return mExportVar_gProgFragmentCustom;
    }

    private final static int mExportVarIdx_gProgVertexCustom2 = 39;
    private ProgramVertex mExportVar_gProgVertexCustom2;
    public void set_gProgVertexCustom2(ProgramVertex v) {
        mExportVar_gProgVertexCustom2 = v;
        setVar(mExportVarIdx_gProgVertexCustom2, v);
    }

    public ProgramVertex get_gProgVertexCustom2() {
        return mExportVar_gProgVertexCustom2;
    }

    private final static int mExportVarIdx_gProgFragmentCustom2 = 40;
    private ProgramFragment mExportVar_gProgFragmentCustom2;
    public void set_gProgFragmentCustom2(ProgramFragment v) {
        mExportVar_gProgFragmentCustom2 = v;
        setVar(mExportVarIdx_gProgFragmentCustom2, v);
    }

    public ProgramFragment get_gProgFragmentCustom2() {
        return mExportVar_gProgFragmentCustom2;
    }

    private final static int mExportVarIdx_gProgVertexCube = 41;
    private ProgramVertex mExportVar_gProgVertexCube;
    public void set_gProgVertexCube(ProgramVertex v) {
        mExportVar_gProgVertexCube = v;
        setVar(mExportVarIdx_gProgVertexCube, v);
    }

    public ProgramVertex get_gProgVertexCube() {
        return mExportVar_gProgVertexCube;
    }

    private final static int mExportVarIdx_gProgFragmentCube = 42;
    private ProgramFragment mExportVar_gProgFragmentCube;
    public void set_gProgFragmentCube(ProgramFragment v) {
        mExportVar_gProgFragmentCube = v;
        setVar(mExportVarIdx_gProgFragmentCube, v);
    }

    public ProgramFragment get_gProgFragmentCube() {
        return mExportVar_gProgFragmentCube;
    }

    private final static int mExportVarIdx_gProgFragmentMultitex = 43;
    private ProgramFragment mExportVar_gProgFragmentMultitex;
    public void set_gProgFragmentMultitex(ProgramFragment v) {
        mExportVar_gProgFragmentMultitex = v;
        setVar(mExportVarIdx_gProgFragmentMultitex, v);
    }

    public ProgramFragment get_gProgFragmentMultitex() {
        return mExportVar_gProgFragmentMultitex;
    }

    private final static int mExportVarIdx_gDt = 44;
    private float mExportVar_gDt;
    public void set_gDt(float v) {
        mExportVar_gDt = v;
        setVar(mExportVarIdx_gDt, v);
    }

    public float get_gDt() {
        return mExportVar_gDt;
    }

}

