/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * This file is auto-generated. DO NOT MODIFY!
 * The source Renderscript file: frameworks/base/tests/RenderScriptTests/ImageProcessing/src/com/android/rs/image/threshold.rs
 */
package com.android.rs.image;

import android.renderscript.*;
import android.content.res.Resources;

/**
 * @hide
 */
public class ScriptC_threshold extends ScriptC {
    // Constructor
    public  ScriptC_threshold(RenderScript rs, Resources resources, int id) {
        super(rs, resources, id);
        mExportVar_CMD_FINISHED = 1;
    }

    private final static int mExportVarIdx_height = 0;
    private int mExportVar_height;
    public void set_height(int v) {
        mExportVar_height = v;
        setVar(mExportVarIdx_height, v);
    }

    public int get_height() {
        return mExportVar_height;
    }

    private final static int mExportVarIdx_width = 1;
    private int mExportVar_width;
    public void set_width(int v) {
        mExportVar_width = v;
        setVar(mExportVarIdx_width, v);
    }

    public int get_width() {
        return mExportVar_width;
    }

    private final static int mExportVarIdx_radius = 2;
    private int mExportVar_radius;
    public void set_radius(int v) {
        mExportVar_radius = v;
        setVar(mExportVarIdx_radius, v);
    }

    public int get_radius() {
        return mExportVar_radius;
    }

    private final static int mExportVarIdx_InPixel = 3;
    private Allocation mExportVar_InPixel;
    public void bind_InPixel(Allocation v) {
        mExportVar_InPixel = v;
        if (v == null) bindAllocation(null, mExportVarIdx_InPixel);
        else bindAllocation(v, mExportVarIdx_InPixel);
    }

    public Allocation get_InPixel() {
        return mExportVar_InPixel;
    }

    private final static int mExportVarIdx_OutPixel = 4;
    private Allocation mExportVar_OutPixel;
    public void bind_OutPixel(Allocation v) {
        mExportVar_OutPixel = v;
        if (v == null) bindAllocation(null, mExportVarIdx_OutPixel);
        else bindAllocation(v, mExportVarIdx_OutPixel);
    }

    public Allocation get_OutPixel() {
        return mExportVar_OutPixel;
    }

    private final static int mExportVarIdx_ScratchPixel1 = 5;
    private Allocation mExportVar_ScratchPixel1;
    public void bind_ScratchPixel1(Allocation v) {
        mExportVar_ScratchPixel1 = v;
        if (v == null) bindAllocation(null, mExportVarIdx_ScratchPixel1);
        else bindAllocation(v, mExportVarIdx_ScratchPixel1);
    }

    public Allocation get_ScratchPixel1() {
        return mExportVar_ScratchPixel1;
    }

    private final static int mExportVarIdx_ScratchPixel2 = 6;
    private Allocation mExportVar_ScratchPixel2;
    public void bind_ScratchPixel2(Allocation v) {
        mExportVar_ScratchPixel2 = v;
        if (v == null) bindAllocation(null, mExportVarIdx_ScratchPixel2);
        else bindAllocation(v, mExportVarIdx_ScratchPixel2);
    }

    public Allocation get_ScratchPixel2() {
        return mExportVar_ScratchPixel2;
    }

    private final static int mExportVarIdx_vBlurScript = 7;
    private Script mExportVar_vBlurScript;
    public void set_vBlurScript(Script v) {
        mExportVar_vBlurScript = v;
        setVar(mExportVarIdx_vBlurScript, v);
    }

    public Script get_vBlurScript() {
        return mExportVar_vBlurScript;
    }

    private final static int mExportVarIdx_hBlurScript = 8;
    private Script mExportVar_hBlurScript;
    public void set_hBlurScript(Script v) {
        mExportVar_hBlurScript = v;
        setVar(mExportVarIdx_hBlurScript, v);
    }

    public Script get_hBlurScript() {
        return mExportVar_hBlurScript;
    }

    private final static int mExportVarIdx_CMD_FINISHED = 9;
    private int mExportVar_CMD_FINISHED;
    public int get_CMD_FINISHED() {
        return mExportVar_CMD_FINISHED;
    }

    private final static int mExportFuncIdx_filter = 0;
    public void invoke_filter() {
        invoke(mExportFuncIdx_filter);
    }

}

