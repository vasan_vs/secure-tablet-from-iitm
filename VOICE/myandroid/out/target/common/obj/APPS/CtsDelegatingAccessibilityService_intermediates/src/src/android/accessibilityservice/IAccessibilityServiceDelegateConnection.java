/*
 * This file is auto-generated.  DO NOT MODIFY.
 * Original file: cts/tests/accessibilityservice/src/android/accessibilityservice/IAccessibilityServiceDelegateConnection.aidl
 */
package android.accessibilityservice;
/**
 * Interface for registering an accessibility service delegate
 * and asking it to perform some querying of the window for us.
 */
public interface IAccessibilityServiceDelegateConnection extends android.os.IInterface
{
/** Local-side IPC implementation stub class. */
public static abstract class Stub extends android.os.Binder implements android.accessibilityservice.IAccessibilityServiceDelegateConnection
{
private static final java.lang.String DESCRIPTOR = "android.accessibilityservice.IAccessibilityServiceDelegateConnection";
/** Construct the stub at attach it to the interface. */
public Stub()
{
this.attachInterface(this, DESCRIPTOR);
}
/**
 * Cast an IBinder object into an android.accessibilityservice.IAccessibilityServiceDelegateConnection interface,
 * generating a proxy if needed.
 */
public static android.accessibilityservice.IAccessibilityServiceDelegateConnection asInterface(android.os.IBinder obj)
{
if ((obj==null)) {
return null;
}
android.os.IInterface iin = (android.os.IInterface)obj.queryLocalInterface(DESCRIPTOR);
if (((iin!=null)&&(iin instanceof android.accessibilityservice.IAccessibilityServiceDelegateConnection))) {
return ((android.accessibilityservice.IAccessibilityServiceDelegateConnection)iin);
}
return new android.accessibilityservice.IAccessibilityServiceDelegateConnection.Stub.Proxy(obj);
}
public android.os.IBinder asBinder()
{
return this;
}
@Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
{
switch (code)
{
case INTERFACE_TRANSACTION:
{
reply.writeString(DESCRIPTOR);
return true;
}
case TRANSACTION_setAccessibilityServiceDelegate:
{
data.enforceInterface(DESCRIPTOR);
android.os.IBinder _arg0;
_arg0 = data.readStrongBinder();
this.setAccessibilityServiceDelegate(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_findAccessibilityNodeInfosByText:
{
data.enforceInterface(DESCRIPTOR);
android.view.accessibility.AccessibilityNodeInfo _arg0;
if ((0!=data.readInt())) {
_arg0 = android.view.accessibility.AccessibilityNodeInfo.CREATOR.createFromParcel(data);
}
else {
_arg0 = null;
}
java.lang.String _arg1;
_arg1 = data.readString();
java.util.List<android.view.accessibility.AccessibilityNodeInfo> _result = this.findAccessibilityNodeInfosByText(_arg0, _arg1);
reply.writeNoException();
reply.writeTypedList(_result);
return true;
}
case TRANSACTION_getParent:
{
data.enforceInterface(DESCRIPTOR);
android.view.accessibility.AccessibilityNodeInfo _arg0;
if ((0!=data.readInt())) {
_arg0 = android.view.accessibility.AccessibilityNodeInfo.CREATOR.createFromParcel(data);
}
else {
_arg0 = null;
}
android.view.accessibility.AccessibilityNodeInfo _result = this.getParent(_arg0);
reply.writeNoException();
if ((_result!=null)) {
reply.writeInt(1);
_result.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
}
else {
reply.writeInt(0);
}
return true;
}
case TRANSACTION_getChild:
{
data.enforceInterface(DESCRIPTOR);
android.view.accessibility.AccessibilityNodeInfo _arg0;
if ((0!=data.readInt())) {
_arg0 = android.view.accessibility.AccessibilityNodeInfo.CREATOR.createFromParcel(data);
}
else {
_arg0 = null;
}
int _arg1;
_arg1 = data.readInt();
android.view.accessibility.AccessibilityNodeInfo _result = this.getChild(_arg0, _arg1);
reply.writeNoException();
if ((_result!=null)) {
reply.writeInt(1);
_result.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
}
else {
reply.writeInt(0);
}
return true;
}
case TRANSACTION_performAccessibilityAction:
{
data.enforceInterface(DESCRIPTOR);
android.view.accessibility.AccessibilityNodeInfo _arg0;
if ((0!=data.readInt())) {
_arg0 = android.view.accessibility.AccessibilityNodeInfo.CREATOR.createFromParcel(data);
}
else {
_arg0 = null;
}
int _arg1;
_arg1 = data.readInt();
boolean _result = this.performAccessibilityAction(_arg0, _arg1);
reply.writeNoException();
reply.writeInt(((_result)?(1):(0)));
return true;
}
case TRANSACTION_getSource:
{
data.enforceInterface(DESCRIPTOR);
android.view.accessibility.AccessibilityEvent _arg0;
if ((0!=data.readInt())) {
_arg0 = android.view.accessibility.AccessibilityEvent.CREATOR.createFromParcel(data);
}
else {
_arg0 = null;
}
android.view.accessibility.AccessibilityNodeInfo _result = this.getSource(_arg0);
reply.writeNoException();
if ((_result!=null)) {
reply.writeInt(1);
_result.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
}
else {
reply.writeInt(0);
}
return true;
}
}
return super.onTransact(code, data, reply, flags);
}
private static class Proxy implements android.accessibilityservice.IAccessibilityServiceDelegateConnection
{
private android.os.IBinder mRemote;
Proxy(android.os.IBinder remote)
{
mRemote = remote;
}
public android.os.IBinder asBinder()
{
return mRemote;
}
public java.lang.String getInterfaceDescriptor()
{
return DESCRIPTOR;
}
public void setAccessibilityServiceDelegate(android.os.IBinder binder) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeStrongBinder(binder);
mRemote.transact(Stub.TRANSACTION_setAccessibilityServiceDelegate, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
public java.util.List<android.view.accessibility.AccessibilityNodeInfo> findAccessibilityNodeInfosByText(android.view.accessibility.AccessibilityNodeInfo root, java.lang.String text) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
java.util.List<android.view.accessibility.AccessibilityNodeInfo> _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
if ((root!=null)) {
_data.writeInt(1);
root.writeToParcel(_data, 0);
}
else {
_data.writeInt(0);
}
_data.writeString(text);
mRemote.transact(Stub.TRANSACTION_findAccessibilityNodeInfosByText, _data, _reply, 0);
_reply.readException();
_result = _reply.createTypedArrayList(android.view.accessibility.AccessibilityNodeInfo.CREATOR);
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
public android.view.accessibility.AccessibilityNodeInfo getParent(android.view.accessibility.AccessibilityNodeInfo child) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
android.view.accessibility.AccessibilityNodeInfo _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
if ((child!=null)) {
_data.writeInt(1);
child.writeToParcel(_data, 0);
}
else {
_data.writeInt(0);
}
mRemote.transact(Stub.TRANSACTION_getParent, _data, _reply, 0);
_reply.readException();
if ((0!=_reply.readInt())) {
_result = android.view.accessibility.AccessibilityNodeInfo.CREATOR.createFromParcel(_reply);
}
else {
_result = null;
}
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
public android.view.accessibility.AccessibilityNodeInfo getChild(android.view.accessibility.AccessibilityNodeInfo parent, int index) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
android.view.accessibility.AccessibilityNodeInfo _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
if ((parent!=null)) {
_data.writeInt(1);
parent.writeToParcel(_data, 0);
}
else {
_data.writeInt(0);
}
_data.writeInt(index);
mRemote.transact(Stub.TRANSACTION_getChild, _data, _reply, 0);
_reply.readException();
if ((0!=_reply.readInt())) {
_result = android.view.accessibility.AccessibilityNodeInfo.CREATOR.createFromParcel(_reply);
}
else {
_result = null;
}
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
public boolean performAccessibilityAction(android.view.accessibility.AccessibilityNodeInfo target, int action) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
boolean _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
if ((target!=null)) {
_data.writeInt(1);
target.writeToParcel(_data, 0);
}
else {
_data.writeInt(0);
}
_data.writeInt(action);
mRemote.transact(Stub.TRANSACTION_performAccessibilityAction, _data, _reply, 0);
_reply.readException();
_result = (0!=_reply.readInt());
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
public android.view.accessibility.AccessibilityNodeInfo getSource(android.view.accessibility.AccessibilityEvent event) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
android.view.accessibility.AccessibilityNodeInfo _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
if ((event!=null)) {
_data.writeInt(1);
event.writeToParcel(_data, 0);
}
else {
_data.writeInt(0);
}
mRemote.transact(Stub.TRANSACTION_getSource, _data, _reply, 0);
_reply.readException();
if ((0!=_reply.readInt())) {
_result = android.view.accessibility.AccessibilityNodeInfo.CREATOR.createFromParcel(_reply);
}
else {
_result = null;
}
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
}
static final int TRANSACTION_setAccessibilityServiceDelegate = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
static final int TRANSACTION_findAccessibilityNodeInfosByText = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
static final int TRANSACTION_getParent = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
static final int TRANSACTION_getChild = (android.os.IBinder.FIRST_CALL_TRANSACTION + 3);
static final int TRANSACTION_performAccessibilityAction = (android.os.IBinder.FIRST_CALL_TRANSACTION + 4);
static final int TRANSACTION_getSource = (android.os.IBinder.FIRST_CALL_TRANSACTION + 5);
}
public void setAccessibilityServiceDelegate(android.os.IBinder binder) throws android.os.RemoteException;
public java.util.List<android.view.accessibility.AccessibilityNodeInfo> findAccessibilityNodeInfosByText(android.view.accessibility.AccessibilityNodeInfo root, java.lang.String text) throws android.os.RemoteException;
public android.view.accessibility.AccessibilityNodeInfo getParent(android.view.accessibility.AccessibilityNodeInfo child) throws android.os.RemoteException;
public android.view.accessibility.AccessibilityNodeInfo getChild(android.view.accessibility.AccessibilityNodeInfo parent, int index) throws android.os.RemoteException;
public boolean performAccessibilityAction(android.view.accessibility.AccessibilityNodeInfo target, int action) throws android.os.RemoteException;
public android.view.accessibility.AccessibilityNodeInfo getSource(android.view.accessibility.AccessibilityEvent event) throws android.os.RemoteException;
}
