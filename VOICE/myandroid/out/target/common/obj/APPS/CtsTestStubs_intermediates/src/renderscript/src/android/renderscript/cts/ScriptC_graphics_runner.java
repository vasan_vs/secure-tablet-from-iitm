/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * This file is auto-generated. DO NOT MODIFY!
 * The source Renderscript file: cts/tests/src/android/renderscript/cts/graphics_runner.rs
 */
package android.renderscript.cts;

import android.renderscript.*;
import android.content.res.Resources;

/**
 * @hide
 */
public class ScriptC_graphics_runner extends ScriptC {
    // Constructor
    public  ScriptC_graphics_runner(RenderScript rs, Resources resources, int id) {
        super(rs, resources, id);
    }

    private final static int mExportVarIdx_c1 = 0;
    private ScriptField_ConstMatrix mExportVar_c1;
    public void bind_c1(ScriptField_ConstMatrix v) {
        mExportVar_c1 = v;
        if (v == null) bindAllocation(null, mExportVarIdx_c1);
        else bindAllocation(v.getAllocation(), mExportVarIdx_c1);
    }

    public ScriptField_ConstMatrix get_c1() {
        return mExportVar_c1;
    }

    private final static int mExportVarIdx_c2 = 1;
    private ScriptField_ConstComplex mExportVar_c2;
    public void bind_c2(ScriptField_ConstComplex v) {
        mExportVar_c2 = v;
        if (v == null) bindAllocation(null, mExportVarIdx_c2);
        else bindAllocation(v.getAllocation(), mExportVarIdx_c2);
    }

    public ScriptField_ConstComplex get_c2() {
        return mExportVar_c2;
    }

    private final static int mExportVarIdx_c3 = 2;
    private ScriptField_ConstExtra mExportVar_c3;
    public void bind_c3(ScriptField_ConstExtra v) {
        mExportVar_c3 = v;
        if (v == null) bindAllocation(null, mExportVarIdx_c3);
        else bindAllocation(v.getAllocation(), mExportVarIdx_c3);
    }

    public ScriptField_ConstExtra get_c3() {
        return mExportVar_c3;
    }

    private final static int mExportVarIdx_avt = 3;
    private ScriptField_AllVectorTypes mExportVar_avt;
    public void bind_avt(ScriptField_AllVectorTypes v) {
        mExportVar_avt = v;
        if (v == null) bindAllocation(null, mExportVarIdx_avt);
        else bindAllocation(v.getAllocation(), mExportVarIdx_avt);
    }

    public ScriptField_AllVectorTypes get_avt() {
        return mExportVar_avt;
    }

    private final static int mExportFuncIdx_testProgramVertex = 0;
    public void invoke_testProgramVertex(ProgramVertex pv) {
        FieldPacker testProgramVertex_fp = new FieldPacker(4);
        testProgramVertex_fp.addObj(pv);
        invoke(mExportFuncIdx_testProgramVertex, testProgramVertex_fp);
    }

    private final static int mExportFuncIdx_testProgramFragment = 1;
    public void invoke_testProgramFragment(ProgramFragment pf) {
        FieldPacker testProgramFragment_fp = new FieldPacker(4);
        testProgramFragment_fp.addObj(pf);
        invoke(mExportFuncIdx_testProgramFragment, testProgramFragment_fp);
    }

}

