package android.util;
public final class JsonReader
  implements java.io.Closeable
{
public  JsonReader(java.io.Reader in) { throw new RuntimeException("Stub!"); }
public  void setLenient(boolean lenient) { throw new RuntimeException("Stub!"); }
public  boolean isLenient() { throw new RuntimeException("Stub!"); }
public  void beginArray() throws java.io.IOException { throw new RuntimeException("Stub!"); }
public  void endArray() throws java.io.IOException { throw new RuntimeException("Stub!"); }
public  void beginObject() throws java.io.IOException { throw new RuntimeException("Stub!"); }
public  void endObject() throws java.io.IOException { throw new RuntimeException("Stub!"); }
public  boolean hasNext() throws java.io.IOException { throw new RuntimeException("Stub!"); }
public  android.util.JsonToken peek() throws java.io.IOException { throw new RuntimeException("Stub!"); }
public  java.lang.String nextName() throws java.io.IOException { throw new RuntimeException("Stub!"); }
public  java.lang.String nextString() throws java.io.IOException { throw new RuntimeException("Stub!"); }
public  boolean nextBoolean() throws java.io.IOException { throw new RuntimeException("Stub!"); }
public  void nextNull() throws java.io.IOException { throw new RuntimeException("Stub!"); }
public  double nextDouble() throws java.io.IOException { throw new RuntimeException("Stub!"); }
public  long nextLong() throws java.io.IOException { throw new RuntimeException("Stub!"); }
public  int nextInt() throws java.io.IOException { throw new RuntimeException("Stub!"); }
public  void close() throws java.io.IOException { throw new RuntimeException("Stub!"); }
public  void skipValue() throws java.io.IOException { throw new RuntimeException("Stub!"); }
public  java.lang.String toString() { throw new RuntimeException("Stub!"); }
}
