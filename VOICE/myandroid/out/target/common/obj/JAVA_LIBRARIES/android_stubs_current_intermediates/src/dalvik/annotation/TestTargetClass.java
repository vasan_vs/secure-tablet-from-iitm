package dalvik.annotation;
@java.lang.annotation.Retention(value=java.lang.annotation.RetentionPolicy.RUNTIME)
@java.lang.annotation.Target(value={java.lang.annotation.ElementType.TYPE})
@java.lang.Deprecated()
public @interface TestTargetClass
{
java.lang.Class<?> value();
}
