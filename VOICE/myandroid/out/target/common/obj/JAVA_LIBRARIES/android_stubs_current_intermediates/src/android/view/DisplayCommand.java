package android.view;
public class DisplayCommand
{
public static class ConfigParam
{
public  ConfigParam() { throw new RuntimeException("Stub!"); }
public  void writeToParcel(android.os.Parcel out) { throw new RuntimeException("Stub!"); }
public int displayId;
public int operateCode;
public int rotation;
public int xOverScan;
public int yOverScan;
public int mirror;
public int colorDepth;
public int keepRate;
public java.lang.String mode;
}
public  DisplayCommand() { throw new RuntimeException("Stub!"); }
public  int enable(int displayId, java.lang.String mode, int rotation, int xoverScan, int yoverScan, int mirror, int colorDepth, int keepRate) { throw new RuntimeException("Stub!"); }
public  int disable(int displayId) { throw new RuntimeException("Stub!"); }
public  int setResolution(int displayId, java.lang.String mode) { throw new RuntimeException("Stub!"); }
public  int setOverScan(int displayId, int Xratio, int Yratio) { throw new RuntimeException("Stub!"); }
public  int setMirror(int displayId, int mirror) { throw new RuntimeException("Stub!"); }
public  int setColorDepth(int displayId, int depth) { throw new RuntimeException("Stub!"); }
public  int setRotation(int displayId, int rotation) { throw new RuntimeException("Stub!"); }
public  int setKeepRate(int displayId, int keepRate) { throw new RuntimeException("Stub!"); }
public  int broadcastEvent() { throw new RuntimeException("Stub!"); }
public static final int OPERATE_CODE_ENABLE = 4096;
public static final int OPERATE_CODE_DISABLE = 8192;
public static final int OPERATE_CODE_CHANGE = 16384;
public static final int OPERATE_CODE_CHANGE_RESOLUTION = 1;
public static final int OPERATE_CODE_CHANGE_OVERSCAN = 2;
public static final int OPERATE_CODE_CHANGE_MIRROR = 4;
public static final int OPERATE_CODE_CHANGE_COLORDEPTH = 8;
public static final int OPERATE_CODE_CHANGE_ROTATION = 16;
public static final int OPERATE_CODE_CHANGE_KEEPRATE = 32;
public static final int SETTING_MODE_FULL_SCREEN = 4096;
public static final int SETTING_MODE_KEEP_PRIMARY_RATE = 8192;
public static final int SETTING_MODE_KEEP_16_9_RATE = 16384;
public static final int SETTING_MODE_KEEP_4_3_RATE = 32768;
}
