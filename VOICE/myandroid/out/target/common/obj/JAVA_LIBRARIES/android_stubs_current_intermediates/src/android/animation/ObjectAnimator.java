package android.animation;
public final class ObjectAnimator
  extends android.animation.ValueAnimator
{
public  ObjectAnimator() { throw new RuntimeException("Stub!"); }
public  void setPropertyName(java.lang.String propertyName) { throw new RuntimeException("Stub!"); }
public  void setProperty(android.util.Property property) { throw new RuntimeException("Stub!"); }
public  java.lang.String getPropertyName() { throw new RuntimeException("Stub!"); }
public static  android.animation.ObjectAnimator ofInt(java.lang.Object target, java.lang.String propertyName, int... values) { throw new RuntimeException("Stub!"); }
public static <T> android.animation.ObjectAnimator ofInt(T target, android.util.Property<T, java.lang.Integer> property, int... values) { throw new RuntimeException("Stub!"); }
public static  android.animation.ObjectAnimator ofFloat(java.lang.Object target, java.lang.String propertyName, float... values) { throw new RuntimeException("Stub!"); }
public static <T> android.animation.ObjectAnimator ofFloat(T target, android.util.Property<T, java.lang.Float> property, float... values) { throw new RuntimeException("Stub!"); }
public static  android.animation.ObjectAnimator ofObject(java.lang.Object target, java.lang.String propertyName, android.animation.TypeEvaluator evaluator, java.lang.Object... values) { throw new RuntimeException("Stub!"); }
public static <T, V> android.animation.ObjectAnimator ofObject(T target, android.util.Property<T, V> property, android.animation.TypeEvaluator<V> evaluator, V... values) { throw new RuntimeException("Stub!"); }
public static  android.animation.ObjectAnimator ofPropertyValuesHolder(java.lang.Object target, android.animation.PropertyValuesHolder... values) { throw new RuntimeException("Stub!"); }
public  void setIntValues(int... values) { throw new RuntimeException("Stub!"); }
public  void setFloatValues(float... values) { throw new RuntimeException("Stub!"); }
public  void setObjectValues(java.lang.Object... values) { throw new RuntimeException("Stub!"); }
public  void start() { throw new RuntimeException("Stub!"); }
public  android.animation.ObjectAnimator setDuration(long duration) { throw new RuntimeException("Stub!"); }
public  java.lang.Object getTarget() { throw new RuntimeException("Stub!"); }
public  void setTarget(java.lang.Object target) { throw new RuntimeException("Stub!"); }
public  void setupStartValues() { throw new RuntimeException("Stub!"); }
public  void setupEndValues() { throw new RuntimeException("Stub!"); }
public  android.animation.ObjectAnimator clone() { throw new RuntimeException("Stub!"); }
public  java.lang.String toString() { throw new RuntimeException("Stub!"); }
}
