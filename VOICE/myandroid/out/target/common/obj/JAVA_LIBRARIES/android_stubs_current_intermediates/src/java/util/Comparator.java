package java.util;
public interface Comparator<T>
{
public abstract  int compare(T lhs, T rhs);
public abstract  boolean equals(java.lang.Object object);
}
