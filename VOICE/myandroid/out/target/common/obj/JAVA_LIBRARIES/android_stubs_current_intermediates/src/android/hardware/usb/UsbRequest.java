package android.hardware.usb;
public class UsbRequest
{
public  UsbRequest() { throw new RuntimeException("Stub!"); }
public  boolean initialize(android.hardware.usb.UsbDeviceConnection connection, android.hardware.usb.UsbEndpoint endpoint) { throw new RuntimeException("Stub!"); }
public  void close() { throw new RuntimeException("Stub!"); }
protected  void finalize() throws java.lang.Throwable { throw new RuntimeException("Stub!"); }
public  android.hardware.usb.UsbEndpoint getEndpoint() { throw new RuntimeException("Stub!"); }
public  java.lang.Object getClientData() { throw new RuntimeException("Stub!"); }
public  void setClientData(java.lang.Object data) { throw new RuntimeException("Stub!"); }
public  boolean queue(java.nio.ByteBuffer buffer, int length) { throw new RuntimeException("Stub!"); }
public  boolean cancel() { throw new RuntimeException("Stub!"); }
}
