package android.hardware.usb;
public class UsbEndpoint
  implements android.os.Parcelable
{
UsbEndpoint() { throw new RuntimeException("Stub!"); }
public  int getAddress() { throw new RuntimeException("Stub!"); }
public  int getEndpointNumber() { throw new RuntimeException("Stub!"); }
public  int getDirection() { throw new RuntimeException("Stub!"); }
public  int getAttributes() { throw new RuntimeException("Stub!"); }
public  int getType() { throw new RuntimeException("Stub!"); }
public  int getMaxPacketSize() { throw new RuntimeException("Stub!"); }
public  int getInterval() { throw new RuntimeException("Stub!"); }
public  java.lang.String toString() { throw new RuntimeException("Stub!"); }
public  int describeContents() { throw new RuntimeException("Stub!"); }
public  void writeToParcel(android.os.Parcel parcel, int flags) { throw new RuntimeException("Stub!"); }
public static final android.os.Parcelable.Creator<android.hardware.usb.UsbEndpoint> CREATOR;
static { CREATOR = null; }
}
