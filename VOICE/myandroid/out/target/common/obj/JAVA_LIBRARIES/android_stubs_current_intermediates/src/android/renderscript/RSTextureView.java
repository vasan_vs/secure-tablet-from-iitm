package android.renderscript;
public class RSTextureView
  extends android.view.TextureView
  implements android.view.TextureView.SurfaceTextureListener
{
public  RSTextureView(android.content.Context context) { super((android.content.Context)null,(android.util.AttributeSet)null,0); throw new RuntimeException("Stub!"); }
public  RSTextureView(android.content.Context context, android.util.AttributeSet attrs) { super((android.content.Context)null,(android.util.AttributeSet)null,0); throw new RuntimeException("Stub!"); }
public  void onSurfaceTextureAvailable(android.graphics.SurfaceTexture surface, int width, int height) { throw new RuntimeException("Stub!"); }
public  void onSurfaceTextureSizeChanged(android.graphics.SurfaceTexture surface, int width, int height) { throw new RuntimeException("Stub!"); }
public  boolean onSurfaceTextureDestroyed(android.graphics.SurfaceTexture surface) { throw new RuntimeException("Stub!"); }
public  void onSurfaceTextureUpdated(android.graphics.SurfaceTexture surface) { throw new RuntimeException("Stub!"); }
public  void pause() { throw new RuntimeException("Stub!"); }
public  void resume() { throw new RuntimeException("Stub!"); }
public  android.renderscript.RenderScriptGL createRenderScriptGL(android.renderscript.RenderScriptGL.SurfaceConfig sc) { throw new RuntimeException("Stub!"); }
public  void destroyRenderScriptGL() { throw new RuntimeException("Stub!"); }
public  void setRenderScriptGL(android.renderscript.RenderScriptGL rs) { throw new RuntimeException("Stub!"); }
public  android.renderscript.RenderScriptGL getRenderScriptGL() { throw new RuntimeException("Stub!"); }
}
