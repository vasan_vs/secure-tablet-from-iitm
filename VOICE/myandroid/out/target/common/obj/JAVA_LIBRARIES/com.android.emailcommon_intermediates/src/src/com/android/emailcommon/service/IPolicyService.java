/*
 * This file is auto-generated.  DO NOT MODIFY.
 * Original file: packages/apps/Email/emailcommon/src/com/android/emailcommon/service/IPolicyService.aidl
 */
package com.android.emailcommon.service;
public interface IPolicyService extends android.os.IInterface
{
/** Local-side IPC implementation stub class. */
public static abstract class Stub extends android.os.Binder implements com.android.emailcommon.service.IPolicyService
{
private static final java.lang.String DESCRIPTOR = "com.android.emailcommon.service.IPolicyService";
/** Construct the stub at attach it to the interface. */
public Stub()
{
this.attachInterface(this, DESCRIPTOR);
}
/**
 * Cast an IBinder object into an com.android.emailcommon.service.IPolicyService interface,
 * generating a proxy if needed.
 */
public static com.android.emailcommon.service.IPolicyService asInterface(android.os.IBinder obj)
{
if ((obj==null)) {
return null;
}
android.os.IInterface iin = (android.os.IInterface)obj.queryLocalInterface(DESCRIPTOR);
if (((iin!=null)&&(iin instanceof com.android.emailcommon.service.IPolicyService))) {
return ((com.android.emailcommon.service.IPolicyService)iin);
}
return new com.android.emailcommon.service.IPolicyService.Stub.Proxy(obj);
}
public android.os.IBinder asBinder()
{
return this;
}
@Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
{
switch (code)
{
case INTERFACE_TRANSACTION:
{
reply.writeString(DESCRIPTOR);
return true;
}
case TRANSACTION_isActive:
{
data.enforceInterface(DESCRIPTOR);
com.android.emailcommon.provider.Policy _arg0;
if ((0!=data.readInt())) {
_arg0 = com.android.emailcommon.provider.Policy.CREATOR.createFromParcel(data);
}
else {
_arg0 = null;
}
boolean _result = this.isActive(_arg0);
reply.writeNoException();
reply.writeInt(((_result)?(1):(0)));
return true;
}
case TRANSACTION_policiesRequired:
{
data.enforceInterface(DESCRIPTOR);
long _arg0;
_arg0 = data.readLong();
this.policiesRequired(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_policiesUpdated:
{
data.enforceInterface(DESCRIPTOR);
long _arg0;
_arg0 = data.readLong();
this.policiesUpdated(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_setAccountHoldFlag:
{
data.enforceInterface(DESCRIPTOR);
long _arg0;
_arg0 = data.readLong();
boolean _arg1;
_arg1 = (0!=data.readInt());
this.setAccountHoldFlag(_arg0, _arg1);
reply.writeNoException();
return true;
}
case TRANSACTION_isActiveAdmin:
{
data.enforceInterface(DESCRIPTOR);
boolean _result = this.isActiveAdmin();
reply.writeNoException();
reply.writeInt(((_result)?(1):(0)));
return true;
}
case TRANSACTION_remoteWipe:
{
data.enforceInterface(DESCRIPTOR);
this.remoteWipe();
return true;
}
case TRANSACTION_isSupported:
{
data.enforceInterface(DESCRIPTOR);
com.android.emailcommon.provider.Policy _arg0;
if ((0!=data.readInt())) {
_arg0 = com.android.emailcommon.provider.Policy.CREATOR.createFromParcel(data);
}
else {
_arg0 = null;
}
boolean _result = this.isSupported(_arg0);
reply.writeNoException();
reply.writeInt(((_result)?(1):(0)));
return true;
}
case TRANSACTION_clearUnsupportedPolicies:
{
data.enforceInterface(DESCRIPTOR);
com.android.emailcommon.provider.Policy _arg0;
if ((0!=data.readInt())) {
_arg0 = com.android.emailcommon.provider.Policy.CREATOR.createFromParcel(data);
}
else {
_arg0 = null;
}
com.android.emailcommon.provider.Policy _result = this.clearUnsupportedPolicies(_arg0);
reply.writeNoException();
if ((_result!=null)) {
reply.writeInt(1);
_result.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
}
else {
reply.writeInt(0);
}
return true;
}
}
return super.onTransact(code, data, reply, flags);
}
private static class Proxy implements com.android.emailcommon.service.IPolicyService
{
private android.os.IBinder mRemote;
Proxy(android.os.IBinder remote)
{
mRemote = remote;
}
public android.os.IBinder asBinder()
{
return mRemote;
}
public java.lang.String getInterfaceDescriptor()
{
return DESCRIPTOR;
}
public boolean isActive(com.android.emailcommon.provider.Policy policies) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
boolean _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
if ((policies!=null)) {
_data.writeInt(1);
policies.writeToParcel(_data, 0);
}
else {
_data.writeInt(0);
}
mRemote.transact(Stub.TRANSACTION_isActive, _data, _reply, 0);
_reply.readException();
_result = (0!=_reply.readInt());
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
public void policiesRequired(long accountId) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeLong(accountId);
mRemote.transact(Stub.TRANSACTION_policiesRequired, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
public void policiesUpdated(long accountId) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeLong(accountId);
mRemote.transact(Stub.TRANSACTION_policiesUpdated, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
public void setAccountHoldFlag(long accountId, boolean newState) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeLong(accountId);
_data.writeInt(((newState)?(1):(0)));
mRemote.transact(Stub.TRANSACTION_setAccountHoldFlag, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
public boolean isActiveAdmin() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
boolean _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_isActiveAdmin, _data, _reply, 0);
_reply.readException();
_result = (0!=_reply.readInt());
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
// This is about as oneway as you can get

public void remoteWipe() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_remoteWipe, _data, null, android.os.IBinder.FLAG_ONEWAY);
}
finally {
_data.recycle();
}
}
public boolean isSupported(com.android.emailcommon.provider.Policy policies) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
boolean _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
if ((policies!=null)) {
_data.writeInt(1);
policies.writeToParcel(_data, 0);
}
else {
_data.writeInt(0);
}
mRemote.transact(Stub.TRANSACTION_isSupported, _data, _reply, 0);
_reply.readException();
_result = (0!=_reply.readInt());
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
public com.android.emailcommon.provider.Policy clearUnsupportedPolicies(com.android.emailcommon.provider.Policy policies) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
com.android.emailcommon.provider.Policy _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
if ((policies!=null)) {
_data.writeInt(1);
policies.writeToParcel(_data, 0);
}
else {
_data.writeInt(0);
}
mRemote.transact(Stub.TRANSACTION_clearUnsupportedPolicies, _data, _reply, 0);
_reply.readException();
if ((0!=_reply.readInt())) {
_result = com.android.emailcommon.provider.Policy.CREATOR.createFromParcel(_reply);
}
else {
_result = null;
}
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
}
static final int TRANSACTION_isActive = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
static final int TRANSACTION_policiesRequired = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
static final int TRANSACTION_policiesUpdated = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
static final int TRANSACTION_setAccountHoldFlag = (android.os.IBinder.FIRST_CALL_TRANSACTION + 3);
static final int TRANSACTION_isActiveAdmin = (android.os.IBinder.FIRST_CALL_TRANSACTION + 4);
static final int TRANSACTION_remoteWipe = (android.os.IBinder.FIRST_CALL_TRANSACTION + 5);
static final int TRANSACTION_isSupported = (android.os.IBinder.FIRST_CALL_TRANSACTION + 6);
static final int TRANSACTION_clearUnsupportedPolicies = (android.os.IBinder.FIRST_CALL_TRANSACTION + 7);
}
public boolean isActive(com.android.emailcommon.provider.Policy policies) throws android.os.RemoteException;
public void policiesRequired(long accountId) throws android.os.RemoteException;
public void policiesUpdated(long accountId) throws android.os.RemoteException;
public void setAccountHoldFlag(long accountId, boolean newState) throws android.os.RemoteException;
public boolean isActiveAdmin() throws android.os.RemoteException;
// This is about as oneway as you can get

public void remoteWipe() throws android.os.RemoteException;
public boolean isSupported(com.android.emailcommon.provider.Policy policies) throws android.os.RemoteException;
public com.android.emailcommon.provider.Policy clearUnsupportedPolicies(com.android.emailcommon.provider.Policy policies) throws android.os.RemoteException;
}
