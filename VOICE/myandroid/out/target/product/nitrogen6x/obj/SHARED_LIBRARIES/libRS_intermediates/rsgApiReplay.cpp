/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "rsDevice.h"
#include "rsContext.h"
#include "rsThreadIO.h"
#include "rsgApiFuncDecl.h"

namespace android {
namespace renderscript {

void rsp_ContextFinish(Context *con, const void *vp, size_t cmdSizeBytes) {
    const RS_CMD_ContextFinish *cmd = static_cast<const RS_CMD_ContextFinish *>(vp);
    rsi_ContextFinish(con);
};

void rsp_ContextBindRootScript(Context *con, const void *vp, size_t cmdSizeBytes) {
    const RS_CMD_ContextBindRootScript *cmd = static_cast<const RS_CMD_ContextBindRootScript *>(vp);
    rsi_ContextBindRootScript(con,
           cmd->sampler);
};

void rsp_ContextBindProgramStore(Context *con, const void *vp, size_t cmdSizeBytes) {
    const RS_CMD_ContextBindProgramStore *cmd = static_cast<const RS_CMD_ContextBindProgramStore *>(vp);
    rsi_ContextBindProgramStore(con,
           cmd->pgm);
};

void rsp_ContextBindProgramFragment(Context *con, const void *vp, size_t cmdSizeBytes) {
    const RS_CMD_ContextBindProgramFragment *cmd = static_cast<const RS_CMD_ContextBindProgramFragment *>(vp);
    rsi_ContextBindProgramFragment(con,
           cmd->pgm);
};

void rsp_ContextBindProgramVertex(Context *con, const void *vp, size_t cmdSizeBytes) {
    const RS_CMD_ContextBindProgramVertex *cmd = static_cast<const RS_CMD_ContextBindProgramVertex *>(vp);
    rsi_ContextBindProgramVertex(con,
           cmd->pgm);
};

void rsp_ContextBindProgramRaster(Context *con, const void *vp, size_t cmdSizeBytes) {
    const RS_CMD_ContextBindProgramRaster *cmd = static_cast<const RS_CMD_ContextBindProgramRaster *>(vp);
    rsi_ContextBindProgramRaster(con,
           cmd->pgm);
};

void rsp_ContextBindFont(Context *con, const void *vp, size_t cmdSizeBytes) {
    const RS_CMD_ContextBindFont *cmd = static_cast<const RS_CMD_ContextBindFont *>(vp);
    rsi_ContextBindFont(con,
           cmd->pgm);
};

void rsp_ContextPause(Context *con, const void *vp, size_t cmdSizeBytes) {
    const RS_CMD_ContextPause *cmd = static_cast<const RS_CMD_ContextPause *>(vp);
    rsi_ContextPause(con);
};

void rsp_ContextResume(Context *con, const void *vp, size_t cmdSizeBytes) {
    const RS_CMD_ContextResume *cmd = static_cast<const RS_CMD_ContextResume *>(vp);
    rsi_ContextResume(con);
};

void rsp_ContextSetSurface(Context *con, const void *vp, size_t cmdSizeBytes) {
    const RS_CMD_ContextSetSurface *cmd = static_cast<const RS_CMD_ContextSetSurface *>(vp);
    rsi_ContextSetSurface(con,
           cmd->width,
           cmd->height,
           cmd->sur);
};

void rsp_ContextDump(Context *con, const void *vp, size_t cmdSizeBytes) {
    const RS_CMD_ContextDump *cmd = static_cast<const RS_CMD_ContextDump *>(vp);
    rsi_ContextDump(con,
           cmd->bits);
};

void rsp_ContextSetPriority(Context *con, const void *vp, size_t cmdSizeBytes) {
    const RS_CMD_ContextSetPriority *cmd = static_cast<const RS_CMD_ContextSetPriority *>(vp);
    rsi_ContextSetPriority(con,
           cmd->priority);
};

void rsp_ContextDestroyWorker(Context *con, const void *vp, size_t cmdSizeBytes) {
    const RS_CMD_ContextDestroyWorker *cmd = static_cast<const RS_CMD_ContextDestroyWorker *>(vp);
    rsi_ContextDestroyWorker(con);
};

void rsp_AssignName(Context *con, const void *vp, size_t cmdSizeBytes) {
    const RS_CMD_AssignName *cmd = static_cast<const RS_CMD_AssignName *>(vp);
    rsi_AssignName(con,
           cmd->obj,
           cmd->name,
           cmd->name_length);
};

void rsp_ObjDestroy(Context *con, const void *vp, size_t cmdSizeBytes) {
    const RS_CMD_ObjDestroy *cmd = static_cast<const RS_CMD_ObjDestroy *>(vp);
    rsi_ObjDestroy(con,
           cmd->objPtr);
};

void rsp_AllocationCopyToBitmap(Context *con, const void *vp, size_t cmdSizeBytes) {
    const RS_CMD_AllocationCopyToBitmap *cmd = static_cast<const RS_CMD_AllocationCopyToBitmap *>(vp);
    rsi_AllocationCopyToBitmap(con,
           cmd->alloc,
           cmd->data,
           cmd->data_length);
};

void rsp_Allocation1DData(Context *con, const void *vp, size_t cmdSizeBytes) {
    const RS_CMD_Allocation1DData *cmd = static_cast<const RS_CMD_Allocation1DData *>(vp);
    rsi_Allocation1DData(con,
           cmd->va,
           cmd->xoff,
           cmd->lod,
           cmd->count,
           cmd->data,
           cmd->data_length);
};

void rsp_Allocation1DElementData(Context *con, const void *vp, size_t cmdSizeBytes) {
    const RS_CMD_Allocation1DElementData *cmd = static_cast<const RS_CMD_Allocation1DElementData *>(vp);
    rsi_Allocation1DElementData(con,
           cmd->va,
           cmd->x,
           cmd->lod,
           cmd->data,
           cmd->data_length,
           cmd->comp_offset);
};

void rsp_Allocation2DData(Context *con, const void *vp, size_t cmdSizeBytes) {
    const RS_CMD_Allocation2DData *cmd = static_cast<const RS_CMD_Allocation2DData *>(vp);
    rsi_Allocation2DData(con,
           cmd->va,
           cmd->xoff,
           cmd->yoff,
           cmd->lod,
           cmd->face,
           cmd->w,
           cmd->h,
           cmd->data,
           cmd->data_length);
};

void rsp_Allocation2DElementData(Context *con, const void *vp, size_t cmdSizeBytes) {
    const RS_CMD_Allocation2DElementData *cmd = static_cast<const RS_CMD_Allocation2DElementData *>(vp);
    rsi_Allocation2DElementData(con,
           cmd->va,
           cmd->x,
           cmd->y,
           cmd->lod,
           cmd->face,
           cmd->data,
           cmd->data_length,
           cmd->element_offset);
};

void rsp_AllocationGenerateMipmaps(Context *con, const void *vp, size_t cmdSizeBytes) {
    const RS_CMD_AllocationGenerateMipmaps *cmd = static_cast<const RS_CMD_AllocationGenerateMipmaps *>(vp);
    rsi_AllocationGenerateMipmaps(con,
           cmd->va);
};

void rsp_AllocationRead(Context *con, const void *vp, size_t cmdSizeBytes) {
    const RS_CMD_AllocationRead *cmd = static_cast<const RS_CMD_AllocationRead *>(vp);
    rsi_AllocationRead(con,
           cmd->va,
           cmd->data,
           cmd->data_length);
};

void rsp_AllocationSyncAll(Context *con, const void *vp, size_t cmdSizeBytes) {
    const RS_CMD_AllocationSyncAll *cmd = static_cast<const RS_CMD_AllocationSyncAll *>(vp);
    rsi_AllocationSyncAll(con,
           cmd->va,
           cmd->src);
};

void rsp_AllocationResize1D(Context *con, const void *vp, size_t cmdSizeBytes) {
    const RS_CMD_AllocationResize1D *cmd = static_cast<const RS_CMD_AllocationResize1D *>(vp);
    rsi_AllocationResize1D(con,
           cmd->va,
           cmd->dimX);
};

void rsp_AllocationResize2D(Context *con, const void *vp, size_t cmdSizeBytes) {
    const RS_CMD_AllocationResize2D *cmd = static_cast<const RS_CMD_AllocationResize2D *>(vp);
    rsi_AllocationResize2D(con,
           cmd->va,
           cmd->dimX,
           cmd->dimY);
};

void rsp_AllocationCopy2DRange(Context *con, const void *vp, size_t cmdSizeBytes) {
    const RS_CMD_AllocationCopy2DRange *cmd = static_cast<const RS_CMD_AllocationCopy2DRange *>(vp);
    rsi_AllocationCopy2DRange(con,
           cmd->dest,
           cmd->destXoff,
           cmd->destYoff,
           cmd->destMip,
           cmd->destFace,
           cmd->width,
           cmd->height,
           cmd->src,
           cmd->srcXoff,
           cmd->srcYoff,
           cmd->srcMip,
           cmd->srcFace);
};

void rsp_ScriptBindAllocation(Context *con, const void *vp, size_t cmdSizeBytes) {
    const RS_CMD_ScriptBindAllocation *cmd = static_cast<const RS_CMD_ScriptBindAllocation *>(vp);
    rsi_ScriptBindAllocation(con,
           cmd->vtm,
           cmd->va,
           cmd->slot);
};

void rsp_ScriptSetTimeZone(Context *con, const void *vp, size_t cmdSizeBytes) {
    const RS_CMD_ScriptSetTimeZone *cmd = static_cast<const RS_CMD_ScriptSetTimeZone *>(vp);
    rsi_ScriptSetTimeZone(con,
           cmd->s,
           cmd->timeZone,
           cmd->timeZone_length);
};

void rsp_ScriptInvoke(Context *con, const void *vp, size_t cmdSizeBytes) {
    const RS_CMD_ScriptInvoke *cmd = static_cast<const RS_CMD_ScriptInvoke *>(vp);
    rsi_ScriptInvoke(con,
           cmd->s,
           cmd->slot);
};

void rsp_ScriptInvokeV(Context *con, const void *vp, size_t cmdSizeBytes) {
    const RS_CMD_ScriptInvokeV *cmd = static_cast<const RS_CMD_ScriptInvokeV *>(vp);
    rsi_ScriptInvokeV(con,
           cmd->s,
           cmd->slot,
           cmd->data,
           cmd->data_length);
};

void rsp_ScriptForEach(Context *con, const void *vp, size_t cmdSizeBytes) {
    const RS_CMD_ScriptForEach *cmd = static_cast<const RS_CMD_ScriptForEach *>(vp);
    rsi_ScriptForEach(con,
           cmd->s,
           cmd->slot,
           cmd->ain,
           cmd->aout,
           cmd->usr,
           cmd->usr_length);
};

void rsp_ScriptSetVarI(Context *con, const void *vp, size_t cmdSizeBytes) {
    const RS_CMD_ScriptSetVarI *cmd = static_cast<const RS_CMD_ScriptSetVarI *>(vp);
    rsi_ScriptSetVarI(con,
           cmd->s,
           cmd->slot,
           cmd->value);
};

void rsp_ScriptSetVarObj(Context *con, const void *vp, size_t cmdSizeBytes) {
    const RS_CMD_ScriptSetVarObj *cmd = static_cast<const RS_CMD_ScriptSetVarObj *>(vp);
    rsi_ScriptSetVarObj(con,
           cmd->s,
           cmd->slot,
           cmd->value);
};

void rsp_ScriptSetVarJ(Context *con, const void *vp, size_t cmdSizeBytes) {
    const RS_CMD_ScriptSetVarJ *cmd = static_cast<const RS_CMD_ScriptSetVarJ *>(vp);
    rsi_ScriptSetVarJ(con,
           cmd->s,
           cmd->slot,
           cmd->value);
};

void rsp_ScriptSetVarF(Context *con, const void *vp, size_t cmdSizeBytes) {
    const RS_CMD_ScriptSetVarF *cmd = static_cast<const RS_CMD_ScriptSetVarF *>(vp);
    rsi_ScriptSetVarF(con,
           cmd->s,
           cmd->slot,
           cmd->value);
};

void rsp_ScriptSetVarD(Context *con, const void *vp, size_t cmdSizeBytes) {
    const RS_CMD_ScriptSetVarD *cmd = static_cast<const RS_CMD_ScriptSetVarD *>(vp);
    rsi_ScriptSetVarD(con,
           cmd->s,
           cmd->slot,
           cmd->value);
};

void rsp_ScriptSetVarV(Context *con, const void *vp, size_t cmdSizeBytes) {
    const RS_CMD_ScriptSetVarV *cmd = static_cast<const RS_CMD_ScriptSetVarV *>(vp);
    rsi_ScriptSetVarV(con,
           cmd->s,
           cmd->slot,
           cmd->data,
           cmd->data_length);
};

void rsp_ScriptCCreate(Context *con, const void *vp, size_t cmdSizeBytes) {
    const RS_CMD_ScriptCCreate *cmd = static_cast<const RS_CMD_ScriptCCreate *>(vp);
    
    RsScript ret = rsi_ScriptCCreate(con,
           cmd->resName,
           cmd->resName_length,
           cmd->cacheDir,
           cmd->cacheDir_length,
           cmd->text,
           cmd->text_length);
    con->mIO.coreSetReturn(&ret, sizeof(ret));
};

void rsp_ProgramBindConstants(Context *con, const void *vp, size_t cmdSizeBytes) {
    const RS_CMD_ProgramBindConstants *cmd = static_cast<const RS_CMD_ProgramBindConstants *>(vp);
    rsi_ProgramBindConstants(con,
           cmd->vp,
           cmd->slot,
           cmd->constants);
};

void rsp_ProgramBindTexture(Context *con, const void *vp, size_t cmdSizeBytes) {
    const RS_CMD_ProgramBindTexture *cmd = static_cast<const RS_CMD_ProgramBindTexture *>(vp);
    rsi_ProgramBindTexture(con,
           cmd->pf,
           cmd->slot,
           cmd->a);
};

void rsp_ProgramBindSampler(Context *con, const void *vp, size_t cmdSizeBytes) {
    const RS_CMD_ProgramBindSampler *cmd = static_cast<const RS_CMD_ProgramBindSampler *>(vp);
    rsi_ProgramBindSampler(con,
           cmd->pf,
           cmd->slot,
           cmd->s);
};

void rsp_FontCreateFromFile(Context *con, const void *vp, size_t cmdSizeBytes) {
    const RS_CMD_FontCreateFromFile *cmd = static_cast<const RS_CMD_FontCreateFromFile *>(vp);
    
    RsFont ret = rsi_FontCreateFromFile(con,
           cmd->name,
           cmd->name_length,
           cmd->fontSize,
           cmd->dpi);
    con->mIO.coreSetReturn(&ret, sizeof(ret));
};

void rsp_FontCreateFromMemory(Context *con, const void *vp, size_t cmdSizeBytes) {
    const RS_CMD_FontCreateFromMemory *cmd = static_cast<const RS_CMD_FontCreateFromMemory *>(vp);
    
    RsFont ret = rsi_FontCreateFromMemory(con,
           cmd->name,
           cmd->name_length,
           cmd->fontSize,
           cmd->dpi,
           cmd->data,
           cmd->data_length);
    con->mIO.coreSetReturn(&ret, sizeof(ret));
};

void rsp_MeshCreate(Context *con, const void *vp, size_t cmdSizeBytes) {
    const RS_CMD_MeshCreate *cmd = static_cast<const RS_CMD_MeshCreate *>(vp);
    
    RsMesh ret = rsi_MeshCreate(con,
           cmd->vtx,
           cmd->vtx_length,
           cmd->idx,
           cmd->idx_length,
           cmd->primType,
           cmd->primType_length);
    con->mIO.coreSetReturn(&ret, sizeof(ret));
};

void rspr_ContextDestroy(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_ContextDestroy cmd;
    f->read(&cmd, sizeof(cmd));

    rsi_ContextDestroy(con);
};

void rspr_ContextGetMessage(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_ContextGetMessage cmd;
    f->read(&cmd, sizeof(cmd));
    cmd.data = (void *)scratch;
    f->read(scratch, cmd.data_length);
    scratch += cmd.data_length;
    cmd.receiveLen = (size_t *)scratch;
    f->read(scratch, cmd.receiveLen_length);
    scratch += cmd.receiveLen_length;
    cmd.usrID = (uint32_t *)scratch;
    f->read(scratch, cmd.usrID_length);
    scratch += cmd.usrID_length;

    RsMessageToClientType ret =
    rsi_ContextGetMessage(con,
           cmd.data,
           cmd.data_length,
           cmd.receiveLen,
           cmd.receiveLen_length,
           cmd.usrID,
           cmd.usrID_length);
    f->readReturn(&ret, sizeof(ret));
};

void rspr_ContextPeekMessage(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_ContextPeekMessage cmd;
    f->read(&cmd, sizeof(cmd));
    cmd.receiveLen = (size_t *)scratch;
    f->read(scratch, cmd.receiveLen_length);
    scratch += cmd.receiveLen_length;
    cmd.usrID = (uint32_t *)scratch;
    f->read(scratch, cmd.usrID_length);
    scratch += cmd.usrID_length;

    RsMessageToClientType ret =
    rsi_ContextPeekMessage(con,
           cmd.receiveLen,
           cmd.receiveLen_length,
           cmd.usrID,
           cmd.usrID_length);
    f->readReturn(&ret, sizeof(ret));
};

void rspr_ContextInitToClient(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_ContextInitToClient cmd;
    f->read(&cmd, sizeof(cmd));

    rsi_ContextInitToClient(con);
};

void rspr_ContextDeinitToClient(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_ContextDeinitToClient cmd;
    f->read(&cmd, sizeof(cmd));

    rsi_ContextDeinitToClient(con);
};

void rspr_TypeCreate(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_TypeCreate cmd;
    f->read(&cmd, sizeof(cmd));

    RsType ret =
    rsi_TypeCreate(con,
           cmd.e,
           cmd.dimX,
           cmd.dimY,
           cmd.dimZ,
           cmd.mips,
           cmd.faces);
    f->readReturn(&ret, sizeof(ret));
};

void rspr_AllocationCreateTyped(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_AllocationCreateTyped cmd;
    f->read(&cmd, sizeof(cmd));

    RsAllocation ret =
    rsi_AllocationCreateTyped(con,
           cmd.vtype,
           cmd.mips,
           cmd.usages);
    f->readReturn(&ret, sizeof(ret));
};

void rspr_AllocationCreateFromBitmap(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_AllocationCreateFromBitmap cmd;
    f->read(&cmd, sizeof(cmd));
    cmd.data = (const void *)scratch;
    f->read(scratch, cmd.data_length);
    scratch += cmd.data_length;

    RsAllocation ret =
    rsi_AllocationCreateFromBitmap(con,
           cmd.vtype,
           cmd.mips,
           cmd.data,
           cmd.data_length,
           cmd.usages);
    f->readReturn(&ret, sizeof(ret));
};

void rspr_AllocationCubeCreateFromBitmap(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_AllocationCubeCreateFromBitmap cmd;
    f->read(&cmd, sizeof(cmd));
    cmd.data = (const void *)scratch;
    f->read(scratch, cmd.data_length);
    scratch += cmd.data_length;

    RsAllocation ret =
    rsi_AllocationCubeCreateFromBitmap(con,
           cmd.vtype,
           cmd.mips,
           cmd.data,
           cmd.data_length,
           cmd.usages);
    f->readReturn(&ret, sizeof(ret));
};

void rspr_ContextFinish(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_ContextFinish cmd;
    f->read(&cmd, sizeof(cmd));

    rsi_ContextFinish(con);
};

void rspr_ContextBindRootScript(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_ContextBindRootScript cmd;
    f->read(&cmd, sizeof(cmd));

    rsi_ContextBindRootScript(con,
           cmd.sampler);
};

void rspr_ContextBindProgramStore(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_ContextBindProgramStore cmd;
    f->read(&cmd, sizeof(cmd));

    rsi_ContextBindProgramStore(con,
           cmd.pgm);
};

void rspr_ContextBindProgramFragment(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_ContextBindProgramFragment cmd;
    f->read(&cmd, sizeof(cmd));

    rsi_ContextBindProgramFragment(con,
           cmd.pgm);
};

void rspr_ContextBindProgramVertex(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_ContextBindProgramVertex cmd;
    f->read(&cmd, sizeof(cmd));

    rsi_ContextBindProgramVertex(con,
           cmd.pgm);
};

void rspr_ContextBindProgramRaster(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_ContextBindProgramRaster cmd;
    f->read(&cmd, sizeof(cmd));

    rsi_ContextBindProgramRaster(con,
           cmd.pgm);
};

void rspr_ContextBindFont(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_ContextBindFont cmd;
    f->read(&cmd, sizeof(cmd));

    rsi_ContextBindFont(con,
           cmd.pgm);
};

void rspr_ContextPause(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_ContextPause cmd;
    f->read(&cmd, sizeof(cmd));

    rsi_ContextPause(con);
};

void rspr_ContextResume(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_ContextResume cmd;
    f->read(&cmd, sizeof(cmd));

    rsi_ContextResume(con);
};

void rspr_ContextSetSurface(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_ContextSetSurface cmd;
    f->read(&cmd, sizeof(cmd));

    rsi_ContextSetSurface(con,
           cmd.width,
           cmd.height,
           cmd.sur);
};

void rspr_ContextDump(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_ContextDump cmd;
    f->read(&cmd, sizeof(cmd));

    rsi_ContextDump(con,
           cmd.bits);
};

void rspr_ContextSetPriority(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_ContextSetPriority cmd;
    f->read(&cmd, sizeof(cmd));

    rsi_ContextSetPriority(con,
           cmd.priority);
};

void rspr_ContextDestroyWorker(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_ContextDestroyWorker cmd;
    f->read(&cmd, sizeof(cmd));

    rsi_ContextDestroyWorker(con);
};

void rspr_AssignName(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_AssignName cmd;
    f->read(&cmd, sizeof(cmd));
    cmd.name = (const char *)scratch;
    f->read(scratch, cmd.name_length);
    scratch += cmd.name_length;

    rsi_AssignName(con,
           cmd.obj,
           cmd.name,
           cmd.name_length);
};

void rspr_ObjDestroy(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_ObjDestroy cmd;
    f->read(&cmd, sizeof(cmd));

    rsi_ObjDestroy(con,
           cmd.objPtr);
};

void rspr_ElementCreate(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_ElementCreate cmd;
    f->read(&cmd, sizeof(cmd));

    RsElement ret =
    rsi_ElementCreate(con,
           cmd.mType,
           cmd.mKind,
           cmd.mNormalized,
           cmd.mVectorSize);
    f->readReturn(&ret, sizeof(ret));
};

void rspr_ElementCreate2(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_ElementCreate2 cmd;
    f->read(&cmd, sizeof(cmd));
    cmd.elements = (const RsElement *)scratch;
    f->read(scratch, cmd.elements_length);
    scratch += cmd.elements_length;
    size_t sum_names = 0;
    for (size_t ct = 0; ct < (cmd.names_length_length / sizeof(cmd.names_length)); ct++) {
        ((size_t *)scratch)[ct] = cmd.names_length[ct];
        sum_names += cmd.names_length[ct];
    }
    f->read(scratch, sum_names);
    scratch += sum_names;
    cmd.names_length = (const size_t *)scratch;
    f->read(scratch, cmd.names_length_length);
    scratch += cmd.names_length_length;
    cmd.arraySize = (const uint32_t *)scratch;
    f->read(scratch, cmd.arraySize_length);
    scratch += cmd.arraySize_length;

    RsElement ret =
    rsi_ElementCreate2(con,
           cmd.elements,
           cmd.elements_length,
           cmd.names,
           cmd.names_length_length,
           cmd.names_length,
           cmd.arraySize,
           cmd.arraySize_length);
    f->readReturn(&ret, sizeof(ret));
};

void rspr_AllocationCopyToBitmap(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_AllocationCopyToBitmap cmd;
    f->read(&cmd, sizeof(cmd));
    cmd.data = (void *)scratch;
    f->read(scratch, cmd.data_length);
    scratch += cmd.data_length;

    rsi_AllocationCopyToBitmap(con,
           cmd.alloc,
           cmd.data,
           cmd.data_length);
};

void rspr_Allocation1DData(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_Allocation1DData cmd;
    f->read(&cmd, sizeof(cmd));
    cmd.data = (const void *)scratch;
    f->read(scratch, cmd.data_length);
    scratch += cmd.data_length;

    rsi_Allocation1DData(con,
           cmd.va,
           cmd.xoff,
           cmd.lod,
           cmd.count,
           cmd.data,
           cmd.data_length);
};

void rspr_Allocation1DElementData(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_Allocation1DElementData cmd;
    f->read(&cmd, sizeof(cmd));
    cmd.data = (const void *)scratch;
    f->read(scratch, cmd.data_length);
    scratch += cmd.data_length;

    rsi_Allocation1DElementData(con,
           cmd.va,
           cmd.x,
           cmd.lod,
           cmd.data,
           cmd.data_length,
           cmd.comp_offset);
};

void rspr_Allocation2DData(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_Allocation2DData cmd;
    f->read(&cmd, sizeof(cmd));
    cmd.data = (const void *)scratch;
    f->read(scratch, cmd.data_length);
    scratch += cmd.data_length;

    rsi_Allocation2DData(con,
           cmd.va,
           cmd.xoff,
           cmd.yoff,
           cmd.lod,
           cmd.face,
           cmd.w,
           cmd.h,
           cmd.data,
           cmd.data_length);
};

void rspr_Allocation2DElementData(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_Allocation2DElementData cmd;
    f->read(&cmd, sizeof(cmd));
    cmd.data = (const void *)scratch;
    f->read(scratch, cmd.data_length);
    scratch += cmd.data_length;

    rsi_Allocation2DElementData(con,
           cmd.va,
           cmd.x,
           cmd.y,
           cmd.lod,
           cmd.face,
           cmd.data,
           cmd.data_length,
           cmd.element_offset);
};

void rspr_AllocationGenerateMipmaps(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_AllocationGenerateMipmaps cmd;
    f->read(&cmd, sizeof(cmd));

    rsi_AllocationGenerateMipmaps(con,
           cmd.va);
};

void rspr_AllocationRead(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_AllocationRead cmd;
    f->read(&cmd, sizeof(cmd));
    cmd.data = (void *)scratch;
    f->read(scratch, cmd.data_length);
    scratch += cmd.data_length;

    rsi_AllocationRead(con,
           cmd.va,
           cmd.data,
           cmd.data_length);
};

void rspr_AllocationSyncAll(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_AllocationSyncAll cmd;
    f->read(&cmd, sizeof(cmd));

    rsi_AllocationSyncAll(con,
           cmd.va,
           cmd.src);
};

void rspr_AllocationResize1D(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_AllocationResize1D cmd;
    f->read(&cmd, sizeof(cmd));

    rsi_AllocationResize1D(con,
           cmd.va,
           cmd.dimX);
};

void rspr_AllocationResize2D(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_AllocationResize2D cmd;
    f->read(&cmd, sizeof(cmd));

    rsi_AllocationResize2D(con,
           cmd.va,
           cmd.dimX,
           cmd.dimY);
};

void rspr_AllocationCopy2DRange(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_AllocationCopy2DRange cmd;
    f->read(&cmd, sizeof(cmd));

    rsi_AllocationCopy2DRange(con,
           cmd.dest,
           cmd.destXoff,
           cmd.destYoff,
           cmd.destMip,
           cmd.destFace,
           cmd.width,
           cmd.height,
           cmd.src,
           cmd.srcXoff,
           cmd.srcYoff,
           cmd.srcMip,
           cmd.srcFace);
};

void rspr_SamplerCreate(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_SamplerCreate cmd;
    f->read(&cmd, sizeof(cmd));

    RsSampler ret =
    rsi_SamplerCreate(con,
           cmd.magFilter,
           cmd.minFilter,
           cmd.wrapS,
           cmd.wrapT,
           cmd.wrapR,
           cmd.mAniso);
    f->readReturn(&ret, sizeof(ret));
};

void rspr_ScriptBindAllocation(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_ScriptBindAllocation cmd;
    f->read(&cmd, sizeof(cmd));

    rsi_ScriptBindAllocation(con,
           cmd.vtm,
           cmd.va,
           cmd.slot);
};

void rspr_ScriptSetTimeZone(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_ScriptSetTimeZone cmd;
    f->read(&cmd, sizeof(cmd));
    cmd.timeZone = (const char *)scratch;
    f->read(scratch, cmd.timeZone_length);
    scratch += cmd.timeZone_length;

    rsi_ScriptSetTimeZone(con,
           cmd.s,
           cmd.timeZone,
           cmd.timeZone_length);
};

void rspr_ScriptInvoke(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_ScriptInvoke cmd;
    f->read(&cmd, sizeof(cmd));

    rsi_ScriptInvoke(con,
           cmd.s,
           cmd.slot);
};

void rspr_ScriptInvokeV(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_ScriptInvokeV cmd;
    f->read(&cmd, sizeof(cmd));
    cmd.data = (const void *)scratch;
    f->read(scratch, cmd.data_length);
    scratch += cmd.data_length;

    rsi_ScriptInvokeV(con,
           cmd.s,
           cmd.slot,
           cmd.data,
           cmd.data_length);
};

void rspr_ScriptForEach(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_ScriptForEach cmd;
    f->read(&cmd, sizeof(cmd));
    cmd.usr = (const void *)scratch;
    f->read(scratch, cmd.usr_length);
    scratch += cmd.usr_length;

    rsi_ScriptForEach(con,
           cmd.s,
           cmd.slot,
           cmd.ain,
           cmd.aout,
           cmd.usr,
           cmd.usr_length);
};

void rspr_ScriptSetVarI(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_ScriptSetVarI cmd;
    f->read(&cmd, sizeof(cmd));

    rsi_ScriptSetVarI(con,
           cmd.s,
           cmd.slot,
           cmd.value);
};

void rspr_ScriptSetVarObj(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_ScriptSetVarObj cmd;
    f->read(&cmd, sizeof(cmd));

    rsi_ScriptSetVarObj(con,
           cmd.s,
           cmd.slot,
           cmd.value);
};

void rspr_ScriptSetVarJ(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_ScriptSetVarJ cmd;
    f->read(&cmd, sizeof(cmd));

    rsi_ScriptSetVarJ(con,
           cmd.s,
           cmd.slot,
           cmd.value);
};

void rspr_ScriptSetVarF(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_ScriptSetVarF cmd;
    f->read(&cmd, sizeof(cmd));

    rsi_ScriptSetVarF(con,
           cmd.s,
           cmd.slot,
           cmd.value);
};

void rspr_ScriptSetVarD(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_ScriptSetVarD cmd;
    f->read(&cmd, sizeof(cmd));

    rsi_ScriptSetVarD(con,
           cmd.s,
           cmd.slot,
           cmd.value);
};

void rspr_ScriptSetVarV(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_ScriptSetVarV cmd;
    f->read(&cmd, sizeof(cmd));
    cmd.data = (const void *)scratch;
    f->read(scratch, cmd.data_length);
    scratch += cmd.data_length;

    rsi_ScriptSetVarV(con,
           cmd.s,
           cmd.slot,
           cmd.data,
           cmd.data_length);
};

void rspr_ScriptCCreate(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_ScriptCCreate cmd;
    f->read(&cmd, sizeof(cmd));
    cmd.resName = (const char *)scratch;
    f->read(scratch, cmd.resName_length);
    scratch += cmd.resName_length;
    cmd.cacheDir = (const char *)scratch;
    f->read(scratch, cmd.cacheDir_length);
    scratch += cmd.cacheDir_length;
    cmd.text = (const char *)scratch;
    f->read(scratch, cmd.text_length);
    scratch += cmd.text_length;

    RsScript ret =
    rsi_ScriptCCreate(con,
           cmd.resName,
           cmd.resName_length,
           cmd.cacheDir,
           cmd.cacheDir_length,
           cmd.text,
           cmd.text_length);
    f->readReturn(&ret, sizeof(ret));
};

void rspr_ProgramStoreCreate(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_ProgramStoreCreate cmd;
    f->read(&cmd, sizeof(cmd));

    RsProgramStore ret =
    rsi_ProgramStoreCreate(con,
           cmd.colorMaskR,
           cmd.colorMaskG,
           cmd.colorMaskB,
           cmd.colorMaskA,
           cmd.depthMask,
           cmd.ditherEnable,
           cmd.srcFunc,
           cmd.destFunc,
           cmd.depthFunc);
    f->readReturn(&ret, sizeof(ret));
};

void rspr_ProgramRasterCreate(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_ProgramRasterCreate cmd;
    f->read(&cmd, sizeof(cmd));

    RsProgramRaster ret =
    rsi_ProgramRasterCreate(con,
           cmd.pointSprite,
           cmd.cull);
    f->readReturn(&ret, sizeof(ret));
};

void rspr_ProgramBindConstants(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_ProgramBindConstants cmd;
    f->read(&cmd, sizeof(cmd));

    rsi_ProgramBindConstants(con,
           cmd.vp,
           cmd.slot,
           cmd.constants);
};

void rspr_ProgramBindTexture(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_ProgramBindTexture cmd;
    f->read(&cmd, sizeof(cmd));

    rsi_ProgramBindTexture(con,
           cmd.pf,
           cmd.slot,
           cmd.a);
};

void rspr_ProgramBindSampler(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_ProgramBindSampler cmd;
    f->read(&cmd, sizeof(cmd));

    rsi_ProgramBindSampler(con,
           cmd.pf,
           cmd.slot,
           cmd.s);
};

void rspr_ProgramFragmentCreate(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_ProgramFragmentCreate cmd;
    f->read(&cmd, sizeof(cmd));
    cmd.shaderText = (const char *)scratch;
    f->read(scratch, cmd.shaderText_length);
    scratch += cmd.shaderText_length;
    cmd.params = (const uint32_t *)scratch;
    f->read(scratch, cmd.params_length);
    scratch += cmd.params_length;

    RsProgramFragment ret =
    rsi_ProgramFragmentCreate(con,
           cmd.shaderText,
           cmd.shaderText_length,
           cmd.params,
           cmd.params_length);
    f->readReturn(&ret, sizeof(ret));
};

void rspr_ProgramVertexCreate(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_ProgramVertexCreate cmd;
    f->read(&cmd, sizeof(cmd));
    cmd.shaderText = (const char *)scratch;
    f->read(scratch, cmd.shaderText_length);
    scratch += cmd.shaderText_length;
    cmd.params = (const uint32_t *)scratch;
    f->read(scratch, cmd.params_length);
    scratch += cmd.params_length;

    RsProgramVertex ret =
    rsi_ProgramVertexCreate(con,
           cmd.shaderText,
           cmd.shaderText_length,
           cmd.params,
           cmd.params_length);
    f->readReturn(&ret, sizeof(ret));
};

void rspr_FontCreateFromFile(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_FontCreateFromFile cmd;
    f->read(&cmd, sizeof(cmd));
    cmd.name = (const char *)scratch;
    f->read(scratch, cmd.name_length);
    scratch += cmd.name_length;

    RsFont ret =
    rsi_FontCreateFromFile(con,
           cmd.name,
           cmd.name_length,
           cmd.fontSize,
           cmd.dpi);
    f->readReturn(&ret, sizeof(ret));
};

void rspr_FontCreateFromMemory(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_FontCreateFromMemory cmd;
    f->read(&cmd, sizeof(cmd));
    cmd.name = (const char *)scratch;
    f->read(scratch, cmd.name_length);
    scratch += cmd.name_length;
    cmd.data = (const void *)scratch;
    f->read(scratch, cmd.data_length);
    scratch += cmd.data_length;

    RsFont ret =
    rsi_FontCreateFromMemory(con,
           cmd.name,
           cmd.name_length,
           cmd.fontSize,
           cmd.dpi,
           cmd.data,
           cmd.data_length);
    f->readReturn(&ret, sizeof(ret));
};

void rspr_MeshCreate(Context *con, Fifo *f, uint8_t *scratch, size_t scratchSize) {
    RS_CMD_MeshCreate cmd;
    f->read(&cmd, sizeof(cmd));
    cmd.vtx = (RsAllocation *)scratch;
    f->read(scratch, cmd.vtx_length);
    scratch += cmd.vtx_length;
    cmd.idx = (RsAllocation *)scratch;
    f->read(scratch, cmd.idx_length);
    scratch += cmd.idx_length;
    cmd.primType = (uint32_t *)scratch;
    f->read(scratch, cmd.primType_length);
    scratch += cmd.primType_length;

    RsMesh ret =
    rsi_MeshCreate(con,
           cmd.vtx,
           cmd.vtx_length,
           cmd.idx,
           cmd.idx_length,
           cmd.primType,
           cmd.primType_length);
    f->readReturn(&ret, sizeof(ret));
};

RsPlaybackLocalFunc gPlaybackFuncs[61] = {
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    rsp_ContextFinish,
    rsp_ContextBindRootScript,
    rsp_ContextBindProgramStore,
    rsp_ContextBindProgramFragment,
    rsp_ContextBindProgramVertex,
    rsp_ContextBindProgramRaster,
    rsp_ContextBindFont,
    rsp_ContextPause,
    rsp_ContextResume,
    rsp_ContextSetSurface,
    rsp_ContextDump,
    rsp_ContextSetPriority,
    rsp_ContextDestroyWorker,
    rsp_AssignName,
    rsp_ObjDestroy,
    NULL,
    NULL,
    rsp_AllocationCopyToBitmap,
    rsp_Allocation1DData,
    rsp_Allocation1DElementData,
    rsp_Allocation2DData,
    rsp_Allocation2DElementData,
    rsp_AllocationGenerateMipmaps,
    rsp_AllocationRead,
    rsp_AllocationSyncAll,
    rsp_AllocationResize1D,
    rsp_AllocationResize2D,
    rsp_AllocationCopy2DRange,
    NULL,
    rsp_ScriptBindAllocation,
    rsp_ScriptSetTimeZone,
    rsp_ScriptInvoke,
    rsp_ScriptInvokeV,
    rsp_ScriptForEach,
    rsp_ScriptSetVarI,
    rsp_ScriptSetVarObj,
    rsp_ScriptSetVarJ,
    rsp_ScriptSetVarF,
    rsp_ScriptSetVarD,
    rsp_ScriptSetVarV,
    rsp_ScriptCCreate,
    NULL,
    NULL,
    rsp_ProgramBindConstants,
    rsp_ProgramBindTexture,
    rsp_ProgramBindSampler,
    NULL,
    NULL,
    rsp_FontCreateFromFile,
    rsp_FontCreateFromMemory,
    rsp_MeshCreate,
};
RsPlaybackRemoteFunc gPlaybackRemoteFuncs[61] = {
    NULL,
    rspr_ContextDestroy,
    rspr_ContextGetMessage,
    rspr_ContextPeekMessage,
    rspr_ContextInitToClient,
    rspr_ContextDeinitToClient,
    rspr_TypeCreate,
    rspr_AllocationCreateTyped,
    rspr_AllocationCreateFromBitmap,
    rspr_AllocationCubeCreateFromBitmap,
    rspr_ContextFinish,
    rspr_ContextBindRootScript,
    rspr_ContextBindProgramStore,
    rspr_ContextBindProgramFragment,
    rspr_ContextBindProgramVertex,
    rspr_ContextBindProgramRaster,
    rspr_ContextBindFont,
    rspr_ContextPause,
    rspr_ContextResume,
    rspr_ContextSetSurface,
    rspr_ContextDump,
    rspr_ContextSetPriority,
    rspr_ContextDestroyWorker,
    rspr_AssignName,
    rspr_ObjDestroy,
    rspr_ElementCreate,
    rspr_ElementCreate2,
    rspr_AllocationCopyToBitmap,
    rspr_Allocation1DData,
    rspr_Allocation1DElementData,
    rspr_Allocation2DData,
    rspr_Allocation2DElementData,
    rspr_AllocationGenerateMipmaps,
    rspr_AllocationRead,
    rspr_AllocationSyncAll,
    rspr_AllocationResize1D,
    rspr_AllocationResize2D,
    rspr_AllocationCopy2DRange,
    rspr_SamplerCreate,
    rspr_ScriptBindAllocation,
    rspr_ScriptSetTimeZone,
    rspr_ScriptInvoke,
    rspr_ScriptInvokeV,
    rspr_ScriptForEach,
    rspr_ScriptSetVarI,
    rspr_ScriptSetVarObj,
    rspr_ScriptSetVarJ,
    rspr_ScriptSetVarF,
    rspr_ScriptSetVarD,
    rspr_ScriptSetVarV,
    rspr_ScriptCCreate,
    rspr_ProgramStoreCreate,
    rspr_ProgramRasterCreate,
    rspr_ProgramBindConstants,
    rspr_ProgramBindTexture,
    rspr_ProgramBindSampler,
    rspr_ProgramFragmentCreate,
    rspr_ProgramVertexCreate,
    rspr_FontCreateFromFile,
    rspr_FontCreateFromMemory,
    rspr_MeshCreate,
};
};
};
