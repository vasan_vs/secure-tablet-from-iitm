/*
    This file is part of the WebKit open source project.
    This file has been generated by generate-bindings.pl. DO NOT MODIFY!

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.
*/

#include "config.h"
#include "V8AudioContext.h"

#if ENABLE(WEB_AUDIO)

#include "EventListener.h"
#include "ExceptionCode.h"
#include "RuntimeEnabledFeatures.h"
#include "V8AbstractEventListener.h"
#include "V8AudioBufferSourceNode.h"
#include "V8AudioChannelMerger.h"
#include "V8AudioChannelSplitter.h"
#include "V8AudioDestinationNode.h"
#include "V8AudioGainNode.h"
#include "V8AudioListener.h"
#include "V8AudioPannerNode.h"
#include "V8Binding.h"
#include "V8BindingMacros.h"
#include "V8BindingState.h"
#include "V8ConvolverNode.h"
#include "V8DOMWrapper.h"
#include "V8DelayNode.h"
#include "V8HighPass2FilterNode.h"
#include "V8IsolatedContext.h"
#include "V8JavaScriptAudioNode.h"
#include "V8LowPass2FilterNode.h"
#include "V8Proxy.h"
#include "V8RealtimeAnalyserNode.h"
#include <wtf/GetPtr.h>
#include <wtf/RefCounted.h>
#include <wtf/RefPtr.h>

namespace WebCore {

WrapperTypeInfo V8AudioContext::info = { V8AudioContext::GetTemplate, V8AudioContext::derefObject, 0, 0 };

namespace AudioContextInternal {

template <typename T> void V8_USE(T) { }

static v8::Handle<v8::Value> destinationAttrGetter(v8::Local<v8::String> name, const v8::AccessorInfo& info)
{
    INC_STATS("DOM.AudioContext.destination._get");
    AudioContext* imp = V8AudioContext::toNative(info.Holder());
    RefPtr<AudioDestinationNode> result = imp->destination();
    v8::Handle<v8::Value> wrapper = result.get() ? getDOMObjectMap().get(result.get()) : v8::Handle<v8::Value>();
    if (wrapper.IsEmpty()) {
        wrapper = toV8(result.get());
        if (!wrapper.IsEmpty())
            V8DOMWrapper::setHiddenReference(info.Holder(), wrapper);
    }
    return wrapper;
}

static v8::Handle<v8::Value> currentTimeAttrGetter(v8::Local<v8::String> name, const v8::AccessorInfo& info)
{
    INC_STATS("DOM.AudioContext.currentTime._get");
    AudioContext* imp = V8AudioContext::toNative(info.Holder());
    return v8::Number::New(imp->currentTime());
}

static v8::Handle<v8::Value> sampleRateAttrGetter(v8::Local<v8::String> name, const v8::AccessorInfo& info)
{
    INC_STATS("DOM.AudioContext.sampleRate._get");
    AudioContext* imp = V8AudioContext::toNative(info.Holder());
    return v8::Number::New(imp->sampleRate());
}

static v8::Handle<v8::Value> listenerAttrGetter(v8::Local<v8::String> name, const v8::AccessorInfo& info)
{
    INC_STATS("DOM.AudioContext.listener._get");
    AudioContext* imp = V8AudioContext::toNative(info.Holder());
    RefPtr<AudioListener> result = imp->listener();
    v8::Handle<v8::Value> wrapper = result.get() ? getDOMObjectMap().get(result.get()) : v8::Handle<v8::Value>();
    if (wrapper.IsEmpty()) {
        wrapper = toV8(result.get());
        if (!wrapper.IsEmpty())
            V8DOMWrapper::setHiddenReference(info.Holder(), wrapper);
    }
    return wrapper;
}

static v8::Handle<v8::Value> oncompleteAttrGetter(v8::Local<v8::String> name, const v8::AccessorInfo& info)
{
    INC_STATS("DOM.AudioContext.oncomplete._get");
    AudioContext* imp = V8AudioContext::toNative(info.Holder());
    return imp->oncomplete() ? v8::Handle<v8::Value>(static_cast<V8AbstractEventListener*>(imp->oncomplete())->getListenerObject(imp->scriptExecutionContext())) : v8::Handle<v8::Value>(v8::Null());
}

static void oncompleteAttrSetter(v8::Local<v8::String> name, v8::Local<v8::Value> value, const v8::AccessorInfo& info)
{
    INC_STATS("DOM.AudioContext.oncomplete._set");
    AudioContext* imp = V8AudioContext::toNative(info.Holder());
    transferHiddenDependency(info.Holder(), imp->oncomplete(), value, V8AudioContext::eventListenerCacheIndex);
    imp->setOncomplete(V8DOMWrapper::getEventListener(value, true, ListenerFindOrCreate));
    return;
}

static v8::Handle<v8::Value> createBufferSourceCallback(const v8::Arguments& args)
{
    INC_STATS("DOM.AudioContext.createBufferSource");
    AudioContext* imp = V8AudioContext::toNative(args.Holder());
    return toV8(imp->createBufferSource());
}

static v8::Handle<v8::Value> createGainNodeCallback(const v8::Arguments& args)
{
    INC_STATS("DOM.AudioContext.createGainNode");
    AudioContext* imp = V8AudioContext::toNative(args.Holder());
    return toV8(imp->createGainNode());
}

static v8::Handle<v8::Value> createDelayNodeCallback(const v8::Arguments& args)
{
    INC_STATS("DOM.AudioContext.createDelayNode");
    AudioContext* imp = V8AudioContext::toNative(args.Holder());
    return toV8(imp->createDelayNode());
}

static v8::Handle<v8::Value> createLowPass2FilterCallback(const v8::Arguments& args)
{
    INC_STATS("DOM.AudioContext.createLowPass2Filter");
    AudioContext* imp = V8AudioContext::toNative(args.Holder());
    return toV8(imp->createLowPass2Filter());
}

static v8::Handle<v8::Value> createHighPass2FilterCallback(const v8::Arguments& args)
{
    INC_STATS("DOM.AudioContext.createHighPass2Filter");
    AudioContext* imp = V8AudioContext::toNative(args.Holder());
    return toV8(imp->createHighPass2Filter());
}

static v8::Handle<v8::Value> createPannerCallback(const v8::Arguments& args)
{
    INC_STATS("DOM.AudioContext.createPanner");
    AudioContext* imp = V8AudioContext::toNative(args.Holder());
    return toV8(imp->createPanner());
}

static v8::Handle<v8::Value> createConvolverCallback(const v8::Arguments& args)
{
    INC_STATS("DOM.AudioContext.createConvolver");
    AudioContext* imp = V8AudioContext::toNative(args.Holder());
    return toV8(imp->createConvolver());
}

static v8::Handle<v8::Value> createAnalyserCallback(const v8::Arguments& args)
{
    INC_STATS("DOM.AudioContext.createAnalyser");
    AudioContext* imp = V8AudioContext::toNative(args.Holder());
    return toV8(imp->createAnalyser());
}

static v8::Handle<v8::Value> createJavaScriptNodeCallback(const v8::Arguments& args)
{
    INC_STATS("DOM.AudioContext.createJavaScriptNode");
    AudioContext* imp = V8AudioContext::toNative(args.Holder());
    EXCEPTION_BLOCK(unsigned, bufferSize, toUInt32(args[0]));
    return toV8(imp->createJavaScriptNode(bufferSize));
}

static v8::Handle<v8::Value> createChannelSplitterCallback(const v8::Arguments& args)
{
    INC_STATS("DOM.AudioContext.createChannelSplitter");
    AudioContext* imp = V8AudioContext::toNative(args.Holder());
    return toV8(imp->createChannelSplitter());
}

static v8::Handle<v8::Value> createChannelMergerCallback(const v8::Arguments& args)
{
    INC_STATS("DOM.AudioContext.createChannelMerger");
    AudioContext* imp = V8AudioContext::toNative(args.Holder());
    return toV8(imp->createChannelMerger());
}

static v8::Handle<v8::Value> startRenderingCallback(const v8::Arguments& args)
{
    INC_STATS("DOM.AudioContext.startRendering");
    AudioContext* imp = V8AudioContext::toNative(args.Holder());
    imp->startRendering();
    return v8::Handle<v8::Value>();
}

} // namespace AudioContextInternal

static const BatchedAttribute AudioContextAttrs[] = {
    // Attribute 'destination' (Type: 'readonly attribute' ExtAttr: '')
    {"destination", AudioContextInternal::destinationAttrGetter, 0, 0 /* no data */, static_cast<v8::AccessControl>(v8::DEFAULT), static_cast<v8::PropertyAttribute>(v8::None), 0 /* on instance */},
    // Attribute 'currentTime' (Type: 'readonly attribute' ExtAttr: '')
    {"currentTime", AudioContextInternal::currentTimeAttrGetter, 0, 0 /* no data */, static_cast<v8::AccessControl>(v8::DEFAULT), static_cast<v8::PropertyAttribute>(v8::None), 0 /* on instance */},
    // Attribute 'sampleRate' (Type: 'readonly attribute' ExtAttr: '')
    {"sampleRate", AudioContextInternal::sampleRateAttrGetter, 0, 0 /* no data */, static_cast<v8::AccessControl>(v8::DEFAULT), static_cast<v8::PropertyAttribute>(v8::None), 0 /* on instance */},
    // Attribute 'listener' (Type: 'readonly attribute' ExtAttr: '')
    {"listener", AudioContextInternal::listenerAttrGetter, 0, 0 /* no data */, static_cast<v8::AccessControl>(v8::DEFAULT), static_cast<v8::PropertyAttribute>(v8::None), 0 /* on instance */},
    // Attribute 'oncomplete' (Type: 'attribute' ExtAttr: '')
    {"oncomplete", AudioContextInternal::oncompleteAttrGetter, AudioContextInternal::oncompleteAttrSetter, 0 /* no data */, static_cast<v8::AccessControl>(v8::DEFAULT), static_cast<v8::PropertyAttribute>(v8::None), 0 /* on instance */},
};
static const BatchedCallback AudioContextCallbacks[] = {
    {"createBuffer", V8AudioContext::createBufferCallback},
    {"createBufferSource", AudioContextInternal::createBufferSourceCallback},
    {"createGainNode", AudioContextInternal::createGainNodeCallback},
    {"createDelayNode", AudioContextInternal::createDelayNodeCallback},
    {"createLowPass2Filter", AudioContextInternal::createLowPass2FilterCallback},
    {"createHighPass2Filter", AudioContextInternal::createHighPass2FilterCallback},
    {"createPanner", AudioContextInternal::createPannerCallback},
    {"createConvolver", AudioContextInternal::createConvolverCallback},
    {"createAnalyser", AudioContextInternal::createAnalyserCallback},
    {"createJavaScriptNode", AudioContextInternal::createJavaScriptNodeCallback},
    {"createChannelSplitter", AudioContextInternal::createChannelSplitterCallback},
    {"createChannelMerger", AudioContextInternal::createChannelMergerCallback},
    {"startRendering", AudioContextInternal::startRenderingCallback},
};
static v8::Persistent<v8::FunctionTemplate> ConfigureV8AudioContextTemplate(v8::Persistent<v8::FunctionTemplate> desc)
{
    v8::Local<v8::Signature> defaultSignature = configureTemplate(desc, "AudioContext", v8::Persistent<v8::FunctionTemplate>(), V8AudioContext::internalFieldCount,
        AudioContextAttrs, WTF_ARRAY_LENGTH(AudioContextAttrs),
        AudioContextCallbacks, WTF_ARRAY_LENGTH(AudioContextCallbacks));
        desc->SetCallHandler(V8AudioContext::constructorCallback);
    v8::Local<v8::ObjectTemplate> instance = desc->InstanceTemplate();
    v8::Local<v8::ObjectTemplate> proto = desc->PrototypeTemplate();
    

    // Custom toString template
    desc->Set(getToStringName(), getToStringTemplate());
    return desc;
}

v8::Persistent<v8::FunctionTemplate> V8AudioContext::GetRawTemplate()
{
    static v8::Persistent<v8::FunctionTemplate> V8AudioContextRawCache = createRawTemplate();
    return V8AudioContextRawCache;
}

v8::Persistent<v8::FunctionTemplate> V8AudioContext::GetTemplate()
{
    static v8::Persistent<v8::FunctionTemplate> V8AudioContextCache = ConfigureV8AudioContextTemplate(GetRawTemplate());
    return V8AudioContextCache;
}

bool V8AudioContext::HasInstance(v8::Handle<v8::Value> value)
{
    return GetRawTemplate()->HasInstance(value);
}


v8::Handle<v8::Object> V8AudioContext::wrapSlow(AudioContext* impl)
{
    v8::Handle<v8::Object> wrapper;
    V8Proxy* proxy = 0;
    wrapper = V8DOMWrapper::instantiateV8Object(proxy, &info, impl);
    if (wrapper.IsEmpty())
        return wrapper;

    impl->ref();
    v8::Persistent<v8::Object> wrapperHandle = v8::Persistent<v8::Object>::New(wrapper);
    getDOMObjectMap().set(impl, wrapperHandle);
    return wrapper;
}

void V8AudioContext::derefObject(void* object)
{
    static_cast<AudioContext*>(object)->deref();
}

} // namespace WebCore

#endif // ENABLE(WEB_AUDIO)
