#define container_of(ptr, type, member) ({	\
const typeof( ((type *)0)->member ) *__mptr = (ptr);    \
(type *)( (char *)__mptr - offsetof(type,member) );}) 

#define list_entry(ptr, type, member) \
	container_of(ptr, type, member)

#define list_for_each_entry(pos, head, member)	\
	for (pos = list_entry((head)->next, typeof(*pos), member);\
	&pos->member != (head);	\
	pos = list_entry(pos->member.next, typeof(*pos), member))

list_for_each_entry(p,&clock,node)
