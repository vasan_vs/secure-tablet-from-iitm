
Instructions:
=============
    These files are MMA845x full-feature  driver based on linux 2.6.35  kernel source 
    code. The code is suitable for MMA8451/MMA8452/MMA8453 Freescale sensor parts.
    The driver implements  all feature of the sensor.

How to port the driver to linux source code.
===============================================

     1) Copy the include/linux/mma845x.h and drivers/hwmon/mma845x to same dircotry of 
        linux 2.6.35  kernel source code.

     2) Add the lines to /drivers/hwmon/Kconfig:

 	config MXC_MMA845X
        	tristate " Freescale  MMA845X  Accelerometer sensor"
       		depends on I2C && SYSFS
        	default n

       
     3) Add a line to /drivers/hwmon/Makefile:	

		obj-y += mma845x/

     4) Add i2c_board_info data in your board configure  C file ,add following lines 
	 to the file:


         #include <linux/mma845x.h>

	 static struct mxc_mma845x_platform_data mma845x_data = {
		.gpio_pin_get = NULL,
		.gpio_pin_put = NULL,
		.int1 = gpio_to_irq(ACCL_INT1), // ACCL_INT1 is gpio for MMA845X INT1
		.int2 = gpio_to_irq(ACCL_INT2), // ACCL_INT2 is gpio for MMA845X INT2
	 };


	 static struct i2c_board_info  .... [] __initdata = {
 	 {
           ......

	  	{
		.type = "mma845x",
		.addr = 0x1C,     	/*mma845x i2c slave address*/
		.platform_data = (void *)&mma845x_data,
		},

	  ......
	 }

	Note: please ensure add the sensor i2c_board_info to the right i2c bus according 
	      to the sensor hardware design on board


      5) Configure to compile the driver

    	"make menuconfig" ot config the linux source code ,and select the option:
	
     	 Device Drivers  --->  Hardware Monitoring support  ---> Freescale  MMA845X  Accelerometer sensor
	 Or , add a line in defconfig file:	
 		CONFIG_MXC_MMA845X=y

    	"make" to compile the driver to kernel image.



