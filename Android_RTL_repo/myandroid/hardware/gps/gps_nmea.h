/*
**
** IMPORTANT LEGAL NOTICE:
** waiver of Liability: Please note that this software is freeware licensed
** under the Apache v. 2.0 license. This software might have been altered,
** modified, influenced or changed by third parties without knowledge and
** consent of Cinterion Wireless Modules GmbH (“Cinterion”).
** As the user of this freeware software you are fully and solely
** responsible for the compatibility of the software with your system and
** all its (software) components as well as any defects, bugs, malfunctions
** or damages arising from or in connection with its installation or use.
** Cinterion does not accept any liability whatsoever with respect to this
** software, namely, but not limited to, its functionality and its
** compatibility with any other hardware and/or software or parts thereof.
** Cinterion shall not be liable for any defects, bugs, malfunctions or
** damages contained in this software or resulting from or arising in
** connection with its installation or use.
**
** Copyright 2011, Cinterion Wireless Modules GmbH
** Copyright 2006, The Android Open Source Project
**
** Licensed under the Apache License, Version 2.0 (the "License");
** you may not use this file except in compliance with the License.
** You may obtain a copy of the License at
**
**     http://www.apache.org/licenses/LICENSE-2.0
**
** Unless required by applicable law or agreed to in writing, software
** distributed under the License is distributed on an "AS IS" BASIS,
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
** See the License for the specific language governing permissions and
** limitations under the License.
*/

//////////////////////////////////////////////////////////////////////
//
// gps_nmea.h
//
// This file contains all functions related to parsing of the nmea data.
//
//////////////////////////////////////////////////////////////////////

#if defined (__ANDROID_VERSION__) &&  __ANDROID_VERSION__ < ANDROID_VER_2_3
  #include <hardware_legacy/gps.h>
#else
  #include <hardware/gps.h>
#endif

#define  NMEA_MAX_SIZE  83

typedef struct {
    int           pos;
    int           overflow;
    int           utc_year;
    int           utc_mon;
    int           utc_day;
    int           utc_diff;
    GpsLocation   fix;
    GpsSvStatus   sv_status;
    int           sv_status_changed;

    char    in[ NMEA_MAX_SIZE+1 ];
} NmeaReader;

void nmea_reader_init( NmeaReader*  r );
int nmea_reader_addc( NmeaReader*  r, int  c );
int nmea_reader_parse( NmeaReader*  r );

