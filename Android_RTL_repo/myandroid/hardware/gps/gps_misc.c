/*
**
** IMPORTANT LEGAL NOTICE:
** waiver of Liability: Please note that this software is freeware licensed
** under the Apache v. 2.0 license. This software might have been altered,
** modified, influenced or changed by third parties without knowledge and
** consent of Cinterion Wireless Modules GmbH (“Cinterion”).
** As the user of this freeware software you are fully and solely
** responsible for the compatibility of the software with your system and
** all its (software) components as well as any defects, bugs, malfunctions
** or damages arising from or in connection with its installation or use.
** Cinterion does not accept any liability whatsoever with respect to this
** software, namely, but not limited to, its functionality and its
** compatibility with any other hardware and/or software or parts thereof.
** Cinterion shall not be liable for any defects, bugs, malfunctions or
** damages contained in this software or resulting from or arising in
** connection with its installation or use.
**
** Copyright 2011, Cinterion Wireless Modules GmbH
** Copyright 2006, The Android Open Source Project
**
** Licensed under the Apache License, Version 2.0 (the "License");
** you may not use this file except in compliance with the License.
** You may obtain a copy of the License at
**
**     http://www.apache.org/licenses/LICENSE-2.0
**
** Unless required by applicable law or agreed to in writing, software
** distributed under the License is distributed on an "AS IS" BASIS,
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
** See the License for the specific language governing permissions and
** limitations under the License.
*/

//////////////////////////////////////////////////////////////////////
//
// gps_misc.c
// miscellaneous functions
//
//////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <termios.h>
#include <sys/stat.h>
#include <sys/epoll.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#include <errno.h>
#include <cutils/sockets.h>

#define  LOG_NDEBUG 0
#define  LOG_TAG  "gps_cwm"
#include <cutils/log.h>
#include <cutils/properties.h>

#include <gps_misc.h>
#include <gps_version.h>


#define TRACE_BUFFER_SIZE 256

#define SOCKET_NAME_RIL_DEBUG "rild-debug"


/*****************************************************************/
/*****************************************************************/
/**
 * Like the Windows function Sleep().
 */
void
msleep(unsigned long dwMilliSecs) {
struct timespec t;

  t.tv_sec   = dwMilliSecs / 1000;
  t.tv_nsec  = (dwMilliSecs % 1000) * 1000000L;
  nanosleep(&t, NULL);
}

/**
 *
 */
static int
misc_blockingWrite(int fd, const void *buffer, size_t len) {
    size_t writeOffset = 0;
    const uint8_t *toWrite;

    GPSTrace (ZONE_FUNC, "-> %s:", __FUNCTION__);

    toWrite = (const uint8_t *)buffer;

    while (writeOffset < len) {
        ssize_t written;
        do {
            written = write (fd, toWrite + writeOffset,
                                len - writeOffset);
        } while (written < 0 && errno == EINTR);

        if (written >= 0) {
            writeOffset += written;
        } else {   // written < 0
            close(fd);
            GPSTrace (ZONE_FUNC | ZONE_ERROR, "<- %s: unexpected error on write errno:%d", __FUNCTION__, errno);
            return -1;
        }
    }

    GPSTrace (ZONE_FUNC, "<- %s:", __FUNCTION__);
    return 0;
}

/**
 *
 */
static int
misc_sendGPSCommand (int Cmd) {
    int   fdDebug = -1;
    int   argcount = 1;
    char  buf[10];
    int   buflen;

    sprintf (buf, "%d", Cmd);
    buflen = strlen(buf);
    
    fdDebug = socket_local_client(SOCKET_NAME_RIL_DEBUG,
                                  ANDROID_SOCKET_NAMESPACE_RESERVED,
                                  SOCK_STREAM);

//    fdDebug = android_get_control_socket(SOCKET_NAME_RIL_DEBUG);
    if (fdDebug < 0) {
        GPSTrace (ZONE_ERROR, "%s: Failed to get socket '" SOCKET_NAME_RIL_DEBUG "' errno:%d", __FUNCTION__, errno);
        return(-1);
    }
    // send argcount
    misc_blockingWrite (fdDebug, &argcount, sizeof(argcount));
    
    // send strlen;
    misc_blockingWrite (fdDebug, &buflen, sizeof(buflen));

    // send string
    misc_blockingWrite (fdDebug, buf, buflen);
    
    close(fdDebug);
    return 0;
}

int
epoll_register( int  epoll_fd, int  fd )
{
    struct epoll_event  ev;
    int    ret, flags;

    GPSTrace (ZONE_INFO, "%s: fd = %d", __FUNCTION__, fd);

    /* important: make the fd non-blocking */
    flags = fcntl(fd, F_GETFL);
    fcntl(fd, F_SETFL, flags | O_NONBLOCK);

    ev.events  = EPOLLIN;
    ev.data.fd = fd;
    do {
        ret = epoll_ctl( epoll_fd, EPOLL_CTL_ADD, fd, &ev );
    } while (ret < 0 && errno == EINTR);
    return ret;
}

/**
 *
 */
int
epoll_deregister( int  epoll_fd, int  fd )
{
    int  ret;
    
    GPSTrace (ZONE_INFO, "%s: fd = %d", __FUNCTION__, fd);
    do {
        ret = epoll_ctl( epoll_fd, EPOLL_CTL_DEL, fd, NULL );
    } while (ret < 0 && errno == EINTR);
    return ret;
}

/**
 *
 */
int
misc_OpenGPSPort(void)
{
    char   prop[PROPERTY_VALUE_MAX];
    char   device[256];
    struct stat dummy;
    int fd = -1;
    int ret;

    GPSTrace (ZONE_FUNC, "-> %s:", __FUNCTION__);

    // look for a kernel-provided device name
    if (property_get("cwm.gps.port",prop,"") == 0) {
        GPSTrace (ZONE_ERROR, "%s: no kernel-provided gps device name", __FUNCTION__);
        return -1;
    }
    GPSTrace (ZONE_INFO, "cwm.gps.port = %s", prop);

    if ( snprintf(device, sizeof(device), "%s", prop) >= (int)sizeof(device) ) {
        GPSTrace (ZONE_ERROR, "%s: gps serial device name too long: '%s'", __FUNCTION__, prop);
        return -1;
    }

    if(stat(device, &dummy)) {
        GPSTrace(ZONE_ERROR, "%s: gps device does not exists - %s.", __FUNCTION__, device);
        return -1;
    }
    
    fd = open (device, O_RDWR);
    if ( fd >= 0) {
        GPSTrace (ZONE_INFO, "%s: fd = %d", __FUNCTION__, fd);

        // disable echo on serial lines
        if ( isatty( fd ) ) {
            struct termios  ios;
            tcgetattr( fd, &ios );
            ios.c_lflag = 0;  /* disable ECHO, ICANON, etc... */
            ios.c_oflag &= (~ONLCR); /* Stop \n -> \r\n translation on output */
            ios.c_iflag &= (~(ICRNL | INLCR)); /* Stop \r -> \n & \n -> \r translation on input */
            ios.c_iflag |= (IGNCR | IXOFF);  /* Ignore \r & XON/XOFF on input */
            tcsetattr( fd, TCSANOW, &ios );
        }
    } else {
        GPSTrace(ZONE_ERROR, "%s: can not open gps device - %s.", __FUNCTION__, device);
    }

    return fd;
}

/**
 *
 */
int
misc_CloseGPSPort(int fd)
{
    GPSTrace (ZONE_FUNC, "-> %s:", __FUNCTION__);
    
    if (fd >= 0) {
      GPSTrace (ZONE_INFO, "%s: fd = %d", __FUNCTION__, fd);
      close(fd);
    }
    return -1;
}

/**
 *
 */
int
misc_StartGPS (void) {
    GPSTrace (ZONE_FUNC, "%s:", __FUNCTION__);
    return misc_sendGPSCommand(100);
}

/**
 *
 */
int
misc_StopGPS (void) {
    GPSTrace (ZONE_FUNC, "%s:", __FUNCTION__);
    return misc_sendGPSCommand(101);
}

/**
 *
 * GPSTrace()
 * Writes a debug trace. It automatically writes to the standard Android
 * debug output to be seen with "logcat -b radio" and the RIL log file
 * as defined in the RIL configuration file.
 * An debug string should not contain a newline character.
 *
 * Parameters:
 *   zone: any combination of ZONE_xxx types
 *   format: a string with parameter in the printf style
 *
 */
void GPSTrace(int zone, const char *format, ...)
{
    int doDbgOutput = 0;
    int doFileOutput = 0;

    // Find out, if the ZONE is supposed to be printed in the
    // debug output and/or in the log file.
//    if ((getConfigurationInt(RILCFGI_DEBUGZONE_DBG) & zone) != 0) doDbgOutput = 1;
//    if ((getConfigurationInt(RILCFGI_DEBUGZONE_FILE) & zone) != 0) doFileOutput = 1;

    if (DEFAULT_ZONE & zone) doDbgOutput = 1;

    if (doDbgOutput != 0 || doFileOutput != 0) {
        int offset = 0;
        char Buffer[TRACE_BUFFER_SIZE];
        va_list  ap;

        memset(Buffer, 0, TRACE_BUFFER_SIZE);
/*
        if (getConfigurationInt(RILCFGI_DEBUG_TIMESTAMPS)) {
            unsigned long long ulTicks;
            int iTicks;

            ulTicks = GetTickCount();
            iTicks = (int)(ulTicks % 100000);

            sprintf(Buffer, "%05d ", iTicks);
            offset = 6;
        }
*/
        // Create output string. Give an extra byte space for the
        // new line character for the log file.
        va_start(ap, format);
        vsnprintf(Buffer + offset, TRACE_BUFFER_SIZE-2, format, ap);
        va_end(ap);

        // If configured, write to debug output.
        if (doDbgOutput) {
            // Use the LOGE macro for the ZONE_ERROR, otherwise use LOGI.
            if (zone & ZONE_ERROR) {
                LOGE("%s", Buffer);
            }
            else {
                LOGI("%s", Buffer);
            }
        }
/*
        // If configured, write to log file.
        if (doFileOutput) {
            int file;

            // Append a new line character (if not already included)
            if (strstr(Buffer, "\n") == NULL)
                strcat(Buffer, "\n");

            // Open log file.
            file = open(getConfigurationStr(RILCFGS_DEBUG_FILE), O_RDWR);
            if (file < 0) {
                // If log file cannot be opened, try to create it.
                file = open(getConfigurationStr(RILCFGS_DEBUG_FILE), O_RDWR|O_CREAT, S_IRWXU|S_IRWXG |S_IRWXO);
            }
            if (file >= 0) {
                // Jump to end of file.
                lseek(file, 0, SEEK_END);
                // Write line.
                write(file, Buffer, strlen(Buffer));
                close(file);
            }
            else {
                // If writing the log file fails, write an error message to
                // the debug output (if configured).
                if ((getConfigurationInt(RILCFGI_DEBUGZONE_DBG) & ZONE_ERROR) != 0) {
                    LOGE("Error %d on writing to log %s", errno, getConfigurationStr(RILCFGS_DEBUG_FILE));
                }
            }
        }
*/
    }
}

void
misc_VersionInfo(void) {
    LOGI ("CWM GPS Version: %s", GPS_VERSION_STRING);
}
