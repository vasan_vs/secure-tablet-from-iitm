#include <linux/i2c-dev.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>

#define SLAVE_ADDR 0x68

int get_values(int fd, int reg)
{
	int val;
	
	/* Read the register values, retry is Alert (7th) bit is 1, which denoted that the value in the register is being updated"*/
	val=i2c_smbus_read_byte_data(fd, reg);

	if (val<0)
        {
                perror ("Failed to read from slave:");
                exit(EXIT_FAILURE);
        }
	
	return val;
}
	
int check_tamper(int fd)
{
        /*The slave address is given to the IOCTL of the adapter to open tamper device*/
        if (ioctl(fd, I2C_SLAVE, SLAVE_ADDR) <0)
        {
                fprintf (stderr, "Failed to set slave address: %m\n");
                return -1;
        }

        return get_values(fd, 0x20);
}

		
int main(void)
{

	int fd_i2c,fd_con;

	fd_i2c= open("/dev/i2c-0", O_RDWR);
	if(fd_i2c<0)
	{
		write(1,"Failed in opening /dev/i2c-0\n",28);
		exit(0);	
	}

	fd_con= open("/dev/console", O_RDWR);
	if(fd_con<0)
	{
		write(1,"Failed in opening /dev/console\n",30);
		exit(0);	
	}

	if (check_tamper(fd_i2c)!=2)
	{
		write(fd_con,"Board is Tampered\n",18);
		sleep(120);
		execlp("reboot","reboot",NULL);
	}
	else
	{
		write(fd_con,"Board is safe\n",13);
		while (1)
		{	
			if (check_tamper(fd_i2c)!=2)	
			{
				//int lcd_blank=open("/sys/class/graphics/fb0/blank",O_RDWR);
				system("rm /dev/input/event0");
				system("rm /dev/input/event1");
				write(fd_con,"Board is Tampered\n",18);
				sleep(60);
				system("echo 1 > /sys/class/graphics/fb0/blank");
				sleep(20);
				execlp("reboot","reboot",NULL);
			}
			sleep(10);
		}
	}

	return 1;
}
