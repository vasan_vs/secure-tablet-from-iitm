#include "tamper.h"
#include <stdio.h>
#include <stdlib.h>

void write_register(int fd, int reg, int value)
{
	int res;
	
	if ((i2c_smbus_read_byte_data(fd, reg))!=value)
		res=i2c_smbus_write_byte_data(fd, reg, value);
	else
		res=1;

	/*Return error when the read i2c client device fails*/
	if (res<0)
	{	
		perror ("Failed to write to slave:");
		exit(EXIT_FAILURE);
	}
}	

int get_values(int fd, int reg)
{
        int val;

        val=i2c_smbus_read_byte_data(fd, reg);

        if (val<0)
        {
                perror ("Failed to read from slave:");
                exit(EXIT_FAILURE);
        }

        return val;
}

void unset_bit(int fd, int reg, int bit_pos)
{
	int temp = get_values(fd, reg);
	write_register(fd, reg, (temp & ~bit_pos));
}

void set_bit(int fd, int reg, int bit_pos)
{
	int temp = get_values(fd, reg);
	write_register(fd, reg, (temp | bit_pos));
}


int main(int argc,char* argv[]){

	int fd_i2c,fd_con;
//	char wr[15];
//	printf("enter UID");
	
//	scanf("%s",wr);

	fd_i2c= open("/dev/i2c-0", O_RDWR);
	if(fd_i2c<0)
	{
		printf("Failed in opening /dev/i2c-0\n");
		exit(0);	
	}
	
	if (ioctl(fd_i2c, I2C_SLAVE, SLAVE_ADDR) <0)
        {
                fprintf (stderr, "Failed to set slave address: %m\n");
                exit(0);
        }
	/*Procedure to set Tamper detector
	
	1. Read the Flag registers
	2. Tamper enable bit should be cleared to reset Tamper detect bits 
	3. Tamper enable bits should be set
	4. The TCM and TPM bits are to be set accordingly to the connection with the tamper circuitry
	5. Tamper Interrupt bits are set to enable Interuppts
	6. CLR bits are set to clear the NVRAM incase of a tamper event
	7. CLR (EXT) bit should be set to enable clearing of a external RAM. In this case it is connected to the Reset pin */

	printf("\nReading Flag registers\n");
	get_values(fd_i2c, FLAG_REGISTER);		//Reading the Flag Registers


	// Resetting the Tamper Enable bits for both tamper registers	
	printf("Setting TEB bits\n");
	sleep(5);
	unset_bit(fd_i2c, FLAG_REGISTER, OF);
	unset_bit(fd_i2c, TAMPER_REG_1, TEB1);
	unset_bit(fd_i2c, TAMPER_REG_2, TEB2);
	set_bit(fd_i2c, TAMPER_REG_1, TEB1);
	set_bit(fd_i2c, TAMPER_REG_2, TEB2);
	
	// Enabling Interrupts, Reset and setting the type of connection for Tamper 1 register	
	printf("Setting other bits\n\n");
	set_bit(fd_i2c, TAMPER_REG_1, TIE1);
	set_bit(fd_i2c, TAMPER_REG_1, TCM1);
	set_bit(fd_i2c, TAMPER_REG_1, CLR1);
//	set_bit(fd, TAMPER_REG_1, RST1);
	unset_bit(fd_i2c, TAMPER_REG_1, TPM1);

	// Enabling Interrupts, Reset and setting the type of connection for Tamper 2 register	
	set_bit(fd_i2c, TAMPER_REG_2, TIE2);
	set_bit(fd_i2c, TAMPER_REG_2, TCM2);
	set_bit(fd_i2c, TAMPER_REG_2, CLR2);
//	set_bit(fd, TAMPER_REG_2, RST2);
	unset_bit(fd_i2c, TAMPER_REG_2, TPM2);


//writing user space in tamper chip
	
	fd_con= open("/dev/console", O_RDWR);
        if(fd_con<0)
        {
                write(1,"Failed in opening /dev/console\n",30);
                exit(0);
        }


//	printf("hello %d ",argc);
/*	char x[15];
	int i,j=0,k,count;
	while(argc%10 !=0)
	{
		x[j]=argc%10+48;
		argc=argc/10;
		j++;
	}
	x[j]=argc;
	
	char wr[j];
	for(i=0;i<=j;i++)
	wr[i]=x[j-i];
*/	

//	char wr[]={'5','e','7','d','3','8','d','


        int i=0,temp=0;


	int size=(((argv[1][0]-48)*10)+(argv[1][1]-48))+2;


	char wr[size];
	wr[0]=argv[1][0];
	wr[1]=argv[1][1];
	for(i=2;i<size;i++)
	wr[i]=argv[2][i-2];	
//	int temp=strlen(wr);

	for(i=0;i<size;i++)
        {
		
        	temp=i2c_smbus_write_byte_data(fd_i2c, (USER_DATA+i), wr[i]);
//                printf("hello %c",wr[i]);
		if(temp<0)
                write(fd_con,"write error",11);
        }
 

	return 1;
}
