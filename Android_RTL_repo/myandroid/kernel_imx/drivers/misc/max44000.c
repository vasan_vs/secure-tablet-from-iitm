/*
 * Copyright (C) 2011 Motorola Mobility, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
 * 02111-1307, USA
 */


#include <linux/err.h>
#include <linux/errno.h>
#include <linux/delay.h>
#include <linux/fs.h>
#include <linux/i2c.h>
#include <linux/irq.h>
#include <linux/input.h>
#include <linux/interrupt.h>
#include <linux/input-polldev.h>
#include <linux/miscdevice.h>
#include <linux/regulator/consumer.h>
#include <linux/slab.h>
#include <linux/uaccess.h>
#include <linux/gpio.h>

#include "max44000.h"

#define MAX44000_NAME "max44000"

#define POLL_INTERVAL_MIN       1
#define POLL_INTERVAL_MAX       500
#define POLL_INTERVAL           100     /* msecs */

struct max44000_status
{
	int active;
	int position;
}prox_status;

enum{
	PROX_STANDBY=0,
	PROX_ACTIVED,
};

static struct i2c_client *max44000_i2c_client;
static struct input_polled_dev *max44000_idev;
static DEFINE_MUTEX(max44000_lock);

//#define MAX44000_DBG

#ifdef MAX44000_DBG
	#define PRINT_FUNC_LINE printk(KERN_INFO "[%s][%d]: ",__func__, __LINE__);
#else
	#define PRINT_FUNC_LINE 
#endif

static int max44000_hw_init(struct i2c_client *client)
{
	int result;
PRINT_FUNC_LINE
	result = i2c_smbus_write_byte_data(client, MAX44000_MAIN_CONF_REG, 0x20); 
        if (result < 0)
                goto out;

	result = i2c_smbus_write_byte_data(client, MAX44000_TRNS_CONF_REG, 0x0B); 
        if (result < 0)
                goto out;
PRINT_FUNC_LINE

        return 0;
out:PRINT_FUNC_LINE
	return result;
}

static void max44000_dev_poll(struct input_polled_dev *dev)
{

	int result;
	PRINT_FUNC_LINE
	result = i2c_smbus_read_byte_data(max44000_i2c_client, MAX44000_PROX_ADC_REG);

        //printk(KERN_INFO "[%s][%d]: max44000_idev->input->abs_bit:[%d]: value:[%lu]  **************\n", __func__, __LINE__, max44000_idev->input->absbit[0], result);
PRINT_FUNC_LINE
	if(result < 0){
		PRINT_FUNC_LINE;}
	else
		input_report_abs(max44000_idev->input, ABS_DISTANCE, result);
PRINT_FUNC_LINE
	input_sync(max44000_idev->input);
PRINT_FUNC_LINE
}

static ssize_t max44000_enable_show(struct device *dev,
                                   struct device_attribute *attr, char *buf)
{
        struct i2c_client *client;
        u8 val;
        int enable;
PRINT_FUNC_LINE
        mutex_lock(&max44000_lock);
        client = max44000_i2c_client;
        val = i2c_smbus_read_byte_data(client, MAX44000_MAIN_CONF_REG);
        if ((val & 0x14) && prox_status.active == PROX_ACTIVED)
                enable = 1;
        else
                enable = 0;
        mutex_unlock(&max44000_lock);
PRINT_FUNC_LINE
        return sprintf(buf, "%d\n", enable);
}

static ssize_t max44000_enable_store(struct device *dev,
                                    struct device_attribute *attr,
                                    const char *buf, size_t count)
{
        struct i2c_client *client;
        int ret;
        unsigned long enable;
PRINT_FUNC_LINE
        u8 val = 0;
        enable = simple_strtoul(buf, NULL, 10);
        mutex_lock(&max44000_lock);
        client = max44000_i2c_client;
        enable = (enable > 0) ? 1 : 0;

        if (enable && prox_status.active == PROX_STANDBY) {
                val = i2c_smbus_read_byte_data(client, MAX44000_MAIN_CONF_REG);
                ret = i2c_smbus_write_byte_data(client, MAX44000_MAIN_CONF_REG,
                                              val | 0x14);
                if (!ret) {
                        prox_status.active = PROX_ACTIVED;
                }
        } else if (enable == 0 && prox_status.active == PROX_ACTIVED) {
                val = i2c_smbus_read_byte_data(client, MAX44000_MAIN_CONF_REG);
                ret = i2c_smbus_write_byte_data(client, MAX44000_MAIN_CONF_REG,
                                              val & ~0x14);
                if (!ret) {
                        prox_status.active = PROX_STANDBY;
                }
        }
        mutex_unlock(&max44000_lock);
PRINT_FUNC_LINE
        return count;
}

static ssize_t max44000_position_show(struct device *dev,
                                     struct device_attribute *attr, char *buf)
{
        int position = 0;
PRINT_FUNC_LINE
        mutex_lock(&max44000_lock);
        position = prox_status.position;
        mutex_unlock(&max44000_lock);
PRINT_FUNC_LINE
        return sprintf(buf, "%d\n", position);
}

static ssize_t max44000_position_store(struct device *dev,
                                      struct device_attribute *attr,
                                      const char *buf, size_t count)
{
        int position;
PRINT_FUNC_LINE
        position = simple_strtoul(buf, NULL, 10);
        mutex_lock(&max44000_lock);
        prox_status.position = position;
        mutex_unlock(&max44000_lock);
PRINT_FUNC_LINE
        return count;
}

static DEVICE_ATTR(enable, S_IWUSR | S_IRUGO,
		                   max44000_enable_show, max44000_enable_store);
static DEVICE_ATTR(position, S_IWUSR | S_IRUGO,
		                   max44000_position_show, max44000_position_store);

static struct attribute *max44000_attributes[] = {
	        &dev_attr_enable.attr,
		        &dev_attr_position.attr,
			        NULL
};

static const struct attribute_group max44000_attr_group = {
	        .attrs = max44000_attributes,
};


static int max44000_probe(struct i2c_client *client,
                               const struct i2c_device_id *id)
{
	int result,client_id;
	struct input_dev *idev;
	struct i2c_adapter *adapter;
PRINT_FUNC_LINE
	max44000_i2c_client=client;
	adapter =to_i2c_adapter(client->dev.parent);
	result = i2c_check_functionality(adapter,
			I2C_FUNC_SMBUS_BYTE |
			I2C_FUNC_SMBUS_BYTE_DATA);
	if(!result)
		goto err_out;
/*
	client_id=i2c_smbus_read_byte_data(client, MAX44000_WHO_AM_I);

	if(client_id != MAX44000_ID)
	{
		dev_err(&client->dev, "Client id: [%d] read chip ID 0x%x is not eqal to 0x%x", client_id, result, MAX44000_ID);
		result = -EINVAL;
		goto err_out;
	}
*/

PRINT_FUNC_LINE
        max44000_idev = input_allocate_polled_device();
	if (!max44000_idev) {
		result = -ENOMEM;
		dev_err(&client->dev, "alloc poll device failed!\n");
		goto err_out;
	}   

PRINT_FUNC_LINE
	max44000_idev->poll = max44000_dev_poll;
	max44000_idev->poll_interval = POLL_INTERVAL;
	max44000_idev->poll_interval_min = POLL_INTERVAL_MIN;
	max44000_idev->poll_interval_max = POLL_INTERVAL_MAX;

	idev = max44000_idev->input;
	idev->name = "max44000";
	idev->id.bustype = BUS_I2C;
	idev->evbit[0] = BIT_MASK(EV_ABS);
	
	input_set_abs_params(idev, ABS_DISTANCE, 0, 255,32,32);

	input_set_capability(idev, EV_ABS, ABS_DISTANCE);

	result = input_register_polled_device(max44000_idev);
	if (result) {
		dev_err(&client->dev, "register poll device failed!\n");
		goto err_out;
	}

PRINT_FUNC_LINE
	result = sysfs_create_group(&idev->dev.kobj, &max44000_attr_group);
	if (result) {
		dev_err(&client->dev, "create device file failed!\n");
		result = -EINVAL;
		goto err_out;
	}

PRINT_FUNC_LINE
//	prox_status.position = *(int *)client->dev.platform_data;

	max44000_hw_init(client);
	PRINT_FUNC_LINE
	return 0;

err_out:PRINT_FUNC_LINE
	return result;
}

static int __devexit max44000_remove(struct i2c_client *client)
{PRINT_FUNC_LINE
	return 0;
}

static const struct i2c_device_id max44000_id[] = {
	{MAX44000_NAME, 0},
	{},
};

MODULE_DEVICE_TABLE(i2c, max44000_id);

static struct i2c_driver max44000_driver = {
	
	.driver = {
		.name = MAX44000_NAME,
	},
	.probe = max44000_probe,
	.remove = __devexit_p(max44000_remove),
	.id_table = max44000_id,
};

static int __init max44000_init(void)
{PRINT_FUNC_LINE
        printk(KERN_INFO "max44000 init function\n");
	return i2c_add_driver(&max44000_driver);

}

static void __exit max44000_exit(void)
{
	i2c_del_driver(&max44000_driver);
	return;
}

module_init(max44000_init);
module_exit(max44000_exit);

MODULE_DESCRIPTION("max44000 proximity sensor driver");
MODULE_AUTHOR("Motorola Mobility");
MODULE_LICENSE("GPL");
