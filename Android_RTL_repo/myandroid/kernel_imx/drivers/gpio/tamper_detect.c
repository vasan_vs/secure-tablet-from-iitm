#include <linux/module.h>    // included for all kernel modules
#include <linux/kernel.h>    // included for KERN_INFO
#include <linux/init.h>      // included for __init and __exit macros
#include <asm/io.h>
#include <mach/hardware.h>
#include <linux/delay.h>
#include <linux/proc_fs.h>
#include <linux/string.h>
#include <linux/vmalloc.h>
#include <asm/uaccess.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Amit");
MODULE_DESCRIPTION("module to detct tamper");


#define MAX_LEN       4096
unsigned long int read_info( char *page, char **start, off_t off,int count, int *eof, void *data );
ssize_t write_info( struct file *filp, const char __user *buff,unsigned long len, void *data );

static struct proc_dir_entry *proc_entry;
static char *info;
static int write_index;
static int read_index;
unsigned long int SNVS_LPZMKR0=0;

static int __init tamper_detect_init(void)
{
    SNVS_LPZMKR0 = __raw_readl(IO_ADDRESS(0x020cc06c));
    info = (char *)vmalloc( MAX_LEN );
    memset( info, 0, MAX_LEN );
    proc_entry = create_proc_entry( "tamperKey", 0644, NULL );

    if (proc_entry == NULL)
    {
        //ret = -1;
        vfree(info);
        printk(KERN_INFO "tamperKey could not be created\n");
    }
    else
    {
        write_index = 0;
        read_index = 0;
        proc_entry->read_proc = read_info;
        proc_entry->write_proc = write_info;
        printk(KERN_INFO "tamperKey created.\n");
    }
    return 0;
}

static void __exit tamper_detect_exit(void)
{
    remove_proc_entry("tamperKey", NULL);
    printk(KERN_INFO "Exiting Tamper Detect\n");
    vfree(info);
}

ssize_t write_info( struct file *filp, const char __user *buff, unsigned long len, void *data )
{
    int capacity = (MAX_LEN-write_index)+1;
    if (len > capacity)
    {
        printk(KERN_INFO "No space to write in procEntry123!\n");
        return -1;
    }
    if (copy_from_user( &info[write_index], buff, len ))
    {
        return -2;
    }

    write_index += len;
    info[write_index-1] = 0;
    return len;
}

unsigned long int read_info( char *page, char **start, off_t off, int count, int *eof, void *data )
{
    unsigned long int len;

    if (off > 0)
    {
        *eof = 1;
        return 0;
    }

    if (read_index >= write_index)
    read_index = 0;
    len = sprintf(page, "LPZMKR0=%lx\n", SNVS_LPZMKR0);
    read_index += len;
    return len;
}

module_init(tamper_detect_init);
module_exit(tamper_detect_exit);

