#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
 .exit = cleanup_module,
#endif
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0xc036c7e8, "module_layout" },
	{ 0xfbc74f64, "__copy_from_user" },
	{ 0x3c2c5af5, "sprintf" },
	{ 0x779d1cb4, "create_proc_entry" },
	{ 0xfa2a45e, "__memzero" },
	{ 0xd6ee688f, "vmalloc" },
	{ 0xefd6cf06, "__aeabi_unwind_cpp_pr0" },
	{ 0x999e8297, "vfree" },
	{ 0x27e1a049, "printk" },
	{ 0x4d761412, "remove_proc_entry" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";

