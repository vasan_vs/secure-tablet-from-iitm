#include <linux/module.h>    // included for all kernel modules
#include <linux/kernel.h>    // included for KERN_INFO
#include <linux/init.h>      // included for __init and __exit macros
#include <asm/io.h>
#include <mach/hardware.h>
#include <linux/delay.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Amit");
MODULE_DESCRIPTION("module to set tamper");

static unsigned long int id=0;
//static int size=0;
module_param(id, ulong, 0);
//module_param(size, int, 0);

static int __init tamper_set_init(void)
{

       unsigned long int SNVS_LPZMKR0,snvs_check;                                            //backup
	__raw_writel(id, IO_ADDRESS(0x020cc06c));
        mdelay(10);
        snvs_check = __raw_readl(IO_ADDRESS(0x020cc06c));
        printk (KERN_ERR "%lx", snvs_check);
//-----------------------------------------------------------------------

/*
long int clk,cmd,dat0,dat1,dat2,dat3;
        clk = __raw_readl(IO_ADDRESS(0x20E073C));
        printk (KERN_ERR "clk %lx", clk);

        cmd= __raw_readl(IO_ADDRESS(0x20E0740));
        printk (KERN_ERR "cmd %lx", cmd);

        dat0= __raw_readl(IO_ADDRESS(0x20E0368));
        printk (KERN_ERR "dat0 %lx", dat0);

        dat1= __raw_readl(IO_ADDRESS(0x20E0360));
        printk (KERN_ERR "dat1 %lx", dat1);

        dat2 = __raw_readl(IO_ADDRESS(0x20E0364));
        printk (KERN_ERR "dat2 %lx", dat2);

        dat3 = __raw_readl(IO_ADDRESS(0x20E0744));
        printk (KERN_ERR "dat3 %lx", dat3);
*/
    return 0;    
}

static void __exit tamper_set_exit(void)
{
    printk(KERN_INFO "Exiting Tamper Set\n");
}

module_init(tamper_set_init);
module_exit(tamper_set_exit);

