#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
 .exit = cleanup_module,
#endif
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0xc036c7e8, "module_layout" },
	{ 0x3ce4ca6f, "disable_irq" },
	{ 0x53dff67d, "wl12xx_debug_level" },
	{ 0xce21a196, "__pm_runtime_idle" },
	{ 0x55fafb88, "wl1271_free_hw" },
	{ 0x81c689d5, "dev_set_drvdata" },
	{ 0x27bbf221, "disable_irq_nosync" },
	{ 0xd8209c4b, "wl1271_init_ieee80211" },
	{ 0xa183a8ac, "sdio_writesb" },
	{ 0x446dcd2b, "sdio_enable_func" },
	{ 0x796ce943, "__pm_runtime_resume" },
	{ 0x94864c7a, "wl12xx_get_platform_data" },
	{ 0xe6c161a1, "sdio_get_host_pm_caps" },
	{ 0xce2840e7, "irq_set_irq_wake" },
	{ 0x74c97f9c, "_raw_spin_unlock_irqrestore" },
	{ 0x27e1a049, "printk" },
	{ 0xe795ba26, "wl1271_irq" },
	{ 0xd6b8e852, "request_threaded_irq" },
	{ 0x740c16a5, "device_init_wakeup" },
	{ 0x579a8a81, "sdio_readsb" },
	{ 0xde213a61, "sdio_unregister_driver" },
	{ 0xc68e6ac2, "sdio_f0_writeb" },
	{ 0x844ad6e0, "sdio_set_host_pm_flags" },
	{ 0x4059792f, "print_hex_dump" },
	{ 0x372724d4, "sdio_f0_readb" },
	{ 0xc21fc012, "pm_wakeup_event" },
	{ 0x98c22e90, "mmc_power_save_host" },
	{ 0xbd7083bc, "_raw_spin_lock_irqsave" },
	{ 0x7d64b67c, "mmc_power_restore_host" },
	{ 0x3775fbb9, "sdio_memcpy_toio" },
	{ 0xfcec0987, "enable_irq" },
	{ 0x46429a82, "wl1271_unregister_hw" },
	{ 0xefd6cf06, "__aeabi_unwind_cpp_pr0" },
	{ 0x676bbc0f, "_set_bit" },
	{ 0x4f7f2d1b, "complete" },
	{ 0x60a11921, "sdio_register_driver" },
	{ 0x54084f68, "sdio_memcpy_fromio" },
	{ 0xfbafe984, "sdio_claim_host" },
	{ 0xf87ea9d4, "wl1271_alloc_hw" },
	{ 0x54d7ba39, "dev_get_drvdata" },
	{ 0x89c43d0d, "sdio_set_block_size" },
	{ 0x833348b, "wl1271_register_hw" },
	{ 0xee799ef9, "sdio_disable_func" },
	{ 0xf20dabd8, "free_irq" },
	{ 0x1ccc1b5a, "sdio_release_host" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";

MODULE_ALIAS("sdio:c*v0097d4076*");
