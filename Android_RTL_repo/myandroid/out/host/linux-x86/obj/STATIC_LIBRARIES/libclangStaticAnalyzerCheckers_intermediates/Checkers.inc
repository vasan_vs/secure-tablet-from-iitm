
#ifdef GET_GROUPS
#endif // GET_GROUPS


#ifdef GET_PACKAGES
PACKAGE("core", -1, false)
PACKAGE("core.builtin", -1, false)
PACKAGE("core.uninitialized", -1, false)
PACKAGE("cplusplus", -1, false)
PACKAGE("deadcode", -1, false)
PACKAGE("debug", -1, false)
PACKAGE("experimental", -1, false)
PACKAGE("experimental.core", -1, true)
PACKAGE("experimental.cplusplus", -1, true)
PACKAGE("experimental.deadcode", -1, true)
PACKAGE("experimental.osx", -1, true)
PACKAGE("experimental.osx.cocoa", -1, true)
PACKAGE("experimental.security", -1, true)
PACKAGE("experimental.unix", -1, true)
PACKAGE("llvm", -1, false)
PACKAGE("osx", -1, false)
PACKAGE("osx.cocoa", -1, false)
PACKAGE("osx.coreFoundation", -1, false)
PACKAGE("security", -1, false)
PACKAGE("unix", -1, false)
#endif // GET_PACKAGES


#ifdef GET_CHECKERS
CHECKER("core.AdjustedReturnValue", AdjustedReturnValueChecker, AdjustedReturnValueChecker.cpp, "Check to see if the return value of a function call is different than the caller expects (e.g., from calls through function pointers)", -1, false)
CHECKER("debug.Stats", AnalyzerStatsChecker, AnalyzerStatsChecker.cpp, "Emit warnings with analyzer statistics", -1, false)
CHECKER("experimental.security.ArrayBound", ArrayBoundChecker, ArrayBoundChecker.cpp, "Warn about buffer overflows (older checker)", -1, true)
CHECKER("experimental.security.ArrayBoundV2", ArrayBoundCheckerV2, ArrayBoundCheckerV2.cpp, "Warn about buffer overflows (newer checker)", -1, true)
CHECKER("core.AttributeNonNull", AttrNonNullChecker, AttrNonNullChecker.cpp, "Check for null pointers passed as arguments to a function whose arguments are marked with the 'nonnull' attribute", -1, false)
CHECKER("core.builtin.BuiltinFunctions", BuiltinFunctionChecker, BuiltinFunctionChecker.cpp, "Evaluate compiler builtin functions (e.g., alloca())", -1, false)
CHECKER("osx.coreFoundation.CFError", CFErrorChecker, NSErrorChecker.cpp, "Check usage of CFErrorRef* parameters", -1, false)
CHECKER("debug.DumpCFG", CFGDumper, DebugCheckers.cpp, "Display Control-Flow Graphs", -1, false)
CHECKER("debug.ViewCFG", CFGViewer, DebugCheckers.cpp, "View Control-Flow Graphs using GraphViz", -1, false)
CHECKER("osx.coreFoundation.CFNumber", CFNumberCreateChecker, BasicObjCFoundationChecks.cpp, "Check for proper uses of CFNumberCreate", -1, false)
CHECKER("osx.coreFoundation.CFRetainRelease", CFRetainReleaseChecker, BasicObjCFoundationChecks.cpp, "Check for null arguments to CFRetain/CFRelease", -1, false)
CHECKER("experimental.unix.CString", CStringChecker, CStringChecker.cpp, "Check calls to functions in <string.h>", -1, true)
CHECKER("core.CallAndMessage", CallAndMessageChecker, CallAndMessageChecker.cpp, "Check for logical errors for function calls and Objective-C message expressions (e.g., uninitialized arguments, null function pointers)", -1, false)
CHECKER("experimental.core.CastSize", CastSizeChecker, CastSizeChecker.cpp, "Check when casting a malloc'ed type T, whether the size is a multiple of the size of T", -1, true)
CHECKER("experimental.core.CastToStruct", CastToStructChecker, CastToStructChecker.cpp, "Check for cast from non-struct pointer to struct pointer", -1, true)
CHECKER("experimental.unix.Chroot", ChrootChecker, ChrootChecker.cpp, "Check improper use of chroot", -1, true)
CHECKER("osx.cocoa.ClassRelease", ClassReleaseChecker, BasicObjCFoundationChecks.cpp, "Check for sending 'retain', 'release', or 'autorelease' directly to a Class", -1, false)
CHECKER("deadcode.DeadStores", DeadStoresChecker, DeadStoresChecker.cpp, "Check for values stored to variables that are never read afterwards", -1, false)
CHECKER("core.NullDereference", DereferenceChecker, DereferenceChecker.cpp, "Check for dereferences of null pointers", -1, false)
CHECKER("core.DivideZero", DivZeroChecker, DivZeroChecker.cpp, "Check for division by zero", -1, false)
CHECKER("experimental.core.FixedAddr", FixedAddressChecker, FixedAddressChecker.cpp, "Check for assignment of a fixed address to a pointer", -1, true)
CHECKER("deadcode.IdempotentOperations", IdempotentOperationChecker, IdempotentOperationChecker.cpp, "Warn about idempotent operations", -1, false)
CHECKER("experimental.cplusplus.Iterators", IteratorsChecker, IteratorsChecker.cpp, "Check improper uses of STL vector iterators", -1, true)
CHECKER("llvm.Conventions", LLVMConventionsChecker, LLVMConventionsChecker.cpp, "Check code for LLVM codebase conventions", -1, false)
CHECKER("debug.DumpLiveVars", LiveVariablesDumper, DebugCheckers.cpp, "Print results of live variable analysis", -1, false)
CHECKER("osx.SecKeychainAPI", MacOSKeychainAPIChecker, MacOSKeychainAPIChecker.cpp, "Check for proper uses of Secure Keychain APIs", -1, false)
CHECKER("osx.API", MacOSXAPIChecker, MacOSXAPIChecker.cpp, "Check for proper uses of various Mac OS X APIs", -1, false)
CHECKER("experimental.unix.Malloc", MallocChecker, MallocChecker.cpp, "Check for potential memory leaks, double free, and use-after-free problems", -1, true)
CHECKER("experimental.security.MallocOverflow", MallocOverflowSecurityChecker, MallocOverflowSecurityChecker.cpp, "Check for overflows in the arguments to malloc()", -1, true)
CHECKER("osx.cocoa.NSAutoreleasePool", NSAutoreleasePoolChecker, NSAutoreleasePoolChecker.cpp, "Warn for suboptimal uses of NSAutoreleasePool in Objective-C GC mode", -1, false)
CHECKER("osx.cocoa.NSError", NSErrorChecker, NSErrorChecker.cpp, "Check usage of NSError** parameters", -1, false)
CHECKER("osx.cocoa.NilArg", NilArgChecker, BasicObjCFoundationChecks.cpp, "Check for prohibited nil arguments to ObjC method calls", -1, false)
CHECKER("core.builtin.NoReturnFunctions", NoReturnFunctionChecker, NoReturnFunctionChecker.cpp, "Evaluate \"panic\" functions that are known to not return to the caller", -1, false)
CHECKER("osx.AtomicCAS", OSAtomicChecker, OSAtomicChecker.cpp, "Evaluate calls to OSAtomic functions", -1, false)
CHECKER("osx.cocoa.AtSync", ObjCAtSyncChecker, ObjCAtSyncChecker.cpp, "Check for null pointers used as mutexes for @synchronized", -1, false)
CHECKER("experimental.osx.cocoa.Dealloc", ObjCDeallocChecker, CheckObjCDealloc.cpp, "Warn about Objective-C classes that lack a correct implementation of -dealloc", -1, true)
CHECKER("osx.cocoa.IncompatibleMethodTypes", ObjCMethSigsChecker, CheckObjCInstMethSignature.cpp, "Warn about Objective-C method signatures with type incompatibilities", -1, false)
CHECKER("experimental.osx.cocoa.SelfInit", ObjCSelfInitChecker, ObjCSelfInitChecker.cpp, "Check that 'self' is properly initialized inside an initializer method", -1, true)
CHECKER("osx.cocoa.UnusedIvars", ObjCUnusedIvarsChecker, ObjCUnusedIVarsChecker.cpp, "Warn about private ivars that are never used", -1, false)
CHECKER("experimental.core.PointerArithm", PointerArithChecker, PointerArithChecker, "Check for pointer arithmetic on locations other than array elements", -1, true)
CHECKER("experimental.core.PointerSub", PointerSubChecker, PointerSubChecker, "Check for pointer subtractions on two pointers pointing to different memory chunks", -1, true)
CHECKER("experimental.unix.PthreadLock", PthreadLockChecker, PthreadLockChecker.cpp, "Simple lock -> unlock checker", -1, true)
CHECKER("osx.cocoa.RetainCount", RetainCountChecker, RetainCountChecker.cpp, "Check for leaks and improper reference count management", -1, false)
CHECKER("experimental.security.ReturnPtrRange", ReturnPointerRangeChecker, ReturnPointerRangeChecker.cpp, "Check for an out-of-bound pointer being returned to callers", -1, true)
CHECKER("core.uninitialized.UndefReturn", ReturnUndefChecker, ReturnUndefChecker.cpp, "Check for uninitialized values being returned to the caller", -1, false)
CHECKER("experimental.security.SecuritySyntactic", SecuritySyntaxChecker, CheckSecuritySyntaxOnly.cpp, "Perform quick security API checks that require no data flow", -1, true)
CHECKER("experimental.core.SizeofPtr", SizeofPointerChecker, CheckSizeofPointer.cpp, "Warn about unintended use of sizeof() on pointer expressions", -1, true)
CHECKER("core.StackAddressEscape", StackAddrEscapeChecker, StackAddrEscapeChecker.cpp, "Check that addresses to stack memory do not escape the function", -1, false)
CHECKER("experimental.unix.Stream", StreamChecker, StreamChecker.cpp, "Check stream handling functions", -1, true)
CHECKER("core.uninitialized.Branch", UndefBranchChecker, UndefBranchChecker.cpp, "Check for uninitialized values used as branch conditions", -1, false)
CHECKER("core.uninitialized.CapturedBlockVariable", UndefCapturedBlockVarChecker, UndefCapturedBlockVarChecker.cpp, "Check for blocks that capture uninitialized values", -1, false)
CHECKER("core.UndefinedBinaryOperatorResult", UndefResultChecker, UndefResultChecker.cpp, "Check for undefined results of binary operators", -1, false)
CHECKER("core.uninitialized.ArraySubscript", UndefinedArraySubscriptChecker, UndefinedArraySubscriptChecker.cpp, "Check for uninitialized values used as array subscripts", -1, false)
CHECKER("core.uninitialized.Assign", UndefinedAssignmentChecker, UndefinedAssignmentChecker.cpp, "Check for assigning uninitialized values", -1, false)
CHECKER("unix.API", UnixAPIChecker, UnixAPIChecker.cpp, "Check calls to various UNIX/Posix functions", -1, false)
CHECKER("experimental.deadcode.UnreachableCode", UnreachableCodeChecker, UnreachableCodeChecker.cpp, "Check unreachable code", -1, true)
CHECKER("core.VLASize", VLASizeChecker, VLASizeChecker.cpp, "Check for declarations of VLA of undefined or zero size", -1, false)
CHECKER("osx.cocoa.VariadicMethodTypes", VariadicMethodTypeChecker, BasicObjCFoundationChecks.cpp, "Check for passing non-Objective-C types to variadic methods that expectonly Objective-C types", -1, false)
#endif // GET_CHECKERS


#ifdef GET_MEMBER_ARRAYS
static const short SubPackageArray0[] = { 9, 12, 5, 3, 1, 2, 8, 4, 7, 6, -1 };
static const short CheckerArray1[] = { 0, -1 };
static const short CheckerArray2[] = { 4, -1 };
static const short CheckerArray3[] = { 12, -1 };
static const short CheckerArray4[] = { 19, -1 };
static const short CheckerArray5[] = { 18, -1 };
static const short CheckerArray6[] = { 47, -1 };
static const short CheckerArray7[] = { 51, -1 };
static const short CheckerArray8[] = { 56, -1 };
static const short SubPackageArray9[] = { 11, 10, -1 };
static const short CheckerArray10[] = { 5, -1 };
static const short CheckerArray11[] = { 32, -1 };
static const short SubPackageArray12[] = { 13, 14, 15, 16, 17, -1 };
static const short CheckerArray13[] = { 52, -1 };
static const short CheckerArray14[] = { 53, -1 };
static const short CheckerArray15[] = { 49, -1 };
static const short CheckerArray16[] = { 50, -1 };
static const short CheckerArray17[] = { 44, -1 };
static const short SubPackageArray19[] = { 20, 21, -1 };
static const short CheckerArray20[] = { 17, -1 };
static const short CheckerArray21[] = { 21, -1 };
static const short SubPackageArray22[] = { 24, 26, 23, 25, -1 };
static const short CheckerArray23[] = { 7, -1 };
static const short CheckerArray24[] = { 24, -1 };
static const short CheckerArray25[] = { 1, -1 };
static const short CheckerArray26[] = { 8, -1 };
static const short SubPackageArray27[] = { 28, 35, 37, 43, 49, 39, -1 };
static const short SubPackageArray28[] = { 29, 30, 31, 32, 33, 34, -1 };
static const short CheckerArray29[] = { 13, -1 };
static const short CheckerArray30[] = { 14, -1 };
static const short CheckerArray31[] = { 20, -1 };
static const short CheckerArray32[] = { 39, -1 };
static const short CheckerArray33[] = { 40, -1 };
static const short CheckerArray34[] = { 46, -1 };
static const short SubPackageArray35[] = { 36, -1 };
static const short CheckerArray36[] = { 22, -1 };
static const short SubPackageArray37[] = { 38, -1 };
static const short CheckerArray38[] = { 55, -1 };
static const short SubPackageArray39[] = { 40, -1 };
static const short SubPackageArray40[] = { 42, 41, -1 };
static const short CheckerArray41[] = { 35, -1 };
static const short CheckerArray42[] = { 37, -1 };
static const short SubPackageArray43[] = { 48, 44, 45, 47, 46, -1 };
static const short CheckerArray44[] = { 2, -1 };
static const short CheckerArray45[] = { 3, -1 };
static const short CheckerArray46[] = { 28, -1 };
static const short CheckerArray47[] = { 43, -1 };
static const short CheckerArray48[] = { 45, -1 };
static const short SubPackageArray49[] = { 51, 50, 52, 53, 54, -1 };
static const short CheckerArray50[] = { 11, -1 };
static const short CheckerArray51[] = { 15, -1 };
static const short CheckerArray52[] = { 27, -1 };
static const short CheckerArray53[] = { 41, -1 };
static const short CheckerArray54[] = { 48, -1 };
static const short SubPackageArray55[] = { 56, -1 };
static const short CheckerArray56[] = { 23, -1 };
static const short SubPackageArray57[] = { 61, 71, 58, 59, 60, -1 };
static const short CheckerArray58[] = { 26, -1 };
static const short CheckerArray59[] = { 33, -1 };
static const short CheckerArray60[] = { 25, -1 };
static const short SubPackageArray61[] = { 62, 67, 63, 70, 65, 64, 69, 66, 68, -1 };
static const short CheckerArray62[] = { 34, -1 };
static const short CheckerArray63[] = { 16, -1 };
static const short CheckerArray64[] = { 36, -1 };
static const short CheckerArray65[] = { 29, -1 };
static const short CheckerArray66[] = { 30, -1 };
static const short CheckerArray67[] = { 31, -1 };
static const short CheckerArray68[] = { 42, -1 };
static const short CheckerArray69[] = { 38, -1 };
static const short CheckerArray70[] = { 57, -1 };
static const short SubPackageArray71[] = { 73, 74, 72, -1 };
static const short CheckerArray72[] = { 6, -1 };
static const short CheckerArray73[] = { 9, -1 };
static const short CheckerArray74[] = { 10, -1 };
static const short SubPackageArray76[] = { 77, -1 };
static const short CheckerArray77[] = { 54, -1 };
#endif // GET_MEMBER_ARRAYS


#ifdef GET_CHECKNAME_TABLE
  { "core",                                     0, SubPackageArray0, false },
  { "core.AdjustedReturnValue",                 CheckerArray1, 0, false },
  { "core.AttributeNonNull",                    CheckerArray2, 0, false },
  { "core.CallAndMessage",                      CheckerArray3, 0, false },
  { "core.DivideZero",                          CheckerArray4, 0, false },
  { "core.NullDereference",                     CheckerArray5, 0, false },
  { "core.StackAddressEscape",                  CheckerArray6, 0, false },
  { "core.UndefinedBinaryOperatorResult",       CheckerArray7, 0, false },
  { "core.VLASize",                             CheckerArray8, 0, false },
  { "core.builtin",                             0, SubPackageArray9, false },
  { "core.builtin.BuiltinFunctions",            CheckerArray10, 0, false },
  { "core.builtin.NoReturnFunctions",           CheckerArray11, 0, false },
  { "core.uninitialized",                       0, SubPackageArray12, false },
  { "core.uninitialized.ArraySubscript",        CheckerArray13, 0, false },
  { "core.uninitialized.Assign",                CheckerArray14, 0, false },
  { "core.uninitialized.Branch",                CheckerArray15, 0, false },
  { "core.uninitialized.CapturedBlockVariable", CheckerArray16, 0, false },
  { "core.uninitialized.UndefReturn",           CheckerArray17, 0, false },
  { "cplusplus",                                0, 0, false },
  { "deadcode",                                 0, SubPackageArray19, false },
  { "deadcode.DeadStores",                      CheckerArray20, 0, false },
  { "deadcode.IdempotentOperations",            CheckerArray21, 0, false },
  { "debug",                                    0, SubPackageArray22, false },
  { "debug.DumpCFG",                            CheckerArray23, 0, false },
  { "debug.DumpLiveVars",                       CheckerArray24, 0, false },
  { "debug.Stats",                              CheckerArray25, 0, false },
  { "debug.ViewCFG",                            CheckerArray26, 0, false },
  { "experimental",                             0, SubPackageArray27, false },
  { "experimental.core",                        0, SubPackageArray28, true },
  { "experimental.core.CastSize",               CheckerArray29, 0, false },
  { "experimental.core.CastToStruct",           CheckerArray30, 0, false },
  { "experimental.core.FixedAddr",              CheckerArray31, 0, false },
  { "experimental.core.PointerArithm",          CheckerArray32, 0, false },
  { "experimental.core.PointerSub",             CheckerArray33, 0, false },
  { "experimental.core.SizeofPtr",              CheckerArray34, 0, false },
  { "experimental.cplusplus",                   0, SubPackageArray35, true },
  { "experimental.cplusplus.Iterators",         CheckerArray36, 0, false },
  { "experimental.deadcode",                    0, SubPackageArray37, true },
  { "experimental.deadcode.UnreachableCode",    CheckerArray38, 0, false },
  { "experimental.osx",                         0, SubPackageArray39, true },
  { "experimental.osx.cocoa",                   0, SubPackageArray40, true },
  { "experimental.osx.cocoa.Dealloc",           CheckerArray41, 0, false },
  { "experimental.osx.cocoa.SelfInit",          CheckerArray42, 0, false },
  { "experimental.security",                    0, SubPackageArray43, true },
  { "experimental.security.ArrayBound",         CheckerArray44, 0, false },
  { "experimental.security.ArrayBoundV2",       CheckerArray45, 0, false },
  { "experimental.security.MallocOverflow",     CheckerArray46, 0, false },
  { "experimental.security.ReturnPtrRange",     CheckerArray47, 0, false },
  { "experimental.security.SecuritySyntactic",  CheckerArray48, 0, false },
  { "experimental.unix",                        0, SubPackageArray49, true },
  { "experimental.unix.CString",                CheckerArray50, 0, false },
  { "experimental.unix.Chroot",                 CheckerArray51, 0, false },
  { "experimental.unix.Malloc",                 CheckerArray52, 0, false },
  { "experimental.unix.PthreadLock",            CheckerArray53, 0, false },
  { "experimental.unix.Stream",                 CheckerArray54, 0, false },
  { "llvm",                                     0, SubPackageArray55, false },
  { "llvm.Conventions",                         CheckerArray56, 0, false },
  { "osx",                                      0, SubPackageArray57, false },
  { "osx.API",                                  CheckerArray58, 0, false },
  { "osx.AtomicCAS",                            CheckerArray59, 0, false },
  { "osx.SecKeychainAPI",                       CheckerArray60, 0, false },
  { "osx.cocoa",                                0, SubPackageArray61, false },
  { "osx.cocoa.AtSync",                         CheckerArray62, 0, false },
  { "osx.cocoa.ClassRelease",                   CheckerArray63, 0, false },
  { "osx.cocoa.IncompatibleMethodTypes",        CheckerArray64, 0, false },
  { "osx.cocoa.NSAutoreleasePool",              CheckerArray65, 0, false },
  { "osx.cocoa.NSError",                        CheckerArray66, 0, false },
  { "osx.cocoa.NilArg",                         CheckerArray67, 0, false },
  { "osx.cocoa.RetainCount",                    CheckerArray68, 0, false },
  { "osx.cocoa.UnusedIvars",                    CheckerArray69, 0, false },
  { "osx.cocoa.VariadicMethodTypes",            CheckerArray70, 0, false },
  { "osx.coreFoundation",                       0, SubPackageArray71, false },
  { "osx.coreFoundation.CFError",               CheckerArray72, 0, false },
  { "osx.coreFoundation.CFNumber",              CheckerArray73, 0, false },
  { "osx.coreFoundation.CFRetainRelease",       CheckerArray74, 0, false },
  { "security",                                 0, 0, false },
  { "unix",                                     0, SubPackageArray76, false },
  { "unix.API",                                 CheckerArray77, 0, false },
#endif // GET_CHECKNAME_TABLE

