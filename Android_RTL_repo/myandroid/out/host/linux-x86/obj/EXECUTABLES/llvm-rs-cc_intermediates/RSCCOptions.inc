//===- TableGen'erated file -------------------------------------*- C++ -*-===//
//
// Option Parsing Definitions
//
// Automatically generated file, do not edit!
//
//===----------------------------------------------------------------------===//

#ifndef OPTION
#error "Define OPTION prior to including this file!"
#endif

/////////
// Groups

OPTION("M group>", M_Group, Group, INVALID, INVALID, 0, 0, 0, 0)
OPTION("<output type group>", Output_Type_Group, Group, INVALID, INVALID, 0, 0, 0, 0)

//////////
// Options

OPTION("<input>", INPUT, Input, INVALID, INVALID, DriverOption, 0, 0, 0)
OPTION("<unknown>", UNKNOWN, Unknown, INVALID, INVALID, 0, 0, 0, 0)
OPTION("--help", _help, Flag, INVALID, help, 0, 0, 0, 0)
OPTION("--version", _version, Flag, INVALID, version, 0, 0, 0, 0)
OPTION("-I", I, JoinedOrSeparate, INVALID, INVALID, 0, 0,
       "Add directory to include search path", "<directory>")
OPTION("-MD", MD, Flag, M_Group, INVALID, 0, 0, 0, 0)
OPTION("-M", M, Flag, M_Group, INVALID, 0, 0, 0, 0)
OPTION("-S", _emit_asm, Flag, Output_Type_Group, emit_asm, 0, 0, 0, 0)
OPTION("-additional-dep-target", additional_dep_target, Separate, INVALID, INVALID, 0, 0,
       "Additional targets to show up in dependencies output", 0)
OPTION("-allow-rs-prefix", allow_rs_prefix, Flag, INVALID, INVALID, 0, 0,
       "Allow user-defined function prefixed with 'rs'", 0)
OPTION("-a", _additional_dep_target, Separate, INVALID, additional_dep_target, 0, 0, 0, 0)
OPTION("-bitcode-storage", bitcode_storage, Separate, INVALID, INVALID, 0, 0,
       "<value> should be 'ar' or 'jc'", "<value>")
OPTION("-d", _output_dep_dir, Separate, INVALID, output_dep_dir, 0, 0, 0, 0)
OPTION("-emit-asm", emit_asm, Flag, Output_Type_Group, INVALID, 0, 0,
       "Emit target assembly files", 0)
OPTION("-emit-bc", emit_bc, Flag, Output_Type_Group, INVALID, 0, 0,
       "Build ASTs then convert to LLVM, emit .bc file", 0)
OPTION("-emit-dep", emit_dep, Flag, M_Group, M, 0, 0, 0, 0)
OPTION("-emit-llvm", emit_llvm, Flag, Output_Type_Group, INVALID, 0, 0,
       "Build ASTs then convert to LLVM, emit .ll file", 0)
OPTION("-emit-nothing", emit_nothing, Flag, Output_Type_Group, INVALID, 0, 0,
       "Build ASTs then convert to LLVM, but emit nothing", 0)
OPTION("-help", help, Flag, INVALID, INVALID, 0, 0,
       "Print this help text", 0)
OPTION("-h", __help, Flag, INVALID, help, 0, 0, 0, 0)
OPTION("-java-reflection-package-name", java_reflection_package_name, Separate, INVALID, INVALID, 0, 0,
       "Specify the package name that reflected Java files belong to", 0)
OPTION("-java-reflection-path-base", java_reflection_path_base, Separate, INVALID, INVALID, 0, 0,
       "Base directory for output reflected Java files", "<directory>")
OPTION("-j", _java_reflection_package_name, Separate, INVALID, java_reflection_package_name, 0, 0, 0, 0)
OPTION("-no-link", no_link, Flag, INVALID, INVALID, 0, 0, 0, 0)
OPTION("-output-dep-dir", output_dep_dir, Separate, INVALID, INVALID, 0, 0,
       "Specify output directory for dependencies output", "<directory>")
OPTION("-o", o, Separate, INVALID, INVALID, 0, 0,
       "Specify output directory", "<directory>")
OPTION("-p", _java_reflection_path_base, Separate, INVALID, java_reflection_path_base, 0, 0, 0, 0)
OPTION("-s", _bitcode_storage, Separate, INVALID, bitcode_storage, 0, 0, 0, 0)
OPTION("-target-api=", target_api_EQ, Joined, INVALID, target_api, 0, 0, 0, 0)
OPTION("-target-api", target_api, Separate, INVALID, INVALID, 0, 0,
       "Specify target API level (e.g. 14)", 0)
OPTION("-version", version, Flag, INVALID, INVALID, 0, 0,
       "Print the assembler version", 0)
OPTION("include-path", _I, Separate, INVALID, I, 0, 0, 0, "<directory>")
