//Autogenerated code by util.build.BuildDalvikSuite; do not edit.
package dot.junit.opcodes.sub_double;
import dot.junit.opcodes.sub_double.d.*;
import dot.junit.*;
public class Main_testB10 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_sub_double_1 t = new T_sub_double_1();
        assertEquals(0d, t.run(Double.MIN_VALUE, 4.9E-324));
    }
}
