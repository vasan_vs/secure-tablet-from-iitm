//Autogenerated code by util.build.BuildDalvikSuite; do not edit.
package dot.junit.opcodes.add_long;
import dot.junit.opcodes.add_long.d.*;
import dot.junit.*;
public class Main_testB5 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_add_long_1 t = new T_add_long_1();
        assertEquals(-1l, t.run(Long.MAX_VALUE, Long.MIN_VALUE));
    }
}
