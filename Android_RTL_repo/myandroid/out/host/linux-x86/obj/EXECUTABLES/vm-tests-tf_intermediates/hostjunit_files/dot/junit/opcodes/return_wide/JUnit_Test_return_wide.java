//Autogenerated code by util.build.BuildDalvikSuite; do not edit.
package dot.junit.opcodes.return_wide;
import java.io.IOException;
import com.android.tradefed.testtype.DeviceTestCase;

public class JUnit_Test_return_wide extends DeviceTestCase {
public void testE1() throws Exception {
    String res = getDevice().executeShellCommand("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/return_wide/Main_testE1.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/return_wide/d/T_return_wide_3.jar dot.junit.opcodes.return_wide.Main_testE1");
// A sucessful adb shell command returns an empty string.
assertEquals("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/return_wide/Main_testE1.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/return_wide/d/T_return_wide_3.jar dot.junit.opcodes.return_wide.Main_testE1", "", res);}

public void testN1() throws Exception {
    String res = getDevice().executeShellCommand("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/return_wide/Main_testN1.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/return_wide/d/T_return_wide_1.jar dot.junit.opcodes.return_wide.Main_testN1");
// A sucessful adb shell command returns an empty string.
assertEquals("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/return_wide/Main_testN1.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/return_wide/d/T_return_wide_1.jar dot.junit.opcodes.return_wide.Main_testN1", "", res);}

public void testVFE1() throws Exception {
    String res = getDevice().executeShellCommand("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/return_wide/Main_testVFE1.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/return_wide/d/T_return_wide_5.jar dot.junit.opcodes.return_wide.Main_testVFE1");
// A sucessful adb shell command returns an empty string.
assertEquals("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/return_wide/Main_testVFE1.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/return_wide/d/T_return_wide_5.jar dot.junit.opcodes.return_wide.Main_testVFE1", "", res);}

public void testVFE2() throws Exception {
    String res = getDevice().executeShellCommand("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/return_wide/Main_testVFE2.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/return_wide/d/T_return_wide_6.jar dot.junit.opcodes.return_wide.Main_testVFE2");
// A sucessful adb shell command returns an empty string.
assertEquals("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/return_wide/Main_testVFE2.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/return_wide/d/T_return_wide_6.jar dot.junit.opcodes.return_wide.Main_testVFE2", "", res);}

public void testVFE3() throws Exception {
    String res = getDevice().executeShellCommand("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/return_wide/Main_testVFE3.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/return_wide/d/T_return_wide_7.jar dot.junit.opcodes.return_wide.Main_testVFE3");
// A sucessful adb shell command returns an empty string.
assertEquals("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/return_wide/Main_testVFE3.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/return_wide/d/T_return_wide_7.jar dot.junit.opcodes.return_wide.Main_testVFE3", "", res);}

public void testVFE4() throws Exception {
    String res = getDevice().executeShellCommand("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/return_wide/Main_testVFE4.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/return_wide/d/T_return_wide_8.jar dot.junit.opcodes.return_wide.Main_testVFE4");
// A sucessful adb shell command returns an empty string.
assertEquals("ANDROID_DATA=/data/local/tmp/vm-tests dalvikvm -Xint:portable -Xmx512M -Xss32K -Djava.io.tmpdir=/data/local/tmp/vm-tests -classpath /data/local/tmp/vm-tests/dot/junit/dexcore.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/return_wide/Main_testVFE4.jar:/data/local/tmp/vm-tests/dot/junit/opcodes/return_wide/d/T_return_wide_8.jar dot.junit.opcodes.return_wide.Main_testVFE4", "", res);}


}
