//Autogenerated code by util.build.BuildDalvikSuite; do not edit.
package dot.junit.opcodes.move_exception;
import dot.junit.opcodes.move_exception.d.*;
import dot.junit.*;
public class Main_testVFE3 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        try {
            Class.forName("dot.junit.opcodes.move_exception.d.T_move_exception_5");
            fail("expected a verification exception");
        } catch (Throwable t) {
            DxUtil.checkVerifyException(t);
        }
    }
}
