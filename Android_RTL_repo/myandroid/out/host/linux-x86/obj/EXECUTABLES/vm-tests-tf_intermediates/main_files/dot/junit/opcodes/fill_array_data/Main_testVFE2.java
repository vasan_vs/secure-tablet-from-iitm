//Autogenerated code by util.build.BuildDalvikSuite; do not edit.
package dot.junit.opcodes.fill_array_data;
import dot.junit.opcodes.fill_array_data.d.*;
import dot.junit.*;
public class Main_testVFE2 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        try {
            Class.forName("dot.junit.opcodes.fill_array_data.d.T_fill_array_data_4");
            fail("expected a verification exception");
        } catch (Throwable t) {
            DxUtil.checkVerifyException(t);
        }
    }
}
