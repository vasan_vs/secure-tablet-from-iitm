//Autogenerated code by util.build.BuildDalvikSuite; do not edit.
package dot.junit.opcodes.opc_throw;
import dot.junit.opcodes.opc_throw.d.*;
import dot.junit.*;
public class Main_testVFE5 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        try {
            Class.forName("dxc.junit.opcodes.opc_throw.jm.T_opc_throw_10");
            fail("expected a verification exception");
        } catch (Throwable t) {
            DxUtil.checkVerifyException(t);
        }
    }
}
