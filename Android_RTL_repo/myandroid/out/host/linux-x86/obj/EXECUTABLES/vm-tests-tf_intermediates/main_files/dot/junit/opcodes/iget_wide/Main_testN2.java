//Autogenerated code by util.build.BuildDalvikSuite; do not edit.
package dot.junit.opcodes.iget_wide;
import dot.junit.opcodes.iget_wide.d.*;
import dot.junit.*;
public class Main_testN2 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_iget_wide_2 t = new T_iget_wide_2();
        assertEquals(123.0, t.run());
    }
}
