//Autogenerated code by util.build.BuildDalvikSuite; do not edit.
package dot.junit.opcodes.const_string;
import dot.junit.opcodes.const_string.d.*;
import dot.junit.*;
public class Main_testVFE2 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        try {
            Class.forName("dot.junit.opcodes.const_string.d.T_const_string_4");
            fail("expected a verification exception");
        } catch (Throwable t) {
            DxUtil.checkVerifyException(t);
        }
    }
}
