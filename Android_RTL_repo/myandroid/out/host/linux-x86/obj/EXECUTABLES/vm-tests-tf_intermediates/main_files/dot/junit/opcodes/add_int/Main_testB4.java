//Autogenerated code by util.build.BuildDalvikSuite; do not edit.
package dot.junit.opcodes.add_int;
import dot.junit.opcodes.add_int.d.*;
import dot.junit.*;
public class Main_testB4 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_add_int_1 t = new T_add_int_1();
        assertEquals(-2147483647, t.run(Integer.MIN_VALUE, 1));
    }
}
