//Autogenerated code by util.build.BuildDalvikSuite; do not edit.
package dot.junit.opcodes.aput_wide;
import dot.junit.opcodes.aput_wide.d.*;
import dot.junit.*;
public class Main_testE2 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_aput_wide_1 t = new T_aput_wide_1();
        try {
            t.run(null, 1, 100000000000l);
            fail("expected NullPointerException");
        } catch (NullPointerException np) {
            // expected
        }
    }
}
