//Autogenerated code by util.build.BuildDalvikSuite; do not edit.
package dot.junit.opcodes.rem_float;
import dot.junit.opcodes.rem_float.d.*;
import dot.junit.*;
public class Main_testB9 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_rem_float_1 t = new T_rem_float_1();
        assertEquals(7.2584893E-10f, t.run(Float.MAX_VALUE, -1E-9f));
    }
}
