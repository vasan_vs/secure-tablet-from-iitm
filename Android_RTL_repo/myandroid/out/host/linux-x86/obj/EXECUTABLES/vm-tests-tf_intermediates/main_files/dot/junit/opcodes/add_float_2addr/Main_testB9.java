//Autogenerated code by util.build.BuildDalvikSuite; do not edit.
package dot.junit.opcodes.add_float_2addr;
import dot.junit.opcodes.add_float_2addr.d.*;
import dot.junit.*;
public class Main_testB9 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_add_float_2addr_1 t = new T_add_float_2addr_1();
        assertEquals(0f, t.run(Float.MIN_VALUE, -1.4E-45f));
    }
}
