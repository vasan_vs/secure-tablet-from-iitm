//autogenerated by util.build.BuildDalvikSuite, do not change
package dot.junit.opcodes.rem_long_2addr;
import dot.junit.opcodes.rem_long_2addr.d.*;
import dot.junit.*;
public class Main_testB5 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_rem_long_2addr_1 t = new T_rem_long_2addr_1();
        assertEquals(1l, t.run(1l, Long.MAX_VALUE));
    }
}
