//autogenerated by util.build.BuildDalvikSuite, do not change
package dot.junit.opcodes.if_gt;
import dot.junit.opcodes.if_gt.d.*;
import dot.junit.*;
public class Main_testN3 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_if_gt_1 t = new T_if_gt_1();
        assertEquals(1, t.run(5, -5));
    }
}
