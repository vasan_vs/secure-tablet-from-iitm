//autogenerated by util.build.BuildDalvikSuite, do not change
package dot.junit.opcodes.div_int;
import dot.junit.opcodes.div_int.d.*;
import dot.junit.*;
public class Main_testB5 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_div_int_1 t = new T_div_int_1();
        assertEquals(0, t.run(1, Integer.MAX_VALUE));
    }
}
