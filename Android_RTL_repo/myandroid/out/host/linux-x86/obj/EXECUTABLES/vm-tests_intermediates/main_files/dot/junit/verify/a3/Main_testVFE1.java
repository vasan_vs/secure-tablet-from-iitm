//autogenerated by util.build.BuildDalvikSuite, do not change
package dot.junit.verify.a3;
import dot.junit.verify.a3.d.*;
import dot.junit.*;
public class Main_testVFE1 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        try {
            Class.forName("dot.junit.verify.a3.d.T_a3_1");
            fail("expected a verification exception");
        } catch (Throwable t) {
            DxUtil.checkVerifyException(t);
        }
    }
}
