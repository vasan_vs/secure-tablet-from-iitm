//autogenerated by util.build.BuildDalvikSuite, do not change
package dot.junit.opcodes.add_int_lit8;
import dot.junit.opcodes.add_int_lit8.d.*;
import dot.junit.*;
public class Main_testB1 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_add_int_lit8_7 t = new T_add_int_lit8_7();
        assertEquals(Byte.MAX_VALUE, t.run());
    }
}
