//autogenerated by util.build.BuildDalvikSuite, do not change
package dot.junit.opcodes.iput_object;
import dot.junit.opcodes.iput_object.d.*;
import dot.junit.*;
public class Main_testVFE13 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        try {
            Class.forName("dot.junit.opcodes.iput_object.d.T_iput_object_2");
            fail("expected a verification exception");
        } catch (Throwable t) {
            DxUtil.checkVerifyException(t);
        }
    }
}
