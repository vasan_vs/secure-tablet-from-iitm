//autogenerated by util.build.BuildDalvikSuite, do not change
package dot.junit.opcodes.if_eq;
import dot.junit.opcodes.if_eq.d.*;
import dot.junit.*;
public class Main_testN4 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_if_eq_1 t = new T_if_eq_1();
        assertEquals(1234, t.run(0x01001234, 0x1234));
    }
}
