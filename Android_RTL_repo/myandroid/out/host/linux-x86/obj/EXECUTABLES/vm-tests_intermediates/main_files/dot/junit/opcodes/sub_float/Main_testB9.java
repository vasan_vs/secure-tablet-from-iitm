//autogenerated by util.build.BuildDalvikSuite, do not change
package dot.junit.opcodes.sub_float;
import dot.junit.opcodes.sub_float.d.*;
import dot.junit.*;
public class Main_testB9 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_sub_float_1 t = new T_sub_float_1();
        assertEquals(0f, t.run(Float.MAX_VALUE, Float.MAX_VALUE));
    }
}
