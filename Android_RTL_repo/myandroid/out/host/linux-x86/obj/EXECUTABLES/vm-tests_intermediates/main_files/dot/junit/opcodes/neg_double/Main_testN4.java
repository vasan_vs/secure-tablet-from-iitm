//autogenerated by util.build.BuildDalvikSuite, do not change
package dot.junit.opcodes.neg_double;
import dot.junit.opcodes.neg_double.d.*;
import dot.junit.*;
public class Main_testN4 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_neg_double_1 t = new T_neg_double_1();
        assertEquals(2.7d, t.run(-2.7d));
    }
}
