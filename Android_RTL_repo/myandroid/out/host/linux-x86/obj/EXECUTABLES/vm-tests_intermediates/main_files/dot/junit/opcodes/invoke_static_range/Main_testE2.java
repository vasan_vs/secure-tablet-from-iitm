//autogenerated by util.build.BuildDalvikSuite, do not change
package dot.junit.opcodes.invoke_static_range;
import dot.junit.opcodes.invoke_static_range.d.*;
import dot.junit.*;
public class Main_testE2 extends DxAbstractMain {
    public static void main(String[] args) throws Exception {
        T_invoke_static_range_6 t = new T_invoke_static_range_6();
        try {
            t.run();
            fail("expected UnsatisfiedLinkError");
        } catch (UnsatisfiedLinkError ule) {
            // expected
        }
    }
}
