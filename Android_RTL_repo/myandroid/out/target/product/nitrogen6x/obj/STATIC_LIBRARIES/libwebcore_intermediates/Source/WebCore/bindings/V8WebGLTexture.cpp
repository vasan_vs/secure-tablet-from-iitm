/*
    This file is part of the WebKit open source project.
    This file has been generated by generate-bindings.pl. DO NOT MODIFY!

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.
*/

#include "config.h"
#include "V8WebGLTexture.h"

#if ENABLE(WEBGL)

#include "RuntimeEnabledFeatures.h"
#include "V8Binding.h"
#include "V8BindingState.h"
#include "V8DOMWrapper.h"
#include "V8IsolatedContext.h"
#include "V8Proxy.h"
#include <wtf/UnusedParam.h>

namespace WebCore {

WrapperTypeInfo V8WebGLTexture::info = { V8WebGLTexture::GetTemplate, V8WebGLTexture::derefObject, 0, 0 };

namespace WebGLTextureInternal {

template <typename T> void V8_USE(T) { }

} // namespace WebGLTextureInternal

static v8::Persistent<v8::FunctionTemplate> ConfigureV8WebGLTextureTemplate(v8::Persistent<v8::FunctionTemplate> desc)
{
    v8::Local<v8::Signature> defaultSignature = configureTemplate(desc, "WebGLTexture", v8::Persistent<v8::FunctionTemplate>(), V8WebGLTexture::internalFieldCount,
        0, 0,
        0, 0);
    UNUSED_PARAM(defaultSignature); // In some cases, it will not be used.
    

    // Custom toString template
    desc->Set(getToStringName(), getToStringTemplate());
    return desc;
}

v8::Persistent<v8::FunctionTemplate> V8WebGLTexture::GetRawTemplate()
{
    static v8::Persistent<v8::FunctionTemplate> V8WebGLTextureRawCache = createRawTemplate();
    return V8WebGLTextureRawCache;
}

v8::Persistent<v8::FunctionTemplate> V8WebGLTexture::GetTemplate()
{
    static v8::Persistent<v8::FunctionTemplate> V8WebGLTextureCache = ConfigureV8WebGLTextureTemplate(GetRawTemplate());
    return V8WebGLTextureCache;
}

bool V8WebGLTexture::HasInstance(v8::Handle<v8::Value> value)
{
    return GetRawTemplate()->HasInstance(value);
}


v8::Handle<v8::Object> V8WebGLTexture::wrapSlow(WebGLTexture* impl)
{
    v8::Handle<v8::Object> wrapper;
    V8Proxy* proxy = 0;
    wrapper = V8DOMWrapper::instantiateV8Object(proxy, &info, impl);
    if (wrapper.IsEmpty())
        return wrapper;

    impl->ref();
    v8::Persistent<v8::Object> wrapperHandle = v8::Persistent<v8::Object>::New(wrapper);
    getDOMObjectMap().set(impl, wrapperHandle);
    return wrapper;
}

void V8WebGLTexture::derefObject(void* object)
{
    static_cast<WebGLTexture*>(object)->deref();
}

} // namespace WebCore

#endif // ENABLE(WEBGL)
