/* Automatically generated file (DON'T MODIFY) */

/* Repository directory: /media/work3/Android_RTL_repo/myandroid/frameworks/compile/libbcc */

/* File list:
   85d21f30271c689ebba8ef9cf76d3f3b034f63da frameworks/compile/libbcc/lib/ExecutionEngine/bcc.cpp
   6cd0a80f4e36dc4ef41601cca20932f89944bd84 out/target/product/nitrogen6x/obj/STATIC_LIBRARIES/libLLVMARMCodeGen_intermediates/libLLVMARMCodeGen.a
   2aa076e71f255bde44ac64f53ca04980df336e11 out/target/product/nitrogen6x/obj/STATIC_LIBRARIES/libLLVMARMDesc_intermediates/libLLVMARMDesc.a
   a2b8a7e57e5beddd70aa421aa1a9b20eb51280fd out/target/product/nitrogen6x/obj/STATIC_LIBRARIES/libLLVMARMInfo_intermediates/libLLVMARMInfo.a
   a9dc1c6a2b49763a7cdc12f4fa58a6cbadc63259 out/target/product/nitrogen6x/obj/STATIC_LIBRARIES/libLLVMAnalysis_intermediates/libLLVMAnalysis.a
   55935c6c5cd85116cb4801fbbfb202310afba741 out/target/product/nitrogen6x/obj/STATIC_LIBRARIES/libLLVMAsmPrinter_intermediates/libLLVMAsmPrinter.a
   8cce63e19cd24150f0bdbce3c243acf5eef26e8c out/target/product/nitrogen6x/obj/STATIC_LIBRARIES/libLLVMBitReader_intermediates/libLLVMBitReader.a
   06a35606ec3f53491089a598dfbc3aa5d2d2b74c out/target/product/nitrogen6x/obj/STATIC_LIBRARIES/libLLVMCodeGen_intermediates/libLLVMCodeGen.a
   5d6ed19086fef5811fec880bf17fe314b40e110e out/target/product/nitrogen6x/obj/STATIC_LIBRARIES/libLLVMCore_intermediates/libLLVMCore.a
   ab7cb0be34a01ae8cef6956241d16be38e1a2cea out/target/product/nitrogen6x/obj/STATIC_LIBRARIES/libLLVMInstCombine_intermediates/libLLVMInstCombine.a
   11f14f023ad85cd4b8dea4f3dce853b44d3d1d7e out/target/product/nitrogen6x/obj/STATIC_LIBRARIES/libLLVMJIT_intermediates/libLLVMJIT.a
   3e85e793931647ba7ea68acaea5a81e0a373da06 out/target/product/nitrogen6x/obj/STATIC_LIBRARIES/libLLVMLinker_intermediates/libLLVMLinker.a
   81d38f9a2b488a1d9f9ed8b2fe4361fc36126740 out/target/product/nitrogen6x/obj/STATIC_LIBRARIES/libLLVMMCParser_intermediates/libLLVMMCParser.a
   0f037dda5d7b70adf40a34e2a4faff5198227545 out/target/product/nitrogen6x/obj/STATIC_LIBRARIES/libLLVMMC_intermediates/libLLVMMC.a
   0776abacc07c1cece92c570dfa9ee2e390da928f out/target/product/nitrogen6x/obj/STATIC_LIBRARIES/libLLVMScalarOpts_intermediates/libLLVMScalarOpts.a
   8e96d9edecb7f0433df7974d7c0078164703daf2 out/target/product/nitrogen6x/obj/STATIC_LIBRARIES/libLLVMSelectionDAG_intermediates/libLLVMSelectionDAG.a
   ea87d9a199d0809a9642bd5d9b2e54d0507b8ae1 out/target/product/nitrogen6x/obj/STATIC_LIBRARIES/libLLVMSupport_intermediates/libLLVMSupport.a
   614e16023ad4f9933b285705ef49c459b342b3a9 out/target/product/nitrogen6x/obj/STATIC_LIBRARIES/libLLVMTarget_intermediates/libLLVMTarget.a
   360f56abd546c0df2cbb8bab5bd0949bc76361ad out/target/product/nitrogen6x/obj/STATIC_LIBRARIES/libLLVMTransformUtils_intermediates/libLLVMTransformUtils.a
   fe0afc9ec94e14af7a8eaab4dbd4a73591de5ff3 out/target/product/nitrogen6x/obj/STATIC_LIBRARIES/libLLVMipa_intermediates/libLLVMipa.a
   b47087927dcbc6b1c151dfab29e3c4c852075c36 out/target/product/nitrogen6x/obj/STATIC_LIBRARIES/libLLVMipo_intermediates/libLLVMipo.a
   3ea240c66b3c1cf63cba02581614e9497c8daabb out/target/product/nitrogen6x/obj/STATIC_LIBRARIES/libbccCompilerRT_intermediates/libbccCompilerRT.a
   252ca8a51e8b1fe5efe6b1fa16db12ae45b62c17 out/target/product/nitrogen6x/obj/STATIC_LIBRARIES/libbccExecutionEngine_intermediates/libbccExecutionEngine.a
   47fd0d1b4e221e696e7904a243159e52062789da out/target/product/nitrogen6x/obj/STATIC_LIBRARIES/libbccHelper_intermediates/libbccHelper.a
   e50ad5ee41fe201c98a23c2a171b6101e6ce14dc out/target/product/nitrogen6x/obj/STATIC_LIBRARIES/librsloader_intermediates/librsloader.a
   dabf714736bb188af84855ee9769e9344b19e513 out/target/product/nitrogen6x/obj/lib/libcutils.so
   10784bf0d308c3f7be07b8d404a9bdfc401a6bfa out/target/product/nitrogen6x/obj/lib/libdl.so
   0a211ffcbac26026b2c52651d00739cd59b059af out/target/product/nitrogen6x/obj/lib/libstlport.so
   ed6f381c4361025bb992292c8d66608cd5dd11e5 out/target/product/nitrogen6x/obj/lib/libutils.so
*/

#ifdef __cplusplus
extern "C" {
#endif

char const *bccGetBuildTime() {
  return "2014/07/27 21:59:06";
}

char const *bccGetBuildRev() {
  return "31e50d678d2a8aa28c6f38cccebfa3da2f78df4a modified (git)";
}

char const *bccGetBuildSHA1() {
  return "fb90715eaa2dfa6bb8e2246e1a0422b31cf74296";
}

#ifdef __cplusplus
}
#endif


