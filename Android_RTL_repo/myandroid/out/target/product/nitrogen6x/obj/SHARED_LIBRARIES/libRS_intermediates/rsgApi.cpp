/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "rsDevice.h"
#include "rsContext.h"
#include "rsThreadIO.h"
#include "rsgApiFuncDecl.h"
#include "rsFifo.h"

using namespace android;
using namespace android::renderscript;


typedef struct RsApiEntrypoints {
    void (* ContextDestroy) (RsContext rsc);
    RsMessageToClientType (* ContextGetMessage) (RsContext rsc, void * data, size_t data_length, size_t * receiveLen, size_t receiveLen_length, uint32_t * usrID, size_t usrID_length);
    RsMessageToClientType (* ContextPeekMessage) (RsContext rsc, size_t * receiveLen, size_t receiveLen_length, uint32_t * usrID, size_t usrID_length);
    void (* ContextInitToClient) (RsContext rsc);
    void (* ContextDeinitToClient) (RsContext rsc);
    RsType (* TypeCreate) (RsContext rsc, RsElement e, uint32_t dimX, uint32_t dimY, uint32_t dimZ, bool mips, bool faces);
    RsAllocation (* AllocationCreateTyped) (RsContext rsc, RsType vtype, RsAllocationMipmapControl mips, uint32_t usages);
    RsAllocation (* AllocationCreateFromBitmap) (RsContext rsc, RsType vtype, RsAllocationMipmapControl mips, const void * data, size_t data_length, uint32_t usages);
    RsAllocation (* AllocationCubeCreateFromBitmap) (RsContext rsc, RsType vtype, RsAllocationMipmapControl mips, const void * data, size_t data_length, uint32_t usages);
    void (* ContextFinish) (RsContext rsc);
    void (* ContextBindRootScript) (RsContext rsc, RsScript sampler);
    void (* ContextBindProgramStore) (RsContext rsc, RsProgramStore pgm);
    void (* ContextBindProgramFragment) (RsContext rsc, RsProgramFragment pgm);
    void (* ContextBindProgramVertex) (RsContext rsc, RsProgramVertex pgm);
    void (* ContextBindProgramRaster) (RsContext rsc, RsProgramRaster pgm);
    void (* ContextBindFont) (RsContext rsc, RsFont pgm);
    void (* ContextPause) (RsContext rsc);
    void (* ContextResume) (RsContext rsc);
    void (* ContextSetSurface) (RsContext rsc, uint32_t width, uint32_t height, RsNativeWindow sur);
    void (* ContextDump) (RsContext rsc, int32_t bits);
    void (* ContextSetPriority) (RsContext rsc, int32_t priority);
    void (* ContextDestroyWorker) (RsContext rsc);
    void (* AssignName) (RsContext rsc, RsObjectBase obj, const char * name, size_t name_length);
    void (* ObjDestroy) (RsContext rsc, RsAsyncVoidPtr objPtr);
    RsElement (* ElementCreate) (RsContext rsc, RsDataType mType, RsDataKind mKind, bool mNormalized, uint32_t mVectorSize);
    RsElement (* ElementCreate2) (RsContext rsc, const RsElement * elements, size_t elements_length, const char ** names, size_t names_length_length, const size_t * names_length, const uint32_t * arraySize, size_t arraySize_length);
    void (* AllocationCopyToBitmap) (RsContext rsc, RsAllocation alloc, void * data, size_t data_length);
    void (* Allocation1DData) (RsContext rsc, RsAllocation va, uint32_t xoff, uint32_t lod, uint32_t count, const void * data, size_t data_length);
    void (* Allocation1DElementData) (RsContext rsc, RsAllocation va, uint32_t x, uint32_t lod, const void * data, size_t data_length, uint32_t comp_offset);
    void (* Allocation2DData) (RsContext rsc, RsAllocation va, uint32_t xoff, uint32_t yoff, uint32_t lod, RsAllocationCubemapFace face, uint32_t w, uint32_t h, const void * data, size_t data_length);
    void (* Allocation2DElementData) (RsContext rsc, RsAllocation va, uint32_t x, uint32_t y, uint32_t lod, RsAllocationCubemapFace face, const void * data, size_t data_length, uint32_t element_offset);
    void (* AllocationGenerateMipmaps) (RsContext rsc, RsAllocation va);
    void (* AllocationRead) (RsContext rsc, RsAllocation va, void * data, size_t data_length);
    void (* AllocationSyncAll) (RsContext rsc, RsAllocation va, RsAllocationUsageType src);
    void (* AllocationResize1D) (RsContext rsc, RsAllocation va, uint32_t dimX);
    void (* AllocationResize2D) (RsContext rsc, RsAllocation va, uint32_t dimX, uint32_t dimY);
    void (* AllocationCopy2DRange) (RsContext rsc, RsAllocation dest, uint32_t destXoff, uint32_t destYoff, uint32_t destMip, uint32_t destFace, uint32_t width, uint32_t height, RsAllocation src, uint32_t srcXoff, uint32_t srcYoff, uint32_t srcMip, uint32_t srcFace);
    RsSampler (* SamplerCreate) (RsContext rsc, RsSamplerValue magFilter, RsSamplerValue minFilter, RsSamplerValue wrapS, RsSamplerValue wrapT, RsSamplerValue wrapR, float mAniso);
    void (* ScriptBindAllocation) (RsContext rsc, RsScript vtm, RsAllocation va, uint32_t slot);
    void (* ScriptSetTimeZone) (RsContext rsc, RsScript s, const char * timeZone, size_t timeZone_length);
    void (* ScriptInvoke) (RsContext rsc, RsScript s, uint32_t slot);
    void (* ScriptInvokeV) (RsContext rsc, RsScript s, uint32_t slot, const void * data, size_t data_length);
    void (* ScriptForEach) (RsContext rsc, RsScript s, uint32_t slot, RsAllocation ain, RsAllocation aout, const void * usr, size_t usr_length);
    void (* ScriptSetVarI) (RsContext rsc, RsScript s, uint32_t slot, int value);
    void (* ScriptSetVarObj) (RsContext rsc, RsScript s, uint32_t slot, RsObjectBase value);
    void (* ScriptSetVarJ) (RsContext rsc, RsScript s, uint32_t slot, int64_t value);
    void (* ScriptSetVarF) (RsContext rsc, RsScript s, uint32_t slot, float value);
    void (* ScriptSetVarD) (RsContext rsc, RsScript s, uint32_t slot, double value);
    void (* ScriptSetVarV) (RsContext rsc, RsScript s, uint32_t slot, const void * data, size_t data_length);
    RsScript (* ScriptCCreate) (RsContext rsc, const char * resName, size_t resName_length, const char * cacheDir, size_t cacheDir_length, const char * text, size_t text_length);
    RsProgramStore (* ProgramStoreCreate) (RsContext rsc, bool colorMaskR, bool colorMaskG, bool colorMaskB, bool colorMaskA, bool depthMask, bool ditherEnable, RsBlendSrcFunc srcFunc, RsBlendDstFunc destFunc, RsDepthFunc depthFunc);
    RsProgramRaster (* ProgramRasterCreate) (RsContext rsc, bool pointSprite, RsCullMode cull);
    void (* ProgramBindConstants) (RsContext rsc, RsProgram vp, uint32_t slot, RsAllocation constants);
    void (* ProgramBindTexture) (RsContext rsc, RsProgramFragment pf, uint32_t slot, RsAllocation a);
    void (* ProgramBindSampler) (RsContext rsc, RsProgramFragment pf, uint32_t slot, RsSampler s);
    RsProgramFragment (* ProgramFragmentCreate) (RsContext rsc, const char * shaderText, size_t shaderText_length, const uint32_t * params, size_t params_length);
    RsProgramVertex (* ProgramVertexCreate) (RsContext rsc, const char * shaderText, size_t shaderText_length, const uint32_t * params, size_t params_length);
    RsFont (* FontCreateFromFile) (RsContext rsc, const char * name, size_t name_length, float fontSize, uint32_t dpi);
    RsFont (* FontCreateFromMemory) (RsContext rsc, const char * name, size_t name_length, float fontSize, uint32_t dpi, const void * data, size_t data_length);
    RsMesh (* MeshCreate) (RsContext rsc, RsAllocation * vtx, size_t vtx_length, RsAllocation * idx, size_t idx_length, uint32_t * primType, size_t primType_length);
} RsApiEntrypoints_t;

static void LF_ContextDestroy (RsContext rsc)
{
    rsi_ContextDestroy((Context *)rsc);
};

static void RF_ContextDestroy (RsContext rsc)
{
    Fifo *f = NULL;
    RS_CMD_ContextDestroy cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_ContextDestroy;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;


    f->writeAsync(&cmd, cmdSize);
}

static RsMessageToClientType LF_ContextGetMessage (RsContext rsc, void * data, size_t data_length, size_t * receiveLen, size_t receiveLen_length, uint32_t * usrID, size_t usrID_length)
{
    return rsi_ContextGetMessage((Context *)rsc, data, data_length, receiveLen, receiveLen_length, usrID, usrID_length);
};

static RsMessageToClientType RF_ContextGetMessage (RsContext rsc, void * data, size_t data_length, size_t * receiveLen, size_t receiveLen_length, uint32_t * usrID, size_t usrID_length)
{
    Fifo *f = NULL;
    RS_CMD_ContextGetMessage cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_ContextGetMessage;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;

    cmd.data = (void *)offset;
    offset += data_length;
    cmd.data_length = data_length;
    cmd.receiveLen = (size_t *)offset;
    offset += receiveLen_length;
    cmd.receiveLen_length = receiveLen_length;
    cmd.usrID = (uint32_t *)offset;
    offset += usrID_length;
    cmd.usrID_length = usrID_length;

    f->writeAsync(&cmd, cmdSize);
    f->writeAsync(data, data_length);
    f->writeAsync(receiveLen, receiveLen_length);
    f->writeAsync(usrID, usrID_length);
    RsMessageToClientType retValue;
    f->writeWaitReturn(&retValue, sizeof(retValue));
    return retValue;
}

static RsMessageToClientType LF_ContextPeekMessage (RsContext rsc, size_t * receiveLen, size_t receiveLen_length, uint32_t * usrID, size_t usrID_length)
{
    return rsi_ContextPeekMessage((Context *)rsc, receiveLen, receiveLen_length, usrID, usrID_length);
};

static RsMessageToClientType RF_ContextPeekMessage (RsContext rsc, size_t * receiveLen, size_t receiveLen_length, uint32_t * usrID, size_t usrID_length)
{
    Fifo *f = NULL;
    RS_CMD_ContextPeekMessage cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_ContextPeekMessage;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;

    cmd.receiveLen = (size_t *)offset;
    offset += receiveLen_length;
    cmd.receiveLen_length = receiveLen_length;
    cmd.usrID = (uint32_t *)offset;
    offset += usrID_length;
    cmd.usrID_length = usrID_length;

    f->writeAsync(&cmd, cmdSize);
    f->writeAsync(receiveLen, receiveLen_length);
    f->writeAsync(usrID, usrID_length);
    RsMessageToClientType retValue;
    f->writeWaitReturn(&retValue, sizeof(retValue));
    return retValue;
}

static void LF_ContextInitToClient (RsContext rsc)
{
    rsi_ContextInitToClient((Context *)rsc);
};

static void RF_ContextInitToClient (RsContext rsc)
{
    Fifo *f = NULL;
    RS_CMD_ContextInitToClient cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_ContextInitToClient;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;


    f->writeAsync(&cmd, cmdSize);
}

static void LF_ContextDeinitToClient (RsContext rsc)
{
    rsi_ContextDeinitToClient((Context *)rsc);
};

static void RF_ContextDeinitToClient (RsContext rsc)
{
    Fifo *f = NULL;
    RS_CMD_ContextDeinitToClient cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_ContextDeinitToClient;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;


    f->writeAsync(&cmd, cmdSize);
}

static RsType LF_TypeCreate (RsContext rsc, RsElement e, uint32_t dimX, uint32_t dimY, uint32_t dimZ, bool mips, bool faces)
{
    return rsi_TypeCreate((Context *)rsc, e, dimX, dimY, dimZ, mips, faces);
};

static RsType RF_TypeCreate (RsContext rsc, RsElement e, uint32_t dimX, uint32_t dimY, uint32_t dimZ, bool mips, bool faces)
{
    Fifo *f = NULL;
    RS_CMD_TypeCreate cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_TypeCreate;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;

    cmd.e = e;
    cmd.dimX = dimX;
    cmd.dimY = dimY;
    cmd.dimZ = dimZ;
    cmd.mips = mips;
    cmd.faces = faces;

    f->writeAsync(&cmd, cmdSize);
    RsType retValue;
    f->writeWaitReturn(&retValue, sizeof(retValue));
    return retValue;
}

static RsAllocation LF_AllocationCreateTyped (RsContext rsc, RsType vtype, RsAllocationMipmapControl mips, uint32_t usages)
{
    return rsi_AllocationCreateTyped((Context *)rsc, vtype, mips, usages);
};

static RsAllocation RF_AllocationCreateTyped (RsContext rsc, RsType vtype, RsAllocationMipmapControl mips, uint32_t usages)
{
    Fifo *f = NULL;
    RS_CMD_AllocationCreateTyped cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_AllocationCreateTyped;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;

    cmd.vtype = vtype;
    cmd.mips = mips;
    cmd.usages = usages;

    f->writeAsync(&cmd, cmdSize);
    RsAllocation retValue;
    f->writeWaitReturn(&retValue, sizeof(retValue));
    return retValue;
}

static RsAllocation LF_AllocationCreateFromBitmap (RsContext rsc, RsType vtype, RsAllocationMipmapControl mips, const void * data, size_t data_length, uint32_t usages)
{
    return rsi_AllocationCreateFromBitmap((Context *)rsc, vtype, mips, data, data_length, usages);
};

static RsAllocation RF_AllocationCreateFromBitmap (RsContext rsc, RsType vtype, RsAllocationMipmapControl mips, const void * data, size_t data_length, uint32_t usages)
{
    Fifo *f = NULL;
    RS_CMD_AllocationCreateFromBitmap cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_AllocationCreateFromBitmap;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;
    dataSize += data_length;

    cmd.vtype = vtype;
    cmd.mips = mips;
    cmd.data = (const void *)offset;
    offset += data_length;
    cmd.data_length = data_length;
    cmd.usages = usages;

    f->writeAsync(&cmd, cmdSize);
    f->writeAsync(data, data_length);
    RsAllocation retValue;
    f->writeWaitReturn(&retValue, sizeof(retValue));
    return retValue;
}

static RsAllocation LF_AllocationCubeCreateFromBitmap (RsContext rsc, RsType vtype, RsAllocationMipmapControl mips, const void * data, size_t data_length, uint32_t usages)
{
    return rsi_AllocationCubeCreateFromBitmap((Context *)rsc, vtype, mips, data, data_length, usages);
};

static RsAllocation RF_AllocationCubeCreateFromBitmap (RsContext rsc, RsType vtype, RsAllocationMipmapControl mips, const void * data, size_t data_length, uint32_t usages)
{
    Fifo *f = NULL;
    RS_CMD_AllocationCubeCreateFromBitmap cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_AllocationCubeCreateFromBitmap;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;
    dataSize += data_length;

    cmd.vtype = vtype;
    cmd.mips = mips;
    cmd.data = (const void *)offset;
    offset += data_length;
    cmd.data_length = data_length;
    cmd.usages = usages;

    f->writeAsync(&cmd, cmdSize);
    f->writeAsync(data, data_length);
    RsAllocation retValue;
    f->writeWaitReturn(&retValue, sizeof(retValue));
    return retValue;
}

static void LF_ContextFinish (RsContext rsc)
{
    ThreadIO *io = &((Context *)rsc)->mIO;
    const uint32_t size = sizeof(RS_CMD_ContextFinish);
    RS_CMD_ContextFinish *cmd = static_cast<RS_CMD_ContextFinish *>(io->coreHeader(RS_CMD_ID_ContextFinish, size));
    io->coreCommitSync();
};

static void RF_ContextFinish (RsContext rsc)
{
    Fifo *f = NULL;
    RS_CMD_ContextFinish cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_ContextFinish;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;


    f->writeAsync(&cmd, cmdSize);
}

static void LF_ContextBindRootScript (RsContext rsc, RsScript sampler)
{
    ThreadIO *io = &((Context *)rsc)->mIO;
    const uint32_t size = sizeof(RS_CMD_ContextBindRootScript);
    RS_CMD_ContextBindRootScript *cmd = static_cast<RS_CMD_ContextBindRootScript *>(io->coreHeader(RS_CMD_ID_ContextBindRootScript, size));
    cmd->sampler = sampler;
    io->coreCommit();
};

static void RF_ContextBindRootScript (RsContext rsc, RsScript sampler)
{
    Fifo *f = NULL;
    RS_CMD_ContextBindRootScript cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_ContextBindRootScript;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;

    cmd.sampler = sampler;

    f->writeAsync(&cmd, cmdSize);
}

static void LF_ContextBindProgramStore (RsContext rsc, RsProgramStore pgm)
{
    ThreadIO *io = &((Context *)rsc)->mIO;
    const uint32_t size = sizeof(RS_CMD_ContextBindProgramStore);
    RS_CMD_ContextBindProgramStore *cmd = static_cast<RS_CMD_ContextBindProgramStore *>(io->coreHeader(RS_CMD_ID_ContextBindProgramStore, size));
    cmd->pgm = pgm;
    io->coreCommit();
};

static void RF_ContextBindProgramStore (RsContext rsc, RsProgramStore pgm)
{
    Fifo *f = NULL;
    RS_CMD_ContextBindProgramStore cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_ContextBindProgramStore;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;

    cmd.pgm = pgm;

    f->writeAsync(&cmd, cmdSize);
}

static void LF_ContextBindProgramFragment (RsContext rsc, RsProgramFragment pgm)
{
    ThreadIO *io = &((Context *)rsc)->mIO;
    const uint32_t size = sizeof(RS_CMD_ContextBindProgramFragment);
    RS_CMD_ContextBindProgramFragment *cmd = static_cast<RS_CMD_ContextBindProgramFragment *>(io->coreHeader(RS_CMD_ID_ContextBindProgramFragment, size));
    cmd->pgm = pgm;
    io->coreCommit();
};

static void RF_ContextBindProgramFragment (RsContext rsc, RsProgramFragment pgm)
{
    Fifo *f = NULL;
    RS_CMD_ContextBindProgramFragment cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_ContextBindProgramFragment;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;

    cmd.pgm = pgm;

    f->writeAsync(&cmd, cmdSize);
}

static void LF_ContextBindProgramVertex (RsContext rsc, RsProgramVertex pgm)
{
    ThreadIO *io = &((Context *)rsc)->mIO;
    const uint32_t size = sizeof(RS_CMD_ContextBindProgramVertex);
    RS_CMD_ContextBindProgramVertex *cmd = static_cast<RS_CMD_ContextBindProgramVertex *>(io->coreHeader(RS_CMD_ID_ContextBindProgramVertex, size));
    cmd->pgm = pgm;
    io->coreCommit();
};

static void RF_ContextBindProgramVertex (RsContext rsc, RsProgramVertex pgm)
{
    Fifo *f = NULL;
    RS_CMD_ContextBindProgramVertex cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_ContextBindProgramVertex;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;

    cmd.pgm = pgm;

    f->writeAsync(&cmd, cmdSize);
}

static void LF_ContextBindProgramRaster (RsContext rsc, RsProgramRaster pgm)
{
    ThreadIO *io = &((Context *)rsc)->mIO;
    const uint32_t size = sizeof(RS_CMD_ContextBindProgramRaster);
    RS_CMD_ContextBindProgramRaster *cmd = static_cast<RS_CMD_ContextBindProgramRaster *>(io->coreHeader(RS_CMD_ID_ContextBindProgramRaster, size));
    cmd->pgm = pgm;
    io->coreCommit();
};

static void RF_ContextBindProgramRaster (RsContext rsc, RsProgramRaster pgm)
{
    Fifo *f = NULL;
    RS_CMD_ContextBindProgramRaster cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_ContextBindProgramRaster;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;

    cmd.pgm = pgm;

    f->writeAsync(&cmd, cmdSize);
}

static void LF_ContextBindFont (RsContext rsc, RsFont pgm)
{
    ThreadIO *io = &((Context *)rsc)->mIO;
    const uint32_t size = sizeof(RS_CMD_ContextBindFont);
    RS_CMD_ContextBindFont *cmd = static_cast<RS_CMD_ContextBindFont *>(io->coreHeader(RS_CMD_ID_ContextBindFont, size));
    cmd->pgm = pgm;
    io->coreCommit();
};

static void RF_ContextBindFont (RsContext rsc, RsFont pgm)
{
    Fifo *f = NULL;
    RS_CMD_ContextBindFont cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_ContextBindFont;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;

    cmd.pgm = pgm;

    f->writeAsync(&cmd, cmdSize);
}

static void LF_ContextPause (RsContext rsc)
{
    ThreadIO *io = &((Context *)rsc)->mIO;
    const uint32_t size = sizeof(RS_CMD_ContextPause);
    RS_CMD_ContextPause *cmd = static_cast<RS_CMD_ContextPause *>(io->coreHeader(RS_CMD_ID_ContextPause, size));
    io->coreCommit();
};

static void RF_ContextPause (RsContext rsc)
{
    Fifo *f = NULL;
    RS_CMD_ContextPause cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_ContextPause;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;


    f->writeAsync(&cmd, cmdSize);
}

static void LF_ContextResume (RsContext rsc)
{
    ThreadIO *io = &((Context *)rsc)->mIO;
    const uint32_t size = sizeof(RS_CMD_ContextResume);
    RS_CMD_ContextResume *cmd = static_cast<RS_CMD_ContextResume *>(io->coreHeader(RS_CMD_ID_ContextResume, size));
    io->coreCommit();
};

static void RF_ContextResume (RsContext rsc)
{
    Fifo *f = NULL;
    RS_CMD_ContextResume cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_ContextResume;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;


    f->writeAsync(&cmd, cmdSize);
}

static void LF_ContextSetSurface (RsContext rsc, uint32_t width, uint32_t height, RsNativeWindow sur)
{
    ThreadIO *io = &((Context *)rsc)->mIO;
    const uint32_t size = sizeof(RS_CMD_ContextSetSurface);
    RS_CMD_ContextSetSurface *cmd = static_cast<RS_CMD_ContextSetSurface *>(io->coreHeader(RS_CMD_ID_ContextSetSurface, size));
    cmd->width = width;
    cmd->height = height;
    cmd->sur = sur;
    io->coreCommitSync();
};

static void RF_ContextSetSurface (RsContext rsc, uint32_t width, uint32_t height, RsNativeWindow sur)
{
    Fifo *f = NULL;
    RS_CMD_ContextSetSurface cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_ContextSetSurface;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;

    cmd.width = width;
    cmd.height = height;
    cmd.sur = sur;

    f->writeAsync(&cmd, cmdSize);
}

static void LF_ContextDump (RsContext rsc, int32_t bits)
{
    ThreadIO *io = &((Context *)rsc)->mIO;
    const uint32_t size = sizeof(RS_CMD_ContextDump);
    RS_CMD_ContextDump *cmd = static_cast<RS_CMD_ContextDump *>(io->coreHeader(RS_CMD_ID_ContextDump, size));
    cmd->bits = bits;
    io->coreCommit();
};

static void RF_ContextDump (RsContext rsc, int32_t bits)
{
    Fifo *f = NULL;
    RS_CMD_ContextDump cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_ContextDump;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;

    cmd.bits = bits;

    f->writeAsync(&cmd, cmdSize);
}

static void LF_ContextSetPriority (RsContext rsc, int32_t priority)
{
    ThreadIO *io = &((Context *)rsc)->mIO;
    const uint32_t size = sizeof(RS_CMD_ContextSetPriority);
    RS_CMD_ContextSetPriority *cmd = static_cast<RS_CMD_ContextSetPriority *>(io->coreHeader(RS_CMD_ID_ContextSetPriority, size));
    cmd->priority = priority;
    io->coreCommit();
};

static void RF_ContextSetPriority (RsContext rsc, int32_t priority)
{
    Fifo *f = NULL;
    RS_CMD_ContextSetPriority cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_ContextSetPriority;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;

    cmd.priority = priority;

    f->writeAsync(&cmd, cmdSize);
}

static void LF_ContextDestroyWorker (RsContext rsc)
{
    ThreadIO *io = &((Context *)rsc)->mIO;
    const uint32_t size = sizeof(RS_CMD_ContextDestroyWorker);
    RS_CMD_ContextDestroyWorker *cmd = static_cast<RS_CMD_ContextDestroyWorker *>(io->coreHeader(RS_CMD_ID_ContextDestroyWorker, size));
    io->coreCommit();
};

static void RF_ContextDestroyWorker (RsContext rsc)
{
    Fifo *f = NULL;
    RS_CMD_ContextDestroyWorker cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_ContextDestroyWorker;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;


    f->writeAsync(&cmd, cmdSize);
}

static void LF_AssignName (RsContext rsc, RsObjectBase obj, const char * name, size_t name_length)
{
    ThreadIO *io = &((Context *)rsc)->mIO;
    const uint32_t size = sizeof(RS_CMD_AssignName);
    uint32_t dataSize = 0;
    dataSize += name_length;
    RS_CMD_AssignName *cmd = NULL;
    if (dataSize < 1024) {;
        cmd = static_cast<RS_CMD_AssignName *>(io->coreHeader(RS_CMD_ID_AssignName, dataSize + size));
    } else {
        cmd = static_cast<RS_CMD_AssignName *>(io->coreHeader(RS_CMD_ID_AssignName, size));
    }
    uint8_t *payload = (uint8_t *)&cmd[1];
    cmd->obj = obj;
    if (dataSize < 1024) {
        memcpy(payload, name, name_length);
        cmd->name = (const char *)payload;
        payload += name_length;
    } else {
        cmd->name = name;
    }
    cmd->name_length = name_length;
    if (dataSize < 1024) {
        io->coreCommit();
    } else {
        io->coreCommitSync();
    }
};

static void RF_AssignName (RsContext rsc, RsObjectBase obj, const char * name, size_t name_length)
{
    Fifo *f = NULL;
    RS_CMD_AssignName cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_AssignName;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;
    dataSize += name_length;

    cmd.obj = obj;
    cmd.name = (const char *)offset;
    offset += name_length;
    cmd.name_length = name_length;

    f->writeAsync(&cmd, cmdSize);
    f->writeAsync(name, name_length);
}

static void LF_ObjDestroy (RsContext rsc, RsAsyncVoidPtr objPtr)
{
    ThreadIO *io = &((Context *)rsc)->mIO;
    const uint32_t size = sizeof(RS_CMD_ObjDestroy);
    RS_CMD_ObjDestroy *cmd = static_cast<RS_CMD_ObjDestroy *>(io->coreHeader(RS_CMD_ID_ObjDestroy, size));
    cmd->objPtr = objPtr;
    io->coreCommit();
};

static void RF_ObjDestroy (RsContext rsc, RsAsyncVoidPtr objPtr)
{
    Fifo *f = NULL;
    RS_CMD_ObjDestroy cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_ObjDestroy;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;

    cmd.objPtr = objPtr;

    f->writeAsync(&cmd, cmdSize);
}

static RsElement LF_ElementCreate (RsContext rsc, RsDataType mType, RsDataKind mKind, bool mNormalized, uint32_t mVectorSize)
{
    return rsi_ElementCreate((Context *)rsc, mType, mKind, mNormalized, mVectorSize);
};

static RsElement RF_ElementCreate (RsContext rsc, RsDataType mType, RsDataKind mKind, bool mNormalized, uint32_t mVectorSize)
{
    Fifo *f = NULL;
    RS_CMD_ElementCreate cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_ElementCreate;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;

    cmd.mType = mType;
    cmd.mKind = mKind;
    cmd.mNormalized = mNormalized;
    cmd.mVectorSize = mVectorSize;

    f->writeAsync(&cmd, cmdSize);
    RsElement retValue;
    f->writeWaitReturn(&retValue, sizeof(retValue));
    return retValue;
}

static RsElement LF_ElementCreate2 (RsContext rsc, const RsElement * elements, size_t elements_length, const char ** names, size_t names_length_length, const size_t * names_length, const uint32_t * arraySize, size_t arraySize_length)
{
    return rsi_ElementCreate2((Context *)rsc, elements, elements_length, names, names_length_length, names_length, arraySize, arraySize_length);
};

static RsElement RF_ElementCreate2 (RsContext rsc, const RsElement * elements, size_t elements_length, const char ** names, size_t names_length_length, const size_t * names_length, const uint32_t * arraySize, size_t arraySize_length)
{
    Fifo *f = NULL;
    RS_CMD_ElementCreate2 cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_ElementCreate2;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;
    dataSize += elements_length;
    for (size_t ct = 0; ct < (names_length_length / sizeof(names_length)); ct++) {
        dataSize += names_length[ct];
    }
    dataSize += names_length_length;
    dataSize += arraySize_length;

    cmd.elements = (const RsElement *)offset;
    offset += elements_length;
    cmd.elements_length = elements_length;
    cmd.names = (const char **)offset;
    for (size_t ct = 0; ct < (names_length_length / sizeof(names_length)); ct++) {
        offset += names_length[ct];
    }
    cmd.names_length_length = names_length_length;
    cmd.names_length = (const size_t *)offset;
    offset += names_length_length;
    cmd.arraySize = (const uint32_t *)offset;
    offset += arraySize_length;
    cmd.arraySize_length = arraySize_length;

    f->writeAsync(&cmd, cmdSize);
    f->writeAsync(elements, elements_length);
    for (size_t ct = 0; ct < (names_length_length / sizeof(names_length)); ct++) {
        f->writeAsync(names, names_length[ct]);
        offset += names_length[ct];
    }
    f->writeAsync(names_length, names_length_length);
    f->writeAsync(arraySize, arraySize_length);
    RsElement retValue;
    f->writeWaitReturn(&retValue, sizeof(retValue));
    return retValue;
}

static void LF_AllocationCopyToBitmap (RsContext rsc, RsAllocation alloc, void * data, size_t data_length)
{
    ThreadIO *io = &((Context *)rsc)->mIO;
    const uint32_t size = sizeof(RS_CMD_AllocationCopyToBitmap);
    RS_CMD_AllocationCopyToBitmap *cmd = static_cast<RS_CMD_AllocationCopyToBitmap *>(io->coreHeader(RS_CMD_ID_AllocationCopyToBitmap, size));
    cmd->alloc = alloc;
    cmd->data = data;
    cmd->data_length = data_length;
    io->coreCommitSync();
};

static void RF_AllocationCopyToBitmap (RsContext rsc, RsAllocation alloc, void * data, size_t data_length)
{
    Fifo *f = NULL;
    RS_CMD_AllocationCopyToBitmap cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_AllocationCopyToBitmap;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;

    cmd.alloc = alloc;
    cmd.data = (void *)offset;
    offset += data_length;
    cmd.data_length = data_length;

    f->writeAsync(&cmd, cmdSize);
    f->writeAsync(data, data_length);
}

static void LF_Allocation1DData (RsContext rsc, RsAllocation va, uint32_t xoff, uint32_t lod, uint32_t count, const void * data, size_t data_length)
{
    ThreadIO *io = &((Context *)rsc)->mIO;
    const uint32_t size = sizeof(RS_CMD_Allocation1DData);
    uint32_t dataSize = 0;
    dataSize += data_length;
    RS_CMD_Allocation1DData *cmd = NULL;
    if (dataSize < 1024) {;
        cmd = static_cast<RS_CMD_Allocation1DData *>(io->coreHeader(RS_CMD_ID_Allocation1DData, dataSize + size));
    } else {
        cmd = static_cast<RS_CMD_Allocation1DData *>(io->coreHeader(RS_CMD_ID_Allocation1DData, size));
    }
    uint8_t *payload = (uint8_t *)&cmd[1];
    cmd->va = va;
    cmd->xoff = xoff;
    cmd->lod = lod;
    cmd->count = count;
    if (dataSize < 1024) {
        memcpy(payload, data, data_length);
        cmd->data = (const void *)payload;
        payload += data_length;
    } else {
        cmd->data = data;
    }
    cmd->data_length = data_length;
    if (dataSize < 1024) {
        io->coreCommit();
    } else {
        io->coreCommitSync();
    }
};

static void RF_Allocation1DData (RsContext rsc, RsAllocation va, uint32_t xoff, uint32_t lod, uint32_t count, const void * data, size_t data_length)
{
    Fifo *f = NULL;
    RS_CMD_Allocation1DData cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_Allocation1DData;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;
    dataSize += data_length;

    cmd.va = va;
    cmd.xoff = xoff;
    cmd.lod = lod;
    cmd.count = count;
    cmd.data = (const void *)offset;
    offset += data_length;
    cmd.data_length = data_length;

    f->writeAsync(&cmd, cmdSize);
    f->writeAsync(data, data_length);
}

static void LF_Allocation1DElementData (RsContext rsc, RsAllocation va, uint32_t x, uint32_t lod, const void * data, size_t data_length, uint32_t comp_offset)
{
    ThreadIO *io = &((Context *)rsc)->mIO;
    const uint32_t size = sizeof(RS_CMD_Allocation1DElementData);
    uint32_t dataSize = 0;
    dataSize += data_length;
    RS_CMD_Allocation1DElementData *cmd = NULL;
    if (dataSize < 1024) {;
        cmd = static_cast<RS_CMD_Allocation1DElementData *>(io->coreHeader(RS_CMD_ID_Allocation1DElementData, dataSize + size));
    } else {
        cmd = static_cast<RS_CMD_Allocation1DElementData *>(io->coreHeader(RS_CMD_ID_Allocation1DElementData, size));
    }
    uint8_t *payload = (uint8_t *)&cmd[1];
    cmd->va = va;
    cmd->x = x;
    cmd->lod = lod;
    if (dataSize < 1024) {
        memcpy(payload, data, data_length);
        cmd->data = (const void *)payload;
        payload += data_length;
    } else {
        cmd->data = data;
    }
    cmd->data_length = data_length;
    cmd->comp_offset = comp_offset;
    if (dataSize < 1024) {
        io->coreCommit();
    } else {
        io->coreCommitSync();
    }
};

static void RF_Allocation1DElementData (RsContext rsc, RsAllocation va, uint32_t x, uint32_t lod, const void * data, size_t data_length, uint32_t comp_offset)
{
    Fifo *f = NULL;
    RS_CMD_Allocation1DElementData cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_Allocation1DElementData;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;
    dataSize += data_length;

    cmd.va = va;
    cmd.x = x;
    cmd.lod = lod;
    cmd.data = (const void *)offset;
    offset += data_length;
    cmd.data_length = data_length;
    cmd.comp_offset = comp_offset;

    f->writeAsync(&cmd, cmdSize);
    f->writeAsync(data, data_length);
}

static void LF_Allocation2DData (RsContext rsc, RsAllocation va, uint32_t xoff, uint32_t yoff, uint32_t lod, RsAllocationCubemapFace face, uint32_t w, uint32_t h, const void * data, size_t data_length)
{
    ThreadIO *io = &((Context *)rsc)->mIO;
    const uint32_t size = sizeof(RS_CMD_Allocation2DData);
    uint32_t dataSize = 0;
    dataSize += data_length;
    RS_CMD_Allocation2DData *cmd = NULL;
    if (dataSize < 1024) {;
        cmd = static_cast<RS_CMD_Allocation2DData *>(io->coreHeader(RS_CMD_ID_Allocation2DData, dataSize + size));
    } else {
        cmd = static_cast<RS_CMD_Allocation2DData *>(io->coreHeader(RS_CMD_ID_Allocation2DData, size));
    }
    uint8_t *payload = (uint8_t *)&cmd[1];
    cmd->va = va;
    cmd->xoff = xoff;
    cmd->yoff = yoff;
    cmd->lod = lod;
    cmd->face = face;
    cmd->w = w;
    cmd->h = h;
    if (dataSize < 1024) {
        memcpy(payload, data, data_length);
        cmd->data = (const void *)payload;
        payload += data_length;
    } else {
        cmd->data = data;
    }
    cmd->data_length = data_length;
    if (dataSize < 1024) {
        io->coreCommit();
    } else {
        io->coreCommitSync();
    }
};

static void RF_Allocation2DData (RsContext rsc, RsAllocation va, uint32_t xoff, uint32_t yoff, uint32_t lod, RsAllocationCubemapFace face, uint32_t w, uint32_t h, const void * data, size_t data_length)
{
    Fifo *f = NULL;
    RS_CMD_Allocation2DData cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_Allocation2DData;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;
    dataSize += data_length;

    cmd.va = va;
    cmd.xoff = xoff;
    cmd.yoff = yoff;
    cmd.lod = lod;
    cmd.face = face;
    cmd.w = w;
    cmd.h = h;
    cmd.data = (const void *)offset;
    offset += data_length;
    cmd.data_length = data_length;

    f->writeAsync(&cmd, cmdSize);
    f->writeAsync(data, data_length);
}

static void LF_Allocation2DElementData (RsContext rsc, RsAllocation va, uint32_t x, uint32_t y, uint32_t lod, RsAllocationCubemapFace face, const void * data, size_t data_length, uint32_t element_offset)
{
    ThreadIO *io = &((Context *)rsc)->mIO;
    const uint32_t size = sizeof(RS_CMD_Allocation2DElementData);
    uint32_t dataSize = 0;
    dataSize += data_length;
    RS_CMD_Allocation2DElementData *cmd = NULL;
    if (dataSize < 1024) {;
        cmd = static_cast<RS_CMD_Allocation2DElementData *>(io->coreHeader(RS_CMD_ID_Allocation2DElementData, dataSize + size));
    } else {
        cmd = static_cast<RS_CMD_Allocation2DElementData *>(io->coreHeader(RS_CMD_ID_Allocation2DElementData, size));
    }
    uint8_t *payload = (uint8_t *)&cmd[1];
    cmd->va = va;
    cmd->x = x;
    cmd->y = y;
    cmd->lod = lod;
    cmd->face = face;
    if (dataSize < 1024) {
        memcpy(payload, data, data_length);
        cmd->data = (const void *)payload;
        payload += data_length;
    } else {
        cmd->data = data;
    }
    cmd->data_length = data_length;
    cmd->element_offset = element_offset;
    if (dataSize < 1024) {
        io->coreCommit();
    } else {
        io->coreCommitSync();
    }
};

static void RF_Allocation2DElementData (RsContext rsc, RsAllocation va, uint32_t x, uint32_t y, uint32_t lod, RsAllocationCubemapFace face, const void * data, size_t data_length, uint32_t element_offset)
{
    Fifo *f = NULL;
    RS_CMD_Allocation2DElementData cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_Allocation2DElementData;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;
    dataSize += data_length;

    cmd.va = va;
    cmd.x = x;
    cmd.y = y;
    cmd.lod = lod;
    cmd.face = face;
    cmd.data = (const void *)offset;
    offset += data_length;
    cmd.data_length = data_length;
    cmd.element_offset = element_offset;

    f->writeAsync(&cmd, cmdSize);
    f->writeAsync(data, data_length);
}

static void LF_AllocationGenerateMipmaps (RsContext rsc, RsAllocation va)
{
    ThreadIO *io = &((Context *)rsc)->mIO;
    const uint32_t size = sizeof(RS_CMD_AllocationGenerateMipmaps);
    RS_CMD_AllocationGenerateMipmaps *cmd = static_cast<RS_CMD_AllocationGenerateMipmaps *>(io->coreHeader(RS_CMD_ID_AllocationGenerateMipmaps, size));
    cmd->va = va;
    io->coreCommit();
};

static void RF_AllocationGenerateMipmaps (RsContext rsc, RsAllocation va)
{
    Fifo *f = NULL;
    RS_CMD_AllocationGenerateMipmaps cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_AllocationGenerateMipmaps;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;

    cmd.va = va;

    f->writeAsync(&cmd, cmdSize);
}

static void LF_AllocationRead (RsContext rsc, RsAllocation va, void * data, size_t data_length)
{
    ThreadIO *io = &((Context *)rsc)->mIO;
    const uint32_t size = sizeof(RS_CMD_AllocationRead);
    RS_CMD_AllocationRead *cmd = static_cast<RS_CMD_AllocationRead *>(io->coreHeader(RS_CMD_ID_AllocationRead, size));
    cmd->va = va;
    cmd->data = data;
    cmd->data_length = data_length;
    io->coreCommitSync();
};

static void RF_AllocationRead (RsContext rsc, RsAllocation va, void * data, size_t data_length)
{
    Fifo *f = NULL;
    RS_CMD_AllocationRead cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_AllocationRead;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;

    cmd.va = va;
    cmd.data = (void *)offset;
    offset += data_length;
    cmd.data_length = data_length;

    f->writeAsync(&cmd, cmdSize);
    f->writeAsync(data, data_length);
}

static void LF_AllocationSyncAll (RsContext rsc, RsAllocation va, RsAllocationUsageType src)
{
    ThreadIO *io = &((Context *)rsc)->mIO;
    const uint32_t size = sizeof(RS_CMD_AllocationSyncAll);
    RS_CMD_AllocationSyncAll *cmd = static_cast<RS_CMD_AllocationSyncAll *>(io->coreHeader(RS_CMD_ID_AllocationSyncAll, size));
    cmd->va = va;
    cmd->src = src;
    io->coreCommit();
};

static void RF_AllocationSyncAll (RsContext rsc, RsAllocation va, RsAllocationUsageType src)
{
    Fifo *f = NULL;
    RS_CMD_AllocationSyncAll cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_AllocationSyncAll;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;

    cmd.va = va;
    cmd.src = src;

    f->writeAsync(&cmd, cmdSize);
}

static void LF_AllocationResize1D (RsContext rsc, RsAllocation va, uint32_t dimX)
{
    ThreadIO *io = &((Context *)rsc)->mIO;
    const uint32_t size = sizeof(RS_CMD_AllocationResize1D);
    RS_CMD_AllocationResize1D *cmd = static_cast<RS_CMD_AllocationResize1D *>(io->coreHeader(RS_CMD_ID_AllocationResize1D, size));
    cmd->va = va;
    cmd->dimX = dimX;
    io->coreCommit();
};

static void RF_AllocationResize1D (RsContext rsc, RsAllocation va, uint32_t dimX)
{
    Fifo *f = NULL;
    RS_CMD_AllocationResize1D cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_AllocationResize1D;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;

    cmd.va = va;
    cmd.dimX = dimX;

    f->writeAsync(&cmd, cmdSize);
}

static void LF_AllocationResize2D (RsContext rsc, RsAllocation va, uint32_t dimX, uint32_t dimY)
{
    ThreadIO *io = &((Context *)rsc)->mIO;
    const uint32_t size = sizeof(RS_CMD_AllocationResize2D);
    RS_CMD_AllocationResize2D *cmd = static_cast<RS_CMD_AllocationResize2D *>(io->coreHeader(RS_CMD_ID_AllocationResize2D, size));
    cmd->va = va;
    cmd->dimX = dimX;
    cmd->dimY = dimY;
    io->coreCommit();
};

static void RF_AllocationResize2D (RsContext rsc, RsAllocation va, uint32_t dimX, uint32_t dimY)
{
    Fifo *f = NULL;
    RS_CMD_AllocationResize2D cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_AllocationResize2D;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;

    cmd.va = va;
    cmd.dimX = dimX;
    cmd.dimY = dimY;

    f->writeAsync(&cmd, cmdSize);
}

static void LF_AllocationCopy2DRange (RsContext rsc, RsAllocation dest, uint32_t destXoff, uint32_t destYoff, uint32_t destMip, uint32_t destFace, uint32_t width, uint32_t height, RsAllocation src, uint32_t srcXoff, uint32_t srcYoff, uint32_t srcMip, uint32_t srcFace)
{
    ThreadIO *io = &((Context *)rsc)->mIO;
    const uint32_t size = sizeof(RS_CMD_AllocationCopy2DRange);
    RS_CMD_AllocationCopy2DRange *cmd = static_cast<RS_CMD_AllocationCopy2DRange *>(io->coreHeader(RS_CMD_ID_AllocationCopy2DRange, size));
    cmd->dest = dest;
    cmd->destXoff = destXoff;
    cmd->destYoff = destYoff;
    cmd->destMip = destMip;
    cmd->destFace = destFace;
    cmd->width = width;
    cmd->height = height;
    cmd->src = src;
    cmd->srcXoff = srcXoff;
    cmd->srcYoff = srcYoff;
    cmd->srcMip = srcMip;
    cmd->srcFace = srcFace;
    io->coreCommit();
};

static void RF_AllocationCopy2DRange (RsContext rsc, RsAllocation dest, uint32_t destXoff, uint32_t destYoff, uint32_t destMip, uint32_t destFace, uint32_t width, uint32_t height, RsAllocation src, uint32_t srcXoff, uint32_t srcYoff, uint32_t srcMip, uint32_t srcFace)
{
    Fifo *f = NULL;
    RS_CMD_AllocationCopy2DRange cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_AllocationCopy2DRange;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;

    cmd.dest = dest;
    cmd.destXoff = destXoff;
    cmd.destYoff = destYoff;
    cmd.destMip = destMip;
    cmd.destFace = destFace;
    cmd.width = width;
    cmd.height = height;
    cmd.src = src;
    cmd.srcXoff = srcXoff;
    cmd.srcYoff = srcYoff;
    cmd.srcMip = srcMip;
    cmd.srcFace = srcFace;

    f->writeAsync(&cmd, cmdSize);
}

static RsSampler LF_SamplerCreate (RsContext rsc, RsSamplerValue magFilter, RsSamplerValue minFilter, RsSamplerValue wrapS, RsSamplerValue wrapT, RsSamplerValue wrapR, float mAniso)
{
    return rsi_SamplerCreate((Context *)rsc, magFilter, minFilter, wrapS, wrapT, wrapR, mAniso);
};

static RsSampler RF_SamplerCreate (RsContext rsc, RsSamplerValue magFilter, RsSamplerValue minFilter, RsSamplerValue wrapS, RsSamplerValue wrapT, RsSamplerValue wrapR, float mAniso)
{
    Fifo *f = NULL;
    RS_CMD_SamplerCreate cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_SamplerCreate;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;

    cmd.magFilter = magFilter;
    cmd.minFilter = minFilter;
    cmd.wrapS = wrapS;
    cmd.wrapT = wrapT;
    cmd.wrapR = wrapR;
    cmd.mAniso = mAniso;

    f->writeAsync(&cmd, cmdSize);
    RsSampler retValue;
    f->writeWaitReturn(&retValue, sizeof(retValue));
    return retValue;
}

static void LF_ScriptBindAllocation (RsContext rsc, RsScript vtm, RsAllocation va, uint32_t slot)
{
    ThreadIO *io = &((Context *)rsc)->mIO;
    const uint32_t size = sizeof(RS_CMD_ScriptBindAllocation);
    RS_CMD_ScriptBindAllocation *cmd = static_cast<RS_CMD_ScriptBindAllocation *>(io->coreHeader(RS_CMD_ID_ScriptBindAllocation, size));
    cmd->vtm = vtm;
    cmd->va = va;
    cmd->slot = slot;
    io->coreCommit();
};

static void RF_ScriptBindAllocation (RsContext rsc, RsScript vtm, RsAllocation va, uint32_t slot)
{
    Fifo *f = NULL;
    RS_CMD_ScriptBindAllocation cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_ScriptBindAllocation;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;

    cmd.vtm = vtm;
    cmd.va = va;
    cmd.slot = slot;

    f->writeAsync(&cmd, cmdSize);
}

static void LF_ScriptSetTimeZone (RsContext rsc, RsScript s, const char * timeZone, size_t timeZone_length)
{
    ThreadIO *io = &((Context *)rsc)->mIO;
    const uint32_t size = sizeof(RS_CMD_ScriptSetTimeZone);
    uint32_t dataSize = 0;
    dataSize += timeZone_length;
    RS_CMD_ScriptSetTimeZone *cmd = NULL;
    if (dataSize < 1024) {;
        cmd = static_cast<RS_CMD_ScriptSetTimeZone *>(io->coreHeader(RS_CMD_ID_ScriptSetTimeZone, dataSize + size));
    } else {
        cmd = static_cast<RS_CMD_ScriptSetTimeZone *>(io->coreHeader(RS_CMD_ID_ScriptSetTimeZone, size));
    }
    uint8_t *payload = (uint8_t *)&cmd[1];
    cmd->s = s;
    if (dataSize < 1024) {
        memcpy(payload, timeZone, timeZone_length);
        cmd->timeZone = (const char *)payload;
        payload += timeZone_length;
    } else {
        cmd->timeZone = timeZone;
    }
    cmd->timeZone_length = timeZone_length;
    if (dataSize < 1024) {
        io->coreCommit();
    } else {
        io->coreCommitSync();
    }
};

static void RF_ScriptSetTimeZone (RsContext rsc, RsScript s, const char * timeZone, size_t timeZone_length)
{
    Fifo *f = NULL;
    RS_CMD_ScriptSetTimeZone cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_ScriptSetTimeZone;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;
    dataSize += timeZone_length;

    cmd.s = s;
    cmd.timeZone = (const char *)offset;
    offset += timeZone_length;
    cmd.timeZone_length = timeZone_length;

    f->writeAsync(&cmd, cmdSize);
    f->writeAsync(timeZone, timeZone_length);
}

static void LF_ScriptInvoke (RsContext rsc, RsScript s, uint32_t slot)
{
    ThreadIO *io = &((Context *)rsc)->mIO;
    const uint32_t size = sizeof(RS_CMD_ScriptInvoke);
    RS_CMD_ScriptInvoke *cmd = static_cast<RS_CMD_ScriptInvoke *>(io->coreHeader(RS_CMD_ID_ScriptInvoke, size));
    cmd->s = s;
    cmd->slot = slot;
    io->coreCommit();
};

static void RF_ScriptInvoke (RsContext rsc, RsScript s, uint32_t slot)
{
    Fifo *f = NULL;
    RS_CMD_ScriptInvoke cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_ScriptInvoke;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;

    cmd.s = s;
    cmd.slot = slot;

    f->writeAsync(&cmd, cmdSize);
}

static void LF_ScriptInvokeV (RsContext rsc, RsScript s, uint32_t slot, const void * data, size_t data_length)
{
    ThreadIO *io = &((Context *)rsc)->mIO;
    const uint32_t size = sizeof(RS_CMD_ScriptInvokeV);
    uint32_t dataSize = 0;
    dataSize += data_length;
    RS_CMD_ScriptInvokeV *cmd = NULL;
    if (dataSize < 1024) {;
        cmd = static_cast<RS_CMD_ScriptInvokeV *>(io->coreHeader(RS_CMD_ID_ScriptInvokeV, dataSize + size));
    } else {
        cmd = static_cast<RS_CMD_ScriptInvokeV *>(io->coreHeader(RS_CMD_ID_ScriptInvokeV, size));
    }
    uint8_t *payload = (uint8_t *)&cmd[1];
    cmd->s = s;
    cmd->slot = slot;
    if (dataSize < 1024) {
        memcpy(payload, data, data_length);
        cmd->data = (const void *)payload;
        payload += data_length;
    } else {
        cmd->data = data;
    }
    cmd->data_length = data_length;
    if (dataSize < 1024) {
        io->coreCommit();
    } else {
        io->coreCommitSync();
    }
};

static void RF_ScriptInvokeV (RsContext rsc, RsScript s, uint32_t slot, const void * data, size_t data_length)
{
    Fifo *f = NULL;
    RS_CMD_ScriptInvokeV cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_ScriptInvokeV;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;
    dataSize += data_length;

    cmd.s = s;
    cmd.slot = slot;
    cmd.data = (const void *)offset;
    offset += data_length;
    cmd.data_length = data_length;

    f->writeAsync(&cmd, cmdSize);
    f->writeAsync(data, data_length);
}

static void LF_ScriptForEach (RsContext rsc, RsScript s, uint32_t slot, RsAllocation ain, RsAllocation aout, const void * usr, size_t usr_length)
{
    ThreadIO *io = &((Context *)rsc)->mIO;
    const uint32_t size = sizeof(RS_CMD_ScriptForEach);
    uint32_t dataSize = 0;
    dataSize += usr_length;
    RS_CMD_ScriptForEach *cmd = NULL;
    if (dataSize < 1024) {;
        cmd = static_cast<RS_CMD_ScriptForEach *>(io->coreHeader(RS_CMD_ID_ScriptForEach, dataSize + size));
    } else {
        cmd = static_cast<RS_CMD_ScriptForEach *>(io->coreHeader(RS_CMD_ID_ScriptForEach, size));
    }
    uint8_t *payload = (uint8_t *)&cmd[1];
    cmd->s = s;
    cmd->slot = slot;
    cmd->ain = ain;
    cmd->aout = aout;
    if (dataSize < 1024) {
        memcpy(payload, usr, usr_length);
        cmd->usr = (const void *)payload;
        payload += usr_length;
    } else {
        cmd->usr = usr;
    }
    cmd->usr_length = usr_length;
    if (dataSize < 1024) {
        io->coreCommit();
    } else {
        io->coreCommitSync();
    }
};

static void RF_ScriptForEach (RsContext rsc, RsScript s, uint32_t slot, RsAllocation ain, RsAllocation aout, const void * usr, size_t usr_length)
{
    Fifo *f = NULL;
    RS_CMD_ScriptForEach cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_ScriptForEach;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;
    dataSize += usr_length;

    cmd.s = s;
    cmd.slot = slot;
    cmd.ain = ain;
    cmd.aout = aout;
    cmd.usr = (const void *)offset;
    offset += usr_length;
    cmd.usr_length = usr_length;

    f->writeAsync(&cmd, cmdSize);
    f->writeAsync(usr, usr_length);
}

static void LF_ScriptSetVarI (RsContext rsc, RsScript s, uint32_t slot, int value)
{
    ThreadIO *io = &((Context *)rsc)->mIO;
    const uint32_t size = sizeof(RS_CMD_ScriptSetVarI);
    RS_CMD_ScriptSetVarI *cmd = static_cast<RS_CMD_ScriptSetVarI *>(io->coreHeader(RS_CMD_ID_ScriptSetVarI, size));
    cmd->s = s;
    cmd->slot = slot;
    cmd->value = value;
    io->coreCommit();
};

static void RF_ScriptSetVarI (RsContext rsc, RsScript s, uint32_t slot, int value)
{
    Fifo *f = NULL;
    RS_CMD_ScriptSetVarI cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_ScriptSetVarI;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;

    cmd.s = s;
    cmd.slot = slot;
    cmd.value = value;

    f->writeAsync(&cmd, cmdSize);
}

static void LF_ScriptSetVarObj (RsContext rsc, RsScript s, uint32_t slot, RsObjectBase value)
{
    ThreadIO *io = &((Context *)rsc)->mIO;
    const uint32_t size = sizeof(RS_CMD_ScriptSetVarObj);
    RS_CMD_ScriptSetVarObj *cmd = static_cast<RS_CMD_ScriptSetVarObj *>(io->coreHeader(RS_CMD_ID_ScriptSetVarObj, size));
    cmd->s = s;
    cmd->slot = slot;
    cmd->value = value;
    io->coreCommit();
};

static void RF_ScriptSetVarObj (RsContext rsc, RsScript s, uint32_t slot, RsObjectBase value)
{
    Fifo *f = NULL;
    RS_CMD_ScriptSetVarObj cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_ScriptSetVarObj;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;

    cmd.s = s;
    cmd.slot = slot;
    cmd.value = value;

    f->writeAsync(&cmd, cmdSize);
}

static void LF_ScriptSetVarJ (RsContext rsc, RsScript s, uint32_t slot, int64_t value)
{
    ThreadIO *io = &((Context *)rsc)->mIO;
    const uint32_t size = sizeof(RS_CMD_ScriptSetVarJ);
    RS_CMD_ScriptSetVarJ *cmd = static_cast<RS_CMD_ScriptSetVarJ *>(io->coreHeader(RS_CMD_ID_ScriptSetVarJ, size));
    cmd->s = s;
    cmd->slot = slot;
    cmd->value = value;
    io->coreCommit();
};

static void RF_ScriptSetVarJ (RsContext rsc, RsScript s, uint32_t slot, int64_t value)
{
    Fifo *f = NULL;
    RS_CMD_ScriptSetVarJ cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_ScriptSetVarJ;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;

    cmd.s = s;
    cmd.slot = slot;
    cmd.value = value;

    f->writeAsync(&cmd, cmdSize);
}

static void LF_ScriptSetVarF (RsContext rsc, RsScript s, uint32_t slot, float value)
{
    ThreadIO *io = &((Context *)rsc)->mIO;
    const uint32_t size = sizeof(RS_CMD_ScriptSetVarF);
    RS_CMD_ScriptSetVarF *cmd = static_cast<RS_CMD_ScriptSetVarF *>(io->coreHeader(RS_CMD_ID_ScriptSetVarF, size));
    cmd->s = s;
    cmd->slot = slot;
    cmd->value = value;
    io->coreCommit();
};

static void RF_ScriptSetVarF (RsContext rsc, RsScript s, uint32_t slot, float value)
{
    Fifo *f = NULL;
    RS_CMD_ScriptSetVarF cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_ScriptSetVarF;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;

    cmd.s = s;
    cmd.slot = slot;
    cmd.value = value;

    f->writeAsync(&cmd, cmdSize);
}

static void LF_ScriptSetVarD (RsContext rsc, RsScript s, uint32_t slot, double value)
{
    ThreadIO *io = &((Context *)rsc)->mIO;
    const uint32_t size = sizeof(RS_CMD_ScriptSetVarD);
    RS_CMD_ScriptSetVarD *cmd = static_cast<RS_CMD_ScriptSetVarD *>(io->coreHeader(RS_CMD_ID_ScriptSetVarD, size));
    cmd->s = s;
    cmd->slot = slot;
    cmd->value = value;
    io->coreCommit();
};

static void RF_ScriptSetVarD (RsContext rsc, RsScript s, uint32_t slot, double value)
{
    Fifo *f = NULL;
    RS_CMD_ScriptSetVarD cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_ScriptSetVarD;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;

    cmd.s = s;
    cmd.slot = slot;
    cmd.value = value;

    f->writeAsync(&cmd, cmdSize);
}

static void LF_ScriptSetVarV (RsContext rsc, RsScript s, uint32_t slot, const void * data, size_t data_length)
{
    ThreadIO *io = &((Context *)rsc)->mIO;
    const uint32_t size = sizeof(RS_CMD_ScriptSetVarV);
    uint32_t dataSize = 0;
    dataSize += data_length;
    RS_CMD_ScriptSetVarV *cmd = NULL;
    if (dataSize < 1024) {;
        cmd = static_cast<RS_CMD_ScriptSetVarV *>(io->coreHeader(RS_CMD_ID_ScriptSetVarV, dataSize + size));
    } else {
        cmd = static_cast<RS_CMD_ScriptSetVarV *>(io->coreHeader(RS_CMD_ID_ScriptSetVarV, size));
    }
    uint8_t *payload = (uint8_t *)&cmd[1];
    cmd->s = s;
    cmd->slot = slot;
    if (dataSize < 1024) {
        memcpy(payload, data, data_length);
        cmd->data = (const void *)payload;
        payload += data_length;
    } else {
        cmd->data = data;
    }
    cmd->data_length = data_length;
    if (dataSize < 1024) {
        io->coreCommit();
    } else {
        io->coreCommitSync();
    }
};

static void RF_ScriptSetVarV (RsContext rsc, RsScript s, uint32_t slot, const void * data, size_t data_length)
{
    Fifo *f = NULL;
    RS_CMD_ScriptSetVarV cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_ScriptSetVarV;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;
    dataSize += data_length;

    cmd.s = s;
    cmd.slot = slot;
    cmd.data = (const void *)offset;
    offset += data_length;
    cmd.data_length = data_length;

    f->writeAsync(&cmd, cmdSize);
    f->writeAsync(data, data_length);
}

static RsScript LF_ScriptCCreate (RsContext rsc, const char * resName, size_t resName_length, const char * cacheDir, size_t cacheDir_length, const char * text, size_t text_length)
{
    ThreadIO *io = &((Context *)rsc)->mIO;
    const uint32_t size = sizeof(RS_CMD_ScriptCCreate);
    RS_CMD_ScriptCCreate *cmd = static_cast<RS_CMD_ScriptCCreate *>(io->coreHeader(RS_CMD_ID_ScriptCCreate, size));
    cmd->resName = resName;
    cmd->resName_length = resName_length;
    cmd->cacheDir = cacheDir;
    cmd->cacheDir_length = cacheDir_length;
    cmd->text = text;
    cmd->text_length = text_length;
    io->coreCommitSync();

    RsScript ret;
    io->coreGetReturn(&ret, sizeof(ret));
    return ret;
};

static RsScript RF_ScriptCCreate (RsContext rsc, const char * resName, size_t resName_length, const char * cacheDir, size_t cacheDir_length, const char * text, size_t text_length)
{
    Fifo *f = NULL;
    RS_CMD_ScriptCCreate cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_ScriptCCreate;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;
    dataSize += resName_length;
    dataSize += cacheDir_length;
    dataSize += text_length;

    cmd.resName = (const char *)offset;
    offset += resName_length;
    cmd.resName_length = resName_length;
    cmd.cacheDir = (const char *)offset;
    offset += cacheDir_length;
    cmd.cacheDir_length = cacheDir_length;
    cmd.text = (const char *)offset;
    offset += text_length;
    cmd.text_length = text_length;

    f->writeAsync(&cmd, cmdSize);
    f->writeAsync(resName, resName_length);
    f->writeAsync(cacheDir, cacheDir_length);
    f->writeAsync(text, text_length);
    RsScript retValue;
    f->writeWaitReturn(&retValue, sizeof(retValue));
    return retValue;
}

static RsProgramStore LF_ProgramStoreCreate (RsContext rsc, bool colorMaskR, bool colorMaskG, bool colorMaskB, bool colorMaskA, bool depthMask, bool ditherEnable, RsBlendSrcFunc srcFunc, RsBlendDstFunc destFunc, RsDepthFunc depthFunc)
{
    return rsi_ProgramStoreCreate((Context *)rsc, colorMaskR, colorMaskG, colorMaskB, colorMaskA, depthMask, ditherEnable, srcFunc, destFunc, depthFunc);
};

static RsProgramStore RF_ProgramStoreCreate (RsContext rsc, bool colorMaskR, bool colorMaskG, bool colorMaskB, bool colorMaskA, bool depthMask, bool ditherEnable, RsBlendSrcFunc srcFunc, RsBlendDstFunc destFunc, RsDepthFunc depthFunc)
{
    Fifo *f = NULL;
    RS_CMD_ProgramStoreCreate cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_ProgramStoreCreate;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;

    cmd.colorMaskR = colorMaskR;
    cmd.colorMaskG = colorMaskG;
    cmd.colorMaskB = colorMaskB;
    cmd.colorMaskA = colorMaskA;
    cmd.depthMask = depthMask;
    cmd.ditherEnable = ditherEnable;
    cmd.srcFunc = srcFunc;
    cmd.destFunc = destFunc;
    cmd.depthFunc = depthFunc;

    f->writeAsync(&cmd, cmdSize);
    RsProgramStore retValue;
    f->writeWaitReturn(&retValue, sizeof(retValue));
    return retValue;
}

static RsProgramRaster LF_ProgramRasterCreate (RsContext rsc, bool pointSprite, RsCullMode cull)
{
    return rsi_ProgramRasterCreate((Context *)rsc, pointSprite, cull);
};

static RsProgramRaster RF_ProgramRasterCreate (RsContext rsc, bool pointSprite, RsCullMode cull)
{
    Fifo *f = NULL;
    RS_CMD_ProgramRasterCreate cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_ProgramRasterCreate;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;

    cmd.pointSprite = pointSprite;
    cmd.cull = cull;

    f->writeAsync(&cmd, cmdSize);
    RsProgramRaster retValue;
    f->writeWaitReturn(&retValue, sizeof(retValue));
    return retValue;
}

static void LF_ProgramBindConstants (RsContext rsc, RsProgram vp, uint32_t slot, RsAllocation constants)
{
    ThreadIO *io = &((Context *)rsc)->mIO;
    const uint32_t size = sizeof(RS_CMD_ProgramBindConstants);
    RS_CMD_ProgramBindConstants *cmd = static_cast<RS_CMD_ProgramBindConstants *>(io->coreHeader(RS_CMD_ID_ProgramBindConstants, size));
    cmd->vp = vp;
    cmd->slot = slot;
    cmd->constants = constants;
    io->coreCommit();
};

static void RF_ProgramBindConstants (RsContext rsc, RsProgram vp, uint32_t slot, RsAllocation constants)
{
    Fifo *f = NULL;
    RS_CMD_ProgramBindConstants cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_ProgramBindConstants;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;

    cmd.vp = vp;
    cmd.slot = slot;
    cmd.constants = constants;

    f->writeAsync(&cmd, cmdSize);
}

static void LF_ProgramBindTexture (RsContext rsc, RsProgramFragment pf, uint32_t slot, RsAllocation a)
{
    ThreadIO *io = &((Context *)rsc)->mIO;
    const uint32_t size = sizeof(RS_CMD_ProgramBindTexture);
    RS_CMD_ProgramBindTexture *cmd = static_cast<RS_CMD_ProgramBindTexture *>(io->coreHeader(RS_CMD_ID_ProgramBindTexture, size));
    cmd->pf = pf;
    cmd->slot = slot;
    cmd->a = a;
    io->coreCommit();
};

static void RF_ProgramBindTexture (RsContext rsc, RsProgramFragment pf, uint32_t slot, RsAllocation a)
{
    Fifo *f = NULL;
    RS_CMD_ProgramBindTexture cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_ProgramBindTexture;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;

    cmd.pf = pf;
    cmd.slot = slot;
    cmd.a = a;

    f->writeAsync(&cmd, cmdSize);
}

static void LF_ProgramBindSampler (RsContext rsc, RsProgramFragment pf, uint32_t slot, RsSampler s)
{
    ThreadIO *io = &((Context *)rsc)->mIO;
    const uint32_t size = sizeof(RS_CMD_ProgramBindSampler);
    RS_CMD_ProgramBindSampler *cmd = static_cast<RS_CMD_ProgramBindSampler *>(io->coreHeader(RS_CMD_ID_ProgramBindSampler, size));
    cmd->pf = pf;
    cmd->slot = slot;
    cmd->s = s;
    io->coreCommit();
};

static void RF_ProgramBindSampler (RsContext rsc, RsProgramFragment pf, uint32_t slot, RsSampler s)
{
    Fifo *f = NULL;
    RS_CMD_ProgramBindSampler cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_ProgramBindSampler;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;

    cmd.pf = pf;
    cmd.slot = slot;
    cmd.s = s;

    f->writeAsync(&cmd, cmdSize);
}

static RsProgramFragment LF_ProgramFragmentCreate (RsContext rsc, const char * shaderText, size_t shaderText_length, const uint32_t * params, size_t params_length)
{
    return rsi_ProgramFragmentCreate((Context *)rsc, shaderText, shaderText_length, params, params_length);
};

static RsProgramFragment RF_ProgramFragmentCreate (RsContext rsc, const char * shaderText, size_t shaderText_length, const uint32_t * params, size_t params_length)
{
    Fifo *f = NULL;
    RS_CMD_ProgramFragmentCreate cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_ProgramFragmentCreate;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;
    dataSize += shaderText_length;
    dataSize += params_length;

    cmd.shaderText = (const char *)offset;
    offset += shaderText_length;
    cmd.shaderText_length = shaderText_length;
    cmd.params = (const uint32_t *)offset;
    offset += params_length;
    cmd.params_length = params_length;

    f->writeAsync(&cmd, cmdSize);
    f->writeAsync(shaderText, shaderText_length);
    f->writeAsync(params, params_length);
    RsProgramFragment retValue;
    f->writeWaitReturn(&retValue, sizeof(retValue));
    return retValue;
}

static RsProgramVertex LF_ProgramVertexCreate (RsContext rsc, const char * shaderText, size_t shaderText_length, const uint32_t * params, size_t params_length)
{
    return rsi_ProgramVertexCreate((Context *)rsc, shaderText, shaderText_length, params, params_length);
};

static RsProgramVertex RF_ProgramVertexCreate (RsContext rsc, const char * shaderText, size_t shaderText_length, const uint32_t * params, size_t params_length)
{
    Fifo *f = NULL;
    RS_CMD_ProgramVertexCreate cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_ProgramVertexCreate;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;
    dataSize += shaderText_length;
    dataSize += params_length;

    cmd.shaderText = (const char *)offset;
    offset += shaderText_length;
    cmd.shaderText_length = shaderText_length;
    cmd.params = (const uint32_t *)offset;
    offset += params_length;
    cmd.params_length = params_length;

    f->writeAsync(&cmd, cmdSize);
    f->writeAsync(shaderText, shaderText_length);
    f->writeAsync(params, params_length);
    RsProgramVertex retValue;
    f->writeWaitReturn(&retValue, sizeof(retValue));
    return retValue;
}

static RsFont LF_FontCreateFromFile (RsContext rsc, const char * name, size_t name_length, float fontSize, uint32_t dpi)
{
    ThreadIO *io = &((Context *)rsc)->mIO;
    const uint32_t size = sizeof(RS_CMD_FontCreateFromFile);
    RS_CMD_FontCreateFromFile *cmd = static_cast<RS_CMD_FontCreateFromFile *>(io->coreHeader(RS_CMD_ID_FontCreateFromFile, size));
    cmd->name = name;
    cmd->name_length = name_length;
    cmd->fontSize = fontSize;
    cmd->dpi = dpi;
    io->coreCommitSync();

    RsFont ret;
    io->coreGetReturn(&ret, sizeof(ret));
    return ret;
};

static RsFont RF_FontCreateFromFile (RsContext rsc, const char * name, size_t name_length, float fontSize, uint32_t dpi)
{
    Fifo *f = NULL;
    RS_CMD_FontCreateFromFile cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_FontCreateFromFile;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;
    dataSize += name_length;

    cmd.name = (const char *)offset;
    offset += name_length;
    cmd.name_length = name_length;
    cmd.fontSize = fontSize;
    cmd.dpi = dpi;

    f->writeAsync(&cmd, cmdSize);
    f->writeAsync(name, name_length);
    RsFont retValue;
    f->writeWaitReturn(&retValue, sizeof(retValue));
    return retValue;
}

static RsFont LF_FontCreateFromMemory (RsContext rsc, const char * name, size_t name_length, float fontSize, uint32_t dpi, const void * data, size_t data_length)
{
    ThreadIO *io = &((Context *)rsc)->mIO;
    const uint32_t size = sizeof(RS_CMD_FontCreateFromMemory);
    RS_CMD_FontCreateFromMemory *cmd = static_cast<RS_CMD_FontCreateFromMemory *>(io->coreHeader(RS_CMD_ID_FontCreateFromMemory, size));
    cmd->name = name;
    cmd->name_length = name_length;
    cmd->fontSize = fontSize;
    cmd->dpi = dpi;
    cmd->data = data;
    cmd->data_length = data_length;
    io->coreCommitSync();

    RsFont ret;
    io->coreGetReturn(&ret, sizeof(ret));
    return ret;
};

static RsFont RF_FontCreateFromMemory (RsContext rsc, const char * name, size_t name_length, float fontSize, uint32_t dpi, const void * data, size_t data_length)
{
    Fifo *f = NULL;
    RS_CMD_FontCreateFromMemory cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_FontCreateFromMemory;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;
    dataSize += name_length;
    dataSize += data_length;

    cmd.name = (const char *)offset;
    offset += name_length;
    cmd.name_length = name_length;
    cmd.fontSize = fontSize;
    cmd.dpi = dpi;
    cmd.data = (const void *)offset;
    offset += data_length;
    cmd.data_length = data_length;

    f->writeAsync(&cmd, cmdSize);
    f->writeAsync(name, name_length);
    f->writeAsync(data, data_length);
    RsFont retValue;
    f->writeWaitReturn(&retValue, sizeof(retValue));
    return retValue;
}

static RsMesh LF_MeshCreate (RsContext rsc, RsAllocation * vtx, size_t vtx_length, RsAllocation * idx, size_t idx_length, uint32_t * primType, size_t primType_length)
{
    ThreadIO *io = &((Context *)rsc)->mIO;
    const uint32_t size = sizeof(RS_CMD_MeshCreate);
    RS_CMD_MeshCreate *cmd = static_cast<RS_CMD_MeshCreate *>(io->coreHeader(RS_CMD_ID_MeshCreate, size));
    cmd->vtx = vtx;
    cmd->vtx_length = vtx_length;
    cmd->idx = idx;
    cmd->idx_length = idx_length;
    cmd->primType = primType;
    cmd->primType_length = primType_length;
    io->coreCommitSync();

    RsMesh ret;
    io->coreGetReturn(&ret, sizeof(ret));
    return ret;
};

static RsMesh RF_MeshCreate (RsContext rsc, RsAllocation * vtx, size_t vtx_length, RsAllocation * idx, size_t idx_length, uint32_t * primType, size_t primType_length)
{
    Fifo *f = NULL;
    RS_CMD_MeshCreate cmd;
    const uint32_t cmdSize = sizeof(cmd);
    const uint32_t cmdID = RS_CMD_ID_MeshCreate;
    f->writeAsync(&cmdID, sizeof(cmdID));
    intptr_t offset = cmdSize;
    uint32_t dataSize = 0;

    cmd.vtx = (RsAllocation *)offset;
    offset += vtx_length;
    cmd.vtx_length = vtx_length;
    cmd.idx = (RsAllocation *)offset;
    offset += idx_length;
    cmd.idx_length = idx_length;
    cmd.primType = (uint32_t *)offset;
    offset += primType_length;
    cmd.primType_length = primType_length;

    f->writeAsync(&cmd, cmdSize);
    f->writeAsync(vtx, vtx_length);
    f->writeAsync(idx, idx_length);
    f->writeAsync(primType, primType_length);
    RsMesh retValue;
    f->writeWaitReturn(&retValue, sizeof(retValue));
    return retValue;
}


static RsApiEntrypoints_t s_LocalTable = {
    LF_ContextDestroy,
    LF_ContextGetMessage,
    LF_ContextPeekMessage,
    LF_ContextInitToClient,
    LF_ContextDeinitToClient,
    LF_TypeCreate,
    LF_AllocationCreateTyped,
    LF_AllocationCreateFromBitmap,
    LF_AllocationCubeCreateFromBitmap,
    LF_ContextFinish,
    LF_ContextBindRootScript,
    LF_ContextBindProgramStore,
    LF_ContextBindProgramFragment,
    LF_ContextBindProgramVertex,
    LF_ContextBindProgramRaster,
    LF_ContextBindFont,
    LF_ContextPause,
    LF_ContextResume,
    LF_ContextSetSurface,
    LF_ContextDump,
    LF_ContextSetPriority,
    LF_ContextDestroyWorker,
    LF_AssignName,
    LF_ObjDestroy,
    LF_ElementCreate,
    LF_ElementCreate2,
    LF_AllocationCopyToBitmap,
    LF_Allocation1DData,
    LF_Allocation1DElementData,
    LF_Allocation2DData,
    LF_Allocation2DElementData,
    LF_AllocationGenerateMipmaps,
    LF_AllocationRead,
    LF_AllocationSyncAll,
    LF_AllocationResize1D,
    LF_AllocationResize2D,
    LF_AllocationCopy2DRange,
    LF_SamplerCreate,
    LF_ScriptBindAllocation,
    LF_ScriptSetTimeZone,
    LF_ScriptInvoke,
    LF_ScriptInvokeV,
    LF_ScriptForEach,
    LF_ScriptSetVarI,
    LF_ScriptSetVarObj,
    LF_ScriptSetVarJ,
    LF_ScriptSetVarF,
    LF_ScriptSetVarD,
    LF_ScriptSetVarV,
    LF_ScriptCCreate,
    LF_ProgramStoreCreate,
    LF_ProgramRasterCreate,
    LF_ProgramBindConstants,
    LF_ProgramBindTexture,
    LF_ProgramBindSampler,
    LF_ProgramFragmentCreate,
    LF_ProgramVertexCreate,
    LF_FontCreateFromFile,
    LF_FontCreateFromMemory,
    LF_MeshCreate,
};

static RsApiEntrypoints_t s_RemoteTable = {
    RF_ContextDestroy,
    RF_ContextGetMessage,
    RF_ContextPeekMessage,
    RF_ContextInitToClient,
    RF_ContextDeinitToClient,
    RF_TypeCreate,
    RF_AllocationCreateTyped,
    RF_AllocationCreateFromBitmap,
    RF_AllocationCubeCreateFromBitmap,
    RF_ContextFinish,
    RF_ContextBindRootScript,
    RF_ContextBindProgramStore,
    RF_ContextBindProgramFragment,
    RF_ContextBindProgramVertex,
    RF_ContextBindProgramRaster,
    RF_ContextBindFont,
    RF_ContextPause,
    RF_ContextResume,
    RF_ContextSetSurface,
    RF_ContextDump,
    RF_ContextSetPriority,
    RF_ContextDestroyWorker,
    RF_AssignName,
    RF_ObjDestroy,
    RF_ElementCreate,
    RF_ElementCreate2,
    RF_AllocationCopyToBitmap,
    RF_Allocation1DData,
    RF_Allocation1DElementData,
    RF_Allocation2DData,
    RF_Allocation2DElementData,
    RF_AllocationGenerateMipmaps,
    RF_AllocationRead,
    RF_AllocationSyncAll,
    RF_AllocationResize1D,
    RF_AllocationResize2D,
    RF_AllocationCopy2DRange,
    RF_SamplerCreate,
    RF_ScriptBindAllocation,
    RF_ScriptSetTimeZone,
    RF_ScriptInvoke,
    RF_ScriptInvokeV,
    RF_ScriptForEach,
    RF_ScriptSetVarI,
    RF_ScriptSetVarObj,
    RF_ScriptSetVarJ,
    RF_ScriptSetVarF,
    RF_ScriptSetVarD,
    RF_ScriptSetVarV,
    RF_ScriptCCreate,
    RF_ProgramStoreCreate,
    RF_ProgramRasterCreate,
    RF_ProgramBindConstants,
    RF_ProgramBindTexture,
    RF_ProgramBindSampler,
    RF_ProgramFragmentCreate,
    RF_ProgramVertexCreate,
    RF_FontCreateFromFile,
    RF_FontCreateFromMemory,
    RF_MeshCreate,
};
static RsApiEntrypoints_t *s_CurrentTable = &s_LocalTable;

void rsContextDestroy (RsContext rsc)
{
    s_CurrentTable->ContextDestroy((Context *)rsc);
}

RsMessageToClientType rsContextGetMessage (RsContext rsc, void * data, size_t data_length, size_t * receiveLen, size_t receiveLen_length, uint32_t * usrID, size_t usrID_length)
{
    return s_CurrentTable->ContextGetMessage((Context *)rsc, data, data_length, receiveLen, receiveLen_length, usrID, usrID_length);
}

RsMessageToClientType rsContextPeekMessage (RsContext rsc, size_t * receiveLen, size_t receiveLen_length, uint32_t * usrID, size_t usrID_length)
{
    return s_CurrentTable->ContextPeekMessage((Context *)rsc, receiveLen, receiveLen_length, usrID, usrID_length);
}

void rsContextInitToClient (RsContext rsc)
{
    s_CurrentTable->ContextInitToClient((Context *)rsc);
}

void rsContextDeinitToClient (RsContext rsc)
{
    s_CurrentTable->ContextDeinitToClient((Context *)rsc);
}

RsType rsTypeCreate (RsContext rsc, RsElement e, uint32_t dimX, uint32_t dimY, uint32_t dimZ, bool mips, bool faces)
{
    return s_CurrentTable->TypeCreate((Context *)rsc, e, dimX, dimY, dimZ, mips, faces);
}

RsAllocation rsAllocationCreateTyped (RsContext rsc, RsType vtype, RsAllocationMipmapControl mips, uint32_t usages)
{
    return s_CurrentTable->AllocationCreateTyped((Context *)rsc, vtype, mips, usages);
}

RsAllocation rsAllocationCreateFromBitmap (RsContext rsc, RsType vtype, RsAllocationMipmapControl mips, const void * data, size_t data_length, uint32_t usages)
{
    return s_CurrentTable->AllocationCreateFromBitmap((Context *)rsc, vtype, mips, data, data_length, usages);
}

RsAllocation rsAllocationCubeCreateFromBitmap (RsContext rsc, RsType vtype, RsAllocationMipmapControl mips, const void * data, size_t data_length, uint32_t usages)
{
    return s_CurrentTable->AllocationCubeCreateFromBitmap((Context *)rsc, vtype, mips, data, data_length, usages);
}

void rsContextFinish (RsContext rsc)
{
    s_CurrentTable->ContextFinish((Context *)rsc);
}

void rsContextBindRootScript (RsContext rsc, RsScript sampler)
{
    s_CurrentTable->ContextBindRootScript((Context *)rsc, sampler);
}

void rsContextBindProgramStore (RsContext rsc, RsProgramStore pgm)
{
    s_CurrentTable->ContextBindProgramStore((Context *)rsc, pgm);
}

void rsContextBindProgramFragment (RsContext rsc, RsProgramFragment pgm)
{
    s_CurrentTable->ContextBindProgramFragment((Context *)rsc, pgm);
}

void rsContextBindProgramVertex (RsContext rsc, RsProgramVertex pgm)
{
    s_CurrentTable->ContextBindProgramVertex((Context *)rsc, pgm);
}

void rsContextBindProgramRaster (RsContext rsc, RsProgramRaster pgm)
{
    s_CurrentTable->ContextBindProgramRaster((Context *)rsc, pgm);
}

void rsContextBindFont (RsContext rsc, RsFont pgm)
{
    s_CurrentTable->ContextBindFont((Context *)rsc, pgm);
}

void rsContextPause (RsContext rsc)
{
    s_CurrentTable->ContextPause((Context *)rsc);
}

void rsContextResume (RsContext rsc)
{
    s_CurrentTable->ContextResume((Context *)rsc);
}

void rsContextSetSurface (RsContext rsc, uint32_t width, uint32_t height, RsNativeWindow sur)
{
    s_CurrentTable->ContextSetSurface((Context *)rsc, width, height, sur);
}

void rsContextDump (RsContext rsc, int32_t bits)
{
    s_CurrentTable->ContextDump((Context *)rsc, bits);
}

void rsContextSetPriority (RsContext rsc, int32_t priority)
{
    s_CurrentTable->ContextSetPriority((Context *)rsc, priority);
}

void rsContextDestroyWorker (RsContext rsc)
{
    s_CurrentTable->ContextDestroyWorker((Context *)rsc);
}

void rsAssignName (RsContext rsc, RsObjectBase obj, const char * name, size_t name_length)
{
    s_CurrentTable->AssignName((Context *)rsc, obj, name, name_length);
}

void rsObjDestroy (RsContext rsc, RsAsyncVoidPtr objPtr)
{
    s_CurrentTable->ObjDestroy((Context *)rsc, objPtr);
}

RsElement rsElementCreate (RsContext rsc, RsDataType mType, RsDataKind mKind, bool mNormalized, uint32_t mVectorSize)
{
    return s_CurrentTable->ElementCreate((Context *)rsc, mType, mKind, mNormalized, mVectorSize);
}

RsElement rsElementCreate2 (RsContext rsc, const RsElement * elements, size_t elements_length, const char ** names, size_t names_length_length, const size_t * names_length, const uint32_t * arraySize, size_t arraySize_length)
{
    return s_CurrentTable->ElementCreate2((Context *)rsc, elements, elements_length, names, names_length_length, names_length, arraySize, arraySize_length);
}

void rsAllocationCopyToBitmap (RsContext rsc, RsAllocation alloc, void * data, size_t data_length)
{
    s_CurrentTable->AllocationCopyToBitmap((Context *)rsc, alloc, data, data_length);
}

void rsAllocation1DData (RsContext rsc, RsAllocation va, uint32_t xoff, uint32_t lod, uint32_t count, const void * data, size_t data_length)
{
    s_CurrentTable->Allocation1DData((Context *)rsc, va, xoff, lod, count, data, data_length);
}

void rsAllocation1DElementData (RsContext rsc, RsAllocation va, uint32_t x, uint32_t lod, const void * data, size_t data_length, uint32_t comp_offset)
{
    s_CurrentTable->Allocation1DElementData((Context *)rsc, va, x, lod, data, data_length, comp_offset);
}

void rsAllocation2DData (RsContext rsc, RsAllocation va, uint32_t xoff, uint32_t yoff, uint32_t lod, RsAllocationCubemapFace face, uint32_t w, uint32_t h, const void * data, size_t data_length)
{
    s_CurrentTable->Allocation2DData((Context *)rsc, va, xoff, yoff, lod, face, w, h, data, data_length);
}

void rsAllocation2DElementData (RsContext rsc, RsAllocation va, uint32_t x, uint32_t y, uint32_t lod, RsAllocationCubemapFace face, const void * data, size_t data_length, uint32_t element_offset)
{
    s_CurrentTable->Allocation2DElementData((Context *)rsc, va, x, y, lod, face, data, data_length, element_offset);
}

void rsAllocationGenerateMipmaps (RsContext rsc, RsAllocation va)
{
    s_CurrentTable->AllocationGenerateMipmaps((Context *)rsc, va);
}

void rsAllocationRead (RsContext rsc, RsAllocation va, void * data, size_t data_length)
{
    s_CurrentTable->AllocationRead((Context *)rsc, va, data, data_length);
}

void rsAllocationSyncAll (RsContext rsc, RsAllocation va, RsAllocationUsageType src)
{
    s_CurrentTable->AllocationSyncAll((Context *)rsc, va, src);
}

void rsAllocationResize1D (RsContext rsc, RsAllocation va, uint32_t dimX)
{
    s_CurrentTable->AllocationResize1D((Context *)rsc, va, dimX);
}

void rsAllocationResize2D (RsContext rsc, RsAllocation va, uint32_t dimX, uint32_t dimY)
{
    s_CurrentTable->AllocationResize2D((Context *)rsc, va, dimX, dimY);
}

void rsAllocationCopy2DRange (RsContext rsc, RsAllocation dest, uint32_t destXoff, uint32_t destYoff, uint32_t destMip, uint32_t destFace, uint32_t width, uint32_t height, RsAllocation src, uint32_t srcXoff, uint32_t srcYoff, uint32_t srcMip, uint32_t srcFace)
{
    s_CurrentTable->AllocationCopy2DRange((Context *)rsc, dest, destXoff, destYoff, destMip, destFace, width, height, src, srcXoff, srcYoff, srcMip, srcFace);
}

RsSampler rsSamplerCreate (RsContext rsc, RsSamplerValue magFilter, RsSamplerValue minFilter, RsSamplerValue wrapS, RsSamplerValue wrapT, RsSamplerValue wrapR, float mAniso)
{
    return s_CurrentTable->SamplerCreate((Context *)rsc, magFilter, minFilter, wrapS, wrapT, wrapR, mAniso);
}

void rsScriptBindAllocation (RsContext rsc, RsScript vtm, RsAllocation va, uint32_t slot)
{
    s_CurrentTable->ScriptBindAllocation((Context *)rsc, vtm, va, slot);
}

void rsScriptSetTimeZone (RsContext rsc, RsScript s, const char * timeZone, size_t timeZone_length)
{
    s_CurrentTable->ScriptSetTimeZone((Context *)rsc, s, timeZone, timeZone_length);
}

void rsScriptInvoke (RsContext rsc, RsScript s, uint32_t slot)
{
    s_CurrentTable->ScriptInvoke((Context *)rsc, s, slot);
}

void rsScriptInvokeV (RsContext rsc, RsScript s, uint32_t slot, const void * data, size_t data_length)
{
    s_CurrentTable->ScriptInvokeV((Context *)rsc, s, slot, data, data_length);
}

void rsScriptForEach (RsContext rsc, RsScript s, uint32_t slot, RsAllocation ain, RsAllocation aout, const void * usr, size_t usr_length)
{
    s_CurrentTable->ScriptForEach((Context *)rsc, s, slot, ain, aout, usr, usr_length);
}

void rsScriptSetVarI (RsContext rsc, RsScript s, uint32_t slot, int value)
{
    s_CurrentTable->ScriptSetVarI((Context *)rsc, s, slot, value);
}

void rsScriptSetVarObj (RsContext rsc, RsScript s, uint32_t slot, RsObjectBase value)
{
    s_CurrentTable->ScriptSetVarObj((Context *)rsc, s, slot, value);
}

void rsScriptSetVarJ (RsContext rsc, RsScript s, uint32_t slot, int64_t value)
{
    s_CurrentTable->ScriptSetVarJ((Context *)rsc, s, slot, value);
}

void rsScriptSetVarF (RsContext rsc, RsScript s, uint32_t slot, float value)
{
    s_CurrentTable->ScriptSetVarF((Context *)rsc, s, slot, value);
}

void rsScriptSetVarD (RsContext rsc, RsScript s, uint32_t slot, double value)
{
    s_CurrentTable->ScriptSetVarD((Context *)rsc, s, slot, value);
}

void rsScriptSetVarV (RsContext rsc, RsScript s, uint32_t slot, const void * data, size_t data_length)
{
    s_CurrentTable->ScriptSetVarV((Context *)rsc, s, slot, data, data_length);
}

RsScript rsScriptCCreate (RsContext rsc, const char * resName, size_t resName_length, const char * cacheDir, size_t cacheDir_length, const char * text, size_t text_length)
{
    return s_CurrentTable->ScriptCCreate((Context *)rsc, resName, resName_length, cacheDir, cacheDir_length, text, text_length);
}

RsProgramStore rsProgramStoreCreate (RsContext rsc, bool colorMaskR, bool colorMaskG, bool colorMaskB, bool colorMaskA, bool depthMask, bool ditherEnable, RsBlendSrcFunc srcFunc, RsBlendDstFunc destFunc, RsDepthFunc depthFunc)
{
    return s_CurrentTable->ProgramStoreCreate((Context *)rsc, colorMaskR, colorMaskG, colorMaskB, colorMaskA, depthMask, ditherEnable, srcFunc, destFunc, depthFunc);
}

RsProgramRaster rsProgramRasterCreate (RsContext rsc, bool pointSprite, RsCullMode cull)
{
    return s_CurrentTable->ProgramRasterCreate((Context *)rsc, pointSprite, cull);
}

void rsProgramBindConstants (RsContext rsc, RsProgram vp, uint32_t slot, RsAllocation constants)
{
    s_CurrentTable->ProgramBindConstants((Context *)rsc, vp, slot, constants);
}

void rsProgramBindTexture (RsContext rsc, RsProgramFragment pf, uint32_t slot, RsAllocation a)
{
    s_CurrentTable->ProgramBindTexture((Context *)rsc, pf, slot, a);
}

void rsProgramBindSampler (RsContext rsc, RsProgramFragment pf, uint32_t slot, RsSampler s)
{
    s_CurrentTable->ProgramBindSampler((Context *)rsc, pf, slot, s);
}

RsProgramFragment rsProgramFragmentCreate (RsContext rsc, const char * shaderText, size_t shaderText_length, const uint32_t * params, size_t params_length)
{
    return s_CurrentTable->ProgramFragmentCreate((Context *)rsc, shaderText, shaderText_length, params, params_length);
}

RsProgramVertex rsProgramVertexCreate (RsContext rsc, const char * shaderText, size_t shaderText_length, const uint32_t * params, size_t params_length)
{
    return s_CurrentTable->ProgramVertexCreate((Context *)rsc, shaderText, shaderText_length, params, params_length);
}

RsFont rsFontCreateFromFile (RsContext rsc, const char * name, size_t name_length, float fontSize, uint32_t dpi)
{
    return s_CurrentTable->FontCreateFromFile((Context *)rsc, name, name_length, fontSize, dpi);
}

RsFont rsFontCreateFromMemory (RsContext rsc, const char * name, size_t name_length, float fontSize, uint32_t dpi, const void * data, size_t data_length)
{
    return s_CurrentTable->FontCreateFromMemory((Context *)rsc, name, name_length, fontSize, dpi, data, data_length);
}

RsMesh rsMeshCreate (RsContext rsc, RsAllocation * vtx, size_t vtx_length, RsAllocation * idx, size_t idx_length, uint32_t * primType, size_t primType_length)
{
    return s_CurrentTable->MeshCreate((Context *)rsc, vtx, vtx_length, idx, idx_length, primType, primType_length);
}

