package android.renderscript;
public class Matrix2f
{
public  Matrix2f() { throw new RuntimeException("Stub!"); }
public  Matrix2f(float[] dataArray) { throw new RuntimeException("Stub!"); }
public  float[] getArray() { throw new RuntimeException("Stub!"); }
public  float get(int i, int j) { throw new RuntimeException("Stub!"); }
public  void set(int i, int j, float v) { throw new RuntimeException("Stub!"); }
public  void loadIdentity() { throw new RuntimeException("Stub!"); }
public  void load(android.renderscript.Matrix2f src) { throw new RuntimeException("Stub!"); }
public  void loadRotate(float rot) { throw new RuntimeException("Stub!"); }
public  void loadScale(float x, float y) { throw new RuntimeException("Stub!"); }
public  void loadMultiply(android.renderscript.Matrix2f lhs, android.renderscript.Matrix2f rhs) { throw new RuntimeException("Stub!"); }
public  void multiply(android.renderscript.Matrix2f rhs) { throw new RuntimeException("Stub!"); }
public  void rotate(float rot) { throw new RuntimeException("Stub!"); }
public  void scale(float x, float y) { throw new RuntimeException("Stub!"); }
public  void transpose() { throw new RuntimeException("Stub!"); }
}
