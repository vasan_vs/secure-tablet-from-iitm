package android.renderscript;
public class ProgramVertexFixedFunction
  extends android.renderscript.ProgramVertex
{
public static class Builder
{
public  Builder(android.renderscript.RenderScript rs) { throw new RuntimeException("Stub!"); }
public  android.renderscript.ProgramVertexFixedFunction.Builder setTextureMatrixEnable(boolean enable) { throw new RuntimeException("Stub!"); }
public  android.renderscript.ProgramVertexFixedFunction create() { throw new RuntimeException("Stub!"); }
}
public static class Constants
{
public  Constants(android.renderscript.RenderScript rs) { throw new RuntimeException("Stub!"); }
public  void destroy() { throw new RuntimeException("Stub!"); }
public  void setModelview(android.renderscript.Matrix4f m) { throw new RuntimeException("Stub!"); }
public  void setProjection(android.renderscript.Matrix4f m) { throw new RuntimeException("Stub!"); }
public  void setTexture(android.renderscript.Matrix4f m) { throw new RuntimeException("Stub!"); }
}
ProgramVertexFixedFunction() { throw new RuntimeException("Stub!"); }
public  void bindConstants(android.renderscript.ProgramVertexFixedFunction.Constants va) { throw new RuntimeException("Stub!"); }
}
