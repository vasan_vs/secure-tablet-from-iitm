package android.media.effect;
public interface EffectUpdateListener
{
public abstract  void onEffectUpdated(android.media.effect.Effect effect, java.lang.Object info);
}
