package android.net.wifi.p2p;
public class WifiP2pConfig
  implements android.os.Parcelable
{
public  WifiP2pConfig() { throw new RuntimeException("Stub!"); }
public  WifiP2pConfig(android.net.wifi.p2p.WifiP2pConfig source) { throw new RuntimeException("Stub!"); }
public  java.lang.String toString() { throw new RuntimeException("Stub!"); }
public  int describeContents() { throw new RuntimeException("Stub!"); }
public  void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }
public java.lang.String deviceAddress;
public android.net.wifi.WpsInfo wps;
public int groupOwnerIntent;
public static final android.os.Parcelable.Creator<android.net.wifi.p2p.WifiP2pConfig> CREATOR;
static { CREATOR = null; }
}
