package android.os;
public class DisplayManager
{
DisplayManager() { throw new RuntimeException("Stub!"); }
public  java.lang.String[] getDisplayModeList(int dispid) { throw new RuntimeException("Stub!"); }
public  boolean setDisplayMode(int dispid, java.lang.String mode) { throw new RuntimeException("Stub!"); }
public  boolean setDisplayEnable(int dispid, boolean enable) { throw new RuntimeException("Stub!"); }
public  boolean setDisplayMirror(int dispid, boolean enable) { throw new RuntimeException("Stub!"); }
public  boolean setDisplayXOverScan(int dispid, int xOverscan) { throw new RuntimeException("Stub!"); }
public  boolean setDisplayYOverScan(int dispid, int yOverscan) { throw new RuntimeException("Stub!"); }
public  boolean setDisplayRotation(int dispid, boolean enable) { throw new RuntimeException("Stub!"); }
public  boolean setDisplayColorDepth(int dispid, int colordepth) { throw new RuntimeException("Stub!"); }
public  boolean setDisplayKeepRate(int dispid, int keepRate) { throw new RuntimeException("Stub!"); }
public  java.lang.String getDisplayMode(int dispid) { throw new RuntimeException("Stub!"); }
public  java.lang.String getDisplayName(int dispid) { throw new RuntimeException("Stub!"); }
public  boolean getDisplayEnable(int dispid) { throw new RuntimeException("Stub!"); }
public  boolean getDisplayMirror(int dispid) { throw new RuntimeException("Stub!"); }
public  int getDisplayXOverScan(int dispid) { throw new RuntimeException("Stub!"); }
public  int getDisplayYOverScan(int dispid) { throw new RuntimeException("Stub!"); }
public  boolean getDisplayRotation(int dispid) { throw new RuntimeException("Stub!"); }
public  int getDisplayColorDepth(int dispid) { throw new RuntimeException("Stub!"); }
public  int getDisplayKeepRate(int dispid) { throw new RuntimeException("Stub!"); }
public  boolean rebootSystem() { throw new RuntimeException("Stub!"); }
public static final java.lang.String ACTION_DISPLAY_DEVICE_0_ATTACHED = "android.os.action.DISPLAY_DEVICE_0_ATTACHED";
public static final java.lang.String ACTION_DISPLAY_DEVICE_1_ATTACHED = "android.os.action.DISPLAY_DEVICE_1_ATTACHED";
public static final java.lang.String ACTION_DISPLAY_DEVICE_2_ATTACHED = "android.os.action.DISPLAY_DEVICE_2_ATTACHED";
public static final java.lang.String ACTION_DISPLAY_DEVICE_3_ATTACHED = "android.os.action.DISPLAY_DEVICE_3_ATTACHED";
public static final java.lang.String ACTION_DISPLAY_DEVICE_4_ATTACHED = "android.os.action.DISPLAY_DEVICE_4_ATTACHED";
public static final java.lang.String ACTION_DISPLAY_DEVICE_5_ATTACHED = "android.os.action.DISPLAY_DEVICE_5_ATTACHED";
public static final java.lang.String EXTRA_DISPLAY_STATE = "display_state";
public static final int DISPLAY_STATE_DISABLING = 0;
public static final int DISPLAY_STATE_DISABLED = 1;
public static final int DISPLAY_STATE_ENABLING = 2;
public static final int DISPLAY_STATE_ENABLED = 3;
public static final int DISPLAY_STATE_UNKNOWN = 4;
public static final java.lang.String EXTRA_DISPLAY_DEVICE = "display_device";
public static final java.lang.String EXTRA_DISPLAY_CONNECT = "display_connect";
public static final java.lang.String EXTRA_PERMISSION_GRANTED = "permission";
}
