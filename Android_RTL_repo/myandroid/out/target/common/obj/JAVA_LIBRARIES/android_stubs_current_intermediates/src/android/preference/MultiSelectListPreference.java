package android.preference;
public class MultiSelectListPreference
  extends android.preference.DialogPreference
{
public  MultiSelectListPreference(android.content.Context context, android.util.AttributeSet attrs) { super((android.content.Context)null,(android.util.AttributeSet)null); throw new RuntimeException("Stub!"); }
public  MultiSelectListPreference(android.content.Context context) { super((android.content.Context)null,(android.util.AttributeSet)null); throw new RuntimeException("Stub!"); }
public  void setEntries(java.lang.CharSequence[] entries) { throw new RuntimeException("Stub!"); }
public  void setEntries(int entriesResId) { throw new RuntimeException("Stub!"); }
public  java.lang.CharSequence[] getEntries() { throw new RuntimeException("Stub!"); }
public  void setEntryValues(java.lang.CharSequence[] entryValues) { throw new RuntimeException("Stub!"); }
public  void setEntryValues(int entryValuesResId) { throw new RuntimeException("Stub!"); }
public  java.lang.CharSequence[] getEntryValues() { throw new RuntimeException("Stub!"); }
public  void setValues(java.util.Set<java.lang.String> values) { throw new RuntimeException("Stub!"); }
public  java.util.Set<java.lang.String> getValues() { throw new RuntimeException("Stub!"); }
public  int findIndexOfValue(java.lang.String value) { throw new RuntimeException("Stub!"); }
protected  void onPrepareDialogBuilder(android.app.AlertDialog.Builder builder) { throw new RuntimeException("Stub!"); }
protected  void onDialogClosed(boolean positiveResult) { throw new RuntimeException("Stub!"); }
protected  java.lang.Object onGetDefaultValue(android.content.res.TypedArray a, int index) { throw new RuntimeException("Stub!"); }
protected  void onSetInitialValue(boolean restoreValue, java.lang.Object defaultValue) { throw new RuntimeException("Stub!"); }
protected  android.os.Parcelable onSaveInstanceState() { throw new RuntimeException("Stub!"); }
}
