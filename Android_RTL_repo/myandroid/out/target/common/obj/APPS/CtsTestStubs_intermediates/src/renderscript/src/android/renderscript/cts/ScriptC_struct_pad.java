/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * This file is auto-generated. DO NOT MODIFY!
 * The source Renderscript file: cts/tests/src/android/renderscript/cts/struct_pad.rs
 */
package android.renderscript.cts;

import android.renderscript.*;
import android.content.res.Resources;

/**
 * @hide
 */
public class ScriptC_struct_pad extends ScriptC {
    // Constructor
    public  ScriptC_struct_pad(RenderScript rs, Resources resources, int id) {
        super(rs, resources, id);
    }

    private final static int mExportVarIdx_s = 0;
    private ScriptField_PadMe.Item mExportVar_s;
    public void set_s(ScriptField_PadMe.Item v) {
        mExportVar_s = v;
        FieldPacker fp = new FieldPacker(48);
        fp.addI32(v.i);
        fp.skip(12);
        fp.addF32(v.f4);
        fp.addI32(v.j);
        fp.skip(12);
        setVar(mExportVarIdx_s, fp);
    }

    public ScriptField_PadMe.Item get_s() {
        return mExportVar_s;
    }

}

