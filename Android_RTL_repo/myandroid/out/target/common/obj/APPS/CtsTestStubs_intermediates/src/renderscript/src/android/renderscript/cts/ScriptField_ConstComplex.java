/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * This file is auto-generated. DO NOT MODIFY!
 * The source Renderscript file: cts/tests/src/android/renderscript/cts/graphics_runner.rs
 */
package android.renderscript.cts;

import android.renderscript.*;
import android.content.res.Resources;

/**
 * @hide
 */
public class ScriptField_ConstComplex extends android.renderscript.Script.FieldBase {
    static public class Item {
        public static final int sizeof = 176;

        Matrix4f MATRIX;
        Matrix4f EXTRA;
        float extra1;
        Float2 extra2;
        Float3 extra3;
        Float4 extra4;

        Item() {
            MATRIX = new Matrix4f();
            EXTRA = new Matrix4f();
            extra2 = new Float2();
            extra3 = new Float3();
            extra4 = new Float4();
        }

    }

    private Item mItemArray[];
    private FieldPacker mIOBuffer;
    public static Element createElement(RenderScript rs) {
        Element.Builder eb = new Element.Builder(rs);
        eb.add(Element.MATRIX_4X4(rs), "MATRIX");
        eb.add(Element.MATRIX_4X4(rs), "EXTRA");
        eb.add(Element.F32(rs), "extra1");
        eb.add(Element.U32(rs), "#padding_1");
        eb.add(Element.F32_2(rs), "extra2");
        eb.add(Element.F32_3(rs), "extra3");
        eb.add(Element.U32(rs), "#padding_2");
        eb.add(Element.F32_4(rs), "extra4");
        return eb.create();
    }

    public  ScriptField_ConstComplex(RenderScript rs, int count) {
        mItemArray = null;
        mIOBuffer = null;
        mElement = createElement(rs);
        init(rs, count);
    }

    public  ScriptField_ConstComplex(RenderScript rs, int count, int usages) {
        mItemArray = null;
        mIOBuffer = null;
        mElement = createElement(rs);
        init(rs, count, usages);
    }

    private void copyToArrayLocal(Item i, FieldPacker fp) {
        fp.addMatrix(i.MATRIX);
        fp.addMatrix(i.EXTRA);
        fp.addF32(i.extra1);
        fp.skip(4);
        fp.addF32(i.extra2);
        fp.addF32(i.extra3);
        fp.skip(4);
        fp.addF32(i.extra4);
    }

    private void copyToArray(Item i, int index) {
        if (mIOBuffer == null) mIOBuffer = new FieldPacker(Item.sizeof * getType().getX()/* count */);
        mIOBuffer.reset(index * Item.sizeof);
        copyToArrayLocal(i, mIOBuffer);
    }

    public synchronized void set(Item i, int index, boolean copyNow) {
        if (mItemArray == null) mItemArray = new Item[getType().getX() /* count */];
        mItemArray[index] = i;
        if (copyNow)  {
            copyToArray(i, index);
            FieldPacker fp = new FieldPacker(Item.sizeof);
            copyToArrayLocal(i, fp);
            mAllocation.setFromFieldPacker(index, fp);
        }

    }

    public synchronized Item get(int index) {
        if (mItemArray == null) return null;
        return mItemArray[index];
    }

    public synchronized void set_MATRIX(int index, Matrix4f v, boolean copyNow) {
        if (mIOBuffer == null) mIOBuffer = new FieldPacker(Item.sizeof * getType().getX()/* count */);
        if (mItemArray == null) mItemArray = new Item[getType().getX() /* count */];
        if (mItemArray[index] == null) mItemArray[index] = new Item();
        mItemArray[index].MATRIX = v;
        if (copyNow)  {
            mIOBuffer.reset(index * Item.sizeof);
            mIOBuffer.addMatrix(v);
            FieldPacker fp = new FieldPacker(64);
            fp.addMatrix(v);
            mAllocation.setFromFieldPacker(index, 0, fp);
        }

    }

    public synchronized void set_EXTRA(int index, Matrix4f v, boolean copyNow) {
        if (mIOBuffer == null) mIOBuffer = new FieldPacker(Item.sizeof * getType().getX()/* count */);
        if (mItemArray == null) mItemArray = new Item[getType().getX() /* count */];
        if (mItemArray[index] == null) mItemArray[index] = new Item();
        mItemArray[index].EXTRA = v;
        if (copyNow)  {
            mIOBuffer.reset(index * Item.sizeof + 64);
            mIOBuffer.addMatrix(v);
            FieldPacker fp = new FieldPacker(64);
            fp.addMatrix(v);
            mAllocation.setFromFieldPacker(index, 1, fp);
        }

    }

    public synchronized void set_extra1(int index, float v, boolean copyNow) {
        if (mIOBuffer == null) mIOBuffer = new FieldPacker(Item.sizeof * getType().getX()/* count */);
        if (mItemArray == null) mItemArray = new Item[getType().getX() /* count */];
        if (mItemArray[index] == null) mItemArray[index] = new Item();
        mItemArray[index].extra1 = v;
        if (copyNow)  {
            mIOBuffer.reset(index * Item.sizeof + 128);
            mIOBuffer.addF32(v);
            FieldPacker fp = new FieldPacker(4);
            fp.addF32(v);
            mAllocation.setFromFieldPacker(index, 2, fp);
        }

    }

    public synchronized void set_extra2(int index, Float2 v, boolean copyNow) {
        if (mIOBuffer == null) mIOBuffer = new FieldPacker(Item.sizeof * getType().getX()/* count */);
        if (mItemArray == null) mItemArray = new Item[getType().getX() /* count */];
        if (mItemArray[index] == null) mItemArray[index] = new Item();
        mItemArray[index].extra2 = v;
        if (copyNow)  {
            mIOBuffer.reset(index * Item.sizeof + 136);
            mIOBuffer.addF32(v);
            FieldPacker fp = new FieldPacker(8);
            fp.addF32(v);
            mAllocation.setFromFieldPacker(index, 4, fp);
        }

    }

    public synchronized void set_extra3(int index, Float3 v, boolean copyNow) {
        if (mIOBuffer == null) mIOBuffer = new FieldPacker(Item.sizeof * getType().getX()/* count */);
        if (mItemArray == null) mItemArray = new Item[getType().getX() /* count */];
        if (mItemArray[index] == null) mItemArray[index] = new Item();
        mItemArray[index].extra3 = v;
        if (copyNow)  {
            mIOBuffer.reset(index * Item.sizeof + 144);
            mIOBuffer.addF32(v);
            FieldPacker fp = new FieldPacker(12);
            fp.addF32(v);
            mAllocation.setFromFieldPacker(index, 5, fp);
        }

    }

    public synchronized void set_extra4(int index, Float4 v, boolean copyNow) {
        if (mIOBuffer == null) mIOBuffer = new FieldPacker(Item.sizeof * getType().getX()/* count */);
        if (mItemArray == null) mItemArray = new Item[getType().getX() /* count */];
        if (mItemArray[index] == null) mItemArray[index] = new Item();
        mItemArray[index].extra4 = v;
        if (copyNow)  {
            mIOBuffer.reset(index * Item.sizeof + 160);
            mIOBuffer.addF32(v);
            FieldPacker fp = new FieldPacker(16);
            fp.addF32(v);
            mAllocation.setFromFieldPacker(index, 7, fp);
        }

    }

    public synchronized Matrix4f get_MATRIX(int index) {
        if (mItemArray == null) return null;
        return mItemArray[index].MATRIX;
    }

    public synchronized Matrix4f get_EXTRA(int index) {
        if (mItemArray == null) return null;
        return mItemArray[index].EXTRA;
    }

    public synchronized float get_extra1(int index) {
        if (mItemArray == null) return 0;
        return mItemArray[index].extra1;
    }

    public synchronized Float2 get_extra2(int index) {
        if (mItemArray == null) return null;
        return mItemArray[index].extra2;
    }

    public synchronized Float3 get_extra3(int index) {
        if (mItemArray == null) return null;
        return mItemArray[index].extra3;
    }

    public synchronized Float4 get_extra4(int index) {
        if (mItemArray == null) return null;
        return mItemArray[index].extra4;
    }

    public synchronized void copyAll() {
        for (int ct = 0; ct < mItemArray.length; ct++) copyToArray(mItemArray[ct], ct);
        mAllocation.setFromFieldPacker(0, mIOBuffer);
    }

    public synchronized void resize(int newSize) {
        if (mItemArray != null)  {
            int oldSize = mItemArray.length;
            int copySize = Math.min(oldSize, newSize);
            if (newSize == oldSize) return;
            Item ni[] = new Item[newSize];
            System.arraycopy(mItemArray, 0, ni, 0, copySize);
            mItemArray = ni;
        }

        mAllocation.resize(newSize);
        if (mIOBuffer != null) mIOBuffer = new FieldPacker(Item.sizeof * getType().getX()/* count */);
    }

}

