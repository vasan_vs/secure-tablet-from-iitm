/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * This file is auto-generated. DO NOT MODIFY!
 * The source Renderscript file: packages/wallpapers/Galaxy4/src/com/android/galaxy4/galaxy.rs
 */
package com.android.galaxy4;

import android.renderscript.*;
import android.content.res.Resources;

/**
 * @hide
 */
public class ScriptC_galaxy extends ScriptC {
    // Constructor
    public  ScriptC_galaxy(RenderScript rs, Resources resources, int id) {
        super(rs, resources, id);
    }

    private final static int mExportVarIdx_vpConstants = 0;
    private ScriptField_VpConsts mExportVar_vpConstants;
    public void bind_vpConstants(ScriptField_VpConsts v) {
        mExportVar_vpConstants = v;
        if (v == null) bindAllocation(null, mExportVarIdx_vpConstants);
        else bindAllocation(v.getAllocation(), mExportVarIdx_vpConstants);
    }

    public ScriptField_VpConsts get_vpConstants() {
        return mExportVar_vpConstants;
    }

    private final static int mExportVarIdx_spaceClouds = 1;
    private ScriptField_Particle mExportVar_spaceClouds;
    public void bind_spaceClouds(ScriptField_Particle v) {
        mExportVar_spaceClouds = v;
        if (v == null) bindAllocation(null, mExportVarIdx_spaceClouds);
        else bindAllocation(v.getAllocation(), mExportVarIdx_spaceClouds);
    }

    public ScriptField_Particle get_spaceClouds() {
        return mExportVar_spaceClouds;
    }

    private final static int mExportVarIdx_bgStars = 2;
    private ScriptField_Particle mExportVar_bgStars;
    public void bind_bgStars(ScriptField_Particle v) {
        mExportVar_bgStars = v;
        if (v == null) bindAllocation(null, mExportVarIdx_bgStars);
        else bindAllocation(v.getAllocation(), mExportVarIdx_bgStars);
    }

    public ScriptField_Particle get_bgStars() {
        return mExportVar_bgStars;
    }

    private final static int mExportVarIdx_spaceCloudsMesh = 3;
    private Mesh mExportVar_spaceCloudsMesh;
    public void set_spaceCloudsMesh(Mesh v) {
        mExportVar_spaceCloudsMesh = v;
        setVar(mExportVarIdx_spaceCloudsMesh, v);
    }

    public Mesh get_spaceCloudsMesh() {
        return mExportVar_spaceCloudsMesh;
    }

    private final static int mExportVarIdx_bgStarsMesh = 4;
    private Mesh mExportVar_bgStarsMesh;
    public void set_bgStarsMesh(Mesh v) {
        mExportVar_bgStarsMesh = v;
        setVar(mExportVarIdx_bgStarsMesh, v);
    }

    public Mesh get_bgStarsMesh() {
        return mExportVar_bgStarsMesh;
    }

    private final static int mExportVarIdx_vertSpaceClouds = 5;
    private ProgramVertex mExportVar_vertSpaceClouds;
    public void set_vertSpaceClouds(ProgramVertex v) {
        mExportVar_vertSpaceClouds = v;
        setVar(mExportVarIdx_vertSpaceClouds, v);
    }

    public ProgramVertex get_vertSpaceClouds() {
        return mExportVar_vertSpaceClouds;
    }

    private final static int mExportVarIdx_vertBgStars = 6;
    private ProgramVertex mExportVar_vertBgStars;
    public void set_vertBgStars(ProgramVertex v) {
        mExportVar_vertBgStars = v;
        setVar(mExportVarIdx_vertBgStars, v);
    }

    public ProgramVertex get_vertBgStars() {
        return mExportVar_vertBgStars;
    }

    private final static int mExportVarIdx_fragSpaceClouds = 7;
    private ProgramFragment mExportVar_fragSpaceClouds;
    public void set_fragSpaceClouds(ProgramFragment v) {
        mExportVar_fragSpaceClouds = v;
        setVar(mExportVarIdx_fragSpaceClouds, v);
    }

    public ProgramFragment get_fragSpaceClouds() {
        return mExportVar_fragSpaceClouds;
    }

    private final static int mExportVarIdx_fragBgStars = 8;
    private ProgramFragment mExportVar_fragBgStars;
    public void set_fragBgStars(ProgramFragment v) {
        mExportVar_fragBgStars = v;
        setVar(mExportVarIdx_fragBgStars, v);
    }

    public ProgramFragment get_fragBgStars() {
        return mExportVar_fragBgStars;
    }

    private final static int mExportVarIdx_vertBg = 9;
    private ProgramVertex mExportVar_vertBg;
    public void set_vertBg(ProgramVertex v) {
        mExportVar_vertBg = v;
        setVar(mExportVarIdx_vertBg, v);
    }

    public ProgramVertex get_vertBg() {
        return mExportVar_vertBg;
    }

    private final static int mExportVarIdx_fragBg = 10;
    private ProgramFragment mExportVar_fragBg;
    public void set_fragBg(ProgramFragment v) {
        mExportVar_fragBg = v;
        setVar(mExportVarIdx_fragBg, v);
    }

    public ProgramFragment get_fragBg() {
        return mExportVar_fragBg;
    }

    private final static int mExportVarIdx_textureSpaceCloud = 11;
    private Allocation mExportVar_textureSpaceCloud;
    public void set_textureSpaceCloud(Allocation v) {
        mExportVar_textureSpaceCloud = v;
        setVar(mExportVarIdx_textureSpaceCloud, v);
    }

    public Allocation get_textureSpaceCloud() {
        return mExportVar_textureSpaceCloud;
    }

    private final static int mExportVarIdx_textureFGStar = 12;
    private Allocation mExportVar_textureFGStar;
    public void set_textureFGStar(Allocation v) {
        mExportVar_textureFGStar = v;
        setVar(mExportVarIdx_textureFGStar, v);
    }

    public Allocation get_textureFGStar() {
        return mExportVar_textureFGStar;
    }

    private final static int mExportVarIdx_textureBg = 13;
    private Allocation mExportVar_textureBg;
    public void set_textureBg(Allocation v) {
        mExportVar_textureBg = v;
        setVar(mExportVarIdx_textureBg, v);
    }

    public Allocation get_textureBg() {
        return mExportVar_textureBg;
    }

    private final static int mExportVarIdx_xOffset = 14;
    private float mExportVar_xOffset;
    public void set_xOffset(float v) {
        mExportVar_xOffset = v;
        setVar(mExportVarIdx_xOffset, v);
    }

    public float get_xOffset() {
        return mExportVar_xOffset;
    }

    private final static int mExportFuncIdx_positionParticles = 0;
    public void invoke_positionParticles() {
        invoke(mExportFuncIdx_positionParticles);
    }

}

