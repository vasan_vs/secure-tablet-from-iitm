/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * This file is auto-generated. DO NOT MODIFY!
 * The source Renderscript file: frameworks/base/tests/RenderScriptTests/ComputePerf/src/com/example/android/rs/computeperf/launchtestxlw.rs
 */
package com.example.android.rs.computeperf;

import android.renderscript.*;
import android.content.res.Resources;

/**
 * @hide
 */
public class ScriptC_launchtestxlw extends ScriptC {
    // Constructor
    public  ScriptC_launchtestxlw(RenderScript rs, Resources resources, int id) {
        super(rs, resources, id);
        mExportVar_dim = 2048;
        __U8 = Element.U8(rs);
    }

    private Element __U8;
    private final static int mExportVarIdx_dim = 0;
    private int mExportVar_dim;
    public int get_dim() {
        return mExportVar_dim;
    }

    private final static int mExportVarIdx_buf = 1;
    private Allocation mExportVar_buf;
    public void bind_buf(Allocation v) {
        mExportVar_buf = v;
        if (v == null) bindAllocation(null, mExportVarIdx_buf);
        else bindAllocation(v, mExportVarIdx_buf);
    }

    public Allocation get_buf() {
        return mExportVar_buf;
    }

    private final static int mExportForEachIdx_root = 0;
    public void forEach_root(Allocation aout) {
        // check aout
        if (!aout.getType().getElement().isCompatible(__U8)) {
            throw new RSRuntimeException("Type mismatch with U8!");
        }
        forEach(mExportForEachIdx_root, null, aout, null);
    }

}

