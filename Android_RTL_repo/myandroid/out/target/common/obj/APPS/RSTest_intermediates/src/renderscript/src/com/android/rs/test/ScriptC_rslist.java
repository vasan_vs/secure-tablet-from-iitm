/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * This file is auto-generated. DO NOT MODIFY!
 * The source Renderscript file: frameworks/base/tests/RenderScriptTests/tests/src/com/android/rs/test/rslist.rs
 */
package com.android.rs.test;

import android.renderscript.*;
import android.content.res.Resources;

/**
 * @hide
 */
public class ScriptC_rslist extends ScriptC {
    // Constructor
    public  ScriptC_rslist(RenderScript rs, Resources resources, int id) {
        super(rs, resources, id);
        mExportVar_textPos = 0;
    }

    private final static int mExportVarIdx_gDY = 0;
    private float mExportVar_gDY;
    public void set_gDY(float v) {
        mExportVar_gDY = v;
        setVar(mExportVarIdx_gDY, v);
    }

    public float get_gDY() {
        return mExportVar_gDY;
    }

    private final static int mExportVarIdx_gFont = 1;
    private Font mExportVar_gFont;
    public void set_gFont(Font v) {
        mExportVar_gFont = v;
        setVar(mExportVarIdx_gFont, v);
    }

    public Font get_gFont() {
        return mExportVar_gFont;
    }

    private final static int mExportVarIdx_gList = 2;
    private ScriptField_ListAllocs_s mExportVar_gList;
    public void bind_gList(ScriptField_ListAllocs_s v) {
        mExportVar_gList = v;
        if (v == null) bindAllocation(null, mExportVarIdx_gList);
        else bindAllocation(v.getAllocation(), mExportVarIdx_gList);
    }

    public ScriptField_ListAllocs_s get_gList() {
        return mExportVar_gList;
    }

    private final static int mExportVarIdx_textPos = 3;
    private int mExportVar_textPos;
    public void set_textPos(int v) {
        mExportVar_textPos = v;
        setVar(mExportVarIdx_textPos, v);
    }

    public int get_textPos() {
        return mExportVar_textPos;
    }

}

