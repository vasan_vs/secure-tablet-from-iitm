# 1 "test.c"
# 1 "<built-in>"
# 1 "<command-line>"
# 1 "test.c"


# 1 "./linux/compiler.h" 1
# 4 "test.c" 2
# 137 "test.c"
typedef int (*initcall_t)(void);
typedef void (*exitcall_t)(void);

extern initcall_t __con_initcall_start[], __con_initcall_end[];
extern initcall_t __security_initcall_start[], __security_initcall_end[];


typedef void (*ctor_fn_t)(void);


extern int do_one_initcall(initcall_t fn);
extern char __attribute__ ((__section__(".init.data"))) boot_command_line[];
extern char *saved_command_line;
extern unsigned int reset_devices;


void setup_arch(char **);
void prepare_namespace(void);

extern void (*late_time_init)(void);

extern int initcall_debug;
# 224 "test.c"
struct obs_kernel_param {
 const char *str;
 int (*setup_func)(char *);
 int early;
};
# 253 "test.c"
void __attribute__ ((__section__(".init.text"))) notrace parse_early_param(void);
void __attribute__ ((__section__(".init.text"))) notrace parse_early_options(char *cmdline);
# 349 "test.c"
static initcall_t __initcall_Hello_init6 __used __attribute__((__section__(".initcall" "6" ".init"))) = Hello_init;;
