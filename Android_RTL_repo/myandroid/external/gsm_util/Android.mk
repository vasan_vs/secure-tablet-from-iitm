LOCAL_PATH:= $(call my-dir)

#
# gsm_at
#
include $(CLEAR_VARS)

LOCAL_ARM_MODE := arm

LOCAL_SRC_FILES:= gsm_at.c

LOCAL_CFLAGS += -W -Wall
LOCAL_CFLAGS += -fPIC -DPIC
LOCAL_CFLAGS += -I$(LOCAL_PATH)


LOCAL_MODULE:= gsm_atdt
LOCAL_MODULE_TAGS := optional
LOCAL_SHARED_LIBRARIES := libcutils

LOCAL_PRELINK_MODULE := false

include $(BUILD_EXECUTABLE)
